<?php

require_once(ROOT_DIR.'_config_ny.php');
require_once(ROOT_DIR.'functions/admin_func.php');
require_once(ROOT_DIR.'functions/html_func.php');
require_once(ROOT_DIR.'functions/form_func.php');
require_once(ROOT_DIR.'functions/common_func.php');
require_once(ROOT_DIR.'functions/mls_func.php');
require_once(ROOT_DIR.'functions/string_func.php');
require_once(ROOT_DIR.'functions/mysql_func.php');

$fields = array(
	'property_type'		=> 'required int',
	'property_type2'		=> 'int',
	'residential'		=> 'required int',
	'id'=>'int',
	'base'=>'int'
);
//создание массива $post
$post = form_smart($fields,stripslashes_smart($_GET)); //print_r($post);

//сообщения с ошибкой заполнения
$message = form_validate($fields,$post);
//print_r($message);
if (count($message)>0) {
	echo 'Select Sale/Rent and Real Estate Type';
}
else {
	if ($post['base']==0) $post['base'] = 1;
	if ($post['id'] AND $product = mysql_select("SELECT * FROM shop_products WHERE id=".$post['id'],'row')) {
		$parameters = unserialize($product['parameters']);
		$post['base'] = $product['base'];
	}
	echo '<h2>';
	if ($post['property_type2'] AND $text = mysql_select("
		SELECT name FROM shop_property_types2 WHERE id=".$post['property_type2']." ORDER BY rank DESC, name
	",'string')) echo $text;
	else echo random_property_type ($post);
	echo ' ';
	echo $config['object_residential'][$post['residential']].'</h2>';
	$form = array();
	$key = 0;
	//land
	if ($post['property_type']==3) $key = 201;
	//commercial
	if ($post['property_type']==2) {
		if ($post['residential']==1) $key = 701;
		else $key = 301;
	}
	//residential
	if ($post['property_type']==1) {
		if ($post['residential']==1) $key = 701;
		else {
			//11 Condo
			if ($post['property_type2'] == 11) $key = 401;
			//12 Co-Op
			elseif ($post['property_type2'] == 12) $key = 401;
			//10 Multi-Family
			//13 Single Family
			//14 Mixed Use/Store &amp; Family
			else $key = 601;
		}
	}
	if ($key) {
		foreach ($config['shop_params'] as $k => $v) if (@$v['group'] == $key) {
			if (@$v['h2']) $form[$key][] = '<h2>-'.@$v['h2'].'-</h2>';
			$param =  array('name' => $v['name']);
			if (@$v['placeholder']) $param['attr'] = 'placeholder="'.$v['placeholder'].'"';
			$td = @$v['td'] ? $v['td'] : 'td3';
			//if (@$v['display']==1) $v['name'] = '<strong style="color:darkgreen">'.$v['name'].'</strong>';
			if (@$v['hide']==1) $v['name'] = '<strong style="color:#999">'.$v['name'].'</strong>';
			if (@$v['type'] == 'yn') {
				$form[$key][] = array('select ' . $td, 'parameters[' . $k . ']', array(@$parameters[$k], array(1 => 'Yes', 2 => 'No'), '--'), array('name' => $v['name']));
			}
			elseif (@$v['type'] == 'date') {
				$form[$key][] = array('input datepicker ' . $td, 'parameters[' . $k . ']', @$parameters[$k], array('name' => $v['name']));
			}
			elseif (@$v['type'] == 'multicheckbox') {
				$array = array();
				foreach ($v['data'] as $k1 => $v1) $array[$v1] = $v1;
				$form[$key][] = array('multicheckbox ' . $td, 'parameters[' . $k . ']', array(@$parameters[$k], $array), array('name' => $v['name']));
			}
			elseif (@$v['type'] == 'checkbox') {
				$form[$key][] = array('checkbox td3', 'parameters[' . $k . ']', @$parameters[$k], array('name' => $v['name']));
			}
			elseif (isset($v['data'])) {
				$array = array();
				foreach ($v['data'] as $k1 => $v1) $array[$v1] = $v1;
				$form[$key][] = array('select ' . $td, 'parameters[' . $k . ']', array(@$parameters[$k], $array, '--'), array('name' => $v['name']));
			}
			else $form[$key][] = array('input ' . $td, 'parameters[' . $k . ']', @$parameters[$k],$param);
			if (@$v['clear'] == 1) $form[$key][] = '<div class="clear"></div>';
		}

		foreach ($form[$key] as $k2 => $v2) {
			if (is_array($v2)) echo call_user_func_array(preg_match('/mysql|simple|file|file_multi/', $v2[0]) ? 'form_file' : 'form', $v2);
			else echo $v2;
		}

		if ($key==601) {
			echo '<h2>APT. & STORE INFORMATION</h2>';
			$type = '<option value="0">select</option>';
			$type.= '<option value="1" {type_1}>Store</option>';
			$type.= '<option value="2" {type_2}>Apt</option>';
			$lease = '<option value="0">select</option>';
			$lease.= '<option value="1" {lease_1}>Yes</option>';
			$lease.= '<option value="2" {lease_2}>No</option>';
			$template['store_info'] = '
			<tr data-i="{i}">
				<td>{i}</td>
				<td><select name="store_info[{i}][type]">'.$type.'</select></td>
				<td><input name="store_info[{i}][number]" value="{number}" /></td>
				<td><select name="store_info[{i}][lease]">'.$lease.'</select></td>
				<td><input name="store_info[{i}][rent]" value="{rent}" /></td>
				<td><input name="store_info[{i}][rooms]" value="{rooms}" /></td>
				<td><input name="store_info[{i}][beds]" value="{beds}" /></td>
				<td><input name="store_info[{i}][baths]" value="{baths}" /></td>
				<td><a href="#" class="sprite boolean_0"></a></td>
			</tr>
			
		';
			?>
			<div style="clear:both; background:#E9E9E9; padding:5px 10px; width:875px; margin:0 -10px">
			<table class="store_info">
				<tr data-i="0">
					<th>#</th>
					<th>Type (Apt.or Store)</th>
					<th>Number</th>
					<th>Lease Y/N</th>
					<th>Rent</th>
					<th>Rooms</th>
					<th>Beds</th>
					<th>Baths</th>
					<th><a href="#" style="background:#35B374; display:inline-block; padding:2px; border-radius:10px"><span class="sprite plus"></span></a></th>
				</tr>
				<?php
				$store_info = @$product['store_info'] ? unserialize($product['store_info']) : false;
				if (!$store_info) {
					$store_info = array(
						1=>array(),
						2=>array()
					);
				}
				foreach ($store_info as $k=>$v) {
					$v['i'] = $k;
					$v['lease_'.@$v['lease']] = ' selected="selected" ' ;
					$v['type_'.@$v['type']] = ' selected="selected" ' ;
					echo template($template['store_info'],$v);
				}

				?>
			</table>
			<textarea style="display:none" id="template_store_info"><?=htmlspecialchars($template['store_info'])?></textarea>
			<?php
		}

	}
}