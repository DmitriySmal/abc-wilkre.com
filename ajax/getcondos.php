<?php
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек

require_once(ROOT_DIR.'functions/common_func.php');
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
require_once(ROOT_DIR.'functions/html_func.php');
require_once(ROOT_DIR.'functions/form_func.php');
require_once(ROOT_DIR.'functions/auth_func.php');
require_once(ROOT_DIR.'functions/string_func.php');
require_once(ROOT_DIR.'functions/lang_func.php');

$url = parse_url($_SERVER['HTTP_REFERER']);
$u = array_slice(explode('/',$url['path']),1);

//DIE();

$lang = lang(1);
$state = (isset($u[0]) && $u[0]) ? mysql_select("SELECT * FROM shop_states WHERE url = '{$u[0]}'",'row',true) : mysql_select("SELECT * FROM shop_states WHERE id = 9999",'row',true);//$config['default_state'][9999];
$_cities = ($state['id']=='9999') ? mysql_select("SELECT c.*, s.url state_url FROM shop_cities AS c LEFT JOIN shop_states AS s ON s.id = c.state WHERE c.display = 1 ORDER BY c.rank DESC, c.name ASC",'rows_id',60*60) : mysql_select("SELECT c.*, s.url state_url FROM shop_cities AS c LEFT JOIN shop_states AS s ON s.id = c.state WHERE c.display = 1 AND c.state = {$state['id']} ORDER BY c.rank DESC, c.name ASC",'rows_id',60*60);
$_cities_ids = array();
foreach ($_cities as $c) $_cities_ids[] = $c['id'];

$modules = mysql_select("SELECT url name,module id FROM pages WHERE module!='pages' AND language=".$lang['id']." AND display=1",'array',60*60);

$id = intval($_GET['nb']);
if (!$id) die();

$json = array(
    'result' => false,
    'data' => array()
);

$where = $order_by = ' ';
$view_order = array('asc','desc');
$view_type = array('name','year');
$where = $join = '';


$where .= ($u[2] == 'pre-construction') ? " AND sp.type = 0" : " AND sp.type = 1 ";
$where .= (@$_POST['location'] && is_array($_POST['location'])) ? " AND sp.neighborhood IN ('".implode("','",$_POST['location'])."') " : '';

if($u[3]){
    $city_url = mysql_real_escape_string($u[3]);
    $city = mysql_select("SELECT id, name, url FROM shop_cities WHERE url LIKE '{$city_url}' LIMIT 1",'row');
    $where .= ($city && isset($city['id'])) ? " AND sp.city = {$city['id']} " : ( ($state['id']!=9999) ? " AND sp.city IN (".implode(',', $_cities_ids).") " : '' );        
}
else
{
    $where .= ( ($state['id']!=9999) ? " AND sp.city IN (".implode(',', $_cities_ids).") " : '' );
}

if (@$_POST['sort']){        
    $view = explode('-', $_POST['sort']);
    if (in_array($view[0], $view_type)){
        $order_by = " sp.{$view[0]} ".( (in_array($view[1], $view_order)) ? strtoupper($view[1]) : 'DESC' ).', ';
    }
}

$tpl = (!@$_POST['list_view']) ? "condos/condo_list shop" : "condos/condo_listview shop";    

$query = "
            SELECT sp.*, city.name city_name, city.url city_url, neighborhood.name neighborhood_name, neighborhood.url neighborhood_url, state.url state_url   
            FROM shop_condos AS sp 
            LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood 
            LEFT JOIN shop_cities AS city ON city.id = sp.city                                  
            LEFT JOIN shop_states AS state ON state.id = city.state                                   
            {$join} 
            WHERE sp.display = 1 {$where} AND sp.id = {$id} 
            ORDER BY sp.img DESC, {$order_by} sp.special DESC, sp.date DESC  
    ";


$json['data'] = (@$_GET['type'] == 'json') ? mysql_select($query,'rows') : html_query($tpl,$query);
$json['result'] = ($json['data']) ? true : false;    

echo json_encode($json);