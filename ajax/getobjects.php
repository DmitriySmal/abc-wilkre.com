<?php
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек

require_once(ROOT_DIR.'functions/common_func.php');
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
require_once(ROOT_DIR.'functions/html_func.php');
require_once(ROOT_DIR.'functions/form_func.php');
require_once(ROOT_DIR.'functions/auth_func.php');
require_once(ROOT_DIR.'functions/string_func.php');
require_once(ROOT_DIR.'functions/lang_func.php');

$lang = (isset($u[0]) && $u[0]) ? lang($u[0],'url') : lang(1);
$modules = mysql_select("SELECT url name,module id FROM pages WHERE module!='pages' AND language=".$lang['id']." AND display=1",'array',60*60);

$id = intval($_GET['nb']);
if (!$id) die();

$json = array(
    'result' => false,
    'data' => array()
);

$where = $order_by = ' ';
$view_order = array('asc','desc');
$view_type = array('price','date');
$where = $join = '';

$fields = array(        
    //'price'	=> 'min_max',
    'price'	=> '',
    'parameter' => '',
    'p'         => '',
);

$post = form_smart($fields,stripslashes_smart($_POST)); //print_r($post);
$where .= (@$_POST['category'] && intval($_POST['category'] > 0)) ? " AND sp.category = ".intval($_POST['category'])." " : '';        
$where .= (@$_POST['bedrooms'] && intval($_POST['bedrooms'] > 0)) ? " AND param_beds.value = ".intval($_POST['bedrooms'])." " : '';        
$where .= (isset($_POST['for_sale'])) ? (($_POST['for_sale']) ? " AND sp.type = 0 " : " AND sp.type = 1 " ) : '';
if ($post['price'] AND $price=explode(',',$post['price'])) {
    $where.= ($price[0] && $price[1]) ? " AND sp.price BETWEEN {$price[0]} AND {$price[1]}" : '';        
}
/*
$zips = mysql_select("SELECT * FROM shop_neighborhoods_zips WHERE nb_id IN (".$id.")");                
if($zips)
{ 
    $tmp_zip = array();
    foreach($zips as $zip) $tmp_zip[] = "'{$zip['zip']}'";            
}
$zips = $tmp_zip;  */         
//$neighborhoods = mysql_select("SELECT * FROM shop_neighborhoods WHERE id IN (".implode(',',$_GET['location']).")",'rows', 60*60);        
//$where .= (count($zips)>0) ? " AND sp.zip_code IN (".implode(',',$zips).") " : '';        

if (@$_POST['sort']){        
        $view = explode('-', $_POST['sort']);
        if (in_array($view[0], $view_type)){
            $order_by = " sp.{$view[0]} ".( (in_array($view[1], $view_order)) ? strtoupper($view[1]) : 'DESC' ).', ';
        }
    }

    
//$json['data'] = mysql_select("SELECT * FROM shop_neighborhoods WHERE display = 1 AND city = {$id} ORDER BY rank DESC, name ASC, id ASC");
//$json['result'] = ($json['data']) ? true : false;
$query = "
    SELECT sp.*, city.name city_name, city.url city_url, neighborhood.id neighborhood, neighborhood.name neighborhood_name, neighborhood.url neighborhood_url, sc.name category_name,  sc.url category_url, param_beds.value as beds,   param_baths.value as baths, param_square.value square 
    FROM shop_products AS sp     
    LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood    
    LEFT JOIN shop_cities AS city ON city.id = sp.city  
    LEFT JOIN shop_categories AS sc ON sc.id = sp.category   
    LEFT JOIN shop_products_parameters AS param_beds ON param_beds.parameter_id = 25 AND param_beds.product_id = sp.id   
    LEFT JOIN shop_products_parameters AS param_baths ON param_baths.parameter_id = 92 AND param_baths.product_id = sp.id   
    LEFT JOIN shop_products_parameters AS param_square ON param_square.parameter_id = 232 AND param_square.product_id = sp.id   
    LEFT JOIN shop_products_parameters AS spp ON spp.product_id = sp.id  
    {$join} 
    WHERE sp.display = 1 AND sp.neighborhood = {$id} {$where} 
    GROUP BY sp.id 
    ORDER BY {$order_by} sp.special DESC, sp.date DESC                 
";
//echo($query);
$json['data'] = (@$_GET['type'] == 'json') ? mysql_select($query,'rows') : html_query('shop/product_list shop',$query);
$json['result'] = ($json['data']) ? true : false;    

echo json_encode($json);