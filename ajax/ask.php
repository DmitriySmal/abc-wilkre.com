<?php


require_once(ROOT_DIR.'functions/common_func.php');	//общие функции
require_once(ROOT_DIR.'functions/html_func.php');	//функции для работы нтмл кодом
require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами
require_once(ROOT_DIR.'functions/lang_func.php');	//функции словаря
require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
require_once(ROOT_DIR.'functions/string_func.php');	//функции для работы со строками

$json = array(
    'result' => false,
    'data' => array(),
    'message' => ''
);

//основной язык
$lang = lang(1);

//список модулей на сайте
$modules = mysql_select("SELECT url name,module id FROM pages WHERE module!='pages' AND language=".$lang['id']." AND display=1",'array',60*60);

//определение значений формы
$fields = array(
	'email'                 => 'required email',
	'name'                  => 'required text',	
	'question'                  => 'required text',
	//'captcha'		=> 'required captcha2'
);
//создание массива $post
$post = form_smart($fields,stripslashes_smart($_POST)); //print_r($post);

//сообщения с ошибкой заполнения
$message = form_validate($fields,$post);

//если нет ошибок то отправляем сообщение
if (count($message)==0) {
    //unset($_SESSION['captcha'],$post['captcha']); //убиваем капчу чтобы второй раз не отправлялось
    //$post['date'] = date('Y-m-d H:i:s');
    //$post['text'] = '<p>'.preg_replace("/\n/","<br />",$post['text']).'</p>';
    $json['result'] = mailer('ask',$lang['id'],$post);		
}
else {
    $json['message'] = $message;
}


echo json_encode($json);


?>