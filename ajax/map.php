<?php
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек

require_once(ROOT_DIR.'functions/common_func.php');
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
require_once(ROOT_DIR.'functions/html_func.php');
require_once(ROOT_DIR.'functions/form_func.php');
require_once(ROOT_DIR.'functions/auth_func.php');
require_once(ROOT_DIR.'functions/string_func.php');
require_once(ROOT_DIR.'functions/lang_func.php');

$lang = lang(1);
$modules = mysql_select("SELECT url name,module id FROM pages WHERE module!='pages' AND language=".$lang['id']." AND display=1",'array',60*60);

$id = intval($_GET['id']);
if (!$id) die();

if (@$_GET['type']=='product') {
	$query = "
		SELECT sp.*,
		neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
		FROM shop_products AS sp
		LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
		WHERE sp.id=".$id;
	echo html_query('shop/product_coords',$query,'');
}
if (@$_GET['type']=='condo') {
	$query = "
		SELECT sp.*,
		neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
		FROM shop_condos AS sp
		LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
		WHERE sp.id=".$id;
	echo html_query('condos/condo_coords',$query,'');
}

if (@$_GET['type']=='preconstruction') {
	$query = "
		SELECT sp.*,
		neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
		FROM shop_preconstructions AS sp
		LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
		WHERE sp.id=".$id;
	echo html_query('preconstructions/preconstruction_coords',$query,'');
}
