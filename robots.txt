User-agent: *
Disallow: *&count=*
Disallow: /*?*_width
Disallow: /*?*_length
Disallow: /new-york_old*
Disallow: /new-york_old/*
Disallow: /ajax.php*

Host: www.wilkrealestate.com
Sitemap: https://www.wilkrealestate.com/sitemap.xml