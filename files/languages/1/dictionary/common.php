<?php
$lang['common'] = array(
	'site_name'=>'WilkRealEstate',
	'txt_meta'=>'<meta name="viewport" content="width=device-width, initial-scale=1">',
	'txt_head'=>'Welcome to WilkRealEstate site',
	'txt_index'=>'Text for main page',
	'txt_footer'=>'<div style="float:right">
<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href=\'http://www.liveinternet.ru/click\' "+
"target=_blank><img src=\'//counter.yadro.ru/hit?t15.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"\' alt=\'\' title=\'LiveInternet: показано число просмотров за 24"+
" часа, посетителей за 24 часа и за сегодня\' "+
"border=\'0\' width=\'88\' height=\'31\'><\/a>")
//--></script><!--/LiveInternet-->
</div>
copiright &copy; abc-cms.com 2013',
	'str_no_page_name'=>'Page Not Found',
	'txt_no_page_text'=>'<p>Page Not Found</p>',
	'wrd_more'=>'More',
	'msg_no_results'=>'Sorry, 0 rows found.',
	'wrd_no_photo'=>'No photo',
	'breadcrumb_index'=>'Home',
	'breadcrumb_separator'=>' - ',
	'make_selection'=>'Change',
	'google_translate_script'=>'<div id="google_translate_element"></div>
                        <script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({pageLanguage: \'ru\', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, \'google_translate_element\');
                            }
                        </script>
                        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>',
	'phone_top'=>'718-376-0606',
	'call_back'=>'Call Back',
	'letter_top'=>'Текст в шапке письма',
	'letter_footer'=>'Текст в подвале письма',
);?>