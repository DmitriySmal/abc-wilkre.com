<?php
$lang['feedback'] = array(
	'name'=>'Name<br />',
	'first_name'=>'First Name',
	'last_name'=>'Last Name',
	'phone'=>'Phone',
	'email'=>'Email',
	'text'=>'Message',
	'send'=>'SEND INQUIRY',
	'attach'=>'Attach File',
	'message_is_sent'=>'Message sent.',
);?>