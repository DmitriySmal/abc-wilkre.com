<?php


/**
 * основной файл обработки всех урл для сайта
 */

//чтобы не запрашивали напрямую index.php
if (strpos($_SERVER['REQUEST_URI'],'/index.php')!==false) { // проверяем есть ли вхождение строки   
	header('HTTP/1.1 301 Moved Permanently');
	// редиректим на адрес без '/index.php', будет выбивать на 404, либо главную
	die(header('location: http://'.$_SERVER['HTTP_HOST'].str_replace('/index.php','',$_SERVER['REQUEST_URI'])));
}

//[4.809] неправильные ссылки или нет редиректа с без www на www
if (!in_array($_SERVER['HTTP_HOST'],array('www.wilkrealestate.com',/*'wilkrealestate.ru','www.wilkrealestate.ru',*/'abc-wilkre.com'))) {
	//$ref=$_SERVER['QUERY_STRING']!=''?'?'.$_SERVER['QUERY_STRING']:'';
	header('HTTP/1.1 301 Moved Permanently');
	//header('HTTP/1.1 302 Temporary redirect');
	//header('HTTP/1.1 307 Temporary Redirect');
	header('Location: http://www.wilkrealestate.com'.$_SERVER['REQUEST_URI']);//, true, 307);
	die();
}

session_start();

// загрузка настроек *********************************************************
define('ROOT_DIR', dirname(__FILE__).'/');
//include(ROOT_DIR.'templates/includes/common/dummy.php');
//die();
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек
require_once(ROOT_DIR.'_config_ny.php');	//установка настроек

// загрузка функций **********************************************************
//require_once(ROOT_DIR.'functions/admin_func.php');	//функции админки
require_once(ROOT_DIR.'functions/auth_func.php');	//функции авторизации
require_once(ROOT_DIR.'functions/common_func.php');	//общие функции
//require_once(ROOT_DIR.'functions/file_func.php');	//функции для работы с файлами
require_once(ROOT_DIR.'functions/html_func.php');	//функции для работы нтмл кодом
require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами
//require_once(ROOT_DIR.'functions/image_func.php');	//функции для работы с картинками
require_once(ROOT_DIR.'functions/lang_func.php');	//функции словаря
//require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
require_once(ROOT_DIR.'functions/string_func.php');	//функции для работы со строками
require_once(ROOT_DIR.'functions/mls_func.php');	//функции для работы со строками

$config['shop_categories'] = mysql_select("SELECT * FROM shop_categories WHERE display=1",'rows_id',60*60);
$config['shop_cities'] = mysql_select("SELECT * FROM shop_cities WHERE display=1",'rows_id',60*60);

//создание двомерного массива $u который передается через реврайтмод
for ($i=0; $i<7; $i++) $u[$i] = isset($_GET['u'][$i]) ? stripslashes_smart($_GET['u'][$i]) : '';

//v1.2.34 - переадресация https
/*
if(
	(@$_SERVER['HTTPS']!='on')
	OR (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) AND $_SERVER['HTTP_X_FORWARDED_PROTO']!='https')
	OR (isset($_SERVER['REQUEST_SCHEME']) AND $_SERVER['REQUEST_SCHEME']=='http')
) {
	//исключение для локальной версии
	if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' AND $_SERVER['SERVER_ADDR']=='127.0.0.1') {

	}
	else {
		header('HTTP/1.1 301 Moved Permanently');
		header('location: https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
		die();
	}
}
*/

// если отсутвует указатель языка, то используем основной язык
/*TODO: добавить выборку из БД на доступные языки, хранить их в кеше/сессии, что бы не дергать бд каждый раз
 * и делать проверку на совпадение, если !in_array($u[0],$langs), то опять вызываем lang(1) иначе lang($u[0],'url')
 */
//$lang = (isset($u[0]) && $u[0]) ? lang($u[0],'url') : lang(1);
$lang = lang(1);
/*$config['shop_state'] = $state = (isset($u[0]) && $u[0]) ? mysql_select("SELECT * FROM shop_states WHERE url = '{$u[0]}'",'row',true) : mysql_select("SELECT * FROM shop_states WHERE id = 9999",'row',true);//$config['default_state'][9999];
////TODO: где-то эта фигня писалась ранее, найти удалить и заменить переменные
$_cities = ($state['id']=='9999') ? mysql_select("SELECT c.*, s.url state_url FROM shop_cities AS c LEFT JOIN shop_states AS s ON s.id = c.state WHERE c.display = 1 ORDER BY c.rank DESC, c.name ASC",'rows_id',60*60) : mysql_select("SELECT c.*, s.url state_url FROM shop_cities AS c LEFT JOIN shop_states AS s ON s.id = c.state WHERE c.display = 1 AND c.state = {$state['id']} ORDER BY c.rank DESC, c.name ASC",'rows_id',60*60);
$_cities_ids = array();
foreach ($_cities as $c) $_cities_ids[] = $c['id'];*/
//var_dump($state);
//список модулей на сайте
$modules = mysql_select("SELECT url name,module id FROM pages WHERE module!='pages' AND language=".$lang['id']." AND display=1",'array');
//аутентификация - создание массива с данными пользователя
$user = user('auth'); //print_r($user);
//принудительная авторизация под админом - для демки
//$_SESSION['user'] = $user = mysql_select("SELECT ut.*,u.*FROM users u LEFT JOIN user_types ut ON u.type = ut.id WHERE u.id=1 LIMIT 1",'row');

//включена заглушка для всех кроме администраторов
if ($config['dummy']==1 AND access('user admin')==false) {
	die(include(ROOT_DIR.'templates/includes/common/dummy.php'));
}

//редиректы

$request_url = explode('?',$_SERVER['REQUEST_URI']); //print_r($request_url);
if ($config['redirects']) {
	if ($redirect = mysql_select("SELECT * FROM redirects WHERE old_url='".mysql_res($request_url[0])."'",'row')) {
		header('HTTP/1.1 301 Moved Permanently');
		//header('location: http://'.$_SERVER['SERVER_NAME'].$redirect['new_url']);
		header('location: '.$redirect['new_url']);
		die();
	}
}
$error = 0;

if (strripos($request_url[0], '//')===false) {
	//условие для главной страницы или модуля
	//$where = ($u[1] == '') ? "module='index'" : "url='" . trunslit($u[1]) . "'";
	$where = ($u[1] == '') ? "module='index'" : "url='" . mysql_res($u[1]) . "'";
	//sql-запрос в таблицу pages
	$query = "
			SELECT *, id AS pid
			FROM pages
			WHERE display=1 AND level=1 AND language=" . $lang['id'] . " AND " . $where . "
			LIMIT 1
		"; //echo $query;
	//массив $page содержит начальную информацию для страницы, которая может быть изменена/дополнена в модуле
	if ($page = mysql_select($query, 'row')) {
		$html['module'] = $page['module'];
		if ($page['level'] > 1) {
			$query = "
			SELECT name,url
			FROM pages
			WHERE left_key <= " . $page['left_key'] . "
				AND right_key >= " . $page['right_key'] . "
			ORDER BY left_key DESC
		";
			$breadcrumb['page'] = breadcrumb($query, '/{url}/', 60 * 60);

		} else $breadcrumb['page'][] = array($page['name'], '/' . $page['url'] . '/');
		//загрузка модуля
		if (is_file(ROOT_DIR . 'modules/' . $page['module'] . '.php')) require_once(ROOT_DIR . 'modules/' . $page['module'] . '.php');
		else $error++;
	}
	elseif ($u[1] AND $agent = mysql_select("
		SELECT *
		FROM users
		WHERE url = '" . mysql_res($u[1]) . "' AND agent = 1
	", 'row')
	) {
		//$u[3] = $u[1];
		require_once(ROOT_DIR . 'modules/agents.php');
	}
	elseif ($u[1] AND $u[2] == '' AND $seo_shop = mysql_select("
		SELECT *
		FROM seo_shop
		WHERE url = '" . mysql_res($u[1]) . "'
		LIMIT 1
	", 'row')
	) {
		//[4.810] Сделать текст для района в перелинковке
		if ($seo_shop['text']=='') {
			if ($neighborhood = mysql_select("SELECT * FROM shop_neighborhoods WHERE id=".$seo_shop['neighborhood'],'row')) {
				if ($neighborhood['text_seo']) {
					$seo_shop['text'] = synonymizer ($neighborhood['text_seo']);
					mysql_fn('update','seo_shop',array('id'=>$seo_shop['id'],'text'=>$seo_shop['text']));
				}
			}
		}
		$post = array(
			'residential' => $seo_shop['residential'],
			'property_type' => $seo_shop['property_type'],
			'property_type2' => $seo_shop['property_type2'],
			'city' => $seo_shop['city'],
			'location' => array($seo_shop['neighborhood']),
			'beds' => $seo_shop['beds'],
			'baths' => $seo_shop['baths'],
			'price_min' => $seo_shop['price_min'],
			'price_max' => $seo_shop['price_max'],
			'quick' => '',
			'view' => '',
			'source' => ''
		);
		if (access('user admin')) {
			//print_r($post);
		}

		$base = $seo_shop['state'];
		$page = $seo_shop;
		$page['title'] .= ' - Wilk Real Estate Agency';
		$html['canonical'] = '/' . $u[1] . '/';
		$html['module'] = 'shop';
		require_once(ROOT_DIR . 'modules/shop.php');
	}

	//короткие ссылки для объектов
	elseif ($u[1] AND $u[2] == '' AND intval($u[1]) > 0 AND strlen(intval($u[1])) == strlen($u[1]) AND $product = mysql_select("
		SELECT sp.*
		FROM shop_products sp
		WHERE sp.display = 1 AND sp.id = '" . intval($u[1]) . "'
		LIMIT 1
	", 'row')
	) {
		//$shop_neighborhoods = get_data('shop_neighborhoods',$q['shop_neighborhood']);
		header('HTTP/1.1 301 Moved Permanently');
		header('location: https://' . $_SERVER['SERVER_NAME'] . get_url('product', $product));
		die();
	} //NY old listing
	elseif ($u[1] AND $u[2] == '' AND $product = mysql_select("
		SELECT sp.*
		FROM shop_products sp
		WHERE sp.display = 1 /*AND sp.base=5 */AND sp.url = '" . mysql_res($u[1]) . "'
		LIMIT 1
	", 'row')
	) {
		if ($product['base'] == 5) {
			//die('1');
			//$u[3] = $u[1];
			$query = "
			SELECT *, id AS pid
			FROM pages
			WHERE display=1 AND language=" . $lang['id'] . " AND module='shop_ny'
			LIMIT 1
			"; //echo $query;
			//массив $page содержит начальную информацию для страницы, которая может быть изменена/дополнена в модуле
			$page = mysql_select($query, 'row', 60 * 60);
			if ($page['level'] > 1) {
				$query = "
				SELECT name,url
				FROM pages
				WHERE left_key <= " . $page['left_key'] . "
					AND right_key >= " . $page['right_key'] . "
				ORDER BY left_key DESC
			";
				$breadcrumb['page'] = breadcrumb($query, '/{url}/', 60 * 60);

			} else $breadcrumb['page'][] = array($page['name'], '/' . $page['url'] . '/');
			$html['module'] = $page['module'];
			require_once(ROOT_DIR . 'modules/shop_ny.php');
		}
		else {
			if ($product['landing'] == 1) {
				$state = in_array($product['base'], array(1, 2)) ? 'FL' : 'NY';
				$url = '/' . $product['id'] . '-' . $product['url'] . '-' . $product['nb_url'] . '-' . $state;
				if ($product['zip_code']) $url .= '-' . $product['zip_code'] . '/';
				header('HTTP/1.1 301 Moved Permanently');
				header('location: http://' . $_SERVER['SERVER_NAME'] . $url);
				die();
			} else $error++;
		}
		//else $error++;
	} //seo_shop
	elseif ($u[1] AND $u[2] == '' AND intval(explode2('-', $u[1])) > 1 AND $product = mysql_select("
		SELECT sp.*
		FROM shop_products sp
		WHERE sp.display = 1 AND sp.landing=1 AND sp.id = '" . intval(explode2('-', $u[1])) . "'
		LIMIT 1
	", 'row')
	) {
		$page = $product;
		require_once(ROOT_DIR . $config['style'] . '/includes/common/landing.php');
		die();
	}
	//новый урл для объекта недвижимости
	elseif ($u[1] AND ($u[2] == '' OR $u[2] == 'mailchimp')) {
		$arr = explode('-', $u[1]);
		$id = end($arr);
		$id = intval($id);
		if ($id>0 AND $product = mysql_select("
			SELECT sp.*
			FROM shop_products sp
			WHERE sp.display = 1 AND sp.id = '" . $id . "'
			LIMIT 1
		", 'row')
		) {
			$url = get_url('product', $product);
			if ($url != '/' . $u[1] . '/') {
				//$shop_neighborhoods = get_data('shop_neighborhoods',$q['shop_neighborhood']);
				header('HTTP/1.1 301 Moved Permanently');
				header('location: https://' . $_SERVER['SERVER_NAME'] . get_url('product', $product));
				die();
			}
			$page = $product;
			$base = $product['base'];
			$new_url = true;
			require_once(ROOT_DIR . 'modules/shop.php');
		}
		else {
			$error++;
		}
	}
	//самый новый урл для объекта недвижимости
	elseif ($u[1]=='property-detail' AND $u[2]) {
		$arr = explode('-', $u[2]);
		$id = end($arr);
		$id = intval($id);
		if ($id>0 AND $product = mysql_select("
			SELECT sp.*
			FROM shop_products sp
			WHERE sp.display = 1 AND sp.id = '" . $id . "'
			LIMIT 1
		", 'row')
		) {
			//переадресация на корректный урл
			header('HTTP/1.1 301 Moved Permanently');
			header('location: https://' . $_SERVER['SERVER_NAME'] . get_url('product3', $product));
			die();

			$url = get_url('product2', $product);
			if ($url != '/' . $u[1] . '/'.$u[2].'/') {
				//if (access('user admin')) die('12345');
				//$shop_neighborhoods = get_data('shop_neighborhoods',$q['shop_neighborhood']);
				header('HTTP/1.1 301 Moved Permanently');
				header('location: https://' . $_SERVER['SERVER_NAME'] . get_url('product2', $product));
				die();
			}
			$page = $product;
			$base = $product['base'];
			$new_url = true;
			require_once(ROOT_DIR . 'modules/shop.php');
		}
		else {
			$error++;
			header('location: /');
			die();
		}
	}
	else {
		$error++;
	}
}
else $error++;
//404
if ($error>0) {
	header("HTTP/1.0 404 Not Found");
	$page['noindex'] = 1;
	$page['title'] = $page['name'] = i18n('common|str_no_page_name');
	$html['module'] = 'error';
}
//редиректим при обращении на главную по url
elseif ((@$html['module']=='index') && ($u[1])) {
	header('HTTP/1.1 301 Moved Permanently');
	die(header('location: /'));
}
//301 редирект при неккоректном урл
elseif($_SERVER['REQUEST_URI']) {
	$request_url = explode('?',$_SERVER['REQUEST_URI']);
	if (substr($request_url[0], -1)!='/') {
		$url = isset($request_url[1]) ? '?'.$request_url[1] : '';
		header('HTTP/1.1 301 Moved Permanently');
		die(header('location: '.$request_url[0].'/'.$url));
	}
	//3) v1.2.30 редирект для uppercase
	$request_url = explode('?',$_SERVER['REQUEST_URI']); //print_r($request_url);
	$lowercase = mb_strtolower($request_url[0],'UTF-8');
	if ($lowercase!=$request_url[0]) {
		header('HTTP/1.1 301 Moved Permanently');
		header('location: http://'.$_SERVER['SERVER_NAME'].$lowercase);
		die();
	}
}



//ob_start();
//ob_start("html_minify");

//загрузка шаблона
if(isset($_GET['print'])) require_once(ROOT_DIR.$config['style'].'/includes/common/print.php');
else require_once(ROOT_DIR.$config['style'].'/includes/common/template.php');

/* *
if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' AND $_SERVER['SERVER_ADDR']=='127.0.0.1') {
	$out = ob_get_clean();
	$out = preg_replace('/(?![^<]*<\/pre>)[\n\r\t]+/', "\n", $out);
	$out = preg_replace('/ {2,}/', ' ', $out);
	$out = preg_replace('/>[\n]+/', '>', $out);
	echo $out;
}
/* */

/*
echo '<!--';
print_r($config['queries']);
echo '-->'; /**/