$(document).ready(function(){
    //$('#answer').modal('show');
    //валидация форм
    if ($.isFunction($.fn.validate)) {
        $('form.validate').each(function(){
            $(this).validate();
        });

    }
    //отправка формы через ajax
    $('form.ajax').submit(function(){
        var form = $(this),
            action = $(form).attr('action');
        if ($(form).valid()){
            form.ajaxSubmit({
                url:		'/ajax.php?file=callback',
                success:	function (data){
                    //alert(1);
                    //if (data == 1){
                    $(form).resetForm();
                    $('#answer').addClass("in myblock");
                    // }
                    // else alert (data);
                },
                error:	function(xhr,txt,err){
                    alert('Ошибка ('+txt+(err&&err.message ? '/'+err.message : '')+')');
                }
            });
        }
        return false;
    });

    $('body').on('click', '.close2',function(){
        $(this).closest(".modal").removeClass('in myblock');
    });

    //очитска урл от путстых значений
    $('form.form_clear').submit(function(){
        $(this).find('select,input').each(function(){
            if($(this).val()=='' || $(this).val()=='0-0') $(this).removeAttr('name');
        });
    });


    //мультичексбокс
    $(document).on("change",'.form_multi_checkbox .data input',function(){
        var arr = [];
        var i = 0;
        $(this).parents('.data').find('input:checked').each(function(){
            arr[i] = $(this).val();
            i++;
        });
        $(this).parents('.data').next('input').val(arr);
    });
    //min-max
    $(document).on("change",'.form_input2 input',function(){
        var min = parseInt($(this).parents('.form_input2').find('input.form_input2_1').val());
        var max = parseInt($(this).parents('.form_input2').find('input.form_input2_2').val());
        $(this).parents('.form_input2').find('input[type=hidden]').val(min+'-'+max);
    });


});
