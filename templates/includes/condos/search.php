<? 
global $html;
$categories = mysql_select("SELECT * FROM shop_categories WHERE display = 1 ORDER BY id ASC",'rows');                                        
$commercial = $residental = $sell = $rent = array();
foreach($categories as $c){
    if($c['commercial']) $commercial[] = $c['id']; else $residental[] = $c['id'];
    if($c['sell']) $sell[] = $c['id']; else $rent[] = $c['id'];
}

$display1 = '';
$display2 = ' style="display:none"';
//если есть параметры полного поиска
if (@$q['location']  OR @$q['beds'] OR @$q['baths'] OR @$q['price_min'] OR @$q['price_max']) {
	$display1 = $display2;
	$display2 = '';
}

?>
<form id="search-offers" action="/<?=$modules['shop']?>/<?=$modules['condos']?>/" method="get" style="<?=(($html['is_product']) ? 'display: none;' :'')?>">
    <!--div id="form-switch">
        <span data-switch-value="false" data-switch-set="state" class="switch-control active">Residental</span> <input type="checkbox" class="switch"> <span data-switch-value="true" data-switch-set="state" class="switch-control">Commercial</span>
    </div-->

                <div class="row">
                    <div class="form-group col-md-10">
                        <input class="form-control" name="quick" placeholder="Enter name of property or address or zip or MLS number" value="<?=@$_GET['quick']?>">
	                    <a href="#" id="open_all" <?=$display1?>>Open all search</a>
                    </div>
                    <div class="col-md-2">
	                    <div class="form-group" id="quick_search" <?=$display1?>>
		                    <button style="display: block; width: 100%;" class="btn btn-warning">QUICK SEARCH</button>
	                    </div>
                    </div>
                </div>

                <div class="row" id="all_search" <?=$display2?>>
                    <div class="form-group col-md-4">
                        <?
                        $cities[0]['id'] = 6;
                            $nbs = (isset($cities[0]['id'])) ? mysql_select("SELECT * FROM shop_neighborhoods WHERE display = 1 AND city = {$cities[0]['id']} ORDER BY rank DESC, name ASC",'rows') : array();                                        
                        ?>
                        <label for="location">Location:</label>
                        <!--input type="text" class="form-control" id="location" placeholder="Neighbourhood / Address / Building"-->
                        <script type="text/javascript">
                            /*$(document).ready(function() {
                                $('#location').multiselect({
                                    enableFiltering: true,
                                    enableCaseInsensitiveFiltering: true
                                    //filterBehavior: 'value'
                                });
                            });*/
                        </script>
	                    <script>
		                    $(function() {
			                    $('.chosen-select').chosen();
			                    $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
		                    });
	                    </script>
	                    <style>.chosen-container {display:block; min-width:100%}</style>
                        <select size="1" data-placeholder="Choose a location" class="form-control chosen-select" id="location" name="location[]" multiple="multiple">
                            <?foreach($nbs as $k=>$v){?>
                                <option value="<?=$v['id']?>" <?=((@in_array($v['id'], @$q['location'])) ? 'selected' :'')?>><?=$v['name']?></option>
                            <?}?>
                        </select>

                    </div>

	                <div class="col-md-2">
		                <div class="form-group">
			                <label>Sale/Rent:</label>
			                <select class="form-control" id="residential" name="residential">
				                <option value="0">None selected</option>
				                <?foreach($config['object_residential'] as $k=>$v){?>
					                <option value="<?=$k?>" <?=((@$q['residential'] == $k) ? 'selected' :'')?> ><?=$v?></option>
				                <?}?>
			                </select>
		                </div>
	                </div>



			                <div class="form-group col-md-1">
				                <label for="bedrooms">Bedrooms:</label>
				                <select class="form-control" id="bedrooms" name="beds">
					                <option value="0">select</option>
					                <?foreach($config['bedrooms_list'] as $k=>$v){?>
						                <option value="<?=$k?>" <?=((@$q['beds'] == $k) ? 'selected' :'')?> ><?=$v?></option>
					                <?}?>
				                </select>
			                </div>
			                <div class="form-group col-md-1">
				                <label for="baths">Baths:</label>
				                <select class="form-control" id="baths" name="baths">
					                <option value="0">select</option>
					                <?foreach($config['baths_list'] as $k=>$v){?>
						                <option value="<?=$k?>" <?=((@$q['baths'] == $k) ? 'selected' :'')?> ><?=$v?></option>
					                <?}?>
				                </select>
			                </div>



                            <div class="form-group col-md-2 prices">
				                <label for="type">Min Price:</label>
				                <input class="form-control"  name="price_min" id="price_min" value="<?=@$q['price_min']?'$'.number_format($q['price_min'],0,'.',','):''?>">
			                </div>
			                <div class="form-group col-md-2 prices">
				                <label for="type">Max Price:</label>
				                <input class="form-control text-right"  name="price_max" id="price_max" value="<?=@$q['price_max']?'$'.number_format($q['price_max'],0,'.',','):''?>">
			                </div>
			                <style>.prices .ui-menu-item {text-align:right}</style>


                </div> 
</form>

<script>
    function ready_condos_search (){
	    $('#open_all').on('click', function() {
		    $(this).hide();
		    $("#all_search").show();
		    return false;
	    });
		    //цены
		    var arr = [
			    <?foreach($config['prices_list'] as $k=>$v){?>
			    "<?=$v?>",
			    <?}?>
		    ];
		    var arr1 = [
			    <?foreach($config['prices_list'] as $k=>$v) if($k<=10000) {?>
			    "<?=$v?>",
			    <?}?>
		    ];
		    var arr2 = [
			    <?foreach($config['prices_list'] as $k=>$v) if($k>=10000) {?>
			    "<?=$v?>",
			    <?}?>
		    ];
		    $( "#price_max,#price_min" ).autocomplete({
			    <?php
			if (@$q['residential'] == 1) echo 'source: arr1,';
			elseif (@$q['residential'] == 2) echo 'source: arr2,';
			else echo 'source: arr,';
			?>
			    minLength: 0
		    });

		    //аренда-продажа
		    $(document).on("change", '#residential', function () {
			    var residential = $(this).val();
			    //автозаполнение
			    $( "#price_max,#price_min" ).autocomplete( "destroy" );
			    if(residential==0) {
				    $( "#price_max,#price_min" ).autocomplete({
					    source: arr,
					    minLength: 0
				    });
			    }
			    if(residential==1) {
					    $( "#price_max,#price_min" ).autocomplete({
					    source: arr1,
					    minLength: 0
				    });
			    }
			    if(residential==2) {
				    $( "#price_max,#price_min" ).autocomplete({
					    source: arr2,
					    minLength: 0
				    });
			    }
		    });
		    //чтобы спрятать подтипы и типы
		    $('#residential').trigger('change');
    }
    document.addEventListener("DOMContentLoaded", ready_condos_search);
</script>