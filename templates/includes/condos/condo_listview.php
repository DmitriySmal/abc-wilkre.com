<?php
$q['city_name'] = $config['shop_cities'][$q['city']]['name'];
$q['city_url']	= $config['shop_cities'][$q['city']]['url'];
$img = $q['img'] ? '/files/shop_condos/'.$q['id'].'/img/m-'.$q['img'] : '/templates/images/no_img.jpg';
//$img = '/templates/images/product-empty.jpg';
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$alt = $q['img'] ? 'p-'.$q['img'] : i18n('common|wrd_no_photo');
//$url = '/'.$modules['shop'].'/'.$q['category'].'-'.$q['category_url'].'/'.$q['id'].'-'.$q['url'].'/';
$url = get_url('condo',$q);
?>
<?if (($i==1)){?>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped wo_lines">
                <tr>
                    <th>Name <?= (@$_GET['sort']) ? ((@$_GET['sort'] == 'name-desc') ? '<span class="glyphicon glyphicon-triangle-bottom"></span>' : ((@$_GET['sort'] == 'name-asc') ? '<span class="glyphicon glyphicon-triangle-top"></span>':'') ) : '' ?></th>
                    <th>Min Price</th>
                    <th>Max Price</th>
                    <th>Delivery <?= (@$_GET['sort']) ? ((@$_GET['sort'] == 'year-desc') ? '<span class="glyphicon glyphicon-triangle-bottom"></span>' : ((@$_GET['sort'] == 'year-asc') ? '<span class="glyphicon glyphicon-triangle-top"></span>':'') ) : '' ?></th>
                </tr>
<?}?>

                <tr>
                    <td>
                        <a class="name" style="margin-bottom: 0;" href="<?=$url?>" title="<?=$title?>"><?=$q['name']?></a>               
                    </td>
                    <td><?=$q['price_min']?></td>
                    <td><?=$q['price_max']?></td>
                    <td><?=$q['year']?></td>
                </tr>
            
            
                

<?if (($i==$num_rows)){?>
            </table>
        </div>
    </div>
<?}?>
