<form id="subscribe2" class="bg-light-gray-pattern widget-subscribe-2" style="color:#262626;">
	    <div class="container">

	        <div class="row" style="padding-bottom:15px">
	            <div class="col-md-12 text-center">
	                <div class="thanks">
		                Didn't find a property?
	                </div>
	                <div class="info">
		                Enter all information and we will find a best deal!
	                </div>
	            </div>
	         </div>

		    <div class="row">
			    <div class="form-group col-md-3">
				    <?
				    $cities[0]['id'] = 6;
				    $nbs = (isset($cities[0]['id'])) ? mysql_select("SELECT * FROM shop_neighborhoods WHERE display = 1 AND city = {$cities[0]['id']} ORDER BY rank DESC, name ASC",'rows') : array();
				    ?>
				    <label for="location">Location:</label>
				    <script>
					    $(function() {
						    $('.chosen-select').chosen();
						    $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
					    });
				    </script>
				    <style>.chosen-container {display:block; min-width:100%}</style>
				    <select size="1" class="form-control chosen-select" data-placeholder="Choose a location" id="sub_location" name="location[]" multiple="multiple">
					    <?foreach($nbs as $k=>$v){?>
						    <option value="<?=$v['id']?>" <?=((@in_array($v['id'], @$q['location'])) ? 'selected' :'')?>><?=$v['name']?></option>
					    <?}?>
				    </select>

			    </div>

			    <div class="col-md-3">
				    <div class="form-group">
					    <label>Sale/Rent:</label>
					    <select class="form-control" id="sub_residential" name="residential"><?=select('',$config['object_residential'],'None selected')?></select>
				    </div>
			    </div>

			    <?php
			    $groups = explode(',',@$q['groups']);
			    ?>
			    <div class="col-md-3">
				    <div class="form-group">
					    <label>Property Type:</label>
					    <select class="form-control" id="sub_groups" name="group">
						    <option value="0">None selected</option>
						    <?foreach($config['object_groups'] as $k=>$v){?>
							    <option value="<?=$k?>" <?=((@$q['group'] == $k) ? 'selected' :'')?> ><?=$v['name']?></option>
						    <?}?>
					    </select>
				    </div>
			    </div>
			    <div class="col-md-3">
				    <label>&nbsp;</label>
				    <select class="form-control" id="sub_group_4" name="group_4" <?=@$q['group'] == 4?'':'style="display:none"'?>>
					    <option value="0">None selected</option>
					    <?foreach($config['object_groups_4'] as $k=>$v){?>
						    <option value="<?=$v?>" <?=((@$q['group_4'] == $v) ? 'selected' :'')?> ><?=$v?></option>
					    <?}?>
				    </select>
				    <select class="form-control" id="sub_group_3" name="group_3" <?=@$q['group'] == 3?'':'style="display:none"'?>>
					    <option value="0">None selected</option>
					    <?foreach($config['object_groups_3'] as $k=>$v){?>
						    <option value="<?=$v?>" <?=((@$q['group_3'] == $v) ? 'selected' :'')?> ><?=$v?></option>
					    <?}?>
				    </select>
				    <select class="form-control" id="sub_group_6" name="group_6" <?=@$q['group'] == 6?'':'style="display:none"'?>>
					    <option value="0">None selected</option>
					    <?foreach($config['object_groups_6'] as $k=>$v){?>
						    <option value="<?=$v?>" <?=((@$q['group_6'] == $v) ? 'selected' :'')?> ><?=$v?></option>
					    <?}?>
				    </select>
				    <script>
					    $(document).ready(function() {
						    //цены
						    var arr = [
							    <?foreach($config['prices_list'] as $k=>$v){?>
							    "<?=$v?>",
							    <?}?>
						    ];
						    var arr1 = [
							    <?foreach($config['prices_list'] as $k=>$v) if($k<=10000) {?>
							    "<?=$v?>",
							    <?}?>
						    ];
						    var arr2 = [
							    <?foreach($config['prices_list'] as $k=>$v) if($k>=10000) {?>
							    "<?=$v?>",
							    <?}?>
						    ];
						    $( "#sub_price_max,#sub_price_min" ).autocomplete({
							    <?php
							if (@$q['residential'] == 1) echo 'source: arr1,';
							elseif (@$q['residential'] == 2) echo 'source: arr2,';
							else echo 'source: arr,';
							?>
							    minLength: 0
						    });

						    //подтипы
						    $(document).on("change", '#sub_groups', function () {
							    $('#sub_group_4, #sub_group_3,#sub_group_6').hide();
							    if ($(this).val() == 3) {
								    $('#sub_group_3').show();
							    }
							    if ($(this).val() == 4) {
								    $('#sub_group_4').show();
							    }
							    if ($(this).val() == 6) {
								    $('#sub_group_6').show();
							    }
							    //прячем Baths и Bedrooms
							    if ($(this).val() == 4 || $(this).val() == 5) {
								    //alert(1);
								    $('#sub_bedrooms,#sub_baths').val('0').attr('disabled','disabled');
							    }
							    else {
								    $('#sub_bedrooms,#sub_baths').val('0').removeAttr('disabled');
							    }
						    });
						    //аренда-продажа
						    $(document).on("change", '#sub_residential', function () {
							    var group = $('#sub_groups').val(),
								    residential = $(this).val(),
								    rent = [3,4],
								    sale = [1,2,4,5,6];
							    $('#sub_groups option').hide();
							    $('#sub_groups').val(0);
							    //автозаполнение
							    $( "#sub_price_max,#sub_price_min" ).autocomplete( "destroy" );
							    if(residential==0) {
								    $('#sub_groups option').show();
								    $('#sub_groups').val(group);
								    $( "#sub_price_max,#sub_price_min" ).autocomplete({
									    source: arr,
									    minLength: 0
								    });
							    }
							    if(residential==1) {
								    $.each(rent, function(index, value){
									    $('#sub_groups option[value='+value+']').show();
									    if (value==group) $('#sub_groups').val(value);
								    });
								    $( "#sub_price_max,#sub_price_min" ).autocomplete({
									    source: arr1,
									    minLength: 0
								    });
							    }
							    if(residential==2) {
								    $.each(sale, function(index, value){
									    $('#sub_groups option[value='+value+']').show();
									    if (value==group) $('#sub_groups').val(value);
								    });
								    $( "#sub_price_max,#sub_price_min" ).autocomplete({
									    source: arr2,
									    minLength: 0
								    });
							    }
							    $('#sub_groups').trigger('change');
						    });
						    //чтобы спрятать подтипы и типы
						    $('#sub_residential').trigger('change');
					    });
				    </script>
			    </div>
		    </div>
		    <div class="row">
			    <div class="form-group col-md-6 prices">
				    <div class="row">
					    <div class="col-md-6">
						    <label for="type">Min Price:</label>
						    <input class="form-control"  name="price_min" id="sub_price_min" value="<?=@$q['price_min']?'$'.number_format($q['price_min'],0,'.',','):''?>">
					    </div>
					    <div class="col-md-6">
						    <label for="type">Max Price:</label>
						    <input class="form-control text-right"  name="price_max" id="sub_price_max" value="<?=@$q['price_max']?'$'.number_format($q['price_max'],0,'.',','):''?>">
					    </div>
					    <style>.prices .ui-menu-item {text-align:right}</style>
				    </div>
			    </div>
			    <div class="form-group col-md-3">
				    <label for="bedrooms">Bedrooms:</label>
				    <select class="form-control" id="sub_bedrooms" name="beds"><?=select('',$config['bedrooms_list'],'select')?></select>
			    </div>
			    <div class="form-group col-md-3">
				    <label for="baths">Baths:</label>
				    <select class="form-control" id="sub_baths" name="baths"><?=select('',$config['baths_list'],'select')?></select>
			    </div>
		    </div>

		    <div class="row">
	            <div class="col-md-3 form-group">
		            <label for="email">Your contact email</label>
		            <input type="text" class="form-control" name="email">
	            </div>
			    <div class="col-md-4 form-group">
				    <label for="email">Periodicity</label><div style="padding:3px 0 0">
				    <?php
				    foreach ($config['days_list'] as $k=>$v) {
						echo '<label style="width:14%;"><input type="checkbox" name="day[]" value="'.$k.'"> '.$v.'</label>';
				    }
				    ?>
					</div>
			    </div>
			    <div class="col-md-2 form-group">
				    <label for="email">Time</label>
				    <select class="form-control" name="time"><?=select('',$config['time_list'],'select')?></select>
			    </div>
				<div class="col-md-3 form-group">
					<label style="display:block; height:20px"> </label>
                    <button class="btn btn-warning" style="display:block; width:100%" id="subscribe2_send">SEND</button>
				</div>
            </div>

	    </div>
	<input type="hidden" name="type" value="subscribe2">
</form>


<!-- Modal -->
<div class="modal fade" id="subscribe2_modal" tabindex="-1" role="dialog" aria-labelledby="subscribe2_modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				Thank you! We wil sent all offers on your e-mail!
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
	$('#subscribe2').on('submit', function(){
		var form = $(this);
		$(form).find('.btn').attr('disabled','true');
		$.ajax({
			type: 'POST',
			dataType: 'JSON',
			url: "/ajax.php?file=callback",
			data: $(form).serialize(),
			success: function(data){
				if(data.result) {
					$("#subscribe2_modal").modal('show');
				}
				else {
					alert(data.message);
					console.log(data.message);
				}
			},
			complete: function(){
				$(form).find('.btn').removeAttr('disabled');
			}
		});
		return false;
	});

</script>