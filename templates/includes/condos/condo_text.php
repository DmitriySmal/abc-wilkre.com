<?php
$img = $q['img'] ? '/files/shop_condos/'.$q['id'].'/img/'.$q['img'] : '/templates/images/no_img.jpg';
//$images = $q['imgs'] ? unserialize($q['imgs']) : false;
if ($units = mysql_select("SELECT * FROM shop_products WHERE condo = {$q['id']} AND status = 'A' AND display = 1 ORDER BY price",'rows')) {
	$price1 = $price2 = $units1 = $units2 = $price_min1 = $price_max1 = $price_min2 = $price_max2 = $square1 = $square2 = 0;
	foreach ($units as $k=>$v) {
		//echo $v['residential'].'-'.$v['price'].'<br>';
		if ($v['residential']==1) {
			$units1++;
			//if ($v['price']<$price_min1) $price_min1 = $v['price'];
			//if ($v['price']>$price_max1) $price_max1 = $v['price'];
			$price1+=$v['price'];
			$square1+=$v['square'];
		}
		else {
			$units2++;
			//if ($v['price']<$price_min2) $price_min2 = $v['price'];
			//if ($v['price']>$price_max2) $price_max2 = $v['price'];
			$price2+=$v['price'];
			$square2+=$v['square'];
		}
	}
	$average_price1 = $price1 / count($units1);
	$average_price2 = $price2 / count($units2);
	$average_square1 = $price1 / $square1;
	$average_square2 = $price2 / $square2;
}
$title = filter_var($q['name'], FILTER_SANITIZE_STRING);
?>
<div class="row">    
    <div class="col-md-12">    
        <div class="shop-condo-text">
	        <div class="col-md-12">
		        <div class="page-header"><?=$q['name']?>
			        <span><?=@$q['address']?>, <?= $q['nb_name'] ?> <?=$q['city_name']?', '.$q['city_name']:''?><?=$q['zip_code']?', '.$q['zip_code']:''?></span>
		        </div>
	        </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="gallary">
                        <?php if ($q['img']) { ?><a title="<?= $title ?>" onclick="return hs.expand(this, config1)" href="/files/shop_condos/<?= $q['id'] ?>/img/<?= $q['img'] ?>"><?php } ?>
                            <img class="main" src="<?= $img ?>" alt="<?= $title ?>" />
                            <?php if ($q['img']) { ?></a><?php } ?>
							<?php
							if ($units OR $q['video']) {
								$n = 0;
								$list = '';
								if ($units) foreach ($units as $k => $v) if ($n<10 AND $v['img'] AND file_exists(ROOT_DIR.'files/shop_products/'.$v['id'].'/img/'.$v['img'])) {
									$n++;
									$title2 = filter_var($v['name'], FILTER_SANITIZE_STRING);
									$path = '/files/shop_products/' . $v['id'] . '/img/';
									$list .= '<li><a title="' . $title2 . '" onclick="return hs.expand(this, {slideshowGroup: \'group1\',transitions: [\'expand\', \'crossfade\']})" href="' . $path . $v['img'] . '"><img src="' . $path . 'p-' . $v['img'] . '" alt="' . $title2 . '" /></a></li>';
								}
								if ($q['video']) {
									$list .= '<li><a data-toggle="modal" data-target="#video" title="video" href="#video"><img src="/templates/images/youtube.png" /></a></li>';
								}
							?>
                            <div class="carousel">
                                <ul style=""><?= $list ?></ul>                            
                            </div>                           
                            <?php
							}
						?>
                    </div>
                    <div class="control-buttons">
                        <div class="row" style="margin-bottom: 25px;">
                                <div class="col-md-6">
                                    <a target="_blank" href="?print=1" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-print"></span> <?=i18n('shop|btn_print')?></a>
                                </div>
                                <div class="col-md-6">
                                    <a href="#" data-url="<?=$data_url?>" data-name="<?=$data_name?>"  data-toggle="modal" data-target="#saveFavorite" data-trigger="You Saved to Favorite http://<?=$_SERVER['HTTP_HOST']?><?=$_SERVER['REQUEST_URI']?>" class="btn btn-default btn-lg"><span
		                                    class="glyphicon glyphicon-star"></span> <?=i18n('shop|btn_sent_email')?></a>
                                </div>                                
                        </div>
                        <div class="row">                                
                                <div class="col-md-6">
                                    <a href="#" data-url="<?=$data_url?>" data-name="<?=$data_name?>" data-toggle="modal" data-target="#sheduleShowing" data-trigger="Shedule a Showing :<?=$q['name']?>" class="btn btn-default btn-lg"><span
		                                    class="glyphicon glyphicon-calendar"></span> <?=i18n('shop|btn_schedule_showing')?></a>
                                </div>                                
                                <?if(@$q['booklet']){
                                    $booklet_url = "files/shop_condos/{$q['id']}/booklet/{$q['booklet']}";
                                    ?>
                                    <?if(is_file(ROOT_DIR.$booklet_url)){?>
                                        <div class="col-md-6">
                                            <a href="/<?=$booklet_url?>" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-save"></span> Download booklet</a>
                                        </div>
                                    <?}?>
                                <?}?>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="h4 social-header">Share and discuss with your friends!</div>
                            </div>
                        </div>
                        <?
                            $share_url = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
                            $share_text = $q['name'];
                        ?>
                        <div class="row">                            
                            <div class="col-md-3">
                                <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?=$share_url?>" class="btn btn-default btn-lg btn-fb">Facebook</a>
                            </div>
                            <div class="col-md-3">
                                <a target="_blank" href="https://www.linkedin.com/cws/share?url=<?=$share_url?>" class="btn btn-default btn-lg btn-li">LinkedIn</a>
                            </div>
                            <div class="col-md-3">
                                <a target="_blank" href="http://twitter.com/share?url=<?=$share_url?>&text=<?=$share_text?>&via=wilkre" class="btn btn-default btn-lg btn-tw">Twitter</a>
                            </div>
                            <div class="col-md-3">
                                <a target="_blank" href="http://plus.google.com/share?url=<?=$share_url?>" class="btn btn-default btn-lg btn-gl">Google+</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="info">


                        <div class="send-request" style="margin-bottom: 20px;">
                            <?=i18n('condos|send_request_1')?>
                            <?=i18n('condos|send_request_2')?>   <a class="btn btn-warning" data-toggle="modal" data-target="#sendRequest" data-trigger="<?=$q['name']?>">Send Request</a>
                        </div>
                        
                        <div class="price_box">

                            <table class=" table parameters">
                                <?php if (@$q['year']) { ?>
                                    <tr>
                                        <td class="param"><?= i18n('shop|year_built') ?>:</td>
                                        <td><?= $q['year'] ?></td>
                                    </tr>
                                <?php } ?>

                                <tr>
	                                <td class="param">Bedrooms:</td>
	                                <td><?=$q['beds_min']?> to <?=$q['beds_max']?></td>
                                </tr>
                                <tr>
	                                <td class="param">Square Ft. Range:</td>
	                                <td><?=$q['square_min']?> to <?=$q['square_max']?></td>
                                </tr>
                                <?php if (@$q['floors']) { ?>
	                                <tr>
		                                <td class="param">No. of Floors:</td>
		                                <td><?= $q['floors'] ?></td>
	                                </tr>
                                <?php } ?>
	                            <?php if ($q['price_max']) { ?>
                                <tr>
	                                <td class="param">Price Range for rent:</td>
	                                <td>$<?=number_format($q['price_min'],0,'.',',')?> to $<?=number_format($q['price_max'],0,'.',',')?></td>
                                </tr>
	                            <?php } ?>
	                            <?php if ($q['price_max2']) { ?>
                                <tr>
	                                <td class="param">Price Range for sale:</td>
	                                <td>$<?=number_format($q['price_min2'],0,'.',',')?> to $<?=number_format($q['price_max2'],0,'.',',')?></td>
                                </tr>
	                            <?php } ?>
                                <tr>
	                                <td class="param">Available Units:</td>
	                                <td><?=count($units)?></td>
                                </tr>
	                            <?php if ($average_square1) { ?>
                                <tr>
	                                <td class="param">Avg $/Square Ft. for rent:</td>
	                                <td>$<?=number_format($average_square1,0,'.',',')?></td>
                                </tr>
	                            <?php } ?>
	                            <?php if ($average_square2) { ?>
                                <tr>
	                                <td class="param">Avg $/Square Ft. for sale:</td>
	                                <td>$<?=number_format($average_square2,0,'.',',')?></td>
                                </tr>
	                            <?php } ?>
	                            <?php if ($average_price1) { ?>
                                <tr>
	                                <td class="param">Average Price for rent:</td>
	                                <td>$<?=number_format($average_price1,0,'.',',')?></td>
                                </tr>
	                            <?php } ?>
	                            <?php if ($average_price2) { ?>
                                <tr>
	                                <td class="param">Average Price for sale:</td>
	                                <td>$<?=number_format($average_price2,0,'.',',')?></td>
                                </tr>
	                            <?php } ?>
                                <?php if (@$q['units']) { ?>
	                                <tr>
		                                <td class="param">No. of Units:</td>
		                                <td><?=$q['units']?></td>
	                                </tr>
                                <?php } ?>
                            </table>
                        </div>
                        <?//= html_array('common/share') ?>                    
                    </div>
                </div>
            </div>

            <?if(@$q['text']){?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="h3"><span>Description</span></div>
                        <div class="text">
                            <div<?= editable('shop_products|text|' . $q['id'], 'editable_text', 'text') ?>><?= $q['text'] ?></div>
                        </div>
                    </div>
                </div>
            <?}?>


	        <?php
	        $similar1 = '';
	        for ($n=0; $n<=5; $n++) {
		        $where = $n<5 ? ' beds = '.$n : ' beds>='.$n;
		        if ($str = html_query('shop/similar', "
					SELECT * FROM shop_products
					WHERE display=1 AND status='A' AND residential=1 AND condo=" . $q['id'] . "
					AND $where
					ORDER BY price
				", '')) {
			        $similar1.= '<div class="page-header text-center">';
			        if ($n==0) $similar1.= 'Studio';
			        elseif($n<5) $similar1.= $n.' bedrooms Condos';
			        else $similar1.= $n.' and more bedrooms Condos';
			        $similar1.= '</div>';
			        $similar1.= $str;
		        }
	        }
	        $similar2 = '';
	        for ($n=0; $n<=5; $n++) {
		        $where = $n<5 ? ' beds = '.$n : ' beds>='.$n;
		        if ($str = html_query('shop/similar', "
					SELECT * FROM shop_products
					WHERE display=1 AND status='A' AND residential=2 AND condo=" . $q['id'] . "
					AND $where
					ORDER BY price
				", '')) {
			        $similar2.= '<div class="page-header text-center">';
			        if ($n==0) $similar2.= 'Studio';
			        elseif($n<5) $similar2.= $n.' bedrooms Condos';
			        else $similar2.= $n.' and more bedrooms Condos';
			        $similar2.= '</div>';
			        $similar2.= $str;
		        }
	        }
	        if($similar1 OR $similar2) {
		        ?>
		        <div class="hidden-xs" role="tabpanel" style="padding-bottom:15px">
			        <ul class="nav nav-tabs" role="tablist">
				        <?php if ($similar1) { ?>
					        <li role="presentation" class="active"><a href="#similar1" aria-controls="map" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-list-alt"></span> Rent</a></li>
				        <?php } if ($similar2) { ?>
					        <li role="presentation" <?=$similar1?'':'class="active"'?>><a href="#similar2" aria-controls="map" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-list-alt"></span> Sale</a></li>
				        <?php } ?>
			        </ul>
		        </div>
		        <div class="row hidden-xs">
			        <div class="col-md-12">
				        <div class="tab-content">
					        <?php if ($similar1) { ?>
						        <div class="tab-pane active" role="tabpanel" id="similar1"><?=$similar1?></div>
					        <?php } if ($similar2) { ?>
						        <div class="tab-pane<?=$similar1?'':' active'?>" role="tabpanel" id="similar2"><?=$similar2?></div>
					        <?php } ?>
				        </div>
			        </div>
		        </div>
	        <?php
	        }
	        ?>

            <div class="row">
                <div class="col-md-12">
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#map" aria-controls="map" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-map-marker"></span> Map location</a></li>                            
                            <!--
                            <li role="presentation"><a href="#street" aria-controls="street" role="tab" data-toggle="tab">Street view</a></li>          
                            
                            <li role="presentation"><a href="#schools" aria-controls="schools" role="tab" data-toggle="tab">Schools</a></li>
                            <li role="presentation"><a href="#walkscore" aria-controls="walkscore" role="tab" data-toggle="tab">Walk Score</a></li>
                            <li role="presentation"><a href="#calculator" aria-controls="calculator" role="tab" data-toggle="tab">Mortgage Calculator</a></li>
                            -->
                            <?php /*if(@$q['video']){?>
                                <li role="presentation"><a href="#video" aria-controls="video" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-facetime-video"></span> Video</a></li>                            
                            <?}*/?>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="map">                                
                                 <?=html_array('common/widgets/map',$q)?>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="street">
                                <div id="map_street_view" style="width: 100%; height: 500px;"></div>
                            </div>  
                            <div role="tabpanel" class="tab-pane fade" id="schools">
                                ...
                            </div> 
                            <div role="tabpanel" class="tab-pane fade" id="walkscore">
                                ...
                            </div> 
                            <div role="tabpanel" class="tab-pane fade" id="calculator">
                                <?//=html_array('common/widgets/calc', array('price'=>$q['price']))?>
                            </div>   
                            <?php /*if(@$q['video']){
                                $video_url = YouTubeURLs($q['video']);
                                ?>
                                <div role="tabpanel" class="tab-pane fade" id="video">
                                    <?//var_dump($video_url)?>
                                    <iframe style="width: 100%; height: 420px; margin: 0 auto; display: inline-block;" src="//www.youtube.com/embed/<?=$video_url?>" frameborder="0" allowfullscreen></iframe>
                                </div> 
                            <?} */?>
                        </div>
                    </div>
                </div>
            </div>

        </div>    
    </div>
</div>

<?php
if(@$q['video']) {
	$video_url = YouTubeURLs($q['video']);?>
	<div class="modal fade wilkModal" id="video" tabindex="-1" role="dialog" aria-labelledby="video" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				<div class="modal-body">
					<iframe style="width: 100%; height: 420px; margin: 0 auto; display: inline-block;"
					        src="//www.youtube.com/embed/<?= $video_url ?>" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
<?php
}
?>