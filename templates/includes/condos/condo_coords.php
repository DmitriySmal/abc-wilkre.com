<?php
$img = $q['img'] ? '/files/shop_condos/'.$q['id'].'/img/m-'.$q['img'] : '/templates/images/no_img.jpg';
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$url = get_url('condo',$q);
?>
<strong><?=$q['name']?></strong>
<table>
	<tr valign="top">
		<td style="padding-right:10px">
			<a href="<?=$url?>" title="<?=$title?>"><img style="width:130px"  src="<?= $img ?>" alt="<?= $title ?>" /></a></td>
		<td>
			<?=$q['address']?>
			<br><a href="<?=$url?>" title="<?=$title?>">See all information</a>
		</td>
	</tr>
</table>

