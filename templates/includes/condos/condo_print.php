<?php
$img = $q['img'] ? '/files/shop_condos/'.$q['id'].'/img/m-'.$q['img'] : '/templates/images/no_img.jpg';
//$images = $q['imgs'] ? unserialize($q['imgs']) : false;
if ($units = mysql_select("SELECT * FROM shop_products WHERE condo = {$q['id']} AND status = 'A' AND display = 1 ORDER BY price",'rows')) {
	$price1 = $price2 = $units1 = $units2 = $price_min1 = $price_max1 = $price_min2 = $price_max2 = $square1 = $square2 = 0;
	foreach ($units as $k=>$v) {
		//echo $v['residential'].'-'.$v['price'].'<br>';
		if ($v['residential']==1) {
			$units1++;
			//if ($v['price']<$price_min1) $price_min1 = $v['price'];
			//if ($v['price']>$price_max1) $price_max1 = $v['price'];
			$price1+=$v['price'];
			$square1+=$v['square'];
		}
		else {
			$units2++;
			//if ($v['price']<$price_min2) $price_min2 = $v['price'];
			//if ($v['price']>$price_max2) $price_max2 = $v['price'];
			$price2+=$v['price'];
			$square2+=$v['square'];
		}
	}
	$average_price1 = $price1 / count($units1);
	$average_price2 = $price2 / count($units2);
	$average_square1 = $price1 / $square1;
	$average_square2 = $price2 / $square2;
}
$title = filter_var($q['name'], FILTER_SANITIZE_STRING);
?>
<div class="row">
	<div class="col-md-12">
		<div class="shop-condo-text">
			<h3><?=$q['name']?></h3>
			<?=@$q['address']?>, <?= $q['nb_name'] ?>, <?= $q['city_name'] ?><?=$q['zip_code']?', '.$q['zip_code']:''?>
			<div class="row">
				<div class="col-xs-6">
					<div class="gallary">
						<img class="main" src="<?= $img ?>" alt="<?= $title ?>" />
						<?php
							if ($units) {
							$n = 0;
							$list = '';
							foreach ($units as $k => $v) if ($n<10 AND $v['img'] AND file_exists(ROOT_DIR.'files/shop_products/'.$v['id'].'/img/'.$v['img'])) {
								$n++;
								$title2 = filter_var($v['name'], FILTER_SANITIZE_STRING);
								$path = '/files/shop_products/' . $v['id'] . '/img/';
								$list .= '<li><img src="' . $path . 'p-' . $v['img'] . '" alt="' . $title2 . '" /></li>';
							}
							?>
							<div class="carousel">
								<ul style=""><?= $list ?></ul>
							</div>
							<?php
						}
						?>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="info">
						<?php if ($q['price'] > 0) { ?>
							<div class="price">
								<?= i18n('shop|currency') ?><span><?= number_format($q['price'],0,'.',',')?></span>
							</div>
						<?php } ?>
						<div class="price_box">
							<table class=" table parameters">
								<?php if (@$q['year']) { ?>
									<tr>
										<td class="param"><?= i18n('shop|year_built') ?>:</td>
										<td><?= $q['year'] ?></td>
									</tr>
								<?php } ?>

								<tr>
									<td class="param">Bedrooms:</td>
									<td><?=$q['beds_min']?> to <?=$q['beds_max']?></td>
								</tr>
								<tr>
									<td class="param">Square Ft. Range:</td>
									<td><?=$q['square_min']?> to <?=$q['square_max']?></td>
								</tr>
								<?php if (@$q['floors']) { ?>
									<tr>
										<td class="param">No. of Floors:</td>
										<td><?= $q['floors'] ?></td>
									</tr>
								<?php } ?>
								<?php if ($q['price_max']) { ?>
									<tr>
										<td class="param">Price Range for rent:</td>
										<td>$<?=number_format($q['price_min'],0,'.',',')?> to $<?=number_format($q['price_max'],0,'.',',')?></td>
									</tr>
								<?php } ?>
								<?php if ($q['price_max2']) { ?>
									<tr>
										<td class="param">Price Range for sale:</td>
										<td>$<?=number_format($q['price_min2'],0,'.',',')?> to $<?=number_format($q['price_max2'],0,'.',',')?></td>
									</tr>
								<?php } ?>
								<tr>
									<td class="param">Available Units:</td>
									<td><?=count($units)?></td>
								</tr>
								<?php if ($average_square1) { ?>
									<tr>
										<td class="param">Avg $/Square Ft. for rent:</td>
										<td>$<?=number_format($average_square1,0,'.',',')?></td>
									</tr>
								<?php } ?>
								<?php if ($average_square2) { ?>
									<tr>
										<td class="param">Avg $/Square Ft. for sale:</td>
										<td>$<?=number_format($average_square2,0,'.',',')?></td>
									</tr>
								<?php } ?>
								<?php if ($average_price1) { ?>
									<tr>
										<td class="param">Average Price for rent:</td>
										<td>$<?=number_format($average_price1,0,'.',',')?></td>
									</tr>
								<?php } ?>
								<?php if ($average_price2) { ?>
									<tr>
										<td class="param">Average Price for sale:</td>
										<td>$<?=number_format($average_price2,0,'.',',')?></td>
									</tr>
								<?php } ?>
								<?php if (@$q['units']) { ?>
									<tr>
										<td class="param">No. of Units:</td>
										<td><?=$q['units']?></td>
									</tr>
								<?php } ?>
							</table>
						</div>
					</div>
				</div>
			</div>

			<?if(@$q['text']){?>
				<div class="row">
					<div class="col-md-12">
						<div class="h3"><span>Description</span></div>
						<div class="text">
							<?= $q['text'] ?>
						</div>
					</div>
				</div>
			<?}?>
			<div class="row">
				<div class="col-md-12">
			<?php
			$similar1 = '';
			for ($n=0; $n<=5; $n++) {
				$where = $n<5 ? ' beds = '.$n : ' beds>='.$n;
				if ($str = html_query('shop/similar', "
				SELECT * FROM shop_products
				WHERE display=1 AND status='A' AND residential=1 AND condo=" . $q['id'] . "
				AND $where
				ORDER BY price
				", '')) {
					$similar1.= '<h3 class="text-center">';
						if ($n==0) $similar1.= 'Studio';
						elseif($n<5) $similar1.= $n.' bedrooms Condos';
						else $similar1.= $n.' and more bedrooms Condos';
						$similar1.= '</h3>';
					$similar1.= $str;
				}
			}
			echo strip_tags($similar1,'<table><h3><img>');
			$similar2 = '';
			for ($n=0; $n<=5; $n++) {
				$where = $n<5 ? ' beds = '.$n : ' beds>='.$n;
				if ($str = html_query('shop/similar', "
				SELECT * FROM shop_products
				WHERE display=1 AND status='A' AND residential=2 AND condo=" . $q['id'] . "
				AND $where
				ORDER BY price
				", '')) {
					$similar2.= '<h3 class="text-center">';
						if ($n==0) $similar2.= 'Studio';
						elseif($n<5) $similar2.= $n.' bedrooms Condos';
						else $similar2.= $n.' and more bedrooms Condos';
						$similar2.= '</h3>';
					$similar2.= $str;
				}
			}
			echo strip_tags($similar2,'<table><thead><tbody><tr><td><h3><img>');
			?>
				</div>
			</div>

		</div>
	</div>
</div>
