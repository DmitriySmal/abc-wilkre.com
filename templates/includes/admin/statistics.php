<?php

//dd($q['schedules']);

$date = $q['year'].'-'.$q['month'].'-01';
$time = strtotime($date);
$current = date("d",$time);
$time = $time - ($current-1)*60*60*24;

$years = array();
$start = date('Y');
$end = $start+1;
for ($i=$end; $i>=$start; $i--) {
	$years[$i]= $i;
}

$months = array(
	'1'	=>	'января',
	'2'	=>	'февраля',
	'3'	=>	'марта',
	'4'	=>	'апреля',
	'5'	=>	'мая',
	'6'	=>	'июня',
	'7'	=>	'июля',
	'8'	=>	'августа',
	'9'	=>	'сентября',
	'10'	=>	'октября',
	'11'	=>	'ноября',
	'12'	=>	'декабря',
);

$config['statistics'] = array(
	'added'=>'добавлено',
	'deleted'=>'удалено',
	'removed'=>'помечено',
	'returned'=>'возвращено',
	'updated'=>'обновлено',
)


//dd($q);
?>
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
<style>
	.calendar_col {/*float:left; width:30px; */text-align: center}
	.calendar_col.day6,.calendar_col.day0 {background: #eff3f7}
	.calendar_col.current {background: rgba(213, 216, 16, 0.24);}
	.calendar_row {border-top:1px solid #dee2e6; height: 30px; padding-top: 8px}
	.calendar_row.is_order {padding: 0px}
	.calendar_row a {background: #10b759; color: #fff; height: 30px;
		font-family: 'Roboto Condensed', sans-serif; letter-spacing: -1.5px; padding: 5px 0; display: block; width: 100%; cursor: pointer}
	.calendar_row a:hover {background: orange;}
	.calendar {
		display: -webkit-flex;  /* для поддержки ранних версий браузеров */
		display: flex;  /* элемент отображается как блочный flex-контейнер */
		/*justify-content: space-around;
		/*-webkit-flex-direction: row;  /* для поддержки ранних версий браузеров */
		/*flex-direction: row;  /* флекс элементы отображаются вертикально как колонны */
	}
	.calendar>div {flex-grow: 1; min-width: 30px; /*flex-basis: 100%*/ }
</style>
<div class="form-row" id="filter">
	<?=filter('year',$years,null)?>
	<?=filter('month',$months,null)?>
</div>
<div class="clear"></div>
<div class="row">
	<div class="card" style="min-width: 100%; overflow: auto">
		<div class="card-body" style="min-width: 100px;">
			<div class="calendar">
				<div style="/*float:left; */ min-width: 50px; width:100px; text-align: right">
					<div style="height: 40px"></div>
					<?php
					foreach ($config['statistics'] as $k=>$v) {
						?>
						<div class="calendar_row"><?=$v?></div>
						<?php
					}
					?>
				</div>
				<?
				//один день столбик
				for ($i=1; $i<32; $i++) {
					if ($i>1 AND date("j",$time)==1) break;
					$day = strftime('%Y-%m-%d',$time);
					?>
					<div data-type="col" data-date="<?=$day?>"
					     class="calendar_col day<?=date("w",$time)?> <?=date('Y-m-d',$time)==$config['date']?'current':''?>">
						<div style="height: 40px">
							<div style="font-size: 18px; line-height: 1"><?=date("d",$time)?></div>
						</div>
						<?php
						$all = 1;
						foreach ($config['statistics'] as $k=>$v) {
							?>
							<div class="calendar_row">
								<?=@$q['statistics'][$day][$k]?>
							</div>
							<?php
						}
						?>
					</div>
					<?php
					//добавляем день
					$time+= 60*60*24;
				}
				?>
			</div>
		</div>
	</div>
</div>