<?
$zips = mysql_select("SELECT * FROM shop_neighborhoods_zips WHERE nb_id = 0 LIMIT 10");
$nbs = mysql_select("SELECT n.id, n.name, c.name city_name FROM shop_neighborhoods AS n LEFT JOIN shop_cities AS c ON c.id = n.city ORDER BY n.city ASC, n.name ASC");
?>

<h2>Zips</h2>

<div id="zips">
    <?foreach ($zips as $z){?>
        <div class="zip" style="margin: 10px;">
            <form>
                <select name="nb_id">
                    <option value="0">-select-</option>
                    <?  foreach ($nbs as $n){?>
                        <option value="<?=$n['id']?>">(<?=$n['city_name']?>) <?=$n['name']?></option>
                    <?}?>
                </select>
                <input name="" value="<?=$z['zip']?>" disabled="disabled">
                <input type="hidden" name="zip" value="<?=$z['zip']?>" />
                <button type="submit">change</button>
            </form>
        </div>
    <?}?>    
</div>

<a style="display: inline-block; margin-top: 30px;" href="/admin.php?m=<?=$_GET['m']?>">Update page</a>

<script>
    function ready_admin_empty_zip(){
        $('#zips form').on('submit', function(){
            var f = $(this);
            $.ajax({
                url: '/ajax.php?file=zips',
                type: 'post',
                dataType: 'json',
                data: $(f).serialize(),
                success: function(json){
                    if(json.result){
                        $(f).closest('.zip').remove();
                    }
                }
            });
            return false;
        });
    }
    document.addEventListener("DOMContentLoaded", ready_admin_empty_zip);
</script>