<?php

$style = $q['img'] ? 'background: url(/templates/images/opacity50.png), url(/files/shop_products/'.$q['id'].'/img/'.$q['img'].') no-repeat; background-size: cover;' : '';
$style = $style ? 'style="'.$style.'"':'';

$style2 = $q['img'] ? 'background: url(/templates/images/opacity50.png), url(/files/shop_products/'.$q['id'].'/img/'.$q['img'].') no-repeat; background-size: cover;' : '';
$style2 = $style2 ? 'style="'.$style2.'"':'';

$state = in_array($q['base'],array(1,2)) ? 'Florida':'New York';
$q['city_name'] = $config['shop_cities'][$q['city']]['name'];
?>
<header class="page-header  bg-overlay-1 navbar-absolute" <?=$style2?>>
	<!-- RD Navbar -->
	<div class="rd-navbar-wrap">
		<nav class="rd-navbar bg-default-before-md" data-layout="rd-navbar-static">
			<div class="rd-navbar-inner">
				<!-- RD Navbar Panel -->
				<div class="rd-navbar-panel-canvas"></div>
				<div class="rd-navbar-panel">

					<!-- RD Navbar Toggle -->
					<button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
					<!-- END RD Navbar Toggle -->

					<!-- RD Navbar Collapse -->
					<div class="rd-navbar-collapse-wrap">
						<button class="rd-navbar-collapse-toggle" data-rd-navbar-toggle=".rd-navbar-collapse, .rd-navbar-brand">
							<span></span>
						</button>
						<?php /*
						<div class="rd-navbar-collapse">
							<address class="btn contact-info heading-6 siz">516-532-1816</address>
						</div>
 */?>
					</div>
					<!-- END RD Navbar Collapse -->

					<!-- RD Navbar Brand -->
					<div class="rd-navbar-brand">
						<a href="/"><img src="/templates/images/logo-wilk.png" alt=""></a>
						<br>Since 1987
					</div>
					<!-- END RD Navbar Brand -->
					<br>Albert Wilk, Licensed Real Estate Broker in New York & Florida
					<!-- RD Navbar Nav -->
					<div class="rd-navbar-nav-wrap">
						<ul class="rd-navbar-nav">
							<li class="active"><a href="#about">about</a></li>
							<?php /*<li><a href="#property">property</a></li>*/?>
							<li><a href="#location">location</a></li>
							<?php if ($q['imgs']) {?><li><a href="#gallery">gallery</a></li><?php } ?>
							<li><a href="#agents">agent</a></li>
							<?php /*<li><a href="#contacts">contacts</a></li>*/?>
						</ul>
					</div>
					<!-- END RD Navbar Nav -->

				</div>
			</div>
		</nav>
	</div>
	<!-- END RD Navbar -->

	<!-- RD Video -->
	<section class="rd-video">
		<div class="container">
			<div class="row row-xs-middle well-md well-md--inset-1">
				<div class="col-xs-12">
					<h1><?= i18n('shop|currency', true) ?> <?= number_format($q['price'],0,'.',',')?> <br><?=$q['name']?>,
						<?= $q['city_name'] ?>, <?=$state?></h1>
					<?php if ($q['3d']) {?>
					<div class="btn-group">
						<a href="#" data-toggle="modal" data-target="#modal" class="btn btn-xl btn-secondary">Explore the Property</a>
					</div>
					<?php } ?>
					<div class="row">
						<div class="col-md-4">
							<div class="icon-xl icon-default--mod-1"><div class="icon icon-hotel-05"></div></div>
							<h5><?=$q['beds']?> BEDROOMS</h5>
						</div>
						<div class="col-md-4">
							<div class="icon-xl icon-default--mod-1"><div class="icon icon-hotel-10"></div></div>
							<h5><?=$q['baths']?> BATHROOMS</h5>
						</div>
						<?php ?>
						<div class="col-md-4">
							<div class="icon-xl icon-default--mod-1"><div class="icon icon-hotel-29"></div></div>
							<h5><?=$q['square']?> SQ./FEET</h5>
						</div>
						<?php /*for($i=1; $i<100; $i++) {?>
							<div class="col-md-4">
								<div class="icon-xl icon-default--mod-1"><div class="icon icon-hotel-<?=$i?>"></div></div>
							</div>
						<?php } */?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- END Intro-->
</header>
<section id="about" class="well-md bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-7 text-md-left">
				<div class="row">
					<div class="col-lg-11">
						<h1><?=random_property_type ($q)?> <?=$q['residential']==2?'for sale':'for rent'?></h1>
						<hr class="devider-sm devider-center devider-md-left text-secondary">
						<?=$q['text']?>
						<br><br>Listing courtesy of <?=$q['office_name']?>
					</div>
				</div>
			</div>
			<div class="col-md-5 col-md-preffix-0 col-lg-4 col-lg-preffix-1">
				<div class="row">
					<div class="col-sm-6 col-sm-preffix-3 col-md-12 col-md-preffix-0">
						<div class="box box-inset bg-contrast">
							<h5>SEND REQUEST NOW</h5>
							<h6 class="heading-5 text-primary text-italic text-inline">Get expert advice</h6>
							<?=html_array('landing/feedback',array('id'=>$q['id']));?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- END Forms -->
<!-- RD Navbar -->
<!-- Our partners -->
<?php /*
<section id="property" class="bg-image" <?=$style?>>
	<div class="bg-default well-lg">
		<div class="container">
			<div class="counter-group">
				<div class="counter-group-item">
					<!-- Count To -->
					<div class="counter counter-secondary" data-from="0" data-to="<?=$q['beds']?>"></div>
					<!-- END Count To -->
					<h5>Bedrooms</h5>
				</div>
				<div class="counter-group-item">
					<!-- Count To -->
					<div class="counter counter-secondary" data-from="0" data-to="<?=$q['baths']?>"></div>
					<!-- END Count To -->
					<h5>Bathrooms</h5>
				</div>
				<div class="counter-group-item">
					<!-- Count To -->
					<div class="counter counter-secondary" data-from="0" data-to="<?=$q['square']?>"></div>
					<!-- END Count To -->
					<h5>SQ./FEET</h5>
				</div>
			</div>
		</div>
		<div class="block">
			<a class="btn btn-block btn-sm btn-secondary" href="#contacts">SEND REQUEST</a>
		</div>
	</div>
</section>
<!-- END Our partners-->
<!-- END RD Navbar -->
*/?>
<!-- General Content -->
<section class="bg-light" id="location">
	<!-- RD Google Map -->
	<div class="rd-google-map">
		<?php
		if ($q['coordinates']) {
			$pieces = explode(",", substr($q['coordinates'],1,-1));
			?>
			<div id="google-map" class="rd-google-map__model" data-x="<?=trim($pieces[1])?>" data-y="<?=trim($pieces[0])?>"></div>
		<?php
		}
		else {
			?>
			<div id="google-map" class="rd-google-map__model" data-x="25.7616798" data-y="-80.191"></div>
			<?php
		}
		?>
		<div class="rd-google-map-overlay">
			<div class="row row-sm-middle bg-default well-sm">
				<div class="col-sm-12 text-sm-left">
					<div class="rd-google-map-overlay-inner">
						<h4><?= $q['nb_name']?$q['nb_name'].', ':'' ?><?= $q['city_name'] ?>, <?=$state?></h4>

						<hr class="devider-sm devider-center devider-sm-left text-secondary">
						<?=mysql_select("SELECT text FROM shop_neighborhoods WHERE id=" . $q['neighborhood'], 'string');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END RD Google Map -->
</section>

<section id="param" class="well-md bg-light">
	<div class="container">
		<h4>Details</h4>
		<hr class="devider-sm devider-center text-secondary">
		<br>
		<div class="param_box"><?=html_array('shop/product_parameters',$q);?></div>
	</div>
</section>

<!-- END General Content --><!-- Gallery -->
<section id="gallery">
	<div class="row row-no-gutter bg-default">
		<?=html_sources('return','highslide_gallery')?>
		<?php
		$images = $q['imgs'] ? unserialize($q['imgs']) : false;
		if ($images) {
			$count = count($images);
			$fmod = fmod($count,3);
			$count = $count - $fmod;
			$i=1;
			foreach ($images as $k=>$v) if (@$v['display']==1) {
				$i++;

				$alt=filter_var(@$v['name'],FILTER_SANITIZE_STRING);
				$title2=filter_var(@$v['title'],FILTER_SANITIZE_STRING);
				$title = $title2 ? $title2 : $alt;
				$path = '/files/shop_products/'.$q['id'].'/imgs/'.$k.'/';
				?>
				<div class="col-sm-4">
					<!-- Magnific Popup Image -->
					<a onclick="return hs.expand(this, config1 )" class="thumb" href="<?=$path?><?=$v['file']?>">
						<img src="<?=$path?><?=$v['file']?>" alt="<?=$alt?>" title="<?=$title?>">
						<span class="thumb__overlay bg-secondary">
							<span class="icon-secondary icon-md">
								<span class="material-icons-add icon"></span>
							</span>
						</span>
					</a>
				</div>
			<?php
				if ($i>$count) break;
			}
		}
		?>
	</div>
</section>
<!-- END Gallery --><!-- General Content -->

<?php if ($q['agent']) {?>
	<section id="agents" class="well-md bg-light">
		<div class="container">
			<h4>Realtor</h4>
			<hr class="devider-sm devider-center text-secondary">
			<div class="row offset-2 text-md-left">
			<?php
			/*$where = '';
			if ($q['agent'] AND $q['agent']!=34) {
				$where = ' AND agent IN ('.$q['agent'].',34)';
			}*/
			$agent = mysql_select ("SELECT * FROM users WHERE agent=1 AND id='".$q['agent']."' ORDER BY rank DESC",'row');
				?>
				<div class="col-sm-4">
					<img src="/files/users/<?=$agent['id']?>/img/<?=$agent['img']?>" alt=" " class="round inset-3">
					<h5><?=$agent['name']?></h5>
					<p><?=$agent['text']?></p>
					<address class="contact-info heading-6"><?=$agent['phone']?></address>
					<p><a href="mailto:<?=$agent['email']?>"><?=$agent['email']?></a></p>
				</div>
				<div class="col-sm-8 col-sm-preffix-3 col-md-preffix-0">
					<div class="box box-inset bg-contrast">
						<h5>SEND REQUEST NOW</h5>
						<h6 class="heading-5 text-primary text-italic text-inline">Get expert advice</h6>
						<?=html_array('landing/feedback',array('id'=>$q['id']));?>
					</div>
				</div>
			</div>

		</div>
	</section>
<?php } else {?>
	<section id="agents" class="well-md bg-light">
		<div class="container">
			<div class="row offset-2 text-md-left">
				<div class="col-sm-2"></div>
				<div class="col-sm-8 col-sm-preffix-3 col-md-preffix-0">
					<div class="box box-inset bg-contrast">
						<h5>SEND REQUEST NOW</h5>
						<h6 class="heading-5 text-primary text-italic text-inline">Get expert advice</h6>
						<?=html_array('landing/feedback',array('id'=>$q['id']));?>
					</div>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</section>
<?php } ?>

<?php /*
<!-- END General Content --><!-- Forms -->
<section id="contacts" class="well-md">
	<div class="container">
		<h4>CONTACT US</h4>
		<hr class="devider-sm devider-center text-secondary">
		<!-- RD Mailform -->
		<?=html_array('landing/feedback2',@$post);?>
		<!-- END RD Mailform -->
	</div>
</section>
*/?>
<?php if ($q['3d']) {?>
<div class="modal fade" id="modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<iframe width="640" height="480" data-src="<?=$q['3d']?>" src="<?=$q['3d']?>" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" onmousewheel=""></iframe>
		</div>
	</div>
</div>
<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function () {
		$('body').on('click', '#modal .close',function(){
			var src = $(this).closest(".modal-dialog").find("iframe").attr("src");
			$(this).closest(".modal-dialog").find("iframe").attr("src","").attr("src",src);
		});
	});
</script>
<?php } ?>
<div class="modal fade" id="answer" role="dialog">
	<div class="modal-dialog2">
		<div class="modal-content">
			<button type="button" class="close2" data-dismiss="modal" aria-hidden="true">&times;</button>
			Thank You For Your Request!<br>The Agent Will Contact You during the hour!
		</div>
	</div>
</div>
