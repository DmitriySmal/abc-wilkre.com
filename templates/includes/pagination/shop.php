<?php

//массив возможных количеств выдачи записей
$count_array = array(12,24,48,'all');
//$count_array = array(3);
//номер страницы с записями
$n = (isset($_GET['n']) && $_GET['n']>=1) ? intval($_GET['n']) : 1;
//количество записей на страницу
$c = (isset($_GET['count']) && in_array($_GET['count'],$count_array)) ? $_GET['count'] : $count_array[0];
//полное количество записей
if (isset($m[2])) $num_rows = $m[2];
else $num_rows = mysql_select($query,'num_rows',$cache);
$config['_num_rows'] = $num_rows;

//КОД ПАГИНАТОРА ***************************************************************
if ($num_rows>0 && $c>0) {
	//если количество переданное через урл больше реального количества то сравнивается
	if ($c>$num_rows) $c = $num_rows;
	//количество страниц пагинатора
	$quantity = ceil($num_rows/$c);

	//количество ссылок
	$lc = 7;

	//страниц меньше или равно $lc
	if ($quantity <= $lc) {
		for ($i=1; $i<=$quantity; $i++) $list[] = array($i,$i);

	//если страниц пагинатора больше $lc, так как пагинатор расчитан только на $lc ссылок
	} else {
		//активная в начале  [1][2][3][4][5][..][100], если она не замыкает группу (5)
		if ($n < ($e = $lc - 2)) {
			for ($i=1; $i<=$e; $i++) $list[] = array($i,$i);			//$lc-2 первых ссылок
			$list[] = array(ceil(($quantity + $e)/2),0);				//[..]
			$list[] = array($quantity,$quantity);						//последняя ссылка

		//активная в коце [1][..][96][97][98][99][100], если она не начинает группу (96)
		} elseif ($n > ($s = $quantity - $lc + 2 + 1)) {
			$list[] = array(1,1);										//первая ссылка
			$list[] = array(ceil(($s + 1)/2),0);						//[..]
			for ($i = $s; $i<=$quantity; $i++) $list[] = array($i,$i);	//$lc-2 последних ссылок

		//активная в середине [1][..][49][50][51][..][100]
		} else {
			$s = $n - ceil(($lc - 4 - 1)/2);
			$e = $n + floor(($lc - 4 - 1)/2);

			$list[] = array(1,1);										//первая ссылка
			$list[] = array((ceil(($s + 1)/2)),0);						//[..]
			for ($i = $s; $i<=$e; $i++) $list[] = array ($i,$i);		//$lc-4 средних ссылок
			$list[] = array(ceil(($quantity + $e)/2),0);				//[..]
			$list[] = array($quantity,$quantity);						//последняя ссылка
		}
	}
}

//HTML *************************************************************************
//v1.2.37 - переадресация если неккоректный $_GET['n'] или $_GET['n']=1
if (isset($_GET['n']) AND ($_GET['n']!=$n OR $_GET['n']=='1') ) {
	die(header('location: ' . pagination_link ('n',1,1), true, 301));
}
if ($num_rows==0 OR $n>count($list) OR isset($_GET['view']) OR isset($_GET['count'])) {
	$page['noindex'] = 1;
}
//если есть пагинатор
if (isset($list) && count($list)>1) {
	$pagination = '{content}<div class="row">
	<div class="col-md-12"><div class="pagination-shop">';
	//$pagination.= ($n==1) ? '<span class="prev">Previous</span>' : '<a class="prev" href="'.(($url=='' AND $n==2) ? $clear : '?'.$url.'n='.($n-1)).'">Previous</a>';
	$pagination.= ($n==1) ? '<span class="hidden-xs prev">Previous</span>' : '<a class="hidden-xs prev" href="'.pagination_link ('n',$n-1,1).'">Previous</a>';
	//$pagination.= ($n==1) ? '<span class="visible-xs prev">&lt;</span>' : '<a class="prev visible-xs" href="'.(($url=='' AND $n==2) ? $clear : '?'.$url.'n='.($n-1)).'">&lt;</a>';

	foreach ($list as $k=>$v) {
		$name = $v[1]==0 ? '...' : $v[0];
		$pagination.= '<a class="number'.($v[0]==$n ? ' active' : '').'" href="'.pagination_link ('n',$v[0],1).'">'.$name.'</a>';
	}
	//$pagination.= ($n==$quantity) ? '<span class="next">Next</span>' : '<a class="next" href="?'.$url.'n='.($n+1).'">Next</a>';
	$pagination.= ($n==$quantity) ? '<span class="hidden-xs next">Next</span>' : '<a class="hidden-xs next" href="'.pagination_link ('n',$n+1,1).'">Next</a>';
	//$pagination.= ($n==$quantity) ? '<span class="visible-xs next">&gt;</span>' : '<a class="next visible-xs" href="?'.$url.'n='.($n+1).'">&gt;</a>';
	$pagination.= '';

	$pagination.= '<div style="padding:10px 0 0" class="visible-xs">';
	$pagination.= ($n==1) ? '<span class="prev">Previous</span>' : '<a class="prev" href="'.pagination_link ('n',$n-1,1).'">Previous</a>';
	$pagination.= ($n==$quantity) ? '<span class="next">Next</span>' : '<a class="next" href="'.pagination_link ('n',$n+1,1).'">Next</a>';
	$pagination.= '</div></div>';

	$pagination.= '</div></div>';
	//v1.2.31 - следующая
	if ($n<$quantity) {
		$page['next'] = pagination_link ('n',$n+1,1);
	}
	//v1.2.31 - предыдущая
	if ($n>1 AND $n<=count($list)) {
		$page['prev'] = pagination_link ('n',$n-1,1);
	}

//если нет результата
} else {
	$pagination = '{content}';
	if ($c=='all' AND 24*$n<$num_rows) {
		$get = $_GET;
		unset($get['u'],$get['n']);
		$get['n'] = $n+1;
		$get['action'] = 'pagination';
		$url = http_build_query($get);
		$pagination.= '<div class="pagination_ajax2" data-url="'.$clear.'?'.$url.'">';
		$pagination.= '<a href="#" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-repeat"></span> Show 24 more</a>';
		$pagination.= '<div>Show <span>'.(24*$n).'</span> of '.$num_rows.'</div>';
		$pagination.= '</div>';
	}
}

//QUERY ************************************************************************
if ($c=='all') $c = 24;
$begin = $n*$c-$c;
$query.= ' LIMIT '.$begin.','.$c;

?>