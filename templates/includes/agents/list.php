<?php
$q['text'] = strip_tags($q['text']);
$text = iconv_strlen($q['text'])>100 ? iconv_substr($q['text'],0,100,"UTF-8").'...' : $q['text'];
//$text = ($text) ? $text : 'No info';
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$url = "/".$u[1]."/".$u[2]."/{$q['url']}/";
$url = '/'.$q['url'].'/';
/*$url = '#';href="<?=$url?>" */
$img = ($q['img']) ? "/files/users/{$q['id']}/img/p-{$q['img']}" : "/templates/images/agent-empty.png";

if ($i==1) {
	//[4.494] h1 на карточках, h2 сквозное
	?>
	<div class="h3"><h1>Wilk Real Estate Realtors</h1></div>
	<div class="row agent-list">
			<div class="col-md-3">
                <img class="avatar" src="<?=$img?>">
                <?php
                /*
				<a class="img" href="<?=$url?>">
					<img class="avatar" src="<?=$img?>">
				</a>
                 */
                ?>
			</div>
			<div class="col-md-9">
				<div class="name">
					<?=$q['name']?>
                    <?php
                    /*
					<a title="<?=$title?>" href="<?=$url?>"><?=$q['name']?></a>
                     */
                    ?>
				</div>
				<div class="phone">
					<?=i18n("agents|phone")?> <?=$q['phone']?>
				</div>
				<div class="email">
					<a href="mailto:<?=$q['email']?>"><?=$q['email']?></a>
				</div>
				<div class="text">
					<?=$q['text']?>
				</div>
			</div>
	</div>
	<?
}
else {
?>
<div class="col-md-3" <?=fmod($i,4)==2?'style="clear:both"':''?>>
    <div class="agent-list">
        <img class="avatar" src="<?=$img?>">
        <?php
        /*
            <a class="img" href="<?=$url?>">
                <img class="avatar" src="<?=$img?>">
            </a>
         */
        ?>
            <div class="name">
                <?=$q['name']?>
                <?php
                /*
                <a title="<?=$title?>" href="<?=$url?>"><?=$q['name']?></a>
                 */
                ?>
            </div>
            <div class="phone">
               <?=i18n("agents|phone")?> <?=$q['phone']?>
            </div>
            <div class="email">
                <a href="mailto:<?=$q['email']?>"><?=$q['email']?></a>
            </div>
            <div class="text"><?=$text?></div>
            <div class="next">
                <?=i18n('agents|more')?>
                <?php
                /*
                <a rel="nofollow" href="<?=$url?>"><?=i18n('agents|more')?></a>
                 */
                ?>
            </div>
    </div>
</div>
<?php
}
?>