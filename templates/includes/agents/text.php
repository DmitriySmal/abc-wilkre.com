<?php
$img = ($q['img']) ? "/files/users/{$q['id']}/img/p-{$q['img']}" : "/templates/images/agent-empty.png";
//$where = $u[1] == $modules['shop'] ? " AND sp.base<3" :" AND sp.base>2";
?>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            <?=$q['name']?>
        </h1>
    </div>
</div>

<div class="row agent-list">
	<div class="col-md-3">
		<div class="img">
			<img class="avatar" src="<?=$img?>">
		</div>
	</div>
	<div class="col-md-9">
        <?php
        /*
		<div class="name"><?=$q['name']?></div>
         */
        ?>
		<div class="phone">
			<?=i18n("agents|phone")?> <?=$q['phone']?>
		</div>
		<div class="email">
			<a href="mailto:<?=$q['email']?>"><?=$q['email']?></a>
		</div>
		<div class="text">
			<?=$q['text']?>
		</div>
	</div>
</div>

<?php
$query = "
	SELECT sp.*,
		neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
	FROM shop_products AS sp
	LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
	WHERE sp.display = 1 AND sp.agent=".$q['id']."
	ORDER BY sp.special DESC, sp.date_change DESC
	LIMIT 40
";
if ($content = html_query('shop/product_list', $query,'')) {
	?>
	<h5><span>Real Estate Properties of <?=$q['name']?></span></h5>
	<?=$content?>
<?php
}
?>
