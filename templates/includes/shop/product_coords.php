<?php
$img = $q['img'] ? '/files/shop_products/'.$q['id'].'/img/m-'.$q['img'] : '/templates/images/no_img.jpg';
if ($q['img'] AND is_file(ROOT_DIR . $img)==false) {
	//отключаем объект и на повторный парсинг
	mysql_fn('update','shop_products',array('id'=>$q['id'],'display'=>0,'date'=>date('Y-m-d H:i:s')));
	$img = '/templates/images/no_img.jpg';
}
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$parameters = $q['parameters'] ? unserialize($q['parameters']) : false;
$url = get_url('product',$q);
?>
<strong><?=$q['name']?></strong>
<table>
	<tr valign="top">
		<td style="padding-right:10px">
			<a href="<?=$url?>" title="<?=$title?>"><img style="width:130px"  src="<?= $img ?>" alt="<?= $title ?>" /></a></td>
		<td>
			<?= i18n('shop|currency', true) ?><span><?= number_format($q['price'],0,'.',',')?>
			<br><?=$q['address']?>
			<br><a href="<?=$url?>" title="<?=$title?>">See all information</a>
		</td>
	</tr>
</table>

