<table class="table table-striped">
	<thead>
	<tr>
		<td></td>
		<td>Type</td>
		<td class="text-right">Number</td>
		<td class="text-center">Lease</td>
		<td class="text-right">Rent</td>
		<td class="text-right">Rooms</td>
		<td class="text-right">Bedrooms</td>
		<td class="text-right">Bathrooms</td>
	</tr>
	</thead>
	<tbody>
<?php
foreach ($q as $k=>$v) {
?>
	<tr>
		<td><?=$k?></td>
		<td><?=@$config['store_info']['type'][@$v['type']]?></td>
		<td class="text-right"><?=@$v['number']?></td>
		<td><?=@$config['store_info']['lease'][@$v['lease']]?></td>
		<td class="text-right"><?=@$v['rent']?'$'.number_format($v['rent'],0,'.',','):''?></td>
		<td class="text-right"><?=@$v['rooms']?></td>
		<td class="text-right"><?=@$v['beds']?></td>
		<td class="text-right"><?=@$v['baths']?></td>
	</tr>
<?php } ?>
	<tbody>
</table>