<?php
require_once(ROOT_DIR.'functions/mls.php');
//отключил лендинги
$q['landing'] = 0;

if (access('user admin')) {
    //dd($q);
   // die();
}

/*
$q['category_name'] = $config['shop_categories'][$q['category']]['name'];
$q['category_url']	= $config['shop_categories'][$q['category']]['url'];
$q['city_url']	= $config['shop_cities'][$q['city']]['url'];
*/
$q['city_name'] = $config['shop_cities'][$q['city']]['name'];
//if (!isset($q['neighborhood_name'])) {
	$neighborhood = get_data('shop_neighborhoods',$q['neighborhood']);
	$q['neighborhood_name'] = $neighborhood['name'];
	$q['neighborhood_wiki'] = $neighborhood['wikipedia'];
//}
//if ($config['local']) $config['server'] = '';
$img = $q['img'] ? $config['server'].'/files/shop_products/'.$q['id'].'/img/'.$q['img'] : '/templates/images/no_img.jpg';
if ($config['local']==0 AND $q['img'] AND is_file(ROOT_DIR . $img)==false) {
	//на повторный парсинг
	//[4.686] Во Флориде много объектов загружается без картинок
	//[4.714] нет картинок
	if (in_array($q['base'],array(2,6))) {
		mysql_fn('update', 'shop_products', array('id' => $q['id'], 'date' => '', 'date_photo' => ''));
	}
	$img = '/templates/images/no_img.jpg';
	$img = '/templates/images/no_img.jpg';
}
if ($q['img']) $page['og:image'] = '/files/shop_products/'.$q['id'].'/img/'.$q['img'];
$page['og:url'] = get_url('product',$q);
$images = $q['imgs'] ? unserialize($q['imgs']) : false;
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);

$state = in_array($q['base'],array(1,2,8,9)) ? 'FL':'NY';

if ($q['office_name']=='') {
	//если для своих объектов не указало то ставим вилк
	if (in_array($q['base'],array(1,7))) {
		$q['office_name'] = 'Wilk Real Estate I LLC.';
	}
}


//[4.535] Срочно сделать
if ($q['residential']==1 AND in_array($q['base'],array(3,4)) AND in_array($q['property_type'],array(2,3))) {
	$config['ny_shop_parameters']['ListPrice']['name'] = 'Rent';
}

//[1.18] убрать маями
if ($q['base']==9) $h2 = name_search2($q['name']).', '.$state.' '.$q['zip_code'];
else $h2 = name_search2($q['name']).' '.$q['city_name'].', '.$state.' '.$q['zip_code'];
//[4.52] названия улиц
$h2 = object_name($q);

//if (access('user admin')) echo '-'.$q['timestamp'].'-';

//itemprop="location"
//http://schema.org/TradeAction
//SingleFamilyResidence
?>
<div class="row" <?php /*itemscope itemtype="http://schema.org/House http://schema.org/Product"*/?> data-base="<?=$q['base']?>">

        <?php /*
        <meta itemprop="category" content="Real Estate > Homes For Sale">
        <meta itemprop="numberOfRooms" content="">
        <meta itemprop="floorSize" content="1116">
 */?>
    <?php
    /*
    if ($q['lat']) {?>
        <span itemprop="geo" itemscope="" itemtype="http://schema.org/GeoCoordinates">
                <meta itemprop="latitude" content="<?=$q['lat']?>">
                <meta itemprop="longitude" content="<?=$q['lng']?>">
         </span>
    <?php } ?>
        <span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
            <meta itemprop="streetAddress" content="<?=$q['address']?>">
            <meta itemprop="addressLocality" content="<?=$q['city_name']?>">
            <meta itemprop="addressRegion" content="<?=$state?>">
            <meta itemprop="postalCode" content="<?=$q['zip_code']?>">
        </span>
    */?>

    <div class="col-md-12">    
        <div class="shop-product-text">
            <div class="row">
	            <div class="col-md-12">
		            <h1 class="page-header"><span <?php /*itemprop="name"*/?>>
                            <?=object_name($q)?></span>
                            <?php /*if ($state=='NY') {
	                            if ($q['mln']) echo 'MLS#' . $q['mln'].' | ';
                            }?>
                            <?=$q['name']?></span>
                        <?php /*
			            <span itemprop="location" itemscope itemtype="http://schema.org/Place">
							<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
								<?=$q['nb_name']?$q['nb_name'].', ':''?><?=$q['base']==9?'':$q['city_name'].','?> <?=$state?> <?=$q['zip_code']?>
							</span>
						</span>
                           */ ?>
		            </h1>
	            </div>

	            <?if(@$q['text']){?>
		            <div class="col-md-12">
			            <div class="text" <?php /*itemprop="description"*/?>>
				            <div class="text"><?= $q['text'] ?>
				            <?php if (1 OR access('user admin')) {
				                if ($q['neighborhood_wiki']) {
					                $url = $q['neighborhood_wiki'];
				                }
				                else {
					                $n_name = str_replace(' ', '_', $q['neighborhood_name']);
					                if ($q['base'] != 10) {
						                $c_name = str_replace(' ', '_', $q['city_name']);
						                $url = 'https://en.wikipedia.org/wiki/' . $n_name . ',_'.$c_name;
					                } else {
						                $url = 'https://en.wikipedia.org/wiki/' . $n_name . ',_Florida';
					                }
				                }
					            ?>
                                <a style="float:right" target="_blank" class="btn btn-primary" href="<?=$url?>">Neighborhood highlights</a>
				            <?php } ?>
                            </div>

                        </div>
		            </div>
	            <?}?>

                <div class="col-md-7">

	                <?php
	                $config['date'] = date('Y-m-d');
	                if (strtotime($q['oh_date'])>=strtotime($config['date'])) {
		                $agents = mysql_select("SELECT * FROM users WHERE agent=1",'rows_id');
		                $oh = unserialize($q['oh_dates']);
		                //if (access('user admin')) dd($oh);
		                ?>
		                <div class="open_house row" style="margin:0px 0px 10px">
			                <div class="col-md-3" style="padding-top:10px; font-size:23px; font-style: italic">Open house</div>
			                <div class="col-md-9" style="border-left:2px solid #e6e6e6">
				                <?php foreach ($oh as $k=>$v) if (strtotime($v['date'])>=strtotime($config['date'])) {?>
					                Date/Time: <strong><?=date2($v['date'],'%a, %m/%d/%Y')?> | <?=date_time($v['from'])?> &ndash; <?=date_time($v['to'])?></strong>
					                <?php if ($v['agent']) {?>
						            <?=$agents[$v['agent']]['name']?> <?=$agents[$v['agent']]['phone']?>
						            <?php } ?>
					                <br>
				                <?php } ?>
				                <?php /*if ($q['oh_hosted_by']) {?>
					                Name: <?=$q['oh_hosted_by']?><br>
				                <?php } ?>
				                <?php if ($q['oh_phone']) {?>
					                Phone: <?=$q['oh_phone']?><br>
				                <?php } */?>
				                <?php if ($q['oh_text']) {?>
					                <?=$q['oh_text']?>
				                <?php } ?>
				            </div>
		                </div>
	                <?php } ?>

                    <div class="gallary">
	                    <?php
	                    //[4.625] Статусы New, Sold, Leased и т.д. должны отображаться на карточке товара
	                    $status = object_status ($q);
	                    if ($status) {?>
		                    <div class="status <?=$status?>"><?=$status=='Contract'?'IN ':''?><?=$status?></div>
	                    <?php }
	                    ?>
                        <?php if ($q['img']) { ?><a <?php /*itemprop="image"*/?> title="<?= $title ?>" onclick="return hs.expand(this, config1)" href="/files/shop_products/<?= $q['id'] ?>/img/<?= $q['img'] ?>"><?php } ?>
                            <img  class="main" src="<?= $img ?>" title="<?= $title ?>" alt="<?= $title ?>" />
                            <?php if ($q['img']) { ?></a><?php } ?>
                        <?php
                        if ($q['base']==9 AND $q['uid']) {
                            /*
                            ?>
                            <div class="carousel"></div>
                            <script type="text/javascript">
                                document.addEventListener("DOMContentLoaded", function () {
	                                $.get('/ajax.php', {
	                                	'file':'object_imgs','uid':<?=$q['uid']?>},
                                        function(data){
	                                	if(data) {
	                                		$('.carousel').html(data);
		                                }
	                                });

                                });
                             </script>
                            <?
                            */
                        }
                        elseif ($q['base']==10) {
                            if ($q['hide']<2) {
	                            $parameters = unserialize($q['parameters']);
	                            if ($parameters['Media']) {
		                            $list = '';
		                            foreach ($parameters['Media'] as $img) { // $img содержит объект \PHRETS\Models\Object
			                            $list .= '<li><a
											title=""
											onclick="return hs.expand(this, {slideshowGroup: \'group1\',transitions: [\'expand\', \'crossfade\']})"
											href="' . $img['MediaURL'] . '?1"><img
												src="' . $img['MediaURL'] . '"
											
							                /></a></li>';
		                            }
		                            ?>
                                    <div class="carousel">
                                        <ul style=""><?= $list ?></ul>
                                    </div>
		                            <?
	                            }
                            }
                        }
                        else {
	                        if ($images) {
		                        $n = 0;
		                        $list = '';
		                        foreach ($images as $k => $v)
			                        if (@$v['display'] == 1) {
				                        $n++;
				                        $title2 = filter_var($v['name'], FILTER_SANITIZE_STRING);
				                        $path = '/files/shop_products/' . $q['id'] . '/imgs/' . $k . '/';
				                        if (is_file(ROOT_DIR . $path . $v['file'])) {
					                        $filetime = filemtime(ROOT_DIR . $path . $v['file']);
					                        $list .= '<li><a
											title="' . $title2 . '"
											onclick="return hs.expand(this, {slideshowGroup: \'group1\',transitions: [\'expand\', \'crossfade\']})"
											href="' . $path . $v['file'] . '?1"><img
												src="' . $path . 'p-' . $v['file'] . '?'.$filetime.'"
												title="' . $title2 . '"
												alt="' . $title2 . '"/></a></li>';
				                        }
			                        }
		                        ?>
                                <div class="carousel">
                                    <ul style=""><?= $list ?></ul>
                                </div>
		                        <?
	                        }
                        }
                        ?>
                    </div>
                    <div class="control-buttons">
                        <div class="row" style="margin-bottom: 25px;">
                                <?
                                 $data_url = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
                                 $data_name = $q['name'];
                                ?>
                                <div class="col-md-6">
                                    <a target="_blank" href="?print=1" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-print"></span> <?=i18n('shop|btn_print')?></a>
                                </div>
                                <div class="col-md-6">
                                    <a href="#"
                                       data-url="<?=$data_url?>"
                                       data-name="<?=$data_name?>"
                                       data-toggle="modal"
                                       data-target="#saveFavorite"
                                       data-trigger="Sent to e-mail http://<?=$_SERVER['HTTP_HOST']?><?=$_SERVER['REQUEST_URI']?>"
                                       class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> <?=i18n('shop|btn_sent_email')?></a>
                                </div>
                        </div>
	                    <div class="row" style="margin-bottom: 25px;">
		                    <?php if ($q['city']!=6) {?>
                                <div class="col-md-6">
                                    <a href="#" data-url="<?=$data_url?>" data-name="<?=$data_name?>"
                                       data-toggle="modal" data-target="#sheduleShowing"
                                       data-trigger="Schedule a Showing :<?=$q['name']?> <?=((@$q['mln']) ? "(mls# {$q['mln']})" : '') ?>"
                                       onclick="ga('send', 'event', 'scheduleShowing', 'Push'); yaCounter34569400.reachGoal('scheduleShowing'); return true;"
                                       class="btn btn-default btn-lg"><span
		                                    class="glyphicon glyphicon-calendar"></span> <?=i18n('shop|btn_schedule_showing')?></a>
                                </div>
	                        <?php } ?>

	                            <?php if ($q['landing']) {?>
	                        <div class="col-md-6">
		                        <a href="<?=get_url('landing',$q)?>"
		                           class="btn btn-default btn-lg"><span
				                        class="icon icon_landing"></span> Landing Page</a>
	                        </div>
	                        <?php } ?>

                                <?if(@$q['booklet']){
                                    $booklet_url = "files/shop_products/{$q['id']}/booklet/{$q['booklet']}";
                                    if ($q['landing']) echo '</div><div class="row">';
                                    ?>
                                    <?if(is_file(ROOT_DIR.$booklet_url)){?>
                                        <div class="col-md-6">
                                            <a href="/<?=$booklet_url?>" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-save"></span> Flor Plan</a>
                                        </div>
                                    <?}?>
                                <?}?>
                        </div>
	                </div>
                </div>
                <div class="col-md-5" <?php /*itemprop="offers" itemscope itemtype="http://schema.org/Offer" itemid="#offer"*/?>>
                    <div class="info">
                        <h2 style="font-size: 16px;"><?=$h2?></h2>
	                    <?php if (access('user admin')) {?>
                            <div style="text-align:right; float:right">
		                    <a class="btn btn-warning" href="?mailchimp=1">Mailchimp</a>
                            <br><a style="margin:5px 0" class="btn btn-warning" href="?to_rss=1">Отправить в соцсети</a>
                                <div style="font-size: 10px">rss <?=$q['date_rss']?></div>
                            </div>
                        <?php } ?>
                        <?php if ($q['price'] > 0) { ?>
                            <div class="price">
                                <?= i18n('shop|currency', true) ?><span <?php /*itemprop="price" content="<?=$q['price']?>"*/?>><?= number_format($q['price'],0,'.',',')?></span>
                            </div>
	                    <?php /*<meta itemprop="priceCurrency" content="USD"> */?>
                            <a class="btn btn-warning" data-toggle="modal" data-target="#sendRequest" data-trigger="<?=$q['name']?> <?=((@$q['mln']) ? "(mls# {$q['mln']})" : '') ?>">Send Request</a>

                        <?php } ?>

                        <div class="price_box">
							<?=html_array('shop/product_parameters',$q);?>
                        </div>
                    </div>
                </div>
            </div>

            <? /*if(@$q['text']){?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="h3"><span>Description</span></div>
                        <div class="text">
                             <div<?= editable('shop_products|text|' . $q['id'], 'editable_text', 'text') ?>><?= $q['text'] ?></div>
                        </div>
                    </div>
                </div>
            <?} */?>

	        <?php
	        //$q['3d'] = 'dsfvg';
	        if ($q['base']!=5) {?>




			<?php
	        $store_info = $q['store_info'] ? unserialize($q['store_info']) : array();
			$similar1 = '';
		    $similar1 = $similar2 = '';
		       // echo $q['base'].'-'.$q['property_type'];
			//fl
			if (
				(in_array($q['base'],array(1,2,8)))
			) {
				if (in_array($q['property_type'],array(4,5)))
					$config['_similar_no_beds'] = 1;
			}
			//ny
			else {
				if (in_array($q['property_type'],array(2,3)))
					$config['_similar_no_beds'] = 1;
			}
			for ($n=0; $n<=5; $n++) {
				$where = $n<5 ? ' beds = '.$n : ' beds>='.$n;
				//[4.776] Ошибки
				if ($q['zip_code'] AND $str = html_query('shop/similar', "
					SELECT
					    id,
                        name,
                        residential,
                        city,
                        neighborhood,
                        property_type,
                        property_type2,
                        date_change,
                        mln,
                        status,
                        price,
                        beds,
                        baths,
                        square,
                        zip_code,
                        img,
                        base,
                        office_name,
                        oh_date,
                        oh_dates,
                        url,
                        date_add 
					FROM shop_products
					WHERE
					    address = '" . mysql_res($q['address']) . "'
					    AND display=1 AND hide<2 AND square>0 AND residential=".$q['residential']."
                        AND $where
                        AND property_type=".$q['property_type']."
                        AND property_type2='".mysql_res($q['property_type2'])."'
                        AND zip_code=" . $q['zip_code'] . "
                        AND id!=" . $q['id'] . "
					ORDER BY special DESC, price
					LIMIT 20
				", '')) {
					$similar1.= '<div class="page-header text-center">';
					if (($u[1]!=$modules['shop'] AND $q['property_type']==2)) {
						$similar1 .= 'Commercial';
					}
					elseif (($u[1]==$modules['shop'] AND $q['property_type']==4)) {
						$similar1 .= 'Commercial';
					}
					else {
						if ($n == 0) $similar1 .= 'Studio';
						//[4.700] Убрать слово Condos
						elseif ($n < 5) $similar1 .= $n . ' Bedrooms';
						else $similar1 .= $n . ' and more Bedrooms';
					}
					$similar1.= '</div>';
					$similar1.= $str;
				}
			}
			//[4.776] Ошибки
			if ($q['zip_code']) {
				$similar2 = html_query('shop/similar',"
					SELECT 
					 id,
                        name,
                        residential,
                        city,
                        neighborhood,
                        property_type,
                        property_type2,
                        date_change,
                        mln,
                        status,
                        price,
                        beds,
                        baths,
                        square,
                        zip_code,
                        img,
                        base,
                        office_name,
                        oh_date,
                        oh_dates,
                        url,
                        date_add 
					 FROM shop_products
					WHERE zip_code=".$q['zip_code']." AND display=1 AND hide<2 AND square>0 AND residential=".$q['residential']." 
					AND beds = ".$q['beds']." AND baths = ".$q['baths']." AND id!=".$q['id']."
					AND property_type=".$q['property_type']." AND property_type2='".mysql_res($q['property_type2'])."'
					ORDER BY special DESC, price
					LIMIT 20
				",'');
	        }
			if($similar1 OR $similar2 OR $store_info) {
				$active = 0;
				if ($store_info) $active = 1;
				elseif($similar1) $active = 2;
				else $active = 3;
				?>
			<div class="hidden-xs" role="tabpanel" style="padding-bottom:15px">
				<ul class="nav nav-tabs" role="tablist">
					<?php if ($store_info) {?>
					<li role="presentation" <?=$active==1?'class="active"':''?>><a href="#store_info" aria-controls="map" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-list-alt"></span> Apt. & Store Information</a></li>
					<?php } if ($similar1) { ?>
					<li role="presentation" <?=$active==2?'class="active"':''?>><a href="#similar1" aria-controls="map" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-list-alt"></span> Simmilar Offers in this building</a></li>
					<?php } if ($similar2) { ?>
					<li role="presentation" <?=$active==3?'class="active"':''?>><a href="#similar2" aria-controls="map" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-list-alt"></span> Simmilar Offers in Neighborhood</a></li>
					<?php } ?>
				</ul>
			</div>
			<div class="row hidden-xs">
				<div class="col-md-12">
					<div class="tab-content">
						<?php if ($store_info) { ?>
						<div class="tab-pane<?=$active==1?' active':''?>" role="tabpanel" id="store_info"><?=html_array('shop/store_info',$store_info)?></div>
		                <?php } if ($similar1) { ?>
						<div class="tab-pane<?=$active==2?' active':''?>" role="tabpanel" id="similar1"><?=$similar1?></div>
						<?php } if ($similar2) { ?>
						<div class="tab-pane<?=$active==3?' active':''?>" role="tabpanel" id="similar2"><?=$similar2?></div>
						<?php } ?>
					</div>
				</div>
			</div>
				<?php
			}
			?>

		        <div class="row">
			        <div class="col-md-12">
				        <div role="tabpanel">
					        <!-- Nav tabs -->
					        <ul class="nav nav-tabs" role="tablist">
						        <?if(@$q['3d']){?>
							        <li role="3d" class="active"><a href="#3d" aria-controls="3d" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-facetime-video"></span> 3D Virtual Tour</a></li>
						        <?}?>
						        <li role="presentation"  <?=@$q['3d']?'':'class="active"'?>><a href="#map" aria-controls="map" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-map-marker"></span> Map location</a></li>
						        <!--
				<li role="presentation"><a href="#street" aria-controls="street" role="tab" data-toggle="tab">Street view</a></li>
				Большебрусянское
				<li role="presentation"><a href="#schools" aria-controls="schools" role="tab" data-toggle="tab">Schools</a></li>
				<li role="presentation"><a href="#walkscore" aria-controls="walkscore" role="tab" data-toggle="tab">Walk Score</a></li>
				<li role="presentation"><a href="#calculator" aria-controls="calculator" role="tab" data-toggle="tab">Mortgage Calculator</a></li>
				-->
						        <?if(@$q['video']){?>
							        <li role="presentation"><a href="#video" aria-controls="video" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-facetime-video"></span> Video</a></li>
						        <?}?>

					        </ul>
					        <!-- Tab panes -->
					        <div class="tab-content">
						        <?if(@$q['3d']){
							        ?>
							        <div role="tabpanel" class="tab-pane fade in active" id="3d">
								        <iframe width="640" height="640" style="margin:auto" src="<?=$q['3d']?>" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" onmousewheel=""></iframe>
							        </div>
							        <script type="text/javascript">
								        document.addEventListener("DOMContentLoaded", function () {
									        //alert(1);
									        function d() {
										        var width = $('#3d').width();
										        var height = width/4*3;
										        $('#3d iframe').width(width).height(height);
										        //alert(width);
									        }
									        d();
									        $( window ).resize(function() {
										        d();
									        });
								        });
							        </script>
						        <?}?>
						        <div role="tabpanel" class="tab-pane fade <?=@$q['3d']?'':'in active'?>" id="map">
							        <?=html_array('common/widgets/map',$q)?>
						        </div>
						        <div role="tabpanel" class="tab-pane fade" id="street">
							        <div id="map_street_view" style="width: 100%; height: 500px;"></div>
						        </div>
						        <div role="tabpanel" class="tab-pane fade" id="schools">
							        ...
						        </div>
						        <div role="tabpanel" class="tab-pane fade" id="walkscore">
							        ...
						        </div>
						        <div role="tabpanel" class="tab-pane fade" id="calculator">
							        <?//=html_array('common/widgets/calc', array('price'=>$q['price']))?>
						        </div>
						        <?if(@$q['video']){
							        $video_url = YouTubeURLs($q['video']);
							        ?>
							        <div role="tabpanel" class="tab-pane fade" id="video">
								        <?//var_dump($video_url)?>
								        <iframe style="width: 100%; height: 420px; margin: 0 auto; display: inline-block;" data-src="//www.youtube.com/embed/<?=$video_url?>" src="//www.youtube.com/embed/<?=$video_url?>" frameborder="0" allowfullscreen></iframe>
							        </div>
						        <?}?>

					        </div>
				        </div>
			        </div>

		        </div>

	        <?php } ?>

	        <div class="row">
		        <div class="col-md-12">
			        <div class="h4 social-header">Share and discuss with your friends!</div>
		        </div>
	        </div>
	        <?
	        $share_url = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
	        $share_text = $q['name'];
	        ?>
	        <div class="row">
		        <div class="col-md-2"></div>
		        <div class="col-md-2">
			        <a style="display:block" target="_blank" href="http://www.facebook.com/sharer.php?u=<?=$share_url?>" class="btn btn-default btn-lg btn-fb">Facebook</a>
		        </div>
		        <div class="col-md-2">
			        <a style="display:block" target="_blank" href="https://www.linkedin.com/cws/share?url=<?=$share_url?>" class="btn btn-default btn-lg btn-li">LinkedIn</a>
		        </div>
		        <div class="col-md-2">
			        <a style="display:block" target="_blank" href="http://twitter.com/share?url=<?=$share_url?>&text=<?=$share_text?>&via=wilkre" class="btn btn-default btn-lg btn-tw">Twitter</a>
		        </div>

                <div class="col-md-2">
                    <a style="display:block" target="_blank" href="fb-messenger://share/?link=<?=$share_url?>" class="btn btn-default btn-lg btn-li">Messenger</a>
                </div>
                <?php /* *?>
                <div class="col-md-2">
                    <a style="display:block" target="_blank" href="http://t.me/share/url?url=<?=$share_url?>" class="btn btn-default btn-lg btn-tw">Telegram</a>
                </div>
                <?php /*
		        <div class="col-md-2">
			        <a style="display:block" target="_blank" href="http://plus.google.com/share?url=<?=$share_url?>" class="btn btn-default btn-lg btn-gl">Google+</a>
		        </div>
 */?>
		        <div class="col-md-2"></div>
	        </div>

			<?php if ($q['office_name']) {?>
	        <div class="row">
	            <div class="col-md-12 courtesy"><br><br>
                    Listing courtesy of <?=$q['office_name']?>
                    | MLS: <?=$q['mln']?>
                </div>
		    </div>
	        <?php } ?>



            <?php /*if($similar){?>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <tr>
                                <td>Unit</td>
                                <td class="text-center">Beds/Bath</td>
                                <td class="text-center">Living Area, SqFt.</td>
                                <td class="text-center">Status</td>
                                <td class="text-right">Price</td>
                                <td class="text-right">List Date</td>
                            </tr>
                            <?php foreach ($similar as $sp){
                                ?>
                                <tr>
                                    <td><?=$sp['name']?></td>                                    
                                    <td class="text-center"><?=$sp['beds']?>/<?=$sp['baths']?></td> 
                                    <td class="text-center"><?=$sp['living_area']?></td>        
                                    <td class="text-right"><?=$config['object_statuses'][$sp['status']]?></td>                                    
                                    <td class="text-right"><?=$sp['price']?></td>                                    
                                    <td class="text-right"><?=$sp['date']?></td>                                    
                                </tr>
                            <?}?>
                        </table>
                    </div>
                </div>
            <?} */ ?>
        </div>    
    </div>
</div>

<?php
if ($q['base']==5 AND $q['neighborhood']>0) {
	$query = "
	SELECT sp.id,
					sp.name,
					sp.residential,
					sp.city,
					sp.neighborhood,
					sp.property_type,
					sp.property_type2,
					sp.date_change,
					sp.mln,
					sp.status,
					sp.price,
					sp.beds,
					sp.baths,
					sp.square,
					sp.zip_code,
					sp.img,
					sp.base,
					sp.office_name,
					sp.oh_date,
					sp.oh_dates,
					sp.url,
					sp.date_add
	FROM shop_products AS sp
	WHERE sp.display = 1 AND sp.neighborhood=".$q['neighborhood']." AND base IN (3,4)
	ORDER BY sp.special DESC, sp.date_change DESC
	LIMIT 12
	";
	//echo $query;
	echo '<br><h3>Unfortunately the considered property is sold. But you can choose from the directory below.</h3><br>';
	if ($content =  html_query('shop/product_list', $query, '')) {
		echo $content;
	}
	else {
		$query = "
			SELECT sp.id,
					sp.name,
					sp.residential,
					sp.city,
					sp.neighborhood,
					sp.property_type,
					sp.property_type2,
					sp.date_change,
					sp.mln,
					sp.status,
					sp.price,
					sp.beds,
					sp.baths,
					sp.square,
					sp.zip_code,
					sp.img,
					sp.base,
					sp.office_name,
					sp.oh_date,
					sp.oh_dates,
					sp.url,
					sp.date_add
			FROM shop_products AS sp
			WHERE sp.display = 1 AND base IN (3,4)
			ORDER BY sp.special DESC, sp.date_change DESC
			LIMIT 12
		";
		$content =  html_query('shop/product_list', $query, '');
		echo $content;
	}
}
?>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function () {
        $('body').on('click', '#video .close',function(){
            var src = $(this).closest(".modal-dialog").find("iframe").attr("src");
            $(this).closest(".modal-dialog").find("iframe").attr("src","").attr("src",src);
        });
    });
</script>

<?php /*
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function () {
	$('.shop_product_text .gallary .carousel').hover(
		function () {
			$('.shop_product_text .carousel .next, .shop_product_text .carousel .prev').show().css('display','block');
		},
		function () {
			$('.shop_product_text .carousel .next, .shop_product_text .carousel .prev').hide();
		}
	);
	$('.shop_product_text .gallary .next,.shop_product_text .gallary .prev').click(function(){
		var left = parseInt($('.shop_product_text .gallary ul').css('margin-left'));
		var width = parseInt($('.shop_product_text .gallary ul').width());
		var margin = 121;
		//alert (width+' '+left);
		if ($(this).hasClass('next')) {
			if (width+left<400) return false;
			left = left-margin;
		} else {
			if (left>=0) return false;
			left = left+margin;
		}
		$('.shop_product_text .gallary ul').animate({marginLeft:left+'px'},500);
		return false;
	});
});
</script> */?>
