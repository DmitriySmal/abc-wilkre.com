<?php

//добавлять зип код в титл страницы
if (!isset($config['_zip_code'])) {
	if ($q['zip_code']) {
		$config['_zip_code'] = $q['zip_code'];
	}
}
if ($q['office_name']=='') {
    //если для своих объектов не указало то ставим вилк
    if (in_array($q['base'],array(1,7))) {
	    $q['office_name'] = 'Wilk Real Estate I LLC.';
    }
}

//$base = in_array($q['base'],array(1,2)) ? 2 :1;

//????
/*
$q['category_name'] = @$config['shop_categories'][$q['category']]['name'];
$q['category_url']	= $config['shop_categories'][$q['category']]['url'];
$q['city_url']	= $config['shop_cities'][$q['city']]['url'];
*/
$q['city_name'] = @$config['shop_cities'][$q['city']]['name'];
if (!isset($q['neighborhood_name'])) {
    $neighborhood = get_data('shop_neighborhoods',$q['neighborhood']);
	$q['neighborhood_name'] = $neighborhood['name'];
}

$img = $q['img'] ? '/files/shop_products/'.$q['id'].'/img/m-'.$q['img'] : '/templates/images/no_img.jpg';
if ($q['img'] AND is_file(ROOT_DIR . $img)==false) {
	//на повторный парсинг
	//[4.686] Во Флориде много объектов загружается без картинок
	//[4.714] нет картинок
	if (in_array($q['base'],array(2,6))) {
		mysql_fn('update', 'shop_products', array('id' => $q['id'], 'date' => '', 'date_photo' => ''));
	}
	$img = '/templates/images/no_img.jpg';
}
//$img = '/templates/images/product-empty.jpg';

//$url = '/'.$modules['shop'].'/'.$q['category'].'-'.$q['category_url'].'/'.$q['id'].'-'.$q['url'].'/';
$url = get_url('product',$q);

$status = object_status ($q);

$BathsHalf = '';
//todo
/*
$parameters = $q['parameters'] ? unserialize($q['parameters']) : false;
if ($q['base']==3 AND isset($parameters['BathsHalf'])) $BathsHalf = $parameters['BathsHalf'];
if ($q['base']==2 AND isset($parameters['109'])) $BathsHalf = $parameters['109'];
if (in_array($q['base'],array(1,4)) AND isset($parameters['23'])) $BathsHalf = $parameters['23'];
*/

//[4.800] В каталоге сделать полный адрес
/*$names = array();
$names[] = $q['name'];
if ($q['neighborhood_name']) $names[] = $q['neighborhood_name'];
if ($q['base']!=9 AND $q['city_name']) $names[] = $q['city_name'];
$names[] = $config['object_base_state'][$q['base']] . ' ' . $q['zip_code'];
$name = implode(', ',$names);*/
$name = object_name ($q);


$title = implode(', ', array(
	$q['name'],
	//$q['neighborhood_name'],
	$q['city_name'],
	$config['object_base_state'][$q['base']].' '.$q['zip_code']
));
$alt = $title;
$title = $name;
//$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
//$alt = $q['img'] ? 'p-'.$q['img'] : i18n('common|wrd_no_photo');

?>
<?if (($i==1)){?>
    <div class="row">
<?}

$data2 = '';
if (access('user admin')) {
	$data2 = ' data-property_type="' . $q['property_type'] . '"';
	if ($q['base']==9) $data2.= ' data-property_type2="' . $q['property_type2'] . '"';
	$data2.= ' data-rank="'.$q['rank'].'"';
	$data2.= ' data-change="' . $q['date_change'] . '"';
	$data2.= ' data-add="' . $q['date_add'] . '"';
	//$data2.= ' data-date="' . $q['date'] . '"';
	$data2.= ' data-base="' . $q['base'] . '"';
	$data2.= ' data-img="'.$q['img'].'"';
	$data2.= ' data-mln="'.$q['mln'].'"';
	$data2.= ' data-status="'.$q['status'].'"';
	$data2.= ' data-uid="'.$q['uid'].'"';
	$data2.= ' data-special="'.$q['special'].'"';
}
?>
<div class="col-md-4"<?=$data2?>>
	<div class="shop-product-list">
		<?php if ($status) {?>
		<div class="status <?=$status?>"><?=$status=='Contract'?'IN ':''?><?=$status?></div>
		<?php } ?>
		<div class="img">
			<a href="<?=$url?>"><img src="<?=$img?>" alt="<?=$title?>" /></a>
		</div>
		<div class="info">
			<a class="name" href="<?=$url?>" title="<?=$title?>"><?=$name?></a>
			<?php if ($q['price']>0) {?>
			<div class="price">
				<?=i18n('shop|currency',true)?><span><?=  number_format($q['price'],0,'.',',')?></span>
			</div>
			<?php } ?>
			<div class="text">
				<?php
				$params = array();
				if ($q['beds']) $params[] = $q['beds'].' Beds';
				if (@$q['baths']) $params[] =  ((float)$q['baths']).' Baths';
				if ($BathsHalf) $params[] = $BathsHalf.' Half baths';
				if (@$q['square']) $params[] = $q['square'].' Sq. Ft.';
				if (@$q['mln']) $params[] = "MLS: ".$q['mln'];
				else
				/*if ($q['property_type2']) {
					$params[] = trim(str_replace('"',' ',$q['property_type2']));
				}
				else {
					if ($base==1) $params[] = $config['ny_property_types'][$q['property_type']];
					else $params[] = $config['object_groups'][$q['property_type']]['name'];
				}*/
                $params[] = random_property_type ($q);
                if ($q['zip_code']) {
                    $params[] = 'Zip&nbsp;'.$q['zip_code'];
                }
				//if (access('user admin')) $params[] = $q['rank'];

				?>
				<?=$params?implode('&nbsp;/ ',$params):''?>
				<?php
				//if (acces('user admin'))
				?>
			</div>
			<div class="courtesy">Listing courtesy of <?=$q['office_name']?></div>
		</div>
		<?php /*if ($u[2]==$modules['open_houses']) {*/
		if ($q['oh_date']>=date('Y-m-d')) {
			$oh = unserialize($q['oh_dates']);
			$n = 0;
			?>
		<div class="open_house">
			Open House:
			<?php foreach ($oh as $k=>$v) if (strtotime($v['date'])>=strtotime($config['date']) AND $n<2) { $n++?>
			<br><?=date2($v['date'],'%a, %m/%d/%Y')?> &nbsp; <?=date_time($v['from'])?> &ndash; <?=date_time($v['to'])?>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</div>

<?if (($i==$num_rows) || (!fmod($i,3))){?>
    </div>
<?}?>

<?if (($i!=$num_rows) && (!fmod($i,3))){?>
    <div class="row">
<?}?>