<?php if ($i==1) {
	//[4.535] Срочно сделать
	$price = 'Price';
	if ($q['residential']==1 AND in_array($q['base'],array(3,4)) AND in_array($q['property_type'],array(2,3))) {
		$price = 	'Rent';
	}

?>
<table class="table table-striped">
	<thead>
		<tr>
			<td></td>
			<td>Address</td>
			<td class="text-right"><?=$price?></td>
			<?php if (@$config['_similar_no_beds'] == false) {?>
			<td class="text-center">Beds/Bath</td>
			<?php } ?>
			<td class="text-right">SqFt</td>
			<td class="text-right">Price/SqFt</td>
			<td class="text-right">Zip</td>
		</tr>
	</thead>
	<tbody>
<?php }
$img = $q['img'] ? '/files/shop_products/'.$q['id'].'/img/a-'.$q['img'] : '/templates/images/no_img.jpg';
if ($q['img'] AND is_file(ROOT_DIR . $img)==false) {
	//отключаем объект и на повторный парсинг
	//mysql_fn('update','shop_products',array('id'=>$q['id'],'display'=>0,'date'=>date('Y-m-d H:i:s')));
	$img = '/templates/images/no_img.jpg';
}
$url = get_url('product',$q);
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$status = object_status ($q);
?>
		<tr>
			<td>
				<?php if ($q['img']) { ?><a title="<?= $title ?>" onclick="return hs.expand(this, {slideshowGroup: 'group2'})" href="/files/shop_products/<?= $q['id'] ?>/img/<?= $q['img'] ?>"><?php } ?>
				<img style="width:50px" src="<?=$img?>" alt="<?=$title?>">
				<?php if ($q['img']) { ?></a><?php } ?>
			</td>
			<td>
				<?php
				//[4.625] Статусы New, Sold, Leased и т.д. должны отображаться на карточке товара
				$status = object_status ($q);
				if ($status) {?>
					<div class="status <?=$status?>"><?=$status=='Contract'?'IN ':''?><?=$status?></div>
				<?php }
				?>
				<a href="<?=$url?>"><?=$q['name']?></a>
			</td>
			<td class="text-right">$<?=$q['price']?></td>
			<?php if (@$config['_similar_no_beds'] == false) {?>
			<td class="text-center"><?=$q['beds']?>/<?=(float)$q['baths']?></td>
			<?php } ?>
			<td class="text-right"><?=$q['square']?></td>
			<td class="text-right">$<?=number_format($q['price']/$q['square'],0,'.',',')?></td>
			<td class="text-right"><a href="<?=get_url('shop',$q)?>?quick=<?=$q['zip_code']?>"><?=$q['zip_code']?></a></td>
		</tr>
<?php if ($num_rows==$i) { ?>
	<tbody>
</table>
<?php } ?>