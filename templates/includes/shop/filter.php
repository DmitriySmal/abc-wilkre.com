<?
$html['is_map'] = (@$_GET['view']=='map') ? true : false;
//$html['is_map'] = ((@$_GET['view']=='map') || (isset($_GET['location']) && count($_GET['location']))) ? true : false;
$is_map = $html['is_map'];
$per_page = array(12,24,48,'all');
$sort = array(
    'date-desc' => i18n('shop|sort_date_desc'),
    'date-asc' => i18n('shop|sort_date_asc'),
    'price-desc' => i18n('shop|sort_price_desc'),
    'price-asc' => i18n('shop|sort_price_asc'),
);
$get = $_GET;
unset($get['u'],$get['sort']);
$bquery = http_build_query($get);
$url = '/' . $u[1] . '/' . (($bquery) ? '?'.$bquery.'&' : '?');
?>
<div class="page-filter">
    <div class="row">    
        <div class="col-sm-4 col-xs-6">
            <div class="control-group">
	            <?php if ($is_map==false) {?>
	            <label for="sorted">Sorted by price <a href="<?=$url?>sort=price-asc" class="glyphicon glyphicon-arrow-up"></a> <a href="<?=$url?>sort=price-desc" class="glyphicon glyphicon-arrow-down"></a></label>
	            <?php }
	            /*
                <select name="sort" <?//=($is_map) ? 'disabled class="disabled"' :''?> >
                    <option value="0">Preferred Listings</option>
                    <?foreach($sort as $k=>$v){?>
                        <option value="<?=$k?>" <?=(@$_GET['sort'] == $k) ? 'selected' : ''?>><?=$v?></option>
                    <?}?>
                </select>
                */ ?>
            </div>
        </div>
        <div class="col-sm-8 col-xs-6 text-right">
	        <?php if ($is_map==false) {?>
            <div class="hidden-xs per-page <?=($is_map) ? 'disabled' :''?>">
                <span>Per page</span>
                <ul><?
                        $get = $_GET;
                        unset($get['u'],$get['count'],$get['n']);
                        $bquery = http_build_query($get);
                    foreach($per_page as $v){
                        $url = '/' . $u[1] . '/' . (($bquery) ? '?'.$bquery.'&' : '?').'count='.$v;
                        ?><li class="<?=(($v == @$_GET['count']) || (($v == reset($per_page)) && !@$_GET['count']) ) ? 'active' : '' ?>"><a href="<?=$url?>"><?=$v?></a></li><?
                    }
	                ?></ul>
            </div>
	        <?php } ?>
	        <?php
	        $get = $_GET;
	        unset($get['u'],$get['n'],$get['view']);
	        $url = http_build_query($get);
	        ?>
            <a href="/<?=$u[1]?>/<?=$url?'?'.$url:''?>" class="<?=(!$is_map) ? 'active' :''?> btn btn-light-gray-bordered"><span class="hidden-xs">Photo view </span><span class="glyphicon glyphicon-th" aria-hidden="true"></span></a>
	        <?php if ($url) $url = $url.'&amp;'; ?>
	        <a id="map_view" href="?<?=$url?>view=map" class="<?=($is_map) ? 'active' :''?> btn btn-light-gray-bordered"><span class="hidden-xs">Map view </span><span class="glyphicon glyphicon-globe" aria-hidden="true"></span></a>
        </div>
    </div>
</div>

<?php /*
<div id="map_view_content" style="<?=(($is_map) ? 'display: block' : '')?>">
    <?=( isset($_GET['location']) && (count($_GET['location']) == 1) ) ? html_array('shop/map_view_objects', array('nb_id'=>$_GET['location'][0])) : html_array('shop/map_view');?>
</div>
 */
?>

<script>
    function ready_shop_filter (){
        /*$('select[name=sort]').on('change', function(){
            var parsedUrl = $.url(window.location.href);
            var params = parsedUrl.param();
            params["sort"] = $(this).val();
            var newUrl = "?" + $.param(params);
            window.location.href = newUrl;
        });*/
        
        /*$("#map_view").on('click', function(){
            $(this).closest('div').find('.btn-light-gray-bordered').toggleClass('active');
            //$(this).toggleClass('active');
            $("#map_view_content").slideToggle(500,function(){
                //map.refresh();
                //console.log(map);
                google.maps.event.trigger(map, 'resize');
            });            
            return false;
        });*/
        
    }
    document.addEventListener("DOMContentLoaded", ready_shop_filter);
</script>