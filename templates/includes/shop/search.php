<?php
global $html;

if (isset($page['base'])) {
    $base = $page['base'];
	$state = in_array($base,array(6,7)) ? 1:2;
}
//флорида по умолчанию
else {
    $base = 9;
	$state= 2;
}
if ($html['is_product']) {

}
if (access('user admin')) {
    //echo $base.'-'.$state;
}

$where = "";
//if ($u[1]==@$modules['shop_flex']) $where = " AND id = 90";
//if ($u[1]==$modules['shop']) $where = " AND id!= 90";
//флорида
if ($state==2) $where = " AND id=140";
$cities = mysql_select("
	SELECT id,name,url2 FROM shop_cities WHERE state=".$state." $where AND display=1 ORDER BY rank DESC
",'rows_id');
if ($state==2) {
	$property_types = array();
	foreach ($config['object_groups'] as $k => $v) {
		$property_types[$k] = $v['name'];
	}
}
else {
	$property_types = $config['ny_property_types'];
}
//print_r($property_types);

$display1 = '';
$display2 = ' style="display:none"';
//если есть параметры полного поиска
if (@$q['location'] OR @$q['residential'] OR @$q['property_type'] OR @$q['beds'] OR @$q['baths'] OR @$q['price_min'] OR @$q['price_max']) {
	$display1 = $display2;
	$display2 = '';
}

$action = get_url('shop',$page);

?>
<form id="search-offers" class="clear_form" action="<?=$action?>" method="get" style="<?=((@$html['is_product']) ? 'display: none;' :'')?>">
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<label for="location">For Sale / For Rent:</label>
				<select class="form-control" id="residential" name="residential">
					<?=select(@$q['residential'],$config['object_residential'],'Select')?>
				</select>
			</div>
		</div>

		<?php if ($state==1)	{
			?>
		<div class="col-md-2">
			<div class="form-group">
				<label for="location">City:</label>
				<select class="form-control" id="city" name="city">
					<?=select(@$q['city'],$cities,'Select')?>
				</select>
			</div>
			<script type="text/javascript">
				function ready_shop_search4 () {
					$('#city').change(function(){
						$( "#quicksearch" ).autocomplete( "destroy" );
						var city = $(this).val();
						$.get('/ajax.php', {'file':'ny_location','city':city},function(data){
							$('#location_box').html(data);
						});
					});
				}
				document.addEventListener("DOMContentLoaded", ready_shop_search4);
			</script>
		</div>
		<?php } ?>

		<div class="col-md-2">
			<div class="form-group">
				<label>Real Estate Types:</label>
				<select class="form-control" id="property_type" name="property_type">
					<?=select_property_type1($base,@$q['residential'],@$q['property_type'])?>
				</select>
			</div>
		</div>
		<div class="form-group col-md-<?=$state==1?'4':'6'?>">
			<label>Enter name of property or address or zip or MLS number or neighborhoods:</label>
			<input id="quicksearch" class="form-control" name="quick" placeholder="" value="<?=@$q['quick']?>">
		</div>
		<div class="col-md-2">
			<div class="form-group" id="quick_search" <?=$display1?>>
				<label class="hidden-xs">&nbsp</label>
				<button style="display: block; width: 100%;" class="btn btn-warning">QUICK SEARCH</button>
			</div>
		</div>
		<div class="col-md-12" style="margin-bottom:15px"><a href="#" id="open_all" <?=$display1?>>+ Open detailed property search</a></div>
	</div>

	<div id="all_search" <?=$display2?>>
		<div class="row">
			<div class="form-group col-md-6" id="location_box">
				<?
				$keys = array_keys($cities);
				if (@$q['city'] AND array_key_exists($q['city'],$cities)) {
					$keys = array($q['city']);
				}
				$nbs = mysql_select("SELECT * FROM shop_neighborhoods WHERE id>0 AND city IN ('".implode("','",$keys)."') AND display=1 GROUP BY name ORDER BY rank DESC, name ASC",'rows');
				?>
				<label for="location">Location:</label>
				<script type="text/javascript">
					function ready_shop_search3 () {
						$( "#quicksearch" ).autocomplete({
						source: [
						<?php if ($nbs) foreach($nbs as $k=>$v){?>
							'<?=addslashes($v['name'])?>',
							<?
							if ($v['name2'] AND $v['name2']!=$v['name']) echo "'".addslashes($v['name2'])."',";
						}?>
						],
						minLength: 0
						});
						$( "#location" ).change(function(){
						$( "#quicksearch").val('');
						});
						$('.chosen-select').chosen();
						$('.chosen-select-deselect').chosen({ allow_single_deselect: true });
					}
					document.addEventListener("DOMContentLoaded", ready_shop_search3);
				</script>
				<style>.chosen-container {display:block; min-width:100%}</style>
				<select size="1" class="form-control chosen-select" data-placeholder="Select Location" id="location" name="location[]" multiple="multiple">
					<?php if ($nbs) foreach($nbs as $k=>$v) if ($v['display']==1){?>
					<option value="<?=$v['id']?>" <?=(@$q['location'] AND @in_array($v['id'], @$q['location'])) ? 'selected' :''?>><?=$v['name']?></option>
					<?}?>
				</select>
			</div>
			<?php
			$property_type2 = select_property_type($base,@$q['residential'],@$q['property_type'],@$q['property_type2'])
            ?>
			<div class="col-md-3" id="property_type2" <?=$property_type2?'':'style="display:none2"'?> >
				<label>Property Types:</label>
				<select class="form-control" name="property_type2"><?=$property_type2?></select>
				<script>
				function ready_shop_search2 () {
					//цены
					$( "#price_max,#price_min" ).autocomplete({
						source: function(request,response) {
							var residential = $('#residential').val(),
								str = request.term.replace(/[^0-9]/g, ""),
								arr = [],
								count = residential == 1 ? 5:9;
							if (str.length) {
								for (var i = 1; i < count - str.length; i++) {
									var s = '';
									for (var n = 0; n < i; n++) {
										s = s + '0';
									}
									arr.push('$' + number_format(str + s, '', 0,','));
								}
							}
							response(arr);
						},
						minLength: 1
					});
					//подтипы
					$(document).on("change", '#property_type', function () {
						var property_type = $(this).val(),
							property_type2 = $("#property_type2 select").val(),
							residential = $("#residential").val();
						//return true;
						//alert(property_type2);
						$('#property_type2').hide();
						if (property_type>0) {
							$.get(
								'/ajax.php?file=property_type',
								{'residential': residential, 'property_type': property_type, 'property_type2': property_type2, 'base': <?=$base?>},
								function (data) {
									if (data) {
										$('#property_type2').show().find('select').html(data);
									}
									else $('#property_type2').find('select').html('');
								}
							);
						}
						else $('#property_type2').find('select').html('');
						//прячем Baths и Bedrooms
						<?php if ($state==1) {?>
						if ($(this).val() == 3) {
						<?php } else {?>
						if ($(this).val() == 4 || $(this).val() == 5) {
						<?php } ?>
							$('#bedrooms,#baths').val('0').attr('disabled','disabled');
						}
						else {
							$('#bedrooms,#baths').removeAttr('disabled');
						}
					});
					//аренда-продажа
					$(document).on("change", '#residential', function () {
						var property_type = $('#property_type').val(),
							residential = $(this).val();
						$.get(
							'/ajax.php?file=property_type1',
							{'residential': residential, 'property_type': property_type, 'base': <?=$base?>},
							function (data) {
								$('#property_type').html(data);
							}
						);
						<?php /*if ($base==2) {?>
						var property_type = $('#property_type').val(),
							residential = $(this).val(),
							rent = [3,4],
							sale = [1,2,4,5,6];
						$('#property_type option').hide();
						$('#property_type').val(0);
						if(residential==0) {
							$('#property_type option').show();
							$('#property_type option[value=3]').hide();
							$('#property_type').val(property_type);
						}
						if(residential==1) {
							$.each(rent, function(index, value){
								$('#property_type option[value='+value+']').show();
								if (value==property_type) $('#property_type').val(value);
							});
						}
						if(residential==2) {
							$.each(sale, function(index, value){
								$('#property_type option[value='+value+']').show();
								if (value==property_type) $('#property_type').val(value);
							});
						}
						<?php } */?>
						$('#property_type').trigger('change');
					});
					//чтобы спрятать подтипы и типы
					$('#residential').trigger('change');
				}
				document.addEventListener("DOMContentLoaded", ready_shop_search2);
				</script>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-6 prices">
				<div class="row">
					<div class="form-group col-md-6">
						<label for="type">Min Price:</label>
						<input class="form-control"  name="price_min" id="price_min" value="<?=@$q['price_min']?'$'.number_format($q['price_min'],0,'.',','):''?>">
					</div>
					<div class="form-group col-md-6">
						<label for="type">Max Price:</label>
						<input class="form-control"  name="price_max" id="price_max" value="<?=@$q['price_max']?'$'.number_format($q['price_max'],0,'.',','):''?>">
					</div>
					<style>.prices .ui-menu-item {text-align:right}</style>
				</div>
			</div>
			<div class="form-group col-md-2">
				<label for="bedrooms">Bedrooms:</label>
				<select class="form-control" id="bedrooms" name="beds"><?=select(@$q['beds'],$config['bedrooms_list'],'No matter')?></select>
			</div>
			<div class="form-group col-md-2">
				<label for="baths">Baths:</label>
				<select class="form-control" id="baths" name="baths">
					<option value="0">No matter</option>
					<?foreach($config['baths_list'] as $k=>$v){?>
					<option value="<?=$k?>" <?=((@$q['baths'] == $k) ? 'selected' :'')?> ><?=$v?></option>
					<?}?>
				</select>
			</div>

			<div class="form-group col-md-2">
				<label style="display:block;">&nbsp;</label>
				<button
					onclick="ga('send', 'event', 'Button', 'Push'); yaCounter34569400.reachGoal('searchButton'); return true;"
					style="display: block; width: 100%;" class="btn btn-warning">SEARCH</button>
			</div>
		</div>
	</div>

	<?php
	if (@$html['is_product']!=true) {
		//[4.728] убрать пока ссылки внутренние ссылки
		/*
		if ($config['state']==2) {
			?>
			<div class="clearfix" style="line-height:25px; font-size:14px">
				<div style="font-size:16px; padding:0 0 5px">Users are also looking for:</div>
				<?php
				$qq = $q;
				unset($cities[8], $cities[10], $cities[12]); //удалили бронкс, манхеттен, other
				$neighborhoods = mysql_select("
			SELECT * FROM shop_neighborhoods
			WHERE display=1 AND seo=1 AND city IN (" . implode(',', array_keys($cities)) . ")
			ORDER BY id
		", 'rows_id');
				//print_r($neighborhoods);
				$qq['neighborhood'] = @$qq['location'][0];
				//print_r($qq);
				$neighborhood = false;
				if (array_key_exists($qq['neighborhood'], $neighborhoods)) {
					//echo 1;
					while ($neighborhood = current($neighborhoods)) {
						if ($neighborhood['id'] == $qq['neighborhood']) {
							$neighborhood = next($neighborhoods);
							break;
						} else next($neighborhoods);
					}
				}
				//$neighborhood = current($neighborhoods);
				//NY residential
				if ($base == 1) {
					$property_type2 = false;
					$qq['property_type2'] = str_replace(array('%2F', '&amp;'), array('/', '&'), $qq['property_type2']);
					if (in_array($qq['property_type2'], $config['ny_property_types_1_url'])) {
						//echo 1;
						while ($property_type2 = current($config['ny_property_types_1_url'])) {
							if ($property_type2 == $qq['property_type2']) {
								$property_type2 = next($config['ny_property_types_1_url']);
								break;
							} else next($config['ny_property_types_1_url']);
						}
					}

				}
				//echo $property_type2;
				$rent = 0;
				for ($n = 1; $n < 7; $n++) {
					$url = $name = array();

					//sale|rent
					if ($qq['residential'] == 1) $qq['residential'] = 2;
					else  $qq['residential'] = 1;
					$url[1] = $config['object_residential_url'][$qq['residential']];
					$name['residential'] = $config['object_residential'][$qq['residential']];
					$neighborhood = current($neighborhoods);
					if ($neighborhood == false) {
						reset($neighborhoods);
						$neighborhood = current($neighborhoods);
					}
					next($neighborhoods);
					//$url[] = $cities[$city['id']]['url2'];
					//$name[2] = ' in ';
					//neighborhoods
					$url[2] = $neighborhood['url2'];
					@$name['neighborhood'] .= $neighborhood['name'];
					if ($base == 1) {
						@$name['city'] .= ', ' . $cities[$neighborhood['city']]['name'];
						@$name['state'] .= ', NY';
					} else {
						@$name['state'] .= ', FL';
						@$name['city'] .= '';
					}

					//real type
					$qq['property_type']++;
					//NY
					if ($base == 1) {
						if ($qq['property_type'] > 3) $qq['property_type'] = 1;
						$url[3] = $config['ny_property_types'][$qq['property_type']];
						//$name[] = $config['ny_property_types'][$qq['property_type']];
						//sale residetnial
						if ($qq['property_type'] == 1 AND $qq['residential'] == 2) {
							if ($property_type2 == false) {
								reset($config['ny_property_types_1_url']);
								$property_type2 = current($config['ny_property_types_1_url']);
							} else $property_type2 = current($config['ny_property_types_1_url']);
							next($config['ny_property_types_1_url']);
							$url[3] .= '-' . array_search($property_type2, $config['ny_property_types_1_url']);
							if (in_array($property_type2, array('Condo', 'CoOp'))) {
								$name['property_type'] = $property_type2 . ' Apartments';
							} else {
								$random = random_hash(array(
									$property_type2
								));
								if ($property_type2 == 'Single Family') {
									$name['property_type'] = random_template('single family', $random) . ' ' . random_template('houses', $random);;
								} elseif ($property_type2 == 'Multi-Family') {
									$name['property_type'] = random_template('multi family', $random) . ' ' . random_template('houses', $random);
								} else $name['property_type'] = $property_type2 . ' ' . random_template('houses', $random);
								//$name['property_type'] .= $property_type2;
							}
						} else {
							$random = random_hash(array(
								$qq['property_type']
							));
							if ($qq['property_type'] == 1) {
								$name['property_type'] = $config['ny_property_types'][$qq['property_type']] . ' ' . random_template('houses', $random);
							} else $name['property_type'] = $config['ny_property_types'][$qq['property_type']] . ' ' . random_template('real estate', $random);
						}
					} else {
						//1 rent - 3 4
						//2 sale - 1 2 4 5 6
						$property_type = $qq['property_type'];
						if ($qq['property_type'] > 5) $qq['property_type'] = 1;
						if ($qq['residential'] == 1) {
							$rent++;
							if ($rent == 1) $property_type = 3;
							else {
								$rent = 0;
								$property_type = 4;
							}
						} else {
							$qq['property_type']++;
							if ($property_type == 3) $property_type = 4;
						}
						//$name[] = '-'.$qq['property_type'].'-';
						$url[3] = $config['object_groups'][$property_type]['url'];
						$name['property_type'] = $config['object_groups'][$property_type]['name'];
					}
					$random = random_hash($url);
					$key = random_template('4', $random);
					$link = $link = $name['property_type'] . ' ' . $name['residential'] . ' in ' . $name['neighborhood'] . $name['city'] . $name['state'];
					// 1. [Property Types/Real Estate type+ слово]+ [For Sale / For Rent]+in+ [район]+[город]+[NY]
					if ($key == 1) {
						$link = $name['property_type'] . ' ' . $name['residential'] . ' in ' . $name['neighborhood'] . $name['city'] . $name['state'];
					}
					//2. [Property Types/Real Estate type+ слово]+ [For Sale / For Rent]+ in [район]+[город]
					if ($key == 2) {
						$link = $name['property_type'] . ' ' . $name['residential'] . ' in ' . $name['neighborhood'] . $name['city'];
					}
					//3. [Property Types/Real Estate type+ слово]+ [For Sale / For Rent]+ in [район]+[NY]
					if ($key == 3) {
						$link = $name['property_type'] . ' ' . $name['residential'] . ' in ' . $name['neighborhood'] . $name['state'];
					}
					//4. [район]+[NY] + [Property Types/Real Estate type+ слово]+ [For Sale / For Rent]
					if ($key == 4) {
						$link = $name['neighborhood'] . $name['state'] . ' ' . $name['property_type'] . ' ' . $name['residential'];
					}
					//$link.= $key;
					mysql_fn('insert', 'seo_anckors', array(
						'name' => $link,
						'url' => '/' . $u[1] . '/' . $url[1] . '-' . $url[2] . '-' . $url[3] . '/'
					));
					?>
					<div style="float:left; padding:0 15px 0 0"><a
							href="/<?= $u[1] ?>/<?= $url[1] . '-' . $url[2] . '-' . $url[3] ?>/"><?= $link ?></a></div>
				<?php

				} ?>
			</div>
		<?php
		}
		//[4.602] Заменить имеющуюся перелинковку под поиском в Нью Йорке
		else {
			$rand = random_hash($_GET);
			$where = '';
			if (@$q['residential']) {
				$where = " AND residential=".intval($q['residential']);
			}
			if (@$q['location']) {
				$where.= " AND neighborhood IN ('".implode("','",$q['location'])."')";
			}
			if ($seo_shop = mysql_select("
				SELECT * FROM seo_shop
				WHERE state=".$config['state']." ".$where."
				ORDER BY RAND(".$rand.")
				LIMIT 6
			", 'rows')) {
				?>
				<div class="clearfix" style="line-height:25px; font-size:14px">
					<div style="font-size:16px; padding:0 0 5px">Users are also looking for:</div>
				<?php
				foreach ($seo_shop as $k=>$v) {
					$names = explode("\r\n", trim($v['names']));
					$name = random_template2($names, $rand)
					?>
					<div style="float:left; padding:0 15px 0 0"><a
							href="/<?= $v['url'] ?>/"><?=$name?></a></div>

				<?php
				}
				?>
				</div>
			<?php
			}
		}
		*/

	}
	?>
</form>

<script>
	document.addEventListener("DOMContentLoaded", function () {
		$('#open_all').on('click', function() {
			$(this).hide();
			$("#all_search").show();
			$("#quick_search").hide();
			return false;
		});
		/*$('input[name=quick]').on('keyup', function(){
			var word = $(this).val();
			if(word.length){
				console.log('true');
				$(this).closest('form').find('input, select, button').not(this).not('.btn-warning').attr('disabled',true);
			} else {
				console.log('false');
				$(this).closest('form').find('input, select, button').not(this).not('.btn-warning').removeAttr('disabled');
			}
		});*/
	});
</script>