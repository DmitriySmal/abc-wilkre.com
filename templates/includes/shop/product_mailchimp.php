<div style="min-height:100%;margin:0;padding:0;width:100%;background-color:#fafafa">
	<center>
		<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#fafafa">
			<tbody><tr>
				<td align="center" valign="top" style="height:100%;margin:0;padding:10px;width:100%;border-top:0">


					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border:0;max-width:600px!important">
						<tbody>
						<?php
						$config['date'] = date('Y-m-d');
						if (strtotime($q['oh_date'])>=strtotime($config['date'])) {
							$oh = unserialize($q['oh_dates']);
							?>
						<tr>
							<td valign="top" style="background-color:#fafafa;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:9px">
								<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
									<tbody>
									<tr>
										<td valign="top">

											<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
												<tbody><tr>

													<td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;line-height:150%;text-align:left">

														<h1 style="display:block;margin:0;padding:0;color:#ff0000;font-family:Helvetica;font-size:26px;font-style:normal;font-weight:bold;line-height:125%;letter-spacing:normal;text-align:left">
															Open House at
														</h1>
													</td>
													<td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;line-height:150%;text-align:left">
														<h1 style="display:block;margin:0;padding:0;color:#ff0000;font-family:Helvetica;font-size:26px;font-style:normal;font-weight:bold;line-height:125%;letter-spacing:normal;text-align:left">
														<?php foreach ($oh as $k=>$v) {?>
															<?=date2($v['date'],'%m/%d/')?> | <?=$v['from']?> &ndash; <?=$v['to']?><br>
														<?php } ?>
														</h1>

													</td>
												</tr>
												</tbody></table>

										</td>
									</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<?php } ?>
						<tr>
							<td valign="top" style="background-color:#ffffff;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:0"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
									<tbody>
									<tr>
										<td valign="top" style="padding:9px">
											<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse">
												<tbody><tr>
													<td valign="top" style="padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align:center">
														<img align="center" alt="" src="https://gallery.mailchimp.com/004fa7332f990f9b1092f4e3d/images/79b39f57-16b9-4fda-a780-409214825482.png" width="564" style="max-width:851px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;min-height:auto;outline:none;text-decoration:none" class="CToWUd a6T" tabindex="0">
													</td>
												</tr>
												</tbody></table>
										</td>
									</tr>
									</tbody>
								</table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
									<tbody>
									<tr>
										<td valign="top">

											<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
												<tbody><tr>

													<td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left">

														<h1 style="display:block;margin:0;padding:0;color:#202020;font-family:Helvetica;font-size:26px;font-style:normal;font-weight:bold;line-height:125%;letter-spacing:normal;text-align:left">
															<?=random_property_type ($q)?>
															<?=$q['residential']==2?'For Sale':'For Rent'?>
															<?= i18n('shop|currency', true) ?><span><?= number_format($q['price'],0,'.',',')?></span>
															<?php
															$state = in_array($q['base'],array(1,2)) ? 'FL':'NY';
															$q['city_name'] = $config['shop_cities'][$q['city']]['name'];
															?>
															<br><?=$q['name']?>
															<br><span><?= $q['nb_name'] ?>, <?= $q['city_name'] ?>, <?=$state?><?=$q['zip_code']?' '.$q['zip_code']:''?></span>

														</h1>

													</td>
												</tr>
												</tbody></table>

										</td>
									</tr>
									</tbody>
								</table>
								<?php
								$i=0;
								if ($q['img']) $images[] = 'http://www.wilkrealestate.com/files/shop_products/'.$q['id'].'/img/'.$q['img'];
								$imgs = $q['imgs'] ? unserialize($q['imgs']) : false;
								if ($imgs) foreach ($imgs as $k=>$v) {
									$images[] = 'http://www.wilkrealestate.com/files/shop_products/' . $q['id'] . '/imgs/' . $k . '/'.$v['file'];
								}
								if ($images) {
								?>
								<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
									<tbody>
									<?php foreach ($images as $k=>$v) {
										$i++;
										if ($i==5) break;
										if ($i==1 OR $i==3) echo '<tr>';
										?>
										<td valign="top" style="padding:9px 9px 9px 18px">
											<img alt="" src="<?=$v?>" width="264" style="max-width:580px;padding-bottom:0;border:0;min-height:auto;outline:none;text-decoration:none;vertical-align:bottom" class="CToWUd a6T" tabindex="0">
										</td>
										<?php
										if ($i==2 OR $i==4) echo '</tr>';
									}
									if ($i==1 OR $i==3) {
										echo '<td></td></tr>';
									}
									?>
									</tbody>
								</table>
								<?php } ?>
								<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
									<tbody>
									<tr>
										<td valign="top">

											<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
												<tbody><tr>

													<td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left">

														<?=$q['text']?>
														<br>
														<br>
														<?=nl2br($q['comment'])?>
													</td>
												</tr>
												</tbody></table>

										</td>
									</tr>
									</tbody>
								</table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
									<tbody>
									<tr>
										<td style="padding-top:0;padding-right:18px;padding-bottom:18px;padding-left:18px" valign="top" align="center">
											<table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate!important;border-radius:3px;background-color:#2baadf">
												<tbody>
												<tr>
													<td align="center" valign="middle" style="font-family:Arial;font-size:16px;padding:15px">
														<a title="See more information" href="http://www.wilkre.com<?=get_url('product',$q)?>" style="font-weight:bold;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;color:#ffffff;display:block"
														   target="_blank">See more information</a>
													</td>
												</tr>
												</tbody>
											</table>
										</td>
									</tr>
									</tbody>
								</table></td>
						</tr>
						<tr>
							<td valign="top" style="background-color:#ffffff;border-top:0;border-bottom:0;padding-top:0;padding-bottom:0">

								<table align="left" border="0" cellpadding="0" cellspacing="0" width="300" style="border-collapse:collapse">
									<tbody><tr>
										<td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
												<tbody>
												<tr>
													<td valign="top">

														<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
															<tbody><tr>

																<td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left">

																	<strong>Wilk Real Estate I LLC</strong><br>
																	<strong>Address:</strong>&nbsp;626 Ave. U, Brooklyn NY, 11223<br>
																	<strong>New York Phone:</strong>&nbsp;718-376-0606<br>
																	<strong>New York Fax:</strong>&nbsp;718-376-0073<br>
																	<strong>Florida Phone:</strong>&nbsp;305-699-8998<br>
																	<strong>E-mail:</strong>&nbsp;<a href="mailto:info@wilkrealestate.com" style="color:#2baadf;font-weight:normal;text-decoration:underline" target="_blank">info@wilkrealestate.<wbr>com</a>&nbsp;
																</td>
															</tr>
															</tbody></table>

													</td>
												</tr>
												</tbody>
											</table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
												<tbody>
												<tr>
													<td style="padding-top:0;padding-right:18px;padding-bottom:18px;padding-left:18px" valign="top" align="center">
														<table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate!important;border-radius:3px;background-color:#2baadf">
															<tbody>
															<tr>
																<td align="center" valign="middle" style="font-family:Arial;font-size:16px;padding:15px">
																	<a title="Contact Us" href="mailto:wilkrealestate@hotmail.com" style="font-weight:bold;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;color:#ffffff;display:block" target="_blank">Contact Us</a>
																</td>
															</tr>
															</tbody>
														</table>
													</td>
												</tr>
												</tbody>
											</table></td>
									</tr>
									</tbody></table>

								<table align="left" border="0" cellpadding="0" cellspacing="0" width="300" style="border-collapse:collapse">
									<tbody><tr>
										<td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
												<tbody>
												<tr>
													<td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">

														<table align="right" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
															<tbody><tr>
																<td align="left" valign="top" style="padding-top:0px;padding-right:0px;padding-bottom:0;padding-left:0px">



																	<img alt="" src="https://gallery.mailchimp.com/004fa7332f990f9b1092f4e3d/images/c08432a5-4009-4583-b102-a55663d1f46b.png" width="264" style="max-width:559px;border:0;min-height:auto;outline:none;text-decoration:none;vertical-align:bottom" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 793.5px; top: 1145.13px;"><div id=":2nt" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Скачать" role="button" tabindex="0" aria-label="Скачать файл " data-tooltip-class="a1V"><div class="aSK J-J5-Ji aYr"></div></div></div>


																</td>
															</tr>
															<tr>
																<td valign="top" style="padding:9px 18px;color:#f2f2f2;font-family:Helvetica;font-size:14px;font-weight:normal;text-align:center;word-break:break-word;line-height:150%" width="246">

																</td>
															</tr>
															</tbody></table>




													</td>
												</tr>
												</tbody>
											</table></td>
									</tr>
									</tbody></table>

							</td>
						</tr>
						<tr>
							<td valign="top" style="background-color:#ffffff;border-top:0;border-bottom:2px solid #eaeaea;padding-top:0;padding-bottom:9px"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
									<tbody>
									<tr>
										<td valign="top">

											<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
												<tbody><tr>

													<td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left">

														<strong>FIND US ON SOCIAL MEDIA:</strong>
													</td>
												</tr>
												</tbody></table>

										</td>
									</tr>
									</tbody>
								</table></td>
						</tr>
						<tr>
							<td valign="top" style="background-color:#fafafa;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:9px"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
									<tbody>
									<tr>
										<td align="center" valign="top" style="padding:9px">
											<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
												<tbody><tr>
													<td align="center" style="padding-left:9px;padding-right:9px">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border:2px none;background-color:#fffff0;border-collapse:collapse">
															<tbody><tr>
																<td align="center" valign="top" style="padding-top:9px;padding-right:9px;padding-left:9px">
																	<table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
																		<tbody><tr>
																			<td align="center" valign="top">




																				<table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">

																					<tbody><tr>
																						<td align="center" valign="top" style="padding-right:10px;padding-bottom:9px">
																							<a href="https://twitter.com/wilkre" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://twitter.com/wilkre&amp;source=gmail&amp;ust=1460799135150000&amp;usg=AFQjCNGc-UlmN8HXNExRKVk3CNm0A54SqA"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-96.png" alt="Twitter" width="48" style="width:48px;max-width:48px;display:block;border:0;min-height:auto;outline:none;text-decoration:none" class="CToWUd"></a>
																						</td>
																					</tr>


																					</tbody></table>






																				<table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">

																					<tbody><tr>
																						<td align="center" valign="top" style="padding-right:10px;padding-bottom:9px">
																							<a href="https://www.facebook.com/Wilk-Real-Estate-806378079489797/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://www.facebook.com/Wilk-Real-Estate-806378079489797/&amp;source=gmail&amp;ust=1460799135150000&amp;usg=AFQjCNG1jvEzRRrynhbhk0UMl_PPYRulAQ"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-96.png" alt="Facebook" width="48" style="width:48px;max-width:48px;display:block;border:0;min-height:auto;outline:none;text-decoration:none" class="CToWUd"></a>
																						</td>
																					</tr>


																					</tbody></table>






																				<table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">

																					<tbody><tr>
																						<td align="center" valign="top" style="padding-right:10px;padding-bottom:9px">
																							<a href="https://www.youtube.com/channel/UC5DJ60_YX2oQRo8savqHVjw" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://www.youtube.com/channel/UC5DJ60_YX2oQRo8savqHVjw&amp;source=gmail&amp;ust=1460799135150000&amp;usg=AFQjCNGyO19LYMsUGDQZomgT-rBRQD8teQ"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-youtube-96.png" alt="YouTube" width="48" style="width:48px;max-width:48px;display:block;border:0;min-height:auto;outline:none;text-decoration:none" class="CToWUd"></a>
																						</td>
																					</tr>


																					</tbody></table>






																				<table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">

																					<tbody><tr>
																						<td align="center" valign="top" style="padding-right:0;padding-bottom:9px">
																							<a href="http://www.wilkrealestate.com" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=http://www.wilkrealestate.com&amp;source=gmail&amp;ust=1460799135150000&amp;usg=AFQjCNGSObU4aadPFQ0_dYNk33T_NiQKqw"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-96.png" alt="Website" width="48" style="width:48px;max-width:48px;display:block;border:0;min-height:auto;outline:none;text-decoration:none" class="CToWUd"></a>
																						</td>
																					</tr>


																					</tbody></table>





																			</td>
																		</tr>
																		</tbody></table>
																</td>
															</tr>
															</tbody></table>
													</td>
												</tr>
												</tbody></table>

										</td>
									</tr>
									</tbody>
								</table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
									<tbody>
									<tr>
										<td style="padding-top:0;padding-right:18px;padding-bottom:18px;padding-left:18px" valign="top" align="center">
											<table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate!important;border-radius:3px;background-color:#2baadf">
												<tbody>
												<tr>
													<td align="center" valign="middle" style="font-family:Arial;font-size:16px;padding:15px">
														<a title="Unsubscribe from this list" href="*|UNSUB|*"
														   style="font-weight:bold;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;color:#ffffff;display:block"
														   target="_blank" >Unsubscribe from this list</a>
													</td>
												</tr>
												</tbody>
											</table>
										</td>
									</tr>
									</tbody>
								</table></td>
						</tr>
						</tbody></table>


				</td>
			</tr>
			</tbody></table>
	</center>
</div>
