<?php
$img = $q['img'] ? '/files/shop_products/'.$q['id'].'/img/'.$q['img'] : '/templates/images/no_img.jpg';
$images = $q['imgs'] ? unserialize($q['imgs']) : false;
$parameters = $q['parameters'] ? unserialize($q['parameters']) : false;
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$q['city_name'] = $config['shop_cities'][$q['city']]['name'];
$q['city_url']	= $config['shop_cities'][$q['city']]['url'];
?>
<div class="row">    
    <div class="col-md-12">    
        <div class="shop-product-text">
            <?php
            /*
            ?><h3><?=$q['name']?></h3>
	        <?= $q['nb_name'] ?>, <?= $q['city_name'] ?><?=$q['zip_code']?', '.$q['zip_code']:''?>
            */?>
	        <h3><?=object_name($q)?></h3>

            <div class="row" style="padding-top:15px">
                <div class="col-xs-6">

	                <?php
	                $config['date'] = date('Y-m-d');
	                if (strtotime($q['oh_date'])>=strtotime($config['date'])) {
		                $agents = mysql_select("SELECT * FROM users WHERE agent=1",'rows_id');
		                $oh = unserialize($q['oh_dates']);
		                //if (access('user admin')) dd($oh);
		                ?>
                        <div class="open_house row" style="margin:0px 0px 10px">
                            <div class="col-md-3" style="padding-top:10px; font-size:23px; font-style: italic">Open house</div>
                            <div class="col-md-9" style="border-left:2px solid #e6e6e6">
				                <?php foreach ($oh as $k=>$v) if (strtotime($v['date'])>=strtotime($config['date'])) {?>
                                    Date/Time: <strong><?=date2($v['date'],'%a, %m/%d/%Y')?> | <?=date_time($v['from'])?> &ndash; <?=date_time($v['to'])?></strong>
					                <?php if ($v['agent']) {?>
						                <?=$agents[$v['agent']]['name']?> <?=$agents[$v['agent']]['phone']?>
					                <?php } ?>
                                    <br>
				                <?php } ?>
				                <?php /*if ($q['oh_hosted_by']) {?>
					                Name: <?=$q['oh_hosted_by']?><br>
				                <?php } ?>
				                <?php if ($q['oh_phone']) {?>
					                Phone: <?=$q['oh_phone']?><br>
				                <?php } */?>
				                <?php if ($q['oh_text']) {?>
					                <?=$q['oh_text']?>
				                <?php } ?>
                            </div>
                        </div>
	                <?php } ?>

                    <div class="gallary">
	                    <img class="main" src="<?= $img ?>" alt="<?= $title ?>" />
                        <?php
                        if ($images) {
                            //[4.40] распечатка
                            /*
                            $n = 0;
                            $list = '';
                            foreach ($images as $k => $v)
                                if (@$v['display'] == 1) {
                                    $n++;
                                    $title2 = filter_var($v['name'], FILTER_SANITIZE_STRING);
                                    $path = '/files/shop_products/' . $q['id'] . '/imgs/' . $k . '/';
                                    if(is_file(ROOT_DIR.$path.$v['file'])){
                                        $list.= '<li><img src="' . $path . 'p-' . $v['file'] . '" alt="' . $title2 . '" /></li>';
                                    }
                                }                        
                            ?>
                            <div class="carousel">
                                <ul style=""><?= $list ?></ul>                            
                            </div>                            
                            <?
                            */
                       }
                       elseif ($q['base']==9 AND $q['uid']) {
                            /*$config['not_print'] = 1;
	                        ?>
                            <div class="carousel"></div>
                            <script type="text/javascript">
		                        document.addEventListener("DOMContentLoaded", function () {
			                        $.get('/ajax.php', {
					                        'file':'object_imgs','uid':<?=$q['uid']?>},
				                        function(data){
					                        if(data) {
						                        $('.carousel').html(data);
					                        }
				                        });

		                        });
                            </script>
	                        <?
                            */
                        }
                       ?>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="info">
                        <?php if ($q['price'] > 0) { ?>
                            <div class="price">
                                <?= i18n('shop|currency') ?><span><?= number_format($q['price'],0,'.',',')?></span>
                            </div>                            
                        <?php } ?>                
                        <div class="price_box">
	                        <?=html_array('shop/product_parameters',$q);?>
                        </div>
                    </div>
                </div>
            </div>
            <?if(@$q['text']){?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="h3"><span>Description</span></div>
                        <div class="text">
                           <?= $q['text'] ?>
                        </div>
                    </div>
                </div>
            <?}?>

<?php /*
	        <div class="tab-content">
		        <div role="tabpanel" class="tab-pane fade in active" id="map">
			        <?=html_array('common/widgets/map',$q)?>
		        </div>
		    </div>
 */?>

        </div>    
    </div>
</div>
