<?php
$img = $q['img'] ? '/files/shop_products/'.$q['id'].'/img/'.$q['img'] : '/templates/images/no_img.jpg';
$images = $q['imgs'] ? unserialize($q['imgs']) : false;
$parameters = $q['parameters'] ? unserialize($q['parameters']) : false;
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$q['city_name'] = $config['shop_cities'][$q['city']]['name'];
$q['city_url']	= $config['shop_cities'][$q['city']]['url'];
?>
<div style="padding:0 20px 0 0">
<table style="width:100%">
	<tr valign="top">
		<td style="padding:0 0 20px">
			Wilk Real Estate I, LLC
			<br>Albert Wilk licensed real estate broker New York and Florida
			<br>626 Avenue U, Brooklyn, NY 11223
		</td>
		<td class="text-right">
			+1 718 376 – 0606
			<br>www.wilkrealestate.com
			<br>info@wilkrealestate.com
		</td>
	</tr>
	<tr valign="top">
		<td colspan="2">
			<h3><?=$q['name']?></h3>
			<?= $q['nb_name'] ?>, <?= $q['city_name'] ?><?=$q['zip_code']?', '.$q['zip_code']:''?>
		</td>
	</tr>
	<tr valign="top">
        <td valign="top">
            <div class="gallary">
                <img style="width:400px" src="<?= $img ?>" alt="<?= $title ?>" />
                <?php
                /*if ($images) {
                    $n = 0;
                    $list = '';
                    foreach ($images as $k => $v)
                        if (@$v['display'] == 1) {
                            $n++;
                            $title2 = filter_var($v['name'], FILTER_SANITIZE_STRING);
                            $path = '/files/shop_products/' . $q['id'] . '/imgs/' . $k . '/';
                            if(is_file(ROOT_DIR.$path.$v['file'])){
                                $list.= '<li><img src="' . $path . 'p-' . $v['file'] . '" alt="' . $title2 . '" /></li>';
                            }
                        }
                    ?>
                    <div class="carousel">
                        <ul style=""><?= $list ?></ul>
                    </div>
            <?} */?>
            </div>
        </td>
        <td valign="top">
            <div class="info" style="padding:0 0 0 10px">
                <?php if ($q['price'] > 0) { ?>
                    <div class="price" style="font-weight:bold">
                        <?= i18n('shop|currency') ?><span><?= number_format($q['price'],0,'.',',')?></span>
                    </div>
                <?php } ?>
                <div class="price_box">
                    <?=html_array('shop/product_parameters',$q);?>
                </div>
            </div>
        </td>
    </tr>
	<?if(@$q['text']){?>
	<tr valign="top">
		<td colspan="2">
			<div class="h3" style="text-align: center; padding:0 0 20px"><span>Description</span></div>
			<div class="text"><?= $q['text'] ?></div>
		</td>
	</tr>
	<?}?>
</table>
</div>
