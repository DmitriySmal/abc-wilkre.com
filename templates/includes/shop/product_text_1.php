<?=html_sources('return','highslide_gallery')?>
<?php
$img = $q['img'] ? '/files/shop_products/'.$q['id'].'/img/'.$q['img'] : '/templates/images/no_img.png';
$images = $q['imgs'] ? unserialize($q['imgs']) : false;
$parameters = $q['parameters'] ? unserialize($q['parameters']) : false;

$similar = ($q['is_condo'] && $q['similar']) ? mysql_select("SELECT sp.* FROM shop_products AS sp WHERE sp.display = 1 AND id IN ({$q['similar']})") : array();
//var_dump($similar);
if ($similar){
    //if($similar[0]['min_price'] && $similar[0]['max_price']) $condos_prms['Price range'] = $similar[0]['min_price'].' - '.$similar[0]['max_price'];
    $average_price = 0;
    foreach ($similar as $sp){
        $average_price += intval($sp['price']);
    }
    $average_price = ($average_price) ? floor($average_price / count($similar)) : 0;
    if($average_price) $condos_prms['Average price'] = $average_price;
    
}

$shop_parameters = false;
if (is_array($parameters)) {
	$prms=array();
	foreach ($parameters as $k=>$v) if (@$v['display'] AND @$v['product']) $prms[]=$k;
	$shop_parameters = mysql_select("
		SELECT * FROM shop_parameters
		WHERE display=1 AND id IN('".implode("','",$prms)."')
		ORDER BY rank DESC
	",'rows_id');
}
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
?>
<div class="row">    
    <div class="col-md-12">    
        <div class="shop-product-text">
            <div class="row">
                <div class="col-md-7">
                    <div class="gallary">
                        <?php if ($q['img']) { ?><a title="<?= $title ?>" onclick="return hs.expand(this, config1)" href="/files/shop_products/<?= $q['id'] ?>/img/<?= $q['img'] ?>"><?php } ?>
                            <img class="main" src="<?= $img ?>" alt="<?= $title ?>" />
                            <?php if ($q['img']) { ?></a><?php } ?>
                        <?php
                        if ($images) {
                            $n = 0;
                            $list = '';
                            foreach ($images as $k => $v)
                                if (@$v['display'] == 1) {
                                    $n++;
                                    $title2 = filter_var($v['name'], FILTER_SANITIZE_STRING);
                                    $path = '/files/shop_products/' . $q['id'] . '/imgs/' . $k . '/';
                                    $list.= '<li><a title="' . $title2 . '" onclick="return hs.expand(this, {slideshowGroup: \'group1\',transitions: [\'expand\', \'crossfade\']})" href="' . $path . $v['file'] . '"><img src="' . $path . 'p-' . $v['file'] . '" alt="' . $title2 . '" /></a></li>';
                                }                        
                            ?>
                            <div class="carousel">
                                <ul style=""><?= $list ?></ul>                            
                            </div>

                            <div class="control-buttons">
                                <div class="row">
                                        <div class="col-md-4">
                                            <a href="#" class="btn btn-default">Print brochure</a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#" class="btn btn-default">Save to Favorites</a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#" class="btn btn-default">Shedule a showing</a>
                                        </div>
                                </div>
                            </div>
                    <?}?>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="info">
                        <?php if ($q['price'] > 0) { ?>
                            <div class="price">
                                <?= i18n('shop|currency', true) ?><span><?= number_format($q['price'],0,'.',',')?></span>                         
                            </div>
                        <?php } ?>                
                        <div class="price_box">
                            <?php if ($parameters OR $q['brand_name'] OR $q['article'] OR @$condos_prms) { ?>
                                <table class=" table parameters">                            
                                    <?php if ($q['article']) { ?>
                                        <tr>
                                            <td class="param"><?= i18n('shop|article', true) ?>:</td>
                                            <td<?= editable('shop_products|article|' . $q['id']) ?>><?= $q['article'] ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($q['brand_name']) { ?>
                                        <tr>
                                            <td class="param"><?= i18n('shop|brand', true) ?>:</td>
                                            <td><?= $q['brand_name'] ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if (@$condos_prms) { ?>
                                        <?foreach($condos_prms as $cn=>$cp){?>
                                            <tr>
                                                <td class="param"><?= $cn ?>:</td>
                                                <td><?= $cp ?></td>
                                            </tr>
                                        <?}?>
                                    <?php } ?>
                                    <?php if ($parameters) foreach ($parameters as $k => $v)
                                            if ($q['p' . $k] != 0 AND isset($shop_parameters[$k])) { ?>
                                                <tr>
                                                    <td class="param"><?=$shop_parameters[$k]['name']?>:</td>
                                                    <td>
                                                        <?
                                                        $values = $shop_parameters[$k]['values'] ? unserialize($shop_parameters[$k]['values']) : array();
                                                        if (in_array($shop_parameters[$k]['type'], array(1, 3)))
                                                            echo @$values[$q['p' . $k]];
                                                        elseif ($shop_parameters[$k]['type'] == 2)
                                                            echo $q['p' . $k];
                                                        else
                                                            echo '';
                                                        if ($shop_parameters[$k]['units'])
                                                            echo ' ' . $shop_parameters[$k]['units'];
                                                        ?>                                                    
                                                    </td>                                                
                                                </tr>
                                        <?php }  ?>                            
                                </table>
                                <?php } ?>                        

                        </div>
                        <?//= html_array('common/share') ?>                    
                    </div>
                </div>
            </div>

            <?if(@$q['text']){?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="h3"><span>Description</span></div>
                        <div class="text">
                            <div<?= editable('shop_products|text|' . $q['id'], 'editable_text', 'text') ?>><?= $q['text'] ?></div>
                        </div>
                    </div>
                </div>
            <?}?>

            <div class="row">
                <div class="col-md-12">
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#map" aria-controls="map" role="tab" data-toggle="tab">Map location</a></li>
                            <li role="presentation"><a href="#street" aria-controls="street" role="tab" data-toggle="tab">Street view</a></li>
                            <li role="presentation"><a href="#schools" aria-controls="schools" role="tab" data-toggle="tab">Schools</a></li>
                            <li role="presentation"><a href="#walkscore" aria-controls="walkscore" role="tab" data-toggle="tab">Walk Score</a></li>
                            <li role="presentation"><a href="#calculator" aria-controls="calculator" role="tab" data-toggle="tab">Mortgage Calculator</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="map">                                
                                 <?=html_array('common/widgets/map',$q)?>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="street">
                                ...
                            </div>  
                            <div role="tabpanel" class="tab-pane fade" id="schools">
                                ...
                            </div> 
                            <div role="tabpanel" class="tab-pane fade" id="walkscore">
                                ...
                            </div> 
                            <div role="tabpanel" class="tab-pane fade" id="calculator">
                                <?=html_array('common/widgets/calc', array('price'=>$q['price']))?>
                            </div>                         
                        </div>
                    </div>
                </div>
            </div>
            <?if($similar){?>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <tr>
                                <td>Unit</td>
                                <td>Price</td>
                                <td>List Date</td>
                            </tr>
                            <?foreach ($similar as $s){?>
                                <tr>
                                    <td><?=$sp['name']?></td>                                    
                                    <td><?=$sp['price']?></td>                                    
                                    <td><?=$sp['date']?></td>                                    
                                </tr>
                            <?}?>
                        </table>
                    </div>
                </div>
            <?}?>
        </div>    
    </div>
</div>
<!--script type="text/javascript">
document.addEventListener("DOMContentLoaded", function () {
	$('.shop_product_text .gallary .carousel').hover(
		function () {
			$('.shop_product_text .carousel .next, .shop_product_text .carousel .prev').show().css('display','block');
		},
		function () {
			$('.shop_product_text .carousel .next, .shop_product_text .carousel .prev').hide();
		}
	);
	$('.shop_product_text .gallary .next,.shop_product_text .gallary .prev').click(function(){
		var left = parseInt($('.shop_product_text .gallary ul').css('margin-left'));
		var width = parseInt($('.shop_product_text .gallary ul').width());
		var margin = 121;
		//alert (width+' '+left);
		if ($(this).hasClass('next')) {
			if (width+left<400) return false;
			left = left-margin;
		} else {
			if (left>=0) return false;
			left = left+margin;
		}
		$('.shop_product_text .gallary ul').animate({marginLeft:left+'px'},500);
		return false;
	});
});
</script-->
