<?php
if (count($_POST)) {
	require_once ROOT_DIR.'functions/MCAPI.class.php';
	$api = new MCAPI($config['mailchimp_api_key']);
	$type = 'regular';

	require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами
	require_once(ROOT_DIR.'functions/mail_func.php');	//функции для работы со формами
	//определение значений формы
	$fields = array(
		'email'			=>	'required email',
		'subject'			=>	'required text',
		'from'			=>	'required text',
		'individual'    => 'int',
		'receiver'      => 'email',
		'comment'=> 'text'
	);
	//создание массива $post
	$post = form_smart($fields,stripslashes_smart($_POST)); //print_r($post);
	$q['comment'] = $post['comment'];
	$content = array('html' => html_array('shop/product_mailchimp',$q));
	if ($post['individual'] AND $post['receiver']) {
		email($post['email'],$post['receiver'],$post['subject'],$content['html']);
		echo 'Email send to '.$post['receiver'].'<br>';
	}
	if (@$_POST['list']) foreach ($_POST['list'] as $k=>$v) {
		$opts['list_id'] = $v;
		$opts['subject'] = $post['subject'];
		$opts['from_email'] = $post['email'];
		$opts['from_name'] = $post['from'];

		$opts['tracking'] = array('opens' => true, 'html_clicks' => true, 'text_clicks' => false);

		$opts['authenticate'] = true;
//$opts['analytics'] = array('google'=>'my_google_analytics_key');
		$opts['title'] = date('Y-m-d H:i:s').' ' .$post['subject'];



		/** OR we could use this:
		 * $content = array('html_main'=>'some pretty html content',
		 * 'html_sidecolumn' => 'this goes in a side column',
		 * 'html_header' => 'this gets placed in the header',
		 * 'html_footer' => 'the footer with an *|UNSUB|* message',
		 * 'text' => 'text content text content *|UNSUB|*'
		 * );
		 * $opts['template_id'] = "1";
		 **/
		//print_R($opts);
		//print_R($content);


		$retval = $api->campaignCreate($type, $opts, $content);

		if ($api->errorCode) {
			echo "<br>Unable to Create New Campaign!";
			echo "<br>Code=" . $api->errorCode;
			echo "<br>Msg=" . $api->errorMessage;
		}
		else {
			echo "<br>New Campaign ID:" . $retval;

			$retval = $api->campaignSendNow($retval);

			if ($api->errorCode){
				echo "<br>Unable to Send Campaign!";
				echo "<br>Code=".$api->errorCode;
				echo "<br>Msg=".$api->errorMessage."\n";
			} else {
				echo "<br>Campaign Sent!\n";
			}



		}
		/* */
	}

}
else {
	?>
	<a href="?action=template" target="_blank" class="btn btn-warning">PREVIEW</a>
	<form action="" method="post">
		<div class="row">
			<div class="col-lg-12 form-group">
				<label for="subject">Subject</label>
				<input type="text" class="form-control" name="subject" value="<?=$page['name']?>">
			</div>
			<div class="col-lg-6 form-group">
				<label for="subject">Email</label>
				<input type="text" class="form-control" name="email" value="info@wilkrealestate.com">
			</div>
			<div class="col-lg-6 form-group">
				<label for="subject">From</label>
				<input type="text" class="form-control" name="from" value="Wilk Real Estate">
			</div>
			<div class="col-lg-12 form-group">
				<label for="comment">Text</label>
				<textarea type="text" class="form-control" name="comment"></textarea>
			</div>
			<div class="col-lg-6 form-group clearfix">
				<label for="list">Choose list</label>
				<?php
				foreach ($config['mailchimp_list'] as $k => $v) {
					echo '<div><label><input type="checkbox" name="list[]" value="' . $v . '"> ' . $k . '</label></div>';
				}
				?>
				<label><input type="checkbox" name="individual" value="1" onchange="$(this).closest('label').next('input').slideToggle()"> individual</label>
				<input style="display:none" type="text" class="col-lg-6 form-control" name="receiver" placeholder="email@email.com">
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 form-group">
				<label style="display:block; height:20px"> </label>
				<button class="btn btn-warning" style="display:block; width:100%" id="subscribe2_send">SEND</button>
			</div>
		</div>
	</form>
<?php
}