<? 
global $html;
$categories = mysql_select("SELECT * FROM shop_categories WHERE display = 1 ORDER BY id ASC",'rows');                                        
$commercial = $residental = $sell = $rent = array();
foreach($categories as $c){
    if($c['commercial']) $commercial[] = $c['id']; else $residental[] = $c['id'];
    if($c['sell']) $sell[] = $c['id']; else $rent[] = $c['id'];
}

$display1 = '';
$display2 = ' style="display:none"';
//если есть параметры полного поиска
if (@$q['location'] OR @$q['property_type'] OR @$q['beds'] OR @$q['baths'] OR @$q['price_min'] OR @$q['price_max']) {
	$display1 = $display2;
	$display2 = '';
}
$cities = mysql_select("SELECT id,name FROM shop_cities WHERE state=1 AND display=1 ORDER BY rank",'array');

?>
<form id="search-offers" class="clear_form" action="/<?=$modules['shop_ny']?>/" method="get" style="<?=((@$html['is_product']) ? 'display: none;' :'')?>">
    <!--div id="form-switch">
        <span data-switch-value="false" data-switch-set="state" class="switch-control active">Residental</span> <input type="checkbox" class="switch"> <span data-switch-value="true" data-switch-set="state" class="switch-control">Commercial</span>
    </div-->

                <div class="row">
	                <div class="form-group col-md-2">
		                <label for="location">For Sale / For Rent:</label>
		                <select class="form-control" id="residential" name="residential">
			                <?=select(@$q['residential'],$config['object_residential'],'Select')?>
		                </select>

	                </div>
	                <div class="col-md-2">
		                <div class="form-group">
			                <label for="location">City:</label>
			                <select class="form-control" id="city" name="city">
				                <?=select(@$q['city'],$cities,'Select')?>
			                </select>
		                </div>
		                <script type="text/javascript">
			                $(document).ready(function() {
		                        $('#city').change(function(){
			                        $( "#quicksearch" ).autocomplete( "destroy" );
			                        var city = $(this).val();
			                        $.get('/ajax.php', {'file':'ny_location','city':city},function(data){
				                        $('#location_box').html(data);
			                        });
		                        });

			                });
		                </script>
	                </div>
	                <div class="col-md-2">
		                <div class="form-group">
			                <label>Real Estate Types:</label>
			                <select class="form-control" id="property_type" name="property_type">
				                <option value="0">Select</option>
				                <?=select(@$q['property_type'],$config['ny_property_types'])?>
			                </select>
		                </div>
	                </div>
                    <div class="form-group col-md-4">
	                    <label>&nbsp</label>
                        <input id="quicksearch" class="form-control" name="quick" placeholder="Enter addres or zip or Neighborhoods" value="<?=@$_GET['quick']?>">
                    </div>
                    <div class="col-md-2">
	                    <div class="form-group" id="quick_search" <?=$display1?>>
		                    <label>&nbsp</label>
		                    <button style="display: block; width: 100%;" class="btn btn-warning">QUICK SEARCH</button>
	                    </div>
                    </div>
	                <div class="col-md-12"><a href="#" id="open_all" <?=$display1?>>Open detailed property search</a>

		            </div>
                </div>

				<div id="all_search" <?=$display2?>>
	                <div class="row">
	                    <div class="form-group col-md-6" id="location_box">
		                    <?
		                    $cities = @$q['city'] ? $q['city'] : '7,8,9,10,11,12';
		                    $nbs = mysql_select("SELECT * FROM shop_neighborhoods WHERE /*display = 1 AND */city IN (".$cities.") ORDER BY rank DESC, name ASC",'rows');
		                    ?>
	                        <label for="location">Location:</label>
	                        <!--input type="text" class="form-control" id="location" placeholder="Neighbourhood / Address / Building"-->
	                        <script type="text/javascript">
	                            $(document).ready(function() {
		                            $( "#quicksearch" ).autocomplete({
			                            source: [
		                                <?foreach($nbs as $k=>$v){?>
		                                '<?=addslashes($v['name'])?>',
		                                 <?php
		                                 if ($v['name2']) echo "'".addslashes($v['name2'])."',";
		                                 }?>
			                            ],
			                            minLength: 0
		                            });
		                            $( "#location").change(function(){
			                            $( "#quicksearch").val('');
		                            });
	                                /*$('#location').multiselect({
	                                    enableFiltering: true,
	                                    enableCaseInsensitiveFiltering: true
	                                    //filterBehavior: 'value'
	                                });*/
	                            });
	                        </script>
		                    <script>
			                    $(function() {
				                    $('.chosen-select').chosen();
				                    $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
			                    });
		                    </script>
		                    <style>.chosen-container {display:block; min-width:100%}</style>
	                        <select size="1" class="form-control chosen-select" data-placeholder="Select Location" id="location" name="location[]" multiple="multiple">
	                            <?foreach($nbs as $k=>$v) if ($v['display']==1){?>
	                                <option value="<?=$v['id']?>" <?=((@in_array($v['id'], @$q['location'])) ? 'selected' :'')?>><?=$v['name']?></option>
	                            <?}?>
	                        </select>
	                    </div>

		                <div class="col-md-3"  id="property_type2" style="display:none">
			                <label>Type of Property:</label>
			                <select class="form-control" id="property_type_1" name="property_type_1" <?=@$q['property_type'] == 1?'':'style="display:none"'?>>
				                <option value="0">Select type of property</option>
				                <?=select(@$q['property_type_1'],$config['ny_property_types_1'])?>
			                </select>
			                <select class="form-control" id="property_type_2" name="property_type_2" <?=@$q['property_type'] == 2?'':'style="display:none"'?>>
				                <option value="0">Select type of property</option>
				                <?foreach($config['commercial'] as $k=>$v){?>
					                <option value="<?=$v?>" <?=((@$q['property_type_2'] == $v) ? 'selected' :'')?> ><?=$v?></option>
				                <?}?>
			                </select>

							<script>
							$(document).ready(function() {
								//цены
								<?php /*
								var arr = [
									<?foreach($config['prices_list'] as $k=>$v){?>
									"<?=$v?>",
									<?}?>
								];
								var arr1 = [
									<?foreach($config['prices_list'] as $k=>$v) if($k<=10000) {?>
									"<?=$v?>",
									<?}?>
								];
								var arr2 = [
									<?foreach($config['prices_list'] as $k=>$v) if($k>=10000) {?>
									"<?=$v?>",
									<?}?>
								];
								$( "#price_max,#price_min" ).autocomplete({
									<?php
									if (@$q['residential'] == 1) echo 'source: arr1,';
									elseif (@$q['residential'] == 2) echo 'source: arr2,';
									else echo 'source: arr,';
									?>
									minLength: 0
								});*/ ?>
								$( "#price_max,#price_min" ).autocomplete({
									source: function(request,response) {
										var residential = $('#residential').val(),
											str = request.term.replace(/[^0-9]/g, ""),
											arr = [],
											count = residential == 1 ? 5:9;
										if (str.length) {
											for (var i = 1; i < count - str.length; i++) {
												var s = '';
												for (var n = 0; n < i; n++) {
													s = s + '0';
												}
												arr.push('$' + number_format(str + s, '', 0,','));
											}
										}
										response(arr);
									},
									minLength: 1
								});

								//подтипы
								$(document).on("change", '#property_type', function () {
									var residential = $('#residential').val();
									$('#property_type2,#property_type_1, #property_type_2').hide();
									//в аренде нет подвидов или комерция
									if (residential!=1 || $(this).val()==2) {
										if ($(this).val() == 1) {
											$('#property_type2,#property_type_1').show();
										}
										if ($(this).val() == 2) {
											$('#property_type2,#property_type_2').show();
										}
									}
									//прячем Baths и Bedrooms
									if ($(this).val() == 3) {
										//alert(1);
										$('#bedrooms,#baths').val('0').attr('disabled','disabled');
									}
									else {
										$('#bedrooms,#baths').removeAttr('disabled');
									}
								});
								//аренда-продажа
								$(document).on("change", '#residential', function () {
									var residential = $(this).val();
									//автозаполнение
									$('#property_type').trigger('change');
								});
								//чтобы спрятать подтипы и типы
								$('#residential').trigger('change');
							});
							</script>
		                </div>
	                </div>
	                <div class="row">
                    <div class="form-group col-md-6 prices">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="type">Min Price:</label>
	                            <input class="form-control"  name="price_min" id="price_min" value="<?=@$q['price_min']?'$'.number_format($q['price_min'],0,'.',','):''?>">
	                            <?php /*<select class="form-control" id="price_min" name="price_min">
                                    <option value="0">select</option>
                                    <?foreach($config['prices_list'] as $k=>$v){?>
                                        <option value="<?=$k?>" <?=((@$q['price_min'] == $k) ? 'selected' :'')?> ><?=$v?></option>
                                    <?}?>
                                </select>*/?>
                            </div>                                
                            <div class="form-group col-md-6">
                                <label for="type">Max Price:</label>
			                            <input class="form-control"  name="price_max" id="price_max" value="<?=@$q['price_max']?'$'.number_format($q['price_max'],0,'.',','):''?>">
			                            <?php /*
                                <select class="form-control" id="price_max" name="price_max">
                                    <option value="0">select</option>
                                    <?foreach($config['prices_list'] as $k=>$v){?>
                                        <option value="<?=$k?>" <?=((@$q['price_max'] == $k) ? 'selected' :'')?> ><?=$v?></option>
                                    <?}?>
                                </select>*/?>
                            </div>
                            <script>
                                //$('#price_min, #price_max').multiselect({});
                            </script>
	                        <style>.prices .ui-menu-item {text-align:right}</style>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="bedrooms">Bedrooms:</label>                        
                        <select class="form-control" id="bedrooms" name="beds">
                            <option value="0">Select</option>
                            <?foreach($config['bedrooms_list'] as $k=>$v){?>
                                <option value="<?=$k?>" <?=((@$q['beds'] == $k) ? 'selected' :'')?> ><?=$v?></option>
                            <?}?>
                        </select>
                    </div> 
                    <div class="form-group col-md-2">
                        <label for="baths">Baths:</label>
                        <select class="form-control" id="baths" name="baths">
                            <option value="0">Select</option>
                            <?foreach($config['baths_list'] as $k=>$v){?>
                                <option value="<?=$k?>" <?=((@$q['baths'] == $k) ? 'selected' :'')?> ><?=$v?></option>
                            <?}?>
                        </select>
                    </div>
                    <script>
                        //$('#bedrooms, #baths').multiselect({});
                    </script>

                    <div class="form-group col-md-2">
                        <label style="display:block;">&nbsp;</label> 
                        <button style="display: block; width: 100%;" class="btn btn-warning">SEARCH</button>
                    </div>                                            
                </div>
				</div>
</form>

<script>
	document.addEventListener("DOMContentLoaded", function () {
		$('#open_all').on('click', function() {
			$(this).hide();
			$("#all_search").show();
			$("#quick_search").hide();
			return false;
		});
		/*$('input[name=quick]').on('keyup', function(){
			var word = $(this).val();
			if(word.length){
				console.log('true');
				$(this).closest('form').find('input, select, button').not(this).not('.btn-warning').attr('disabled',true);
			} else {
				console.log('false');
				$(this).closest('form').find('input, select, button').not(this).not('.btn-warning').removeAttr('disabled');
			}
		});*/
	});
</script>