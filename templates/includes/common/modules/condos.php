<section id="shop">
    <div class="container">
        
        <?=html_array('common/widgets/nbs');?>
                        
        <div class="row">
            <div class="col-md-12">
                <?=($breadcrumb) ? html_array('common/breadcrumb',$breadcrumb) : '' ?>
            </div>
        </div>

	    <?php if (@$html['is_product']==false) {?>
	    <?=html_array('common/widgets/nbs2',array('city_id'=>6));?>
	    <?php } ?>

	    <div class="container">
		    <div class="row">
			    <div class="col-md-12">
				    <?=$html['search']?>
			    </div>
		    </div>
	    </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <?=@$page['name']?>
                    <?if(@$page['subname']){?>
                         <span><?=$page['subname']?></span>
                    <?}?>
                </div>
            </div>
        </div>
        <?if(@$html['filter']){?>
            <div class="row">
                <div class="col-md-12">                    
                    <?=$html['filter']?>                    
                </div>
            </div>
        <?}?>
        
        <!--div class="row"-->   
        <div id="listings">
            <?=@$html['content']?>
        </div>            
        <!--/div-->



        <?if(!$html['is_product']){?>
            <div class="row">            
                <div class="col-md-12">
                    <div class="page-text">
                        <?=@$page['text']?>
                    </div>
                </div>
            </div>
        <?}?>
    </div>
</section>

<?if($html['is_product']){?>
    <?=html_array('common/widgets/subscribe2')?>
<? } ?>
