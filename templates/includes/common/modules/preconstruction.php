<section id="shop">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <?=@$page['name']?>
                    <?if(@$page['subname']){?>
                         <span><?=$page['subname']?></span>
                    <?}?>
                </div>
            </div>
        </div>
        <?if(@$html['filter']){?>
            <div class="row">
                <div class="col-md-12">                    
                    <?=$html['filter']?>                    
                </div>
            </div>
        <?}?>
        <!--div class="row"-->            
            <?=@$html['content']?>
        <!--/div-->
        <?if(!$html['is_product']){?>
            <div class="row">            
                <div class="col-md-12">
                    <div class="page-text">
                        <?=@$page['text']?>
                    </div>
                </div>
            </div>
        <?}?>
    </div>
</section>

<?if($html['is_product']){?>
    <?=html_array('common/widgets/subscribe2')?>
<? } ?>
