<section id="index">
    <?//=html_array('common/slider')?>
       
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" id="cities_images">
                <div class="cities_blocks">
                    <div class="city_img" style="background: url('/templates/images/ny.jpg') scroll no-repeat 50% 50% transparent; background-size: cover;" >
                        <!--img src="/templates/images/ny.jpg"-->
                        <div class="info">
                            <span class="name">New York</span>
                            <a href="<?="{$modules['shop']}/?city=7"?>" class="btn btn-warning">See all</a>
                        </div>
                    </div>
                    <div class="city_img" style="background: url('/templates/images/miami.jpg') scroll no-repeat 50% 50% transparent; background-size: cover;">
                        <div class="info">
                            <span class="name">Miami</span>
                            <a href="<?="{$modules['shop']}/?city=6"?>" class="btn btn-warning">See all</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?=html_array('common/widgets/search')?>
            </div>
        </div>
    </div>
    
    <?=html_array('sliders/slider2',array(
        'header'=>i18n('slider|new_arrivals'),
        'items' => mysql_select("
                        SELECT sp.*, city.name city_name, city.url city_url, neighborhood.name neighborhood_name, neighborhood.url neighborhood_url, sc.name category_name,  sc.url category_url  
                        FROM shop_products AS sp 
                        LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood 
                        LEFT JOIN shop_cities AS city ON city.id = sp.city  
                        LEFT JOIN shop_categories AS sc ON sc.id = sp.category   
                        WHERE sp.display = 1 ORDER BY sp.id DESC LIMIT 10
                    "),
        'path' => 'shop_products',
    ))?>
    
    <?=html_array('sliders/slider2',array(
        'header'=>i18n('slider|featured_properties'),
        'items' => array(),//mysql_select(),
        'path' => 'slider',
    ))?>
    
    <?=html_array('common/widgets/subscribe1')?>
    <!--div class="bg-light-gray-pattern index-contact-form">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="info">
                        If you want to get the full listing of options regarding your request, please give us <br>your email/cell phone number and we will contact you shortly.
                    </div>
                    <form class="form-inline">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                            <div class="input-group">
                                <input style="width: 380px;" type="text" class="form-control input-lg col-md-5" id="email" placeholder="Your contact phone or e-mail adress">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-warning btn-lg">SEND</button>
                    </form>
                    <div class="thanks">
                        Thank You!
                    </div>
                </div>
            </div>
        </div>
    </div-->   
    
    <div id="city-cards">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="h3"><span>Brooklyn</span></div>
                    <?
                        $nbs = mysql_select("
                                SELECT nb.id, nb.name, nb.url, nb.city, city.name city_name, city.url city_url                                     
                                FROM `shop_neighborhoods` AS nb 
                                LEFT JOIN shop_cities AS city ON city.id = nb.city 
                                WHERE nb.city = 7 AND nb.display = 1 ORDER BY RAND() LIMIT 2");
                        foreach ($nbs as $v) {
                            $nb_ids[] = $v['id'];
                        }                                  
                        $listings = mysql_select("
                                        SELECT COUNT(sp.id) cnt, sp.zip_code  
                                        FROM shop_products AS sp 
                                        LEFT JOIN shop_neighborhoods_zips AS zips ON zips.nb_id IN (".implode(',', $nb_ids).")  
                                        WHERE sp.display = 1 AND sp.zip_code = zips.zip                                         
                                        GROUP BY (sp.id) 
                                     ",'row');
                        // 
                        //var_dump($listings);
                    ?>
                    <? foreach ($nbs as $nb){
                        $img = ($nb['img']) ? "/files/shop_neighborhoods/{$nb['id']}/img/m-{$nb['img']}" : "/templates/images/tmp1.png";
                        //$url = "/{$modules['neighborhoods']}/{$nb['city']}-{$nb['city_url']}/{$nb['id']}-{$nb['url']}";
                        $url = "/{$modules['shop']}/?location[]={$nb['id']}";
                        ?>
                        <div class="city-card">
                            <div class="name"><?=$nb['city_name']?> – <?=$nb['name']?></div>
                            <div class="img"><img src="<?=$img?>"></div>
                            <div class="info">
                                <div class="row">
                                    <div class="col-md-8 listings"><?=  number_format($listings['cnt'],0,'.',',')?> listings</div>
                                    <div class="col-md-4 text-right"><a href="<?=$url?>" class="btn btn-warning">More details</a></div>
                                </div>
                            </div>
                        </div>
                    <?}?>                    
                </div>
                
                <div class="col-md-6">
                    <div class="h3"><span>Miami</span></div>
                    <?
                        $nbs = mysql_select("
                                SELECT nb.id, nb.name, nb.url, nb.city, city.name city_name, city.url city_url                                     
                                FROM `shop_neighborhoods` AS nb 
                                LEFT JOIN shop_cities AS city ON city.id = nb.city 
                                WHERE nb.city = 6 AND nb.display = 1 ORDER BY RAND() LIMIT 2");
                        foreach ($nbs as $v) {
                            $nb_ids[] = $v['id'];
                        }                                  
                        $listings = mysql_select("
                                        SELECT COUNT(sp.id) cnt, sp.zip_code  
                                        FROM shop_products AS sp 
                                        LEFT JOIN shop_neighborhoods_zips AS zips ON zips.nb_id IN (".implode(',', $nb_ids).")  
                                        WHERE sp.display = 1 AND sp.zip_code = zips.zip                                         
                                        GROUP BY (sp.id) 
                                     ",'row');
                        // 
                        //var_dump($listings);
                    ?>
                    <? foreach ($nbs as $nb){
                        $img = ($nb['img']) ? "/files/shop_neighborhoods/{$nb['id']}/img/m-{$nb['img']}" : "/templates/images/tmp1.png";
                        $url = "/{$modules['shop']}/?location[]={$nb['id']}";
                        ?>
                        <div class="city-card">
                            <div class="name"><?=$nb['city_name']?> – <?=$nb['name']?></div>
                            <div class="img"><img src="<?=$img?>"></div>
                            <div class="info">
                                <div class="row">
                                    <div class="col-md-8 listings"><?=  number_format($listings['cnt'],0,'.',',')?> listings</div>
                                    <div class="col-md-4 text-right"><a href="<?=$url?>" class="btn btn-warning">More details</a></div>
                                </div>
                            </div>
                        </div>
                    <?}?>
                </div>
            </div>
        </div>
    </div>
    
    <div id="pluses" class="bg-g">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header">Wilk Real Estate are:</div> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="plus">
                        <div class="name"><?=i18n('plus|one')?></div>
                        <div class="text"><?=i18n('plus|one_desc')?></div>
                    </div> 
                    <div class="plus">
                        <div class="name"><?=i18n('plus|two')?></div>
                        <div class="text"><?=i18n('plus|two_desc')?></div>
                    </div> 
                    <div class="plus">
                        <div class="name"><?=i18n('plus|three')?></div>
                        <div class="text"><?=i18n('plus|three_desc')?></div>
                    </div> 
                </div>
            </div>
        </div>
    </div>            
</section>
