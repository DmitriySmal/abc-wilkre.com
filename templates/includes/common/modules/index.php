<section id="index">
    <div class="container">
        <div class="row" style="padding-bottom:10px">
            <a target="_blank" href="https://www.dos.ny.gov/licensing/docs/FairHousingNotice_new.pdf" class="btn btn-warning">FAIR HOUSING NOTICE</a>
        </div>
    </div>
    <?//=html_array('common/slider')?>
    <?php /*if (@$modules['shop_ny'] AND @$modules['shop']) {*/?>
    <div class="container-fluid">                        
        <div class="row" id="cities_index">
			<div class="city col-md-12 col-md-6" style="background: url('/templates/images/ny.jpg') scroll no-repeat 50% 50% transparent; background-size: cover;">
				<div class="info">
					<span class="name">Property<br>in New York</span>
					<div class="cities_index_search"><a href="/<?=$modules['shop_mx']?>/" class="btn btn-warning">Search property</a></div>
        </div>
			</div>
			<div class="city col-md-12 col-md-6" style="background: url('/templates/images/miami.jpg') scroll no-repeat 50% 50% transparent; background-size: cover;">
				<div class="info">
					<span class="name">Property<br>in Florida</span>
					<div class="cities_index_search"><a href="/<?=$modules['shop_mls10']?>/" class="btn btn-success">Search property</a></div>
				</div>
			</div>
	        <?php /*
            <div class="col-md-12" id="cities_images">
                <div class="cities_blocks">
                    <div class="city_img" style="background: url('/templates/images/ny.jpg') scroll no-repeat 50% 50% transparent; background-size: cover;" >
                        <!--img src="/templates/images/ny.jpg"-->
                        <div class="info">
                            <span class="name">Property<br>in New York</span>
                            <a href="/<?=$modules['shop_ny']?>/" class="btn btn-warning">Search property</a>
                        </div>
                    </div>
                    <div class="city_img" style="background: url('/templates/images/miami.jpg') scroll no-repeat 50% 50% transparent; background-size: cover;">
                        <div class="info">
                            <span class="name">Property<br>in Florida</span>
                            <a href="/<?=$modules['shop']?>/" class="btn btn-success">Search property</a>
                        </div>
                    </div>
                </div>
            </div>
 */ ?>
        </div>
    </div>
	<?php /* } */ ?>
	<div class="container">
        <div class="row" style="padding-top:30px">
            <?php
            $query = "
        SELECT sp.*,
            neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
        FROM shop_products AS sp
        LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
        WHERE sp.display = 1 AND sp.special=1 AND sp.base IN (6,7)
        ORDER BY sp.rank DESC, sp.date_change DESC
        LIMIT 12
    ";
            echo html_query('shop/product_list', $query, false, 60 * 60);
            ?>
            <div class="text-center">
                <a href="/<?=$modules['shop_mx']?>/?count=all">Show All Properties in New York</a>
            </div>
        </div>

		<div class="row">
			<div class="col-md-12">
				<?=$page['text']?>
			</div>
		</div>
	</div>
    
    <!--div class="container">
        <div class="row">
            <div class="col-md-12">
                <?//=html_array('common/widgets/search')?>
            </div>
        </div>
    </div-->
    
    <?
    //    $where2 = ($state['id']!=9999) ? " AND sp.city IN (".implode(', ', $_cities_ids).")" : '';
    /*?>
    <?=html_array('sliders/slider_properties',array( // slider2 просто слайдер, slider3 недвижка
        'header'=>i18n('slider|new_arrivals'),
        'path' => 'shop_products',        
    ))
    ?>
    
    <?=html_array('sliders/slider_properties',array( // slider2 просто слайдер, slider3 недвижка
        'header'=>i18n('slider|featured_properties'),
        //'items' => array(),//mysql_select(),
        'path' => 'shop_products',
        'featured' => true
    ))?>
    <?php /*
    <?=html_array('common/widgets/subscribe1')*/?>
    
    <?
        //$where3 = ($state['id']!=9999) ? " AND sp.city IN (".implode(', ', $_cities_ids).")" : '';
    /*?>
    <?=html_array('sliders/slider_condos',array( // slider2 просто слайдер, slider3 недвижка
        'header'=>i18n('slider|condos'),
        'items' => mysql_select("
                        SELECT sp.*, city.name city_name, city.url city_url, state.url state_url         
                        FROM shop_condos AS sp 
                        LEFT JOIN shop_cities AS city ON city.id = sp.city  
                        LEFT JOIN shop_states AS state ON state.id = city.state  
                        WHERE sp.display = 1 AND sp.img != '' ORDER BY sp.id DESC LIMIT 6
                    "),
        'path' => 'shop_condos',        
    ))?>
    <?php /*
    <?=html_array('sliders/slider_condos',array( // slider2 просто слайдер, slider3 недвижка
        'header'=>i18n('slider|condos_preconstruction'),
        'items' => mysql_select("
                        SELECT sp.*, city.name city_name, city.url city_url, state.url state_url    
                        FROM shop_condos AS sp 
                        LEFT JOIN shop_cities AS city ON city.id = sp.city  
                        LEFT JOIN shop_states AS state ON state.id = city.state  
                        WHERE sp.display = 1 AND sp.type=0 AND sp.img != '' {$where3} ORDER BY sp.id DESC LIMIT 6 
                    "),
        'path' => 'shop_condos',        
    ))?>*/?>

	<?php /*
    <div id="pluses" class="bg-g">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header">Wilk Real Estate are:</div> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="plus">
                        <div class="name"><?=i18n('plus|one')?></div>
                        <div class="text"><?=i18n('plus|one_desc')?></div>
                    </div> 
                    <div class="plus">
                        <div class="name"><?=i18n('plus|two')?></div>
                        <div class="text"><?=i18n('plus|two_desc')?></div>
                    </div> 
                    <div class="plus">
                        <div class="name"><?=i18n('plus|three')?></div>
                        <div class="text"><?=i18n('plus|three_desc')?></div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
 */?>
</section>
