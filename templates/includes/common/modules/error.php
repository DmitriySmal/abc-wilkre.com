<div class="container">
	<div class="row">
		<div class="col-md-12" style="padding-bottom:100px">
			<div class="page-header"><?=i18n('common|str_no_page_name')?></div>
			<?=i18n('common|txt_no_page_text')?>

			<div class="page-header">Featured Properties</div>
<?php
		$where = ' AND sp.special=1';
		$select = '';
		//$select = ', IF (property_type=1 && property_type2=4,1,0) sort ';
		//$select.= ", IF (status='Contract',0,1) sort2 ";
		//$order_by = ' sort2 DESC, sort DESC,';
				$order_by = ' rank DESC,';

		$query = "
			SELECT sp.* ".$select." ,
			neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
			FROM shop_products AS sp
			LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
			WHERE sp.display = 1 AND (sp.base=6 OR sp.base=7) {$where}
			ORDER BY {$order_by} sp.special DESC, sp.date DESC
		";

		echo html_query('shop/product_list shop', $query, false, 60 * 60);
?>

		</div>
	</div>
</div>