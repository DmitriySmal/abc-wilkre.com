<section id="agents">
    <div class="container">
        
        <?=html_array('common/widgets/nbs');?>
        
        <?if(@$page['header']){?>
            <div class="row">
                    <div class="col-md-10">                    
                        <div class="page-header">
                            <?=$page['header']?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <?//кнопка?>
                    </div>
            </div>
        <?}?>
        
        <?if($u[2]){
            $img = ($page['img']) ? "/files/agents/{$page['id']}/img/p-{$page['img']}" : "/templates/images/agent-empty.png"
            ?>
            <div class="row">
                <div class="col-md-3">
                    <img class="agent-avatar" src="<?=$img?>">
                </div>
                <div class="col-md-9">
                    <div class="agent-name">
                        <?=@$page['name']?>
                    </div>
                    <?if(@$page['phone']){?>
                        <div class="agent-phone">
                            <?=i18n("agents|phone")?> <?=$page['phone']?> 
                        </div>
                    <?}?>
                    <?if(@$page['email']){?>
                        <div class="agent-email">
                            <a href="mailto:<?=$page['email']?>"><?=$page['email']?></a> 
                        </div>
                    <?}?>
                    <div class="agent-text">
                        <?=@$page['text']?>
                    </div>
                </div>
            </div>
        <?}?>
        
        <?if(@$html['content']){?>
            <div class="row">
                <div class="col-md-12">
                    <div class="h3"><span><?=i18n('agents|my_agents')?></span></div>
                </div>
            </div>
        <?}?>
        
        <div class="row">
            <?=@$html['content']?>
        </div>                
    </div>        
</section>

<?=html_array('common/widgets/subscribe2')?>