<section id="feedback" style="">
    <div class="container">
        
        <?=html_array('common/widgets/nbs');?>
        
        <div class="row">
            <div class="col-md-12">
                <?=($breadcrumb) ? html_array('common/breadcrumb',$breadcrumb) : '' ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-8">
                <?=(@$html['content'] != @$page['text']) ? @$page['text'] : ''?>
            </div>
            <div class="col-md-4">
                <?=@$html['content']?>
            </div>
        </div>        
    </div>
</section>

<?//=html_array('common/widgets/calc', array('price'=>27000))?>