<section id="shop">
    <div class="container">        
        <?php
        echo html_array('common/widgets/nbs');
        ?>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?=($breadcrumb) ? html_array('common/breadcrumb',$breadcrumb) : '' ?>
            </div>
        </div>
    </div>

	<?php /* todo Для каждого млс свой фильтр*/  ?>

    <?php if(@$html['is_product']){
        //$bimg = (@$page['bimg']) ? "/files/pages/{$page['pid']}/bimg/{$page['bimg']}" : '/templates/images/bimg.jpg';
        $bimg = "/templates/images/fl-title-bg.jpg";
        ?>                    
        <div class="bimg" style="background-image: url('<?=$bimg?>')">
            <div class="container"> 
                <div class="row">
                    <div class="col-md-12">                        
                        <a href="<?=(@$_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER'] : "/{$u[1]}/" ?>" title="Go Back" class="btn btn-white-bordered"><span class="glyphicon glyphicon-menu-left"></span> Back</a>
                        <a id="refine_search" href="#" data-toggle="dropdown" data-target="#search-offers" class="btn btn-white-bordered"><span class="glyphicon glyphicon-search"></span> <span id="refine_text">Refine search</span></a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

	<div class="container">
        <div class="row">
            <div class="col-md-12">
                <?=$html['search']?>
            </div>
        </div>
    </div>


    
    <div class="container">
	    <?if(@$html['filter']){?>
	        <div class="row">
	            <div class="col-md-12">
	                <?php /*<div class="page-header">
	                    <?=@$page['name']?>
	                    <?if(@$page['subname']){?>
	                         <span><?=$page['subname']?></span>
	                    <?}?>
	                </div> */?>
		            <h1><?=@$page['name']?></h1>
		            <?if(!$html['is_product'] AND @$page['text']){?>
			            <div class="page-text" style="padding-bottom:15px">
				            <?=@$page['text']?>
			            </div>
		            <?}?>
	            </div>
	            <!--div class="col-md-2 text-right">
	                <a id="new_search" href="#" class="btn btn-warning" style="margin-top: 25px; display: inline-block;">New Search</a>
	            </div-->
	        </div>

            <div class="row">
                <div class="col-md-12">                    
                    <?=$html['filter']?>                    
                </div>
            </div>
        <?}?>


        
        <?if(!$html['is_product']){//TODO: сделать по человечески, убрать костыль :)?>
            <div id="listings">            
                <?=@$html['content']?>
            </div>
        <?}else{?>
            <?=@$html['content']?>
        <?}?>
        

    </div>
</section>


    <?=@$html['source']!= 'subscribe'?html_array('common/widgets/subscribe2'):''?>


