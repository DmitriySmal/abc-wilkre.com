<?php
$page['title'] = seo_title($page);
$page['title']			= isset($page['title']) ? filter_var($page['title'], FILTER_SANITIZE_STRING) : filter_var($page['name'],FILTER_SANITIZE_STRING);
$page['description']	= isset($page['description']) ? filter_var($page['description'], FILTER_SANITIZE_STRING) : $page['title'];
$page['keywords']		= isset($page['keywords']) ? filter_var($page['keywords'], FILTER_SANITIZE_STRING) : $page['title'];
?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title><?=$page['title']?></title>
	<meta name="description" content="<?=$page['description']?>" />
	<meta name="keywords" content="<?=$page['keywords']?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400italic,700%7CRoboto:300,400,700,300italic" rel="stylesheet" type="text/css">
	<?=html_sources('return','landing.css jquery bootstrap.js')?>
	<?=i18n('common|txt_meta')?>
	<?=html_sources('head')?>
</head>

<body>
<div class="page page-complex text-center">
	<div id="wrap">

	<?=html_array('landing/object',$product)?>

		<footer class="page-footer">
			<div class="well-md bg-default">
				<div class="container text-sm-left">
					<div class="row">
						<div class="col-sm-6">
							<h5>Main office:</h5>
							<address class="contact-info">
								Address: 626 Ave. U, Brooklyn NY, 11223
								<br>New York Phone: 718-376-0606
								<br>New York Fax: 718-376-0073
							</address>
							<p>E-mail: <a href="mailto:info@wilkrealestate.com">info@wilkrealestate.com</a></p>
							<p>110 Wall Street, 2-037, New York,NY 10005</p>
						</div>
						<div class="col-sm-6">
							<h5>Follow us</h5>
							<ul class="inline-list inline-list-xs">
								<li><div class="icon-xs icon-primary"><a class="icon fa fa-facebook" href="https://www.facebook.com/wilkre/"></a></div></li>
								<li><div class="icon-xs icon-primary"><a class="icon fa fa-twitter" href="https://twitter.com/wilkre "></a></div></li>
								<li><div class="icon-xs icon-primary"><a class="icon fa fa-youtube" href="https://www.youtube.com/channel/UC5DJ60_YX2oQRo8savqHVjw "></a></div></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-default well-xs">
				<div class="container text-sm-left">
					<span class="small">
			            Wilk Real Estate © <span id="copyright-year"><?=date('Y')?></span>. All Rights Reserved
			      </span>
				</div>
			</div>
		</footer>
	</div>
	<!-- /#wrap -->
</div>
<?=html_sources('footer')?>
<?=html_sources('return','landing.js jquery_form jquery_validate')?>
</body>
</html>