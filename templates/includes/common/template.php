<?php
$page['title']			= isset($page['title']) ? filter_var($page['title'], FILTER_SANITIZE_STRING) : filter_var($page['name'],FILTER_SANITIZE_STRING);
$page['description']	= isset($page['description']) ? filter_var($page['description'], FILTER_SANITIZE_STRING) : $page['title'];
$page['keywords']		= isset($page['keywords']) ? filter_var($page['keywords'], FILTER_SANITIZE_STRING) : $page['title'];
?><!DOCTYPE html>
<html lang="<?=$lang['url']?>">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title><?=$page['title']?></title>
<meta name="description" content="<?=$page['description']?>" />
<meta name="keywords" content="<?=$page['keywords']?>" />
<meta property="og:title" content="<?=$page['title']?>" />
<meta property="og:description" content="<?=$page['description']?>" />
<?php
if (@$page['og:type']=='') @$page['og:type'] = 'website';
if (@$page['og:type']) {?>
<meta property="og:type" content="<?=$page['og:type']?>" />
<?php } if (@$page['og:url']) {?>
<meta property="og:url" content="https://<?=$_SERVER['SERVER_NAME']?><?=$page['og:url']?>" />
<?php } if (@$page['og:image']) {?>
<meta property="og:image" content="https://<?=$_SERVER['SERVER_NAME']?><?=$page['og:image']?>" />
<?php } ?>
<?php
//v1.2.31 - canonical,next,prev
$page['canonical'] = @$html['canonical'];
if (@$page['canonical'] AND $page['canonical']==$_SERVER['REQUEST_URI']) unset($page['canonical']);
foreach (array('canonical','next','prev') as $k=>$v) {
    if (@$page[$v]) echo '<link rel="'.$v.'" href="'.$config['http'].'://'.$config['domain'].$page[$v].'" />';
    }
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
    if (in_array($_SERVER['HTTP_HOST'],array('www.wilkrealestate.ru'))) {?>
        <meta name="robots" content="noindex, nofollow">
	<?php } ?>
	<?php
	if (@$page['noindex']) {?>
        <meta name="robots" content="noindex, follow">
	<?php } ?>
	<style>
		<?php
		if (@$html['critical']) {
			include(ROOT_DIR.'templates/css/'.$html['critical'].'.css');
		}
		else {
			include(ROOT_DIR.'templates/css/critical_main.css');
		}
		?>
	</style>
<?=i18n('common|txt_meta')?>
	<meta name="google-play-app" content="app-id=com.wilkrealestate.wilkreal">
</head>

<body>

    <header>
        <div class="bg-dark">
            <div class="container">
                <div class="row hidden-xs">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 menu_normal">
	                    <div class="pull-left"><?= html_query('menu/list4', "
                            SELECT id,name,url,module,level
                            FROM pages
                            WHERE display=1 AND menu4 = 1
                            ORDER BY left_key
                        ", '', 60 * 60, 'json') ?></div>
	                    <?php if (in_array($_SERVER['HTTP_HOST'],array('www.wilkrealestate.ru'))) {?>
		                    <div id="google_translate_element" class="pull-left"></div>
		                    <script type="text/javascript">
			                    function googleTranslateElementInit() {
				                    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'ru', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-55114198-21'}, 'google_translate_element');
			                    }
		                    </script>
		                    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
	                    <?php } ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
                        <?= html_query('menu/list2', "
                            SELECT id,name,url,module,level
                            FROM pages
                            WHERE display=1 AND menu2 = 1
                            ORDER BY left_key
                        ", '', 60 * 60, 'json') ?>
                        <span class="phone-top"><?=i18n('common|phone_top')?></span> <a href="#" data-trigger="callback" data-toggle="modal" data-target="#feedback" class="btn btn-white-bordered"><?=i18n('common|call_back')?></a>
                    </div>                         
                </div>
				<div class="row visible-xs menu_mobile">
					<div class="pull-right" style="padding:10px 0 0">
						<span class="phone-top"><?=i18n('common|phone_top')?></span>
					</div>

					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse">
						<?= html_query('menu/list_mobile', "
                            SELECT id,name,url,module,level
                            FROM pages
                            WHERE display=1 AND (menu4 = 1 OR menu2 = 1 OR menu=1)
                            ORDER BY left_key
                        ", '', 60 * 60, 'json') ?>
					</div>
				</div>
            </div>
        </div>
        <div class="container">            
            <div class="row">
                <div class="col-md-4" style="padding-right:0px">
                    <?if(@$html['module']!='index'){?>
                    <a href="/" title="<?=i18n('common|site_name')?>" ><img class="img" src="/templates/images/logo3.png" alt="Albert Wilk broker of Wilk RealEstate Ltd. New-York and Florida licences"></a>
                    <?} else {?>
                        <img class="img" src="/templates/images/logo3.png" alt="Albert Wilk broker of Wilk RealEstate Ltd. New-York and Florida licences">
                    <?}?>
	                <div style="padding:3px 0 0; font-size:13px; color:#999">Since 1987</div>
	                <div style="font-size:13px; overflow:hidden"><?=i18n('common|txt_head')?></div>
                </div>
                <div class="col-md-8 text-right hidden-xs" style="padding-left:0px">
                    <?=isset($html['submenu']) ? $html['submenu'] :
                    html_query('menu/list', "
                        SELECT id,name,url,module,level
                        FROM pages
                        WHERE display=1 AND menu = 1 AND level=1
                        ORDER BY left_key
                    ", '', 60 * 60, 'json')?>
                </div>                         
            </div>
        </div>
    </header>
    <!--div class="container">
        <div class="row">
            <div class="col-md-12">
                <input type="checkbox" class="switch" />                
            </div>
        </div>        
    </div-->
    <?/*if($html['module']!='index'){
        $bimg = (@$page['bimg']) ? "/files/{$html['module']}/{$page['id']}/bimg/{$page['bimg']}" : '/templates/images/bimg.jpg';
        ?>
        <div class="bimg" style="background-image: url('<?=$bimg?>')"></div>
    <?}*/?>
    
    <? //echo '4-'.$_GET['city'].'<br>';
    //if (access('user admin')) echo $html['module'];
        $module_template = ROOT_DIR . $config['style'] . '/includes/common/modules/' . @$html['module'] . '.php';
        if (is_file($module_template))
            include($module_template);
        else
            include(ROOT_DIR . $config['style'] . '/includes/common/modules/default.php');
    //echo '5-'.$_GET['city'].'<br>';
    ?>
    <footer class="bg-dark-blue">
        <?
            $modules['shop'] = isset($modules['shop']) ? $modules['shop'] : 'catalog';
        ?>
        <?php
        //на главной выводим список для НЙ
        if (@$html['module']=='index') $u[1]=$modules['shop_mx'];
        if (1==2 AND @$modules['shop_mx'] AND @$modules['shop']) {
	        if (@$html['source'] != 'subscribe' AND in_array($u[1], array($modules['shop_mx'],$modules['shop'], $modules['shop_ny']))) {
		        ?>
		        <div id="offers-list" class="bg-blue">
			        <?
			        $cities = mysql_select("
						SELECT * FROM shop_cities WHERE display = 1 AND id!=12 ORDER BY rank, name ASC
					", 'rows');
			        if ($u[1] == $modules['shop_mx']) {
				        $active_city_id = 7;
				        if (@$_GET['city'] > 0) $active_city_id = intval($_GET['city']);
			        } else $active_city_id = 6;
			        //echo $_GET['city'].$active_city_id;
			        ?>
			        <form method="get" action="/<?= $active_city_id == 6 ? $modules['shop'] : $modules['shop_mx'] ?>/">
				        <div class="container">
					        <div class="row">
						        <div class="col-md-12">
							        <ul id="cities-tab">
								        <?foreach ($cities as $city) {
									        ?>
									        <li class="<?= ($city['id'] == $active_city_id) ? 'active' : '' ?>"><a
											        href="#" data-id="<?= $city['id'] ?>"
											        data-action="/<?= $city['id'] == 6 ? $modules['shop'] : $modules['shop_mx'] ?>/"><?= $city['name'] ?></a>
									        </li>
								        <?
								        }?>
							        </ul>
						        </div>
					        </div>
					        <div class="row">
						        <div class="col-md-12" id="types-tab">
							        <ul data-id="1" <?= $active_city_id == 6 ? '' : 'style="display:none"' ?>>
								        <?php foreach ($config['object_groups'] as $k => $v) if ($k != 3) { ?>
									        <li <?= ($u[1] == $modules['shop'] AND @$_GET['property_type'] == $k) ? ' class="active"' : '' ?>>
										        <a href="#" data-id="<?= $k ?>"><?= $v['name'] ?></a></li>
								        <?php } ?>
							        </ul>
							        <ul data-id="2" <?= $active_city_id != 6 ? '' : 'style="display:none"' ?>>
								        <?php foreach ($config['ny_property_types'] as $k => $v) { ?>
									        <li <?= ($u[1] == $modules['shop_mx'] AND @$_GET['property_type'] == $k) ? ' class="active"' : '' ?>>
										        <a href="#" data-id="<?= $k ?>"><?= $v ?></a></li>
								        <?php } ?>
							        </ul>
						        </div>
					        </div>
					        <div class="row" id="regions-list">
						        <div class="col-md-12">
							        <?php foreach ($cities as $k => $v) {
								        $neighborhoods = mysql_select("
								SELECT *
								FROM shop_neighborhoods
								WHERE display = 1 AND city=" . $v['id'] . " ORDER BY rank DESC, name ASC
								");
								        ?>
								        <ul data-city="<?= $v['id'] ?>" <?= $v['id'] == $active_city_id ? '' : 'style="display:none"' ?>>
									        <? foreach ($neighborhoods as $neighborhood) { ?>
										        <li>
											        <a <?= (in_array($u[1], array($modules['shop'], $modules['shop_mx'])) AND is_array(@$_GET['location']) AND in_array($neighborhood['id'], $_GET['location'])) ? ' style="display:inline-block; margin:-2px -5px; padding:2px 5px; background: #205484; border-radius:3px;"' : '' ?>href="#offers-list"
											           data-id="<?= $neighborhood['id'] ?>"
											           data-city="<?= $neighborhood['city'] ?>"><?= $neighborhood['name'] ?></a>
										        </li>
									        <? } ?>
								        </ul>

							        <?php } ?>
						        </div>
					        </div>
				        </div>
				        <input type="hidden" name="location[]">
				        <input type="hidden" name="city" value="<?= intval(@$_GET['city']) ?>">
				        <input type="hidden" name="property_type" value="<?= intval(@$_GET['property_type']) ?>">
			        </form>
		        </div>
	        <?
	        }
        }?>
         <div id="footer" class="bg-dark-blue">
            <div class="container">
	            <?php
                /*
	            if (@$modules['shop_mx'] AND @$modules['shop']) {
		            if (@$html['module'] == 'index') {
			            $config['state'] = 1;
			            echo html_array('shop/seo_shop');
			            $config['state'] = 2;
			            echo html_array('shop/seo_shop');
		            }
		            if (@$html['module'] != 'sitemap') {
			            echo html_array('shop/seo_shop');
			            ?>
			            <div class="col-md-12">
				            <div class="bottom-line"></div>
			            </div>
		            <?php
		            }
	            }*/
	            ?>

                <div class="row">
                    <div class="col-md-4">
                        <?=i18n('common|contact_us')?>
                    </div>

                    <div class="col-md-8">
                        <div class="h2">
                            Ask
                        </div>
                        <form id="ask" class="form-inline">
                            <div class="row">
                                <div class="col-md-6">
                                    <input class="form-control col-md-6" name="name" placeholder="Name">
                                </div>
                                <div class="col-md-6">
                                    <input class="form-control col-md-6" name="email" placeholder="E-mail">
                                </div>                            
                            </div>
                            <textarea class="form-control col-md-12" name="text" placeholder="Question"></textarea>
	                        <input type="hidden" name="type" value="feedback">
                            <div class="row">
                                <div class="col-md-12 text-right"><button
		                                onclick="ga('send', 'event', 'QuestionFooter', 'Push'); yaCounter34569400.reachGoal('questionFooter'); return true;"
		                                class="btn btn-warning" type="submit">Send</button></div>
                            </div>
                        </form>
                    </div>

                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="bottom-line"></div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="copyrights"><?=i18n('common|copyright')?></div>
                    </div>
                    <div class="col-md-6 text-right">
                        <?= html_query('menu/list3', "
                            SELECT id,name,url,module,level
                            FROM pages
                            WHERE display=1 AND menu3 = 1
                            ORDER BY left_key
                        ", '', 60 * 60, 'json') ?>
                    </div>
                </div>
            </div>
        </div>                
    </footer>
    <?=  html_array('common/modals')?>
    <?=i18n('common|counters')?>
    <?=html_sources('return','css_reset bootstrap.css css_common')?>
    <?=html_sources('return',' jquery jquery_smartbanner jquery_ui jquery_ui_style bootstrap.js bootstrap_validator bootstrap_switch bootstrap_range_slider bootstrap_multiselect bootstrap_chosen swiper purl')?>
    <?=html_sources('return','script_common')?>
    <?=html_sources('return','highslide_gallery')?>
    <?=access('editable scripts') ? html_sources('return','tinymce editable') : ''?>
    <?
        if(access('user admin') && @$_GET['show_queries']) {
            //var_dump($config['queries']);
            echo count($config['queries'])." queries <br><br>";
            $i = 1;
            /*foreach($config['queries'] as $k=>$v){
                echo "<li>".$i++.". <span style='color:red;'>{$k}</span> - {$v}</li>";//.$v .nl2br("\r\n");
            }*/
            dd($config['queries']);
        }
    ?>
</body>
</html>