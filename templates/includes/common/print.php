<?php
$page['title']			= isset($page['title']) ? filter_var($page['title'], FILTER_SANITIZE_STRING) : filter_var($page['name'],FILTER_SANITIZE_STRING);
$page['description']	= isset($page['description']) ? filter_var($page['description'], FILTER_SANITIZE_STRING) : $page['title'];
$page['keywords']		= isset($page['keywords']) ? filter_var($page['keywords'], FILTER_SANITIZE_STRING) : $page['title'];
?><!DOCTYPE html>
<html lang="<?=$lang['url']?>">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title><?=$page['title']?></title>
<meta name="description" content="<?=$page['description']?>" />
<meta name="keywords" content="<?=$page['keywords']?>" />
<?php
$page['canonical'] = @$html['canonical'];
if (@$page['canonical'] AND $page['canonical']==$_SERVER['REQUEST_URI']) unset($page['canonical']);
foreach (array('canonical','next','prev') as $k=>$v) {
    if (@$page[$v]) echo '<link rel="'.$v.'" href="'.$config['http'].'://'.$config['domain'].$page[$v].'" />';
}
?>
<?=html_sources('return','css_reset jquery bootstrap css_common css_print')?>
</head>

<body>
	<div style="width:800px; margin:auto; padding-top: 40px">
		<div class="row">
			<div class="col-xs-6">
				Wilk Real Estate I, LLC
				<br>Albert Wilk licensed real estate broker New York and Florida
				<br>626 Avenue U, Brooklyn, NY 11223
			</div>
			<div class="col-xs-6 text-right">
				+1 718 376 – 0606
				<br>www.wilkrealestate.com
				<br>info@wilkrealestate.com
			</div>
		</div>
		<script>
    <?php if (@$config['not_print']=='') {?>
	    $(window).load(function () {
		    print();
	    });
	 <?php } ?>
</script>
<?=@$html['content'];?>
	</div>
</body>
</html>