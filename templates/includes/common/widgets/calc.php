<?
$price = (@$q['price']) ? $q['price'] : 0;
//$years = mysql_select("SELECT * FROM calc WHERE display = 1 ORDER BY rank DESC, id ASC");
$banks = mysql_select("SELECT * FROM banks WHERE display = 1 ORDER BY rank DESC, id ASC");
?>
<form class="form calc">
    <div class="form-group">
        <label for="price">Enter the price</label>
        <input type="text" class="form-control" id="price" placeholder="Enter the price" name="price" value="<?=$price?>">
    </div>
    <div class="form-group">
        <label for="bank">Select bank</label>
        <select class="form-control" name="bank">
            <option value="0">- select bank -</option>
            <?foreach($banks as $b){?>
                <option value="<?=$b['id']?>"><?=$b['name']?></option>
            <?}?>
        </select>
    </div>
    <div class="form-group">
        <label for="priod">Select period</label>
        <select class="form-control" name="period">
            <option value="0">- select period -</option>        
        </select>
    </div>
    <div class="h1 result">0</div>
</form>

<script>
    $("form.calc").on('submit',function(){
        return false;
    })
    
    $("select[name=period]").on('change',function(){
        var percent = $(this).val(),
            price = $(this).closest('form').find("input[name=price]").val();
            result = 0;                
        percent = parseFloat(percent);
        price = parseInt(price);
        result = (price / 100 * percent) + price;
        $(this).closest('form').find('.result').html(result);
    });
    
    $("select[name=bank]").on('change',function(){
        var banks = $(this);
        var percents = $(banks).closest('form').find("select[name=period]");
        $.ajax({
            url: "/ajax.php",
            data: {
                file: 'bank_percents',
                bank: $(banks).val(),
            },
            type: 'get',
            dataType: 'json',
            beforeSend: function(){
                $(banks).closest('form').find('.result').html(0);
                $(percents).find('option').slice(1).remove();
            },
            success: function(json){                
                if(json.result)
                {
                    $.each(json.data, function(i, v) {
                        $(percents).append($('<option>').text(v.name).attr('value', v.percent));
                    });
                }
            }
        });
    });
</script>