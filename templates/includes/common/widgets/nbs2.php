<?php
$cities = (isset($q['city_id'])) ? mysql_select("SELECT c.id, c.name, c.url, s.url state_url FROM shop_cities AS c LEFT JOIN shop_states AS s ON s.id = c.state WHERE c.id = {$q['city_id']} AND c.display = 1 LIMIT 1",'rows',60*60) : mysql_select("SELECT c.id, c.name, c.url, s.url state_url FROM shop_cities AS c LEFT JOIN shop_states AS s ON s.id = c.state WHERE c.display = 1 ORDER BY c.state ASC",'rows',60*60);
$neighborhoods = array();


if(count($cities)){
    foreach ($cities as $city){
        //$nbs = mysql_select("SELECT * FROM shop_neighborhoods WHERE city = {$city['id']} AND display = 1",'rows',60*60);
        //$nbs = mysql_select("SELECT sn.* FROM shop_condos AS sc LEFT JOIN shop_neighborhoods AS sn WHERE sc.neighborhood > 0 AND sn.city = {$city['id']} AND sc.display = 1 GROUP BY sn.id",'rows',60*60);    
        $where = ($u[2]=='pre-construction') ? " AND sc.type = 0 " : " AND sc.type = 1 ";
        $nbs = mysql_select("SELECT sn.* FROM shop_condos AS sc LEFT JOIN shop_neighborhoods AS sn ON sn.id = sc.neighborhood WHERE sc.neighborhood > 0 AND sn.city = {$city['id']} AND sc.display = 1 {$where} GROUP BY sn.id",'rows',60*60);
        //var_dump($nbs);
        $neighborhoods[$city['id']] = array_chunk($nbs, 1);
    }        
}?>

<div id="widget-nbs2" class="condos-nbs" style="<?=(@$_GET['location']?"":"display: block;")?>">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header" style="margin-bottom:0px">Select the neighborhoods</div>
        </div>
        <div class="col-md-12">
            <div class="tab-content">
                <?foreach ($cities as $k=>$city){?>
                    <div role="tabpanel" class="tab-pane fade <?= (!@$_GET['city']) ? ((!$k)?'active in':'') : (($_GET['city']==$city['id']) ?'active in':'')?>" id="city<?=$city['id']?>">
                        <div class="row">
                            <?foreach($neighborhoods[$city['id']] as $nbs){?>
                                <div class="col-md-2">
                                    <ul class="nbs-ul">
                                        <?foreach($nbs as $nb){?>
                                            <li class="<?=((@$_GET['location'] && $_GET['location'][0] == $nb['id'] ) ? 'active':'')?>"><a href="/<?=$city['state_url']?>/<?=$modules['condos']?><?=(@$u[2])?"/{$u[2]}":''?>/?city=<?=$city['id']?>&location[]=<?=$nb['id']?>"><?=$nb['name']?></a></li>
                                        <?}?>
                                    </ul>
                                </div>
                            <?}?>
                        </div>
                    </div>            
                <?}?>
            </div>        
        </div>
    </div>
</div>

