<div class="bg-light-gray-pattern widget-subscribe-1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="info">
                    If you want to get the full listing of options regarding your request, please give us <br>your email/cell phone number and we will contact you shortly.
                </div>
                <form class="form-inline">
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                        <div class="input-group">
                            <input style="width: 380px;" type="text" class="form-control input-lg col-md-5" id="email" placeholder="Your contact phone or e-mail adress">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-warning btn-lg">SEND</button>
                </form>
                <div class="thanks">
                    Thank You!
                </div>
            </div>
        </div>
    </div>
</div> 