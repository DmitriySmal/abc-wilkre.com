<? 
global $html; 
$get = $_GET;
$categories = mysql_select("SELECT * FROM shop_categories WHERE display = 1 ORDER BY id ASC",'rows');                                        
$commercial = $residental = $sell = $rent = array();
foreach($categories as $c){
    if($c['commercial']) $commercial[] = $c['id']; else $residental[] = $c['id'];
    if($c['sell']) $sell[] = $c['id']; else $rent[] = $c['id'];
}
echo html_sources('return','jquery_combo_select');
?>
<form id="search-offers" action="/<?=$state['url']?>/<?=$modules['shop']?>/" method="get" style="<?=(($html['is_product']) ? 'display: none;' :'')?>">    
    <!--div id="form-switch">
        <span data-switch-value="false" data-switch-set="state" class="switch-control active">Residental</span> <input type="checkbox" class="switch"> <span data-switch-value="true" data-switch-set="state" class="switch-control">Commercial</span>
    </div-->

                <div class="row">                    
                    <div class="form-group col-md-6">
                        <input class="form-control" name="quick" placeholder="Enter name of property or address or zip or MLS number" value="<?//=@$_GET['quick']?>">
                    </div>
                    <div class="col-md-3">
                        <div class="switch-comres btn-group" role="group" aria-label="...">
                            <button data-state="false" type="button" style="width: 50%;" class="btn btn-default <?=(($get['type']=='residental') || (in_array($get['category'], $residental))) ? 'active' : ''?>">Residental</button>
                            <button data-state="true" type="button" style="width: 50%;" class="btn btn-default <?=(($get['type']=='commercial') || (in_array($get['category'], $commercial))) ? 'active' : ''?>">Commercial</button>                                                        
                        </div>
                        <script>    
                            function ready_widgets_search_3 (){
                                $(".switch-comres button").on('click',function(){
                                    $(this).closest(".btn-group").find('button').removeClass('active');
                                    $(this).addClass('active');
                                    var state = $(this).data('state');
                                    var categories = $('select#category');
                                    var drpdwn = $('select#category + .btn-group');            
                                    var type = (state) ? 1 : 0; 
                                    var sell = $('.switch-sellrent button.active').data('state');
                                    var visible = new Array();
                                    $(drpdwn).find('li').removeClass('active').hide();
                                    $.each($(categories).find('option'), function(i, el){
                                        if (($(el).data('commercial') == type) && ($(el).data('sell') == sell)) visible.push($(el).attr('value'));
                                    });                
                                    $.each($(drpdwn).find('li input[type=radio]'), function(i, el){            
                                        if (in_array($(el).val(), visible))
                                            $(el).closest('li').show();
                                        else
                                            $(el).closest('li').hide();
                                    });
                                    $(categories).multiselect('select',visible[0]);
                                });
                            }
                            document.addEventListener("DOMContentLoaded", ready_widgets_search_3);
                        </script>
                    </div>
                    <div class="col-md-3">
                        <div class="switch-sellrent btn-group" role="group" aria-label="...">
                            <button data-state="1" type="button" class="btn btn-default <?=(($get['deal']=='sell') && $get['category']!=6) ? 'active' : ''?>">Sales</button>
                            <button data-state="0" type="button" class="btn btn-default <?=(($get['deal']=='rent') || ($get['category']==6)) ? 'active' : ''?>">Rentals</button>                                                        
                        </div>
                        <input type="hidden" name="for_sale" value="0">
                        <script>    
                            function ready_widgets_search_2(){
                                //TODO: объеденить в одну функцию без копипаста
                                $(".switch-sellrent button").on('click',function(){
                                    var state = $(this).data('state');
                                    $(this).closest("form").find('input[name=for_sale]').val(state);
                                    $(this).closest(".btn-group").find('button').removeClass('active');
                                    $(this).addClass('active');                                        
                                    var categories = $('select#category');
                                    var drpdwn = $('select#category + .btn-group');            
                                    var type = (state) ? 1 : 0; 
                                    var commercial = $('.switch-comres button.active').data('state');
                                    var visible = new Array();
                                    $(drpdwn).find('li').removeClass('active').hide();
                                    $.each($(categories).find('option'), function(i, el){
                                        if (($(el).data('sell') == type) && ($(el).data('commercial') == commercial)) visible.push($(el).attr('value'));
                                    });                
                                    $.each($(drpdwn).find('li input[type=radio]'), function(i, el){            
                                        if (in_array($(el).val(), visible))
                                            $(el).closest('li').show();
                                        else
                                            $(el).closest('li').hide();
                                    });
                                    $(categories).multiselect('select',visible[0]);
                                    
                                    // параметры цен
                                    var s_min = $('select[name=price_min]');
                                    var s_max = $('select[name=price_max]');
                                    var micro = {
                                        500 : '$500',
                                        750 : '$750',
                                        1000 : '$1,000',
                                        1250 : '$1,250',
                                        1500 : '$1,500',
                                        1750 : '$1,750',
                                        2000 : '$2,000',
                                        2250 : '$2,250',
                                        2500 : '$2,500',
                                        2750 : '$2,750',
                                        3000 : '$3,000',
                                        3500 : '$3,500',
                                        4000 : '$4,000',
                                        4500 : '$4,500',
                                        5000 : '$5,000',
                                        7500 : '$7,500',
                                        10000 : '$10,000',
                                        12500 : '$12,500',
                                        15000 : '$15,000',
                                        20000 : '$20,000'                                        
                                    };
                                    
                                    var macro = {                                        
                                        10000 : '$10,000',
                                        20000 : '$20,000',
                                        30000 : '$30,000',
                                        40000 : '$40,000',                                        
                                        50000 : '$50,000',
                                        60000 : '$60,000',
                                        70000 : '$70,000',
                                        80000 : '$80,000',
                                        90000 : '$90,000',
                                        100000 : '$100,000',
                                        150000 : '$150,000',
                                        200000 : '$200,000',
                                        300000 : '$300,000',
                                        400000 : '$400,000',
                                        500000 : '$500,000',
                                        600000 : '$600,000',
                                        700000 : '$700,000',
                                        800000 : '$800,000',
                                        900000 : '$900,000',
                                        1000000 : '$1M',
                                        2000000 : '$2M',
                                        3000000 : '$3M',
                                        4000000 : '$4M',
                                        5000000 : '$5M',
                                        10000000 : '$10M',
                                        20000000 : '$20M',
                                        30000000 : '$30M',
                                        40000000 : '$40M',
                                        50000000 : '$50M',
                                        999999999 : '$50M+',
                                    };
                                    if(state==1){ // макросуммы
                                        $(s_min).html('');
                                        $(s_max).html('');
                                        $(s_min).append('<option value="0">select</option>');
                                        $(s_max).append('<option value="0">select</option>');
                                        $.each(macro,function(key,value){                                            
                                            $('<option>',{text:value, value:key}).appendTo(s_min);
                                            $('<option>',{text:value, value:key}).appendTo(s_max);
                                        });
                                        $(s_min).multiselect('rebuild');
                                        $(s_max).multiselect('rebuild');
                                    } else { // микросуммы                                        
                                        $(s_min).html('');
                                        $(s_max).html('');
                                        $(s_min).append('<option value="0">select</option>');
                                        $(s_max).append('<option value="0">select</option>');
                                        $.each(micro,function(key,value){                                            
                                            $('<option>',{text:value, value:key}).appendTo(s_min);
                                            $('<option>',{text:value, value:key}).appendTo(s_max);
                                        });
                                        $(s_min).multiselect('rebuild');
                                        $(s_max).multiselect('rebuild');
                                    }
                                });
                            }
                            document.addEventListener("DOMContentLoaded", ready_widgets_search_2);
                        </script>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="city">City:</label>
                        <?
                        $where_state = ($state['id'] != '9999') ? " AND state = {$state['id']}" : '';
                        $cities = mysql_select("SELECT * FROM shop_cities WHERE display = 1 {$where_state} ORDER BY rank DESC, name ASC",'rows');                                        
                        ?>
                        <!--label for="city">City:</label-->
                        <!--input type="text" class="form-control" id="city" placeholder="Select city"-->
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('#city').multiselect({
                                    //enableFiltering: true,
                                    //filterBehavior: 'value'
                                });
                                
                                $('#search-offers select[name=city]').on('change',function(){
                                    var inpt = $(this);
                                    $.ajax({
                                        url: '/ajax.php?file=neighborhoods',
                                        data: {
                                            city: $(inpt).val(),                                           
                                        },
                                        dataType:	'json',
                                        success:	function (json){
                                            if(json.data)
                                            {         
                                                var ul = $(inpt).closest('form').find('select#location');
                                                $(ul).find('option').remove();
                                                $.each(json.data,function(i,obj) {
                                                    $('<option>',{text:obj.name, value:obj.id}).appendTo(ul);                                
                                                });                                                
                                                $(ul).multiselect('rebuild');
                                            }
                                        },
                                        onComplete: function(){
                                            //$(inpt).removeAttr('disabled');
                                        }
                                    });
                                    return false;
                                });
                                
                            });
                        </script>
                        <select class="form-control" id="city" name="city">
                            <?foreach($cities as $k=>$v){?>
                                <option value="<?=$v['id']?>" <?=(($get['city'] == $v['id']) ? 'selected' :'')?>><?=$v['name']?></option>
                            <?}?>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <?
                            $nbs = (isset($cities[0]['id'])) ? mysql_select("SELECT * FROM shop_neighborhoods WHERE display = 1 AND city = {$cities[0]['id']} ORDER BY rank DESC, name ASC",'rows') : array();                                        
                        ?>
                        <label for="location">Location:</label>
                        <!--input type="text" class="form-control" id="location" placeholder="Neighbourhood / Address / Building"-->
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('#location').multiselect({
                                    enableFiltering: true,
                                    enableCaseInsensitiveFiltering: true
                                    //filterBehavior: 'value'
                                });
                            });
                        </script>
                        <select size="1" class="form-control" id="location" name="location[]" multiple="multiple">
                            <?foreach($nbs as $k=>$v){?>
                                <option value="<?=$v['id']?>" <?=((in_array($v['id'], $_GET['location'])) ? 'selected' :'')?>><?=$v['name']?></option>
                            <?}?>
                        </select>                        
                    </div>
                    <!--div class="form-group col-md-5">
                        <label for="location">Location:</label>
                        <input type="text" class="form-control" id="location" placeholder="Neighbourhood / Address / Building">
                    </div-->
                    <div class="form-group col-md-4">
                        <label for="type">Property Type:</label>
                        <!--input type="text" class="form-control" id="type" placeholder="Condos\Coops"-->
                        <? /*
                        $types = mysql_select("SELECT * FROM shop_parameters WHERE id = 1",'row');                                        
                        ?>
                        <select class="form-control" id="type" name="type">
                            <option>Property type</option>
                            <?foreach(unserialize($types['values']) as $k=>$v){?>
                                <option value="<?=$k?>"><?=$v?></option>
                            <?}?>
                        </select>
                        <?*/?>
                        <script>
                            $(document).ready(function() {                                                                
                                $('#category').multiselect({                                         
                                        onChange: function(option, checked, select) {
                                            console.log($(option).val());
                                            // тут тоже достаем параметры
                                        }
                                });
                                var categories = $('select#category');
                                var drpdwn = $('select#category + .btn-group');                                            
                                var visible = new Array();
                                $(drpdwn).find('li').removeClass('active').hide();
                                $.each($(categories).find('option'), function(i, el){
                                    if ($(el).data('commercial') == 0) visible.push($(el).attr('value'));
                                });        
                                $.each($(drpdwn).find('li input[type=radio]'), function(i, el){
                                    if (in_array($(el).val(), visible))
                                        $(el).closest('li').show();
                                    else
                                        $(el).closest('li').hide();
                                });
                            });
                        </script>                        
                        <select class="form-control" id="category" name="category">                            
                            <?foreach($categories as $k=>$v){?>
                                <option value="<?=$v['id']?>" <?=(($get['category'] == $v['id']) ? 'selected' :'')?> data-commercial="<?=$v['commercial']?>" data-sell="<?=$v['sell']?>" ><?=$v['name']?></option>
                            <?}?>
                        </select>
                    </div>
                </div>                  
                <div class="row">
                    <!--div class="form-group col-md-6">                                            
                        <?
                            $prices = mysql_select("SELECT MAX(price) max_price, MIN(price) min_price FROM shop_products WHERE display = 1",'row');                                    
                            $price_values = (@$get['price']) ? explode(',',$get['price']) : array($prices['min_price'], $prices['max_price']);                                                                
                        ?>
                        <label for="range" style="margin-right: 10px;">Price range</label> 
                        <div id="range" class="input-ranges">
                            <input type="text" class="form-control text-right" id="price1" placeholder="$<?=  number_format($price_values[0],0,'.',',')?>" disabled>
                            <input type="text" class="form-control text-right" id="price2" placeholder="$<?=  number_format($price_values[1],0,'.',',')?>" disabled>
                        </div> 
                        <div>                                
                            <input id="range-slider" name="price" type="text" class="" value="" data-slider-min="<?=$prices['min_price']?>" data-slider-max="<?=$prices['max_price']?>" data-slider-step="5" data-slider-value="[<?=implode(',',$price_values)?>]" data-slider-tooltip="hide" data-slider-handle="round"/>
                            <script>
                                document.addEventListener("DOMContentLoaded", function () {
                                    $("#range-slider").slider({}).on('slide', function(e){
                                        $(this).closest(".form-group").find("#price1").val('$'+e.value[0].format());
                                        $(this).closest(".form-group").find("#price2").val('$'+e.value[1].format());
                                    });
                                });
                            </script>
                        </div>                            
                    </div--> 
                    <div class="form-group col-md-6">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="type">Min Price:</label>
                                <select class="form-control" id="price_min" name="price_min">                            
                                    <option value="0">select</option>
                                    <?foreach($config['prices_list'] as $k=>$v){?>
                                        <option value="<?=$k?>" <?=(($get['price_min'] == $k) ? 'selected' :'')?> ><?=$v?></option>
                                    <?}?>
                                </select>
                            </div>                                
                            <div class="form-group col-md-6">
                                <label for="type">Max Price:</label>
                                <select class="form-control" id="price_max" name="price_max">                            
                                    <option value="0">select</option>
                                    <?foreach($config['prices_list'] as $k=>$v){?>
                                        <option value="<?=$k?>" <?=(($get['price_max'] == $k) ? 'selected' :'')?> ><?=$v?></option>
                                    <?}?>
                                </select>
                            </div>
                            <script>
                                $('#price_min, #price_max').multiselect({});
                            </script>
                        </div>
                    </div>                    

                    <!--div class="form-group col-md-2">                                            
                        <label style="" class="" for="bedrooms">Bedrooms</label> 
                        <input type="text" class="form-control text-center" id="bedrooms" value="<?=$get['p'][25]?>" name="p[25]" placeholder="Any Beds" style="">                                                
                    </div-->
                    <div class="form-group col-md-2">
                        <label for="bedrooms">Bedrooms:</label>                        
                        <select class="form-control" id="bedrooms" name="p[25]">                            
                            <option value="0">select</option>
                            <?foreach($config['bedrooms_list'] as $k=>$v){?>
                                <option value="<?=$k?>" <?=(($get['p'][25] == $k) ? 'selected' :'')?> ><?=$v?></option>
                            <?}?>
                        </select>
                    </div> 
                    <div class="form-group col-md-2">
                        <label for="baths">Baths:</label>
                        <select class="form-control" id="baths" name="p[92]">                            
                            <option value="0">select</option>
                            <?foreach($config['baths_list'] as $k=>$v){?>
                                <option value="<?=$k?>" <?=(($get['p'][92] == $k) ? 'selected' :'')?> ><?=$v?></option>
                            <?}?>
                        </select>
                    </div>
                    <script>
                        $('#bedrooms, #baths').multiselect({});
                    </script>
                    <!--div class="form-group col-md-2">                                            
                        <label style="" for="baths">Baths</label> 
                        <input type="text" class="form-control text-center" id="baths" value="<?=$get['p'][92]?>" name="p[92]" placeholder="Any Baths" style="">                                                
                    </div-->

                    <div class="form-group col-md-2">
                        <label style="display:block;">&nbsp;</label> 
                        <button style="display: block; width: 100%;" class="btn btn-warning">SEARCH</button>
                    </div>                                            
                </div> 
</form>

<script>
    function ready_widgets_search(){
        $('input[name=quick]').on('keyup', function(){
            var word = $(this).val();
            if(word.length){
                console.log('true');
                $(this).closest('form').find('input, select, button').not(this).not('.btn-warning').attr('disabled',true);
            } else {
                console.log('false');
                $(this).closest('form').find('input, select, button').not(this).not('.btn-warning').removeAttr('disabled');
            }
        });
	   $('#price_min').comboSelect();
	    /*$('.js-select').change(function(e, v){
		    $('.idx').text(e.target.selectedIndex)
		    $('.val').text(e.target.value)
	    })
	    $('.js-select-open').click(function(e){
		    $('.js-select').focus()
		    e.preventDefault();
	    })
	    $('.js-select-close').click(function(e){
		    $('.js-select').trigger('comboselect:close')
		    e.preventDefault();
	    })
	    var $select = $('.js-select-3');
	    $('.js-select-add').click(function(){
		    $select.prepend($('<option>', {
			    text: 'A new option: ' + Math.floor(Math.random() * 10) + 1
		    })).trigger('comboselect:update')

		    return false;
	    })*/


    }
    document.addEventListener("DOMContentLoaded", ready_widgets_search);
</script>