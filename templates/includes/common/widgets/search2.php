<? 
global $html; 
$get = $_GET;
$categories = mysql_select("SELECT * FROM shop_categories WHERE display = 1 ORDER BY id ASC",'rows');                                        
$commercial = $residental = $sell = $rent = array();
foreach($categories as $c){
    if($c['commercial']) $commercial[] = $c['id']; else $residental[] = $c['id'];
    if($c['sell']) $sell[] = $c['id']; else $rent[] = $c['id'];
}

?>
<form id="search-offers" action="/<?=$state['url']?>/<?=$modules['shop']?>/" method="get" style="<?=(($html['is_product']) ? 'display: none;' :'')?>">    
    <!--div id="form-switch">
        <span data-switch-value="false" data-switch-set="state" class="switch-control active">Residental</span> <input type="checkbox" class="switch"> <span data-switch-value="true" data-switch-set="state" class="switch-control">Commercial</span>
    </div-->

                <div class="row">
                    <div class="form-group col-md-10">
                        <input class="form-control" name="quick" placeholder="Enter name of property or address or zip or MLS number" value="<?=@$_GET['quick']?>">
                    </div>
                    <div class="col-md-2">
	                    <div class="form-group">
		                    <button style="display: block; width: 100%;" class="btn btn-warning">QUICK SEARCH</button>
	                    </div>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div style="border-top: 1px solid #e6e6e6; padding-bottom:15px"></div>
					</div>
				</div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <?
                        $cities[0]['id'] = 6;
                            $nbs = (isset($cities[0]['id'])) ? mysql_select("SELECT * FROM shop_neighborhoods WHERE display = 1 AND city = {$cities[0]['id']} ORDER BY rank DESC, name ASC",'rows') : array();                                        
                        ?>
                        <label for="location">Location:</label>
                        <!--input type="text" class="form-control" id="location" placeholder="Neighbourhood / Address / Building"-->
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('#location').multiselect({
                                    enableFiltering: true,
                                    enableCaseInsensitiveFiltering: true
                                    //filterBehavior: 'value'
                                });
                            });
                        </script>
                        <select size="1" class="form-control" id="location" name="location[]" multiple="multiple">
                            <?foreach($nbs as $k=>$v){?>
                                <option value="<?=$v['id']?>" <?=((in_array($v['id'], @$_GET['location'])) ? 'selected' :'')?>><?=$v['name']?></option>
                            <?}?>
                        </select>

                    </div>

	                <?php
	                //TODO передавать в форму обработтаный $post а не работать с $_GET
	                $groups = explode(',',@$_GET['groups']);
	                ?>
                    <div class="col-md-3">
	                    <div class="form-group">
		                    <label>Property Type:</label><?php /*
		                    <div class="form-inline multicheckbox">

							<?php foreach ($config['object_groups'] as $k=>$v) {?>
								<div class="checkbox"  style="padding-right:10px;">
									<label>
										<input type="checkbox" <?=(in_array($k,$groups) ? 'checked="checked"' :'')?> value="<?=$k?>"> <?=$v['name']?>
									</label>
								</div>
		                    <?php } ?>

			                </div>
		                    <input name="groups" type="hidden" value="<?=implode(',',$groups);?>">
                    <script>
		                $(document).ready(function() {
			                //мультичексбокс
			                $(document).on("change", '.multicheckbox input[type=checkbox]', function () {
				                //alert($(this).val());
				                var arr = [];
				                var i = 0;
				                $(this).parents('.multicheckbox').find('input:checked').each(function () {
					                arr[i] = $(this).val();
					                i++;
				                });
				                $(this).parents('.multicheckbox').next('input').val(arr);
			                });
		                });
	                </script>
                            */?>

		                    <select class="form-control" id="groups" name="group">
			                    <option value="0">None selected</option>
			                    <?foreach($config['object_groups'] as $k=>$v){?>
				                    <option value="<?=$k?>" <?=(($get['group'] == $k) ? 'selected' :'')?> ><?=$v['name']?></option>
			                    <?}?>
		                    </select>
		                </div>
                    </div>
	                <div class="col-md-3">
		                <label>&nbsp;</label>
		                <select class="form-control" id="group_4" name="group_4" <?=$get['group'] == 4?'':'style="display:none"'?>>
			                <option value="0">None selected</option>
			                <?foreach($config['object_groups_4'] as $k=>$v){?>
				                <option value="<?=$k?>" <?=(($get['group_4'] == $k) ? 'selected' :'')?> ><?=$v?></option>
			                <?}?>
		                </select>
		                <select class="form-control" id="group_3" name="group_3" <?=$get['group'] == 3?'':'style="display:none"'?>>
			                <option value="0">None selected</option>
			                <?foreach($config['object_groups_3'] as $k=>$v){?>
				                <option value="<?=$k?>" <?=(($get['group_3'] == $k) ? 'selected' :'')?> ><?=$v?></option>
			                <?}?>
		                </select>
						<script>
						$(document).ready(function() {
							//мультичексбокс
							$(document).on("change", '#groups', function () {
								$('#group_4, #group_3').hide();
								if($(this).val()==3) {
									$('#group_3').show();
								}
								if($(this).val()==4) {
									$('#group_4').show();
								}
							});
						});
						</script>
	                </div>
                </div>                  
                <div class="row">
                    <!--div class="form-group col-md-6">                                            
                        <?
                           // $prices = mysql_select("SELECT MAX(price) max_price, MIN(price) min_price FROM shop_products WHERE display = 1",'row');
                           // $price_values = (@$get['price']) ? explode(',',$get['price']) : array($prices['min_price'], $prices['max_price']);
                        ?>
                        <label for="range" style="margin-right: 10px;">Price range</label> 
                        <div id="range" class="input-ranges">
                            <input type="text" class="form-control text-right" id="price1" placeholder="$<?=  number_format($price_values[0],0,'.',',')?>" disabled>
                            <input type="text" class="form-control text-right" id="price2" placeholder="$<?=  number_format($price_values[1],0,'.',',')?>" disabled>
                        </div> 
                        <div>                                
                            <input id="range-slider" name="price" type="text" class="" value="" data-slider-min="<?=$prices['min_price']?>" data-slider-max="<?=$prices['max_price']?>" data-slider-step="5" data-slider-value="[<?=implode(',',$price_values)?>]" data-slider-tooltip="hide" data-slider-handle="round"/>
                            <script>
                                document.addEventListener("DOMContentLoaded", function () {
                                    $("#range-slider").slider({}).on('slide', function(e){
                                        $(this).closest(".form-group").find("#price1").val('$'+e.value[0].format());
                                        $(this).closest(".form-group").find("#price2").val('$'+e.value[1].format());
                                    });
                                });
                            </script>
                        </div>                            
                    </div--> 
                    <div class="form-group col-md-6">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="type">Min Price:</label>
                                <select class="form-control" id="price_min" name="price_min">                            
                                    <option value="0">select</option>
                                    <?foreach($config['prices_list'] as $k=>$v){?>
                                        <option value="<?=$k?>" <?=(($get['price_min'] == $k) ? 'selected' :'')?> ><?=$v?></option>
                                    <?}?>
                                </select>
                            </div>                                
                            <div class="form-group col-md-6">
                                <label for="type">Max Price:</label>
                                <select class="form-control" id="price_max" name="price_max">                            
                                    <option value="0">select</option>
                                    <?foreach($config['prices_list'] as $k=>$v){?>
                                        <option value="<?=$k?>" <?=(($get['price_max'] == $k) ? 'selected' :'')?> ><?=$v?></option>
                                    <?}?>
                                </select>
                            </div>
                            <script>
                                $('#price_min, #price_max').multiselect({});
                            </script>
                        </div>
                    </div>                    
<?php /*
                    <!--div class="form-group col-md-2">                                            
                        <label style="" class="" for="bedrooms">Bedrooms</label> 
                        <input type="text" class="form-control text-center" id="bedrooms" value="<?=$get['p'][25]?>" name="p[25]" placeholder="Any Beds" style="">                                                
                    </div--> */ ?>
                    <div class="form-group col-md-2">
                        <label for="bedrooms">Bedrooms:</label>                        
                        <select class="form-control" id="bedrooms" name="beds">
                            <option value="0">select</option>
                            <?foreach($config['bedrooms_list'] as $k=>$v){?>
                                <option value="<?=$k?>" <?=(($get['beds'] == $k) ? 'selected' :'')?> ><?=$v?></option>
                            <?}?>
                        </select>
                    </div> 
                    <div class="form-group col-md-2">
                        <label for="baths">Baths:</label>
                        <select class="form-control" id="baths" name="baths">
                            <option value="0">select</option>
                            <?foreach($config['baths_list'] as $k=>$v){?>
                                <option value="<?=$k?>" <?=(($get['bedrooms'] == $k) ? 'selected' :'')?> ><?=$v?></option>
                            <?}?>
                        </select>
                    </div>
                    <script>
                        $('#bedrooms, #baths').multiselect({});
                    </script>
                    <!--div class="form-group col-md-2">                                            
                        <label style="" for="baths">Baths</label> 
                        <input type="text" class="form-control text-center" id="baths" value="<?=$get['p'][92]?>" name="p[92]" placeholder="Any Baths" style="">                                                
                    </div-->

                    <div class="form-group col-md-2">
                        <label style="display:block;">&nbsp;</label> 
                        <button style="display: block; width: 100%;" class="btn btn-warning">SEARCH</button>
                    </div>                                            
                </div> 
</form>

<script>
	document.addEventListener("DOMContentLoaded", function () {
        $('input[name=quick]').on('keyup', function(){
            var word = $(this).val();
            if(word.length){
                console.log('true');
                $(this).closest('form').find('input, select, button').not(this).not('.btn-warning').attr('disabled',true);
            } else {
                console.log('false');
                $(this).closest('form').find('input, select, button').not(this).not('.btn-warning').removeAttr('disabled');
            }
        });
    });
</script>