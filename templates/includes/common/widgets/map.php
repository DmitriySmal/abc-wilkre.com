<?php

/*
 * Здесь будет отображаться карта.
 * Входные параметры: точка на карте, либо точки на карте со своими параметрами.
 */
if (@$q['city']) {
	if (!isset($config['cities_name'])) {
		$config['cities_name'] = mysql_select("SELECT id,name FROM shop_cities",'array');
	}
	if (isset($config['cities_name'][$q['city']])) {
		$q['address'].= ', '.$config['cities_name'][$q['city']];
	}
}
?>
<?//=@$config['sources']['gmaps']?>
<?//=html_sources('return', 'gmaps')?>

<div id="map_canvas" data-name="<?=$q['name']?>" data-address="<?=$q['address']?>" data-coordinates="<?=@$q['coordinates']?>" data-lat="<?=@$q['lat']?>" data-lng="<?=@$q['lng']?>" style=" height:500px;"></div>

<script type="text/javascript" src="https://maps.google.com/maps/api/js?language=en&key=<?=$config['google_maps_key']?>"></script>
<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function () {
		$(window).load(function () {

			var coords = document.getElementById("map_canvas").getAttribute('data-coordinates').replace("(", "").replace(")", "").split(",");
			var address = document.getElementById("map_canvas").getAttribute('data-address');
			coords = (coords.length > 1) ? coords : false;//new Array(25.7616798, -80.191);
			geocoder = new google.maps.Geocoder();
			if (coords.length > 1) {
				var fenway = new google.maps.LatLng(coords[0], coords[1]);
				map = new google.maps.Map(document.getElementById("map_canvas"), {
					zoom: 15,
					scrollwheel: false,
					center: new google.maps.LatLng(coords[0], coords[1]),
					mapTypeId: google.maps.MapTypeId.ROADMAP,
				});
				markersArray = [];
				var marker = new google.maps.Marker({position: new google.maps.LatLng(coords[0], coords[1])});
				marker.setMap(map);
				markersArray.push(marker);
			}
			else {
				geocoder.geocode({"address": address}, function (results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						//for (i in markersArray) markersArray[i].setMap(null);
						var fenway = results[0].geometry.location;
						map = new google.maps.Map(document.getElementById("map_canvas"), {
							zoom: 15,
							//center: new google.maps.LatLng(coords[0],coords[1]),
							mapTypeId: google.maps.MapTypeId.ROADMAP,
						});
						map.setCenter(results[0].geometry.location);
						var marker = new google.maps.Marker({
							map: map,
							position: results[0].geometry.location
						});
						markersArray.push(marker);
						console.log(results[0].geometry.location.toString());
						//$("input[name=coordinates]").val(results[0].geometry.location.toString());
					} else {
						alert("Geocode was not successful for the following reason: " + status);
					}
				});
			}
			var panoramaOptions = {
				position: fenway,
				pov: {
					heading: 34,
					pitch: 10
				}
			};
			//давало ошибку в консоли
			/*
			var panorama = new google.maps.StreetViewPanorama(document.getElementById('map_street_view'), panoramaOptions);
			map.setStreetView(panorama);
*/
			//google.maps.event.trigger(map, 'resize');
			/*
			 google.maps.event.addListener(map, "click", function(event) {
			 for (i in markersArray) markersArray[i].setMap(null);
			 markersArray.length = 0;
			 var marker = new google.maps.Marker({position:event.latLng,map:map});
			 markersArray.push(marker);
			 $("input[name=coordinates]").val(event.latLng);
			 });*/
		});
	});
</script>