<? 
global $html; 
$get = $_GET;
?>
<form id="search-offers" action="/<?=$modules['shop']?>/" method="get" style="<?//=(($html['module'] != 'index') ? 'display: none;' :'')?>">
    <input type="hidden" name="for_sale" value="0">
    <div id="form-switch">
        <span data-switch-value="false" data-switch-set="state" class="switch-control active">Residental</span> <input type="checkbox" class="switch"> <span data-switch-value="true" data-switch-set="state" class="switch-control">Commercial</span>
    </div>

    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a data-value="0" href="#sell" aria-controls="sell" role="tab" data-toggle="tab">Sell</a></li>
            <li role="presentation"><a data-value="1" href="#sell" aria-controls="rent" role="tab" data-toggle="tab">Rent</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="sell">                                
                <div class="row">
                    <div class="form-group col-md-3">
                        <?
                        $where_state = ($state['id'] != '9999') ? " AND state = {$state['id']}" : '';
                        $cities = mysql_select("SELECT * FROM shop_cities WHERE display = 1 {$where_state} ORDER BY rank DESC, name ASC",'rows');                                        
                        ?>
                        <label for="city">City:</label>
                        <!--input type="text" class="form-control" id="city" placeholder="Select city"-->
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('#city').multiselect({
                                    //enableFiltering: true,
                                    //filterBehavior: 'value'
                                });
                                
                                $('#search-offers select[name=city]').on('change',function(){
                                    var inpt = $(this);
                                    $.ajax({
                                        url: '/ajax.php?file=neighborhoods',
                                        data: {
                                            city: $(inpt).val(),                                           
                                        },
                                        dataType:	'json',
                                        success:	function (json){
                                            if(json.data)
                                            {         
                                                var ul = $(inpt).closest('form').find('select#location');
                                                $(ul).find('option').remove();
                                                $.each(json.data,function(i,obj) {
                                                    $('<option>',{text:obj.name, value:obj.id}).appendTo(ul);                                
                                                });                                                
                                                $(ul).multiselect('rebuild');
                                            }
                                        },
                                        onComplete: function(){
                                            //$(inpt).removeAttr('disabled');
                                        }
                                    });
                                    return false;
                                });
                                
                            });
                        </script>
                        <select class="form-control" id="city" name="city">
                            <?foreach($cities as $k=>$v){?>
                                <option value="<?=$v['id']?>" <?=(($get['city'] == $v['id']) ? 'selected' :'')?>><?=$v['name']?></option>
                            <?}?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <?
                            $nbs = (isset($cities[0]['id'])) ? mysql_select("SELECT * FROM shop_neighborhoods WHERE display = 1 AND city = {$cities[0]['id']} ORDER BY rank DESC, name ASC",'rows') : array();                                        
                        ?>
                        <label for="location">Location:</label>
                        <!--input type="text" class="form-control" id="location" placeholder="Neighbourhood / Address / Building"-->
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('#location').multiselect({
                                    enableFiltering: true,
                                    enableCaseInsensitiveFiltering: true
                                    //filterBehavior: 'value'
                                });
                            });
                        </script>
                        <select class="form-control" id="location" name="location[]" multiple="multiple">
                            <?foreach($nbs as $k=>$v){?>
                                <option value="<?=$v['id']?>" <?=((in_array($v['id'], $_GET['location'])) ? 'selected' :'')?>><?=$v['name']?></option>
                            <?}?>
                        </select>                        
                    </div>
                    <!--div class="form-group col-md-5">
                        <label for="location">Location:</label>
                        <input type="text" class="form-control" id="location" placeholder="Neighbourhood / Address / Building">
                    </div-->
                    <div class="form-group col-md-6">
                        <label for="type">Property Type:</label>
                        <!--input type="text" class="form-control" id="type" placeholder="Condos\Coops"-->
                        <? /*
                        $types = mysql_select("SELECT * FROM shop_parameters WHERE id = 1",'row');                                        
                        ?>
                        <select class="form-control" id="type" name="type">
                            <option>Property type</option>
                            <?foreach(unserialize($types['values']) as $k=>$v){?>
                                <option value="<?=$k?>"><?=$v?></option>
                            <?}?>
                        </select>
                        <?*/?>
                        <script>
                            $(document).ready(function() {                                                                
                                $('#category').multiselect({                                         
                                        onChange: function(option, checked, select) {
                                            console.log($(option).val());
                                            // тут тоже достаем параметры
                                        }
                                });
                                var categories = $('select#category');
                                var drpdwn = $('select#category + .btn-group');                                            
                                var visible = new Array();
                                $(drpdwn).find('li').removeClass('active').hide();
                                $.each($(categories).find('option'), function(i, el){
                                    if ($(el).data('commercial') == 0) visible.push($(el).attr('value'));
                                });        
                                $.each($(drpdwn).find('li input[type=radio]'), function(i, el){
                                    if (in_array($(el).val(), visible))
                                        $(el).closest('li').show();
                                    else
                                        $(el).closest('li').hide();
                                });
                            });
                        </script>
                        <? 
                        $categories = mysql_select("SELECT * FROM shop_categories WHERE display = 1 ORDER BY id ASC",'rows');                                        
                        ?>
                        <select class="form-control" id="category" name="category">                            
                            <?foreach($categories as $k=>$v){?>
                                <option value="<?=$v['id']?>" <?=(($get['category'] == $v['id']) ? 'selected' :'')?> data-commercial="<?=$v['commercial']?>" ><?=$v['name']?></option>
                            <?}?>
                        </select>
                    </div>
                </div>  
                <div class="form-inline">
                    <div class="row">
                        <div class="form-group col-md-6">                                            
                            <?
                                $prices = mysql_select("SELECT MAX(price) max_price, MIN(price) min_price FROM shop_products WHERE display = 1",'row');                                    
                                $price_values = (@$get['price']) ? explode(',',$get['price']) : array($prices['min_price'], $prices['max_price']);                                                                
                            ?>
                            <label for="range" style="margin-right: 10px;">Price range</label> 
                            <div id="range" class="input-ranges">
                                <input type="text" class="form-control text-right" id="price1" placeholder="$<?=  number_format($price_values[0],0,'.',',')?>" disabled>
                                <input type="text" class="form-control text-right" id="price2" placeholder="$<?=  number_format($price_values[1],0,'.',',')?>" disabled>
                            </div> 
                            <div>                                
                                <input id="range-slider" name="price" type="text" class="" value="" data-slider-min="<?=$prices['min_price']?>" data-slider-max="<?=$prices['max_price']?>" data-slider-step="5" data-slider-value="[<?=implode(',',$price_values)?>]" data-slider-tooltip="hide" data-slider-handle="round"/>
                                <script>
                                    document.addEventListener("DOMContentLoaded", function () {
                                        $("#range-slider").slider({}).on('slide', function(e){
                                            $(this).closest(".form-group").find("#price1").val('$'+e.value[0].format());
                                            $(this).closest(".form-group").find("#price2").val('$'+e.value[1].format());
                                        });
                                    });
                                </script>
                            </div>
                        </div> 
                        <div class="form-group col-md-6 text-right">                                            
                            <div class="form-group">                                            
                                <label style="margin-right: 10px; top: -5px; position: relative;" class="" for="bedrooms">Bedrooms</label> 
                                <input type="text" class="form-control text-center" id="bedrooms" value="<?=$get['p'][25]?>" name="p[25]" placeholder="Any Beds" style="width: 100px; margin-right: 10px">                                                
                            </div>
                            <div class="form-group">                                            
                                <label style="margin-right: 10px; top: -5px; position: relative;" for="baths">Baths</label> 
                                <input type="text" class="form-control text-center" id="baths" value="<?=$get['p'][92]?>" name="p[92]" placeholder="Any Baths" style="width: 100px; margin-right: 10px;">                                                
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button class="btn btn-warning btn-lg">SEARCH</button>
                                </div>                                            
                            </div>
                        </div> 
                    </div> 
                </div> 
            </div>
            <div role="tabpanel" class="tab-pane fade" id="rent">
                ...
            </div>                            
        </div>
    </div>
</form>