<?php
$base = $u[1]==@$modules['shop'] ? 2:1;
if ($u[1]==$modules['shop_flmx']) $base = 9;
if ($u[1]==$modules['shop_mls10']) $base = 10;
//if ($base==2) $q['property_type'] = @$q['group'];
if ($base==9) {
	if (!isset($config['property_types_9'])) {
		$config['property_types_9'] = mysql_select("
            SELECT uid id, name,url
            FROM shop_property_types
            WHERE base=9 AND parent=0 AND display=1
        ",'rows_id');
	}
	$property_types = $config['property_types_9'];
}
elseif ($base==2) {
	$property_types = array();
	foreach ($config['object_groups'] as $k => $v) {
		$property_types[$k] = $v['name'];
	}
}
else {
	$property_types = $config['ny_property_types'];
}
?>
<form id="subscribe2" class="bg-light-gray-pattern widget-subscribe-2" style="color:#262626;">
		<input type="hidden" name="base" value="<?=$base?>" />
	    <div class="container">

	        <div class="row" style="padding-bottom:15px">
	            <div class="col-md-12 text-center">
	                <div class="thanks">Didn't find a property?</div>
	                <div class="info">
		                Enter information below to receive automatic updates on the newest properties to hit the market!
	                </div>
	            </div>
	         </div>

		    <div class="row">

			    <?php if ($base==2 OR $base==9 OR $base==10) {
			        ?>
			    <div class="col-md-3">
				    <div class="form-group">
					    <label>Sale/Rent:</label>
					    <select class="form-control" id="sub_residential" name="residential"><?=select('',$config['object_residential'],'None selected')?></select>
				    </div>
			    </div>
				    <input type="hidden" name="city" value="<?=$base==9?140:6?>" />
			        <?php
			    }
			    else {
				    $cities = mysql_select("SELECT id,name FROM shop_cities WHERE state=".$base." AND display=1 ORDER BY rank",'array');
				    ?>
			    <div class="col-md-3">
				    <div class="row">
					    <div class="col-md-6">
						    <div class="form-group">
							    <label>Sale/Rent:</label>
							    <select class="form-control" id="sub_residential" name="residential"><?=select('',$config['object_residential'],'None selected')?></select>
						    </div>
					    </div>
				        <div class="col-md-6">
						    <div class="form-group">
							    <label>City:</label>
							    <select class="form-control" id="sub_city" name="city">
								    <?=select(@$q['city'],$cities,'Select city')?>
							    </select>
						    </div>
				        </div>
					</div>
				</div>
				    <script type="text/javascript">
					    function ready_widgets_subscribe2() {
						    $('#sub_city').change(function(){
							   var city = $(this).val();
							    $.get('/ajax.php', {'file':'ny_location','city':city},function(data){
								    $('#sub_location_box').html(data);
							    });
						    });

					    }
					    document.addEventListener("DOMContentLoaded", ready_widgets_subscribe2);
				    </script>
			        <?php
			    }
			    ?>

			    <div class="form-group col-md-3" id="sub_location_box">
				    <?
				    $cities =  $base==1 ? '7,8,9,10,11,12':'6';
				    if ($base==9 OR $base==10) $cities = 140;
				    $nbs = mysql_select("SELECT * FROM shop_neighborhoods WHERE display = 1 AND city IN (".$cities.") ORDER BY rank DESC, name ASC",'rows');
				    ?>
				    <label for="location">Location:</label>
				    <script>
					    function ready_widgets_subscribe2_1 () {
						    $('.chosen-select').chosen();
						    $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
					    }
					    document.addEventListener("DOMContentLoaded", ready_widgets_subscribe2_1);
				    </script>
				    <style>.chosen-container {display:block; min-width:100%}</style>
				    <select size="1" class="form-control chosen-select" data-placeholder="Choose a location" id="sub_location" name="location[]" multiple="multiple">
					    <?php if ($nbs) foreach($nbs as $k=>$v){?>
						    <option value="<?=$v['id']?>" <?=(@$q['location'] AND @in_array($v['id'], @$q['location']) ? 'selected' :'')?>><?=$v['name']?></option>
					    <?}?>
				    </select>

			    </div>



			    <?php
			    /*$property_type = $base==1? $config['ny_property_types'] : $config['object_groups'];
			    if ($base==9 OR $base==10) $property_type = $property_types;
			    <?=select('',$property_type,'None selected')?>
			    */
			    ?>
			    <div class="col-md-3">
				    <div class="form-group">
					    <label>Real Estate Types:</label>
					    <select class="form-control" id="sub_property_type" name="property_type"><?=select_property_type1($base,@$q['residential'],@$q['property_type'])?></select>
                    </div>
			    </div>
			    <div class="col-md-3" id="sub_property_type2" style="display:none">
				    <label>Property Type:</label>
					    <select class="form-control" name="property_type2">
						    <?=select_property_type($base,@$q['residential'],@$q['property_type'],@$q['property_type2'])?>

                        </select>
				    <script>
					    function ready_widgets_subscribe2_2() {
						    //цены
						    $( "#sub_price_max,#sub_price_min" ).autocomplete({
							    source: function(request,response) {
								    var residential = $('#residential').val(),
									    str = request.term.replace(/[^0-9]/g, ""),
									    arr = [],
									    count = residential == 1 ? 5:9;
								    if (str.length) {
									    for (var i = 1; i < count - str.length; i++) {
										    var s = '';
										    for (var n = 0; n < i; n++) {
											    s = s + '0';
										    }
										    arr.push('$' + number_format(str + s, '', 0,','));
									    }
								    }
								    response(arr);
							    },
							    minLength: 1
						    });

						    //подтипы
						    $(document).on("change", '#sub_property_type', function () {
							    var property_type = $(this).val(),
								    residential = $("#residential").val();
							    //alert(property_type);
							    $('#sub_property_type2').hide();
							    if (property_type>0) {
								    $.get(
									    '/ajax.php?file=property_type',
									    {'residential': residential, 'property_type': property_type, 'base': <?=$base?>},
									    function (data) {
										    if (data) {
											    $('#sub_property_type2').show().find('select').html(data);
										    }
										    else $('#sub_property_type2').find('select').html('');
									    }
								    );
							    }
							    else $('#sub_property_type2').find('select').html('');
							    //прячем Baths и Bedrooms
							    <?php if ($base==1) {?>
							    if ($(this).val() == 3) {
								<?php } else {?>
								if ($(this).val() == 4 || $(this).val() == 5) {
								<?php } ?>
								    //alert(1);
								    $('#sub_bedrooms,#sub_baths').val('').attr('disabled','disabled');
							    }
							    else {
								    $('#sub_bedrooms,#sub_baths').val('').removeAttr('disabled');
							    }

						    });
						    //аренда-продажа
						    $(document).on("change", '#sub_residential', function () {
							    <?php if ($base==2) {?>
							    var property_type = $('#sub_property_type').val(),
								    residential = $(this).val(),
								    rent = [3,4],
								    sale = [1,2,4,5,6];
							    $('#sub_property_type option').hide();
							    $('#sub_property_type').val(0);
							    if(residential==0) {
								    $('#sub_property_type option').show();
								    $('#sub_property_type option[value=3]').hide();
								    $('#sub_property_type').val(property_type);
							    }
							    if(residential==1) {
								    $.each(rent, function(index, value){
									    $('#sub_property_type option[value='+value+']').show();
									    if (value==property_type) $('#sub_property_type').val(value);
								    });
							    }
							    if(residential==2) {
								    $.each(sale, function(index, value){
									    $('#sub_property_type option[value='+value+']').show();
									    if (value==property_type) $('#sub_property_type').val(value);
								    });
							    }
							    <?php } ?>
							    $('#sub_property_type').trigger('change');
						    });
						    //чтобы спрятать подтипы и типы
						    $('#sub_residential').trigger('change');
					    }
						document.addEventListener("DOMContentLoaded", ready_widgets_subscribe2_2);
				    </script>
			    </div>
		    </div>
		    <div class="row">
			    <div class="form-group col-md-6 prices">
				    <div class="row">
					    <div class="col-md-6">
						    <label for="type">Min Price:</label>
						    <input class="form-control"  name="price_min" id="sub_price_min" value="<?=@$q['price_min']?'$'.number_format($q['price_min'],0,'.',','):''?>">
					    </div>
					    <div class="col-md-6">
						    <label for="type">Max Price:</label>
						    <input class="form-control"  name="price_max" id="sub_price_max" value="<?=@$q['price_max']?'$'.number_format($q['price_max'],0,'.',','):''?>">
					    </div>
					    <style>.prices .ui-menu-item {text-align:right}</style>
				    </div>
			    </div>
			    <div class="form-group col-md-3">
				    <label for="bedrooms">Bedrooms:</label>
				    <select class="form-control" id="sub_bedrooms" name="beds"><?=select('',$config['bedrooms_list'],'No matter')?></select>
			    </div>
			    <div class="form-group col-md-3">
				    <label for="baths">Baths:</label>
				    <select class="form-control" id="sub_baths" name="baths"><?=select('',$config['baths_list'],'No matter')?></select>
			    </div>
		    </div>

		    <div class="row">
	            <div class="col-md-3 form-group">
		            <label for="email">Your contact email</label>
		            <input type="text" class="form-control" name="email">
	            </div>
			    <div class="col-md-4 form-group">
				    <label for="email">Periodicity</label><div style="padding:3px 0 0">
				    <?php
				    foreach ($config['days_list'] as $k=>$v) {
						echo '<label style="width:14%;"><input type="checkbox" name="day[]" value="'.$k.'"> '.$v.'</label>';
				    }
				    ?>
					</div>
			    </div>
			    <div class="col-md-2 form-group">
				    <label for="email">Time</label>
				    <select class="form-control" name="time"><?=select('',$config['time_list'],'select')?></select>
			    </div>
				<div class="col-md-3 form-group">
					<label style="display:block; height:20px"> </label>
                    <button
	                    onclick="ga('send', 'event', 'SubOptions', 'Push'); yaCounter34569400.reachGoal('subOptions'); return true;"
	                    class="btn btn-warning" style="display:block; width:100%" id="subscribe2_send">SEND</button>
				</div>
            </div>

	    </div>
	<input type="hidden" name="type" value="subscribe2">
</form>


<!-- Modal -->
<div class="modal fade" id="subscribe2_modal" tabindex="-1" role="dialog" aria-labelledby="subscribe2_modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				Thank you! We wil sent all offers on your e-mail!
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
	document.addEventListener("DOMContentLoaded", function () {
		$('#subscribe2').on('submit', function () {
			var form = $(this);
			$(form).find('.btn').attr('disabled', 'true');
			$.ajax({
				type: 'POST',
				dataType: 'JSON',
				url: "/ajax.php?file=callback",
				data: $(form).serialize(),
				success: function (data) {
					if (data.result) {
						$("#subscribe2_modal").modal('show');
					}
					else {
						alert(data.message);
						console.log(data.message);
					}
				},
				complete: function () {
					$(form).find('.btn').removeAttr('disabled');
				}
			});
			return false;
		});
	});

</script>