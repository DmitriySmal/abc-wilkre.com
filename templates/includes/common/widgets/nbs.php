<?php

//shop_mx
//shop

//флорида
if ($u[1]==@$modules['shop']) {
	$nbs = mysql_select("SELECT * FROM shop_neighborhoods WHERE city = 6 AND display = 1 ORDER BY name", 'rows', 60 * 60);
	$neighborhoods = array_chunk($nbs, 1);
	?>
	<div id="widget-nbs" class="widget-nbs" style="display: none;">
		<div class="row">
			<div class="col-md-12">
				<?foreach ($neighborhoods as $nbs) { ?>
					<div class="col-md-2">
						<ul class="nbs-ul">
							<?foreach ($nbs as $nb) { ?>
								<li class="<?= ((@$_GET['location'] && $_GET['location'][0] == $nb['id']) ? 'active' : '') ?>">
									<a href="/<?= $modules['shop'] ?>/?location[]=<?= $nb['id'] ?>"><?= $nb['name'] ?></a>
								</li>
							<?
							} ?>
						</ul>
					</div>
				<?
				} ?>
			</div>
		</div>
	</div>
	<?php
	$for_sale = array(
		'Condo' => '?residential=2&property_type=1&property_type_1=2',
		'Co-Op' => '?residential=2&property_type=1&property_type_1=3',
		'Single Family' => '?residential=2&property_type=1&property_type_1=4',
		'Multi-Family' => '?residential=2&property_type=1&property_type_1=1',
		'Mixed Use/Store & Family' => '?residential=2&property_type=1&property_type_1=5',
		'Commercial' => '?residential=2&property_type=2',
		'Land' => '?residential=2&property_type=3',
	);
	$for_rent = array(
		'Condo' => '?residential=1&property_type=1&property_type_1=2',
		'Co-Op' => '?residential=1&property_type=1&property_type_1=3',
		'Single Family' => '?residential=1&property_type=1&property_type_1=4',
		'Multi-Family' => '?residential=1&property_type=1&property_type_1=1',
		'Mixed Use/Store & Family' => '?residential=1&property_type=1&property_type_1=5',
		'Commercial' => '?residential=1&property_type=2',
		'Land' => '?residential=1&property_type=3',
	);
}
elseif ($u[1]==$modules['shop_flmx']) {
	$nbs = mysql_select("SELECT * FROM shop_neighborhoods WHERE city = 140 AND display = 1 ORDER BY name", 'rows', 60 * 60);
	$neighborhoods = array_chunk($nbs, 1);
	?>
    <div id="widget-nbs" class="widget-nbs" style="display: none;">
        <div class="row">
            <div class="col-md-12">
				<?foreach ($neighborhoods as $nbs) { ?>
                    <div class="col-md-2">
                        <ul class="nbs-ul">
							<?foreach ($nbs as $nb) { ?>
                                <li class="<?= ((@$_GET['location'] && $_GET['location'][0] == $nb['id']) ? 'active' : '') ?>">
                                    <a href="/<?= $modules['shop_flmx'] ?>/?location[]=<?= $nb['id'] ?>"><?= $nb['name'] ?></a>
                                </li>
								<?
							} ?>
                        </ul>
                    </div>
					<?
				} ?>
            </div>
        </div>
    </div>
	<?php
	$for_sale = array(
		'Condo' => '?residential=2&property_type=1&property_type_1=2',
		'Co-Op' => '?residential=2&property_type=1&property_type_1=3',
		'Single Family' => '?residential=2&property_type=1&property_type_1=4',
		'Multi-Family' => '?residential=2&property_type=1&property_type_1=1',
		'Mixed Use/Store & Family' => '?residential=2&property_type=1&property_type_1=5',
		'Commercial' => '?residential=2&property_type=2',
		'Land' => '?residential=2&property_type=3',
	);
	$for_rent = array(
		'Condo' => '?residential=1&property_type=1&property_type_1=2',
		'Co-Op' => '?residential=1&property_type=1&property_type_1=3',
		'Single Family' => '?residential=1&property_type=1&property_type_1=4',
		'Multi-Family' => '?residential=1&property_type=1&property_type_1=1',
		'Mixed Use/Store & Family' => '?residential=1&property_type=1&property_type_1=5',
		'Commercial' => '?residential=1&property_type=2',
		'Land' => '?residential=1&property_type=3',
	);
}

elseif ($u[1]==$modules['shop_mx'])  {
	$cities = mysql_select("SELECT * FROM shop_cities WHERE state=1 AND display = 1  ORDER BY rank,name", 'rows', 60 * 60);
	$neighborhoods = array();
	if (count($cities)) {
		foreach ($cities as $city) {
			$nbs = mysql_select("SELECT * FROM shop_neighborhoods WHERE city = {$city['id']} AND display = 1 ORDER BY name", 'rows', 60 * 60);
			$neighborhoods[$city['id']] = array_chunk($nbs, 3);
		}
	}
	$_GET['city'] = @$_GET['city'] ? $_GET['city'] : $cities[0]['id'];

	?>

	<div id="widget-nbs" class="widget-nbs" style="display: none;">
		<div class="row">
			<div class="col-md-2">
				<ul id="cities-ul">
					<?foreach ($cities as $city) { ?>
						<li class="<?= ((@$_GET['city'] == $city['id']) ? 'active' : '') ?>"><a href="#"
						                                                                        data-toggle="tab"
						                                                                        data-target="#city<?= $city['id'] ?>"
						                                                                        data-id="<?= $city['id'] ?>"><?= $city['name'] ?></a>
						</li>
					<?
					} ?>
				</ul>
			</div>
			<div class="col-md-10">
				<div class="tab-content">
					<?foreach ($cities as $k => $city) { ?>
						<div role="tabpanel"
						     class="tab-pane fade <?= (!@$_GET['city']) ? ((!$k) ? 'active in' : '') : (($_GET['city'] == $city['id']) ? 'active in' : '') ?>"
						     id="city<?= $city['id'] ?>">
							<div class="row">
								<?foreach ($neighborhoods[$city['id']] as $nbs) { ?>
									<div class="col-md-2">
										<ul class="nbs-ul">
											<?foreach ($nbs as $nb) { ?>
												<li class="<?= ((@$_GET['location'] && $_GET['location'][0] == $nb['id']) ? 'active' : '') ?>">
													<a href="/<?= $modules['shop_mx'] ?>/?city=<?= $city['id'] ?>&location[]=<?= $nb['id'] ?>"><?= $nb['name'] ?></a>
												</li>
											<?
											} ?>
										</ul>
									</div>
								<?
								} ?>
							</div>
						</div>
					<?
					} ?>
				</div>
			</div>
		</div>
	</div>
	<?php
	$for_sale = array(
		'Condo/Co-op for sale' => '?residential=2&group=2',
		'Homes for Sale' => '?residential=2&group=1',
		'Residential income for sale' => '?residential=2&group=6',
		'Commercials for sale' => '?residential=2&group=4',
		'Lands for Sale' => '?residential=2&group=5'
	);
	$for_rent = array(
		'Condo/Co-op for rent' => '?residential=1&group=3&group_3=Condo',
		'Homes for rent' => '?residential=1&group=3&group_3=Single',
		'Commercials for rent' => '?residential=1&group=4'
	);
}
/*
?>

<div id="widget-for-sale" class="widget-nbs" style="display: none;">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-4">
				<ul class="nbs-ul">
					<?foreach($for_sale as $k=>$v){?>
					<li><a href="/<?=$u[1]?>/<?=$v?>"><?=$k?></a></li>
					<?}?>
				</ul>
			</div>
		</div>
	</div>
</div>

<div id="widget-for-rent" class="widget-nbs" style="display: none;">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-4">
				<ul class="nbs-ul">
					<?foreach($for_rent as $k=>$v){?>
						<li><a href="/<?=$u[1]?>/<?=$v?>"><?=$k?></a></li>
					<?}?>
				</ul>
			</div>
		</div>
	</div>
</div>
*/
