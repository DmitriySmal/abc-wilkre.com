<div class="modal fade wilkModal" id="feedback" tabindex="-1" role="dialog" aria-labelledby="feedback" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>                
            <form class="form validate">
                <div class="modal-body">
                    <div class="h3"><span>Call Back</span></div>
                        <div class="control-group text-center">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name">                        
                        </div>
                        <div class="control-group text-center">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" name="phone" placeholder="XXX-XXX-XXXX">                        
                        </div>
		                <div class="control-group text-center">
			                <label for="email">Email</label>
			                <input type="text" class="form-control" name="email">
		                </div>
	                <input type="hidden" name="type" value="feedback">

                    </div>
                <div class="modal-footer text-center">                                                
                    <button type="submit"
                            onclick="ga('send', 'event', 'CallBack', 'Push'); yaCounter34569400.reachGoal('callBackModal'); return true;"
                            class="btn btn-warning">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade wilkModal" id="sendRequest" tabindex="-1" role="dialog" aria-labelledby="sendRequest" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			<form class="form validate">
				<div class="modal-body">
					<div class="h3"><span>Send Request</span></div>
					<div class="control-group text-center">
						<label for="name">Name</label>
						<input type="text" class="form-control" name="name">
					</div>
					<div class="control-group text-center">
						<label for="phone">Phone</label>
						<input type="text" class="form-control" name="phone" placeholder="XXX-XXX-XXXX">
					</div>
					<div class="control-group text-center">
						<label for="email">Email</label>
						<input type="text" class="form-control" name="email">
					</div>
					<input type="hidden" name="type" value="sendRequest">
					<input type="hidden" name="object" value="<?=@$page['id']?>">
					<?php if (@$_GET['agent']>0) { ?>
						<input type="hidden" name="agent" value="<?=@$_GET['agent']?>">
					<?php } ?>
					<?php /*
	                <input type="hidden" name="url" value="http://<?=$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']?>">
	                <input type="hidden" name="price" value="<?=@$page['price']?>">
	                <input type="hidden" name="mln" value="<?=@$page['mln']?>">
	                <input type="hidden" name="neighborhood" value="<?=@$page['neighborhood']?>">
	                <input type="hidden" name="address" value="<?=@$page['name']?>">
	                <input type="hidden" name="property_type"
	                       value="<?=($module['shop']==$u[1] AND @$page['property_type'] )?@$config['object_groups'][$page['property_type']]['name']:@$config['ny_property_types'][$page['property_type']]?>">
                        */ ?>
				</div>
				<div class="modal-footer text-center">
					<button type="submit"
					        onclick="ga('send', 'event', 'CallBack', 'Push'); yaCounter34569400.reachGoal('callBackModal'); return true;"
					        class="btn btn-warning">Send</button>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="modal fade wilkModal" id="sheduleShowing" tabindex="-1" role="dialog" aria-labelledby="sendRequest" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>                
            <form class="form validate">
                <div class="modal-body">
                    <div class="h3"><span><?=i18n('shop|btn_schedule_showing')?></span></div>
                    <div class="control-group text-center">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="control-group text-center">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control" name="phone" placeholder="XXX-XXX-XXXX">
                    </div>
	                <div class="control-group text-center">
		                <label for="email">Email</label>
		                <input type="text" class="form-control" name="email">
	                </div>
                    <div class="control-group text-center">
                        <label for="date">Date</label>
                        <input type="text" class="form-control datepicker" name="date" placeholder="Y.m.d">
                    </div>
                    <input type="hidden" name="type" value="sheduleShowing">

	                <input type="hidden" name="object" value="<?=@$page['id']?>">
	                <?php if (@$_GET['agent']>0) { ?>
		                <input type="hidden" name="agent" value="<?=@$_GET['agent']?>">
	                <?php } ?>
	                <?php /*
                    <input type="hidden" name="url" value="http://<?=$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']?>">
	                <input type="hidden" name="url" value="http://<?=$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']?>">
	                <input type="hidden" name="price" value="<?=@$page['price']?>">
	                <input type="hidden" name="mln" value="<?=@$page['mln']?>">
	                <input type="hidden" name="neighborhood" value="<?=@$page['neighborhood']?>">
	                <input type="hidden" name="address" value="<?=@$page['name']?>">
	                <input type="hidden" name="property_type"
	                       value="<?=($module['shop']==$u[1] AND @$page['property_type'] )?@$config['object_groups'][$page['property_type']]['name']:@$config['ny_property_types'][$page['property_type']]?>">
					*/ ?>
                </div>
                <div class="modal-footer text-center">                                                
                    <button type="submit" class="btn btn-warning">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade wilkModal" id="saveFavorite" tabindex="-1" role="dialog" aria-labelledby="sendRequest" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>                
            <form class="form validate">
                <div class="modal-body">
                    <div class="h3"><span><?=i18n('shop|btn_sent_email')?></span></div>
                        <div class="control-group text-center">
                            <label for="name">Email</label>
                            <input type="text" class="form-control" name="email">                        
                        </div>
	                <input type="hidden" name="type" value="saveFavorite">
	                <input type="hidden" name="object" value="<?=@$page['id']?>">
	                <input type="hidden" name="url" value="http://<?=$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']?>">
                </div>
                <div class="modal-footer text-center">                                                
                    <button type="submit" class="btn btn-warning">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade wilkModal" id="thankYou" tabindex="-1" role="dialog" aria-labelledby="thankYou" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>                
            <div class="modal-body">
                <div class="h3"><span>Thank You!</span></div>                
            </div>            
        </div>
    </div>
</div>

<script>
	document.addEventListener("DOMContentLoaded", function () {
	    /*$('.modal').on('show.bs.modal', function (event) {
	        var button = $(event.relatedTarget); // Button that triggered the modal
	        var trigger = button.data('trigger'); // Extract info from data-* attributes
	        var modal = $(this);
	        modal.find('input[name=trigger]').val(trigger);
	    });*/

	    $('.wilkModal form').on('submit', function(){
	        var form = $(this);
	        $(form).find('.btn').attr('disabled','true');
	        $.ajax({
	            type: 'POST',
	            dataType: 'JSON',
	            url: "/ajax.php?file=callback",
	            data: $(form).serialize(),
	            success: function(data){
	                if(data.result)
	                {
	                    $(form).closest(".wilkModal").modal('hide');
	                    $("#thankYou").modal('show');
	                    //$(form).closest(".modal").modal('show');
	                }
	                else
	                {
	                    alert(data.message);
	                    console.log(data.message);
	                }
	            },
	            complete: function(){
	                $(form).find('.btn').removeAttr('disabled');
	            }
	        });
	        return false;
	    });
	});
</script>