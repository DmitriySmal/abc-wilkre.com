<?php
$title = htmlspecialchars($q['name']);
if ($i==1) {    
	$old=0;
	echo '<div id="menu"><ul class="l1">';
}
if ($old>0 && $old>=$q['level'] ) echo '</li>';
if ($old>$q['level']) for ($n=$q['level']; $n<$old; $n++) echo '</ul></li>';
if ($old<$q['level'] && $old>0) echo '<ul class="l'.$q['level'].'">';
$url = $q['module']=='index' ? '' : ( (strpos($q['url'], '://')) ? $q['url'] : ('/'.$q['url'].'/'));//.'/';
if (@$q['submenu']) {
	$url = '/'.$u[1].$url;
	$class = @$u[2] == $q['url']  ? ' active' : '';
}
else {
	//$class = (@$u[1]==$q['url']) ? ' active' : '';
	$class = ((@$u[1] == $q['url']) && (@$u[2] != 'pre-construction')) ? ' active' : (((@$u[2] == 'pre-construction') && ($q['url'] == 'condos/pre-construction')) ? ' active ' : '');
}
if (strtolower($q['name'])=='neighborhoods') $class.= ' submenu" data-target="#widget-nbs';
//if (strtolower($q['name'])=='for rent') $class.= ' submenu" data-target="#widget-for-rent ';
//if (strtolower($q['name'])=='for sale') $class.= ' submenu" data-target="#widget-for-sale';

if ($q['url']==$modules['open_houses']) {
	$where = '';
	if ($u[1]==$modules['shop_ny']) $where = " AND (sp.base=3 OR sp.base=4)";
	if ($u[1]==$modules['shop_mx']) $where = " AND (sp.base IN (3,4,6,7) )";
	if ($where) {
		$query = "
			SELECT COUNT(*)
			FROM shop_products AS sp
			/*LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood*/
			WHERE sp.display = 1 ".$where." AND oh_display=1 AND oh_date>='".date('Y-m-d')."'
			ORDER BY sp.oh_date
		";
		$q['name'].= ' ('.mysql_select($query,'string',60*5).')';
	}
}

echo '<li class="l'.$q['level'].$class.'"><span></span><a data-id="'.$q['id'].'" class="l'.$q['level'].$class.'" href="'.$url.'" title="'.$title.'">'.$q['name'].'</a>';
if (strtolower($q['name'])=='for rent') {
	$array = array(
		'Apartments for rent'=>'?residential=1&property_type=3&property_type2=5',//Condo',
		'Homes for rent'=>'?residential=1&property_type=3&property_type2=8',//Single',
		'Commercials for rent'=>'?residential=1&property_type=4'
	);
	$array = array(
		'Residential for rent'=>'?residential=1&property_type=1',
		'Residential Income for rent'=>'?residential=1&property_type=6',
		'Lands for rent'=>'?residential=1&property_type=3',
		'Commercial for rent'=>'?residential=1&property_type=2',
	);
	if ($u[1]==@$modules['shop_ny'] OR $u[1]==$modules['shop_mx']) {
		$array = array(
			/*'Condo' => '?residential=1&property_type=1&property_type_1=2',
			'Co-Op' => '?residential=1&property_type=1&property_type_1=3',
			'Single Family' => '?residential=1&property_type=1&property_type_1=4',
			'Multi-Family' => '?residential=1&property_type=1&property_type_1=1',
			'Mixed Use/Store & Family' => '?residential=1&property_type=1&property_type_1=5',*/
			'Residential Real Estate' => '?residential=1&property_type=1',
			'Commercial Real Estate' => '?residential=1&property_type=2',
			//'Land' => '?residential=1&property_type=3',
		);
	}
	echo '<ul class="l2" data-id="'.$q['id'].'">';
	foreach($array as $k=>$v){?>
		<li><a href="/<?=$u[1]?>/<?=$v?>"><?=$k?></a></li>
	<?}
	echo '</ul>';
}
if (strtolower($q['name'])=='for sale') {
	$array = array(
		'Condo/Co-op Apartments for sale'=>'?residential=2&property_type=2',
		'Homes for Sale'=>'?residential=2&property_type=1',
		'Residential income for sale'=>'?residential=2&property_type=6',
		'Commercials for sale'=>'?residential=2&property_type=4',
		'Lands for Sale'=>'?residential=2&property_type=5'
	);
	$array = array(
		'Residential for sale'=>'?residential=2&property_type=1',
		'Residential Income for sale'=>'?residential=2&property_type=6',
		'Lands for sale'=>'?residential=2&property_type=3',
		'Commercial for sale'=>'?residential=2&property_type=2',
    );

	if ($u[1]==@$modules['shop_ny'] OR $u[1]==$modules['shop_mx']) {
		$array = array(
			'Condo Appartments' => '?residential=2&property_type=1&property_type2=11',//Condo',
			'Co-Op Appartments' => '?residential=2&property_type=1&property_type2=12',//Co-Op',
			'Single Family Houses' => '?residential=2&property_type=1&property_type2=13',//Single+Family',
			'Multi-Family Homes' => '?residential=2&property_type=1&property_type2=10',//Multi-Family',
			'Mixed Use/Store & Family Houses' => '?residential=2&property_type=1&property_type2=14',//Mixed+Use%2FStore+%26+Family',
			'Commercial Real Estate' => '?residential=2&property_type=2',//Commercial',
			'Land' => '?residential=2&property_type=3',//Land',
		);
	}
	echo '<ul class="l2" data-id="'.$q['id'].'">';
	foreach($array as $k=>$v){?>
		<li><a href="/<?=$u[1]?>/<?=$v?>"><?=$k?></a></li>
	<?}
	echo '</ul>';
}
$old = $q['level'];

if ($i==$num_rows) {
	//for ($a=1; $a<=$q['level']; $a++)
	echo '</li></ul>';
?>
</div>
<script type="text/javascript">
function ready_menu_list () {
	$('#menu ul.l2').parent('li').addClass('parent');
	$('#menu li.parent>a').attr('href','#');
	$(document).on("click",'#menu li.parent>a',function(){
		$(this).next().slideToggle();
		var id = $(this).data('id');
		$('#menu ul.l2').each(function(){
			var i = $(this).data('id');
			//alert(i+' '+id);
			if (i!=id) {
				if ($(this).css('display')=='block') $(this).slideUp();
			}
		});
		return false;
	});


	/*$('#menu li').hover(
		function () {
			$(this).addClass('hover').children('ul').fadeIn(300);
		},
		function () {
			$(this).removeClass('hover').children('ul').fadeOut(300);
		}
	);*/

	//$('#menu .l2.active').parents('li.l1').addClass('active');
}
document.addEventListener("DOMContentLoaded", ready_menu_list);
</script>
<?php } ?>