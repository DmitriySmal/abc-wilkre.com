<?php
//если письмо отправилось
if (isset($q['success'])) {
	echo html_array('form/message',i18n('feedback|message_is_sent',true));
}
else {
	?>
<?=html_sources('return','jquery_validate')?>
<noscript><?=i18n('validate|not_valid_captcha2')?></noscript>
<?=isset($q['message']) ? html_array('form/message',$q['message']) : ''?>
<form
	method="post"
	class="form validate"
>

    <div class="form-group">
        <!-- <label for="first_name"><?=i18n('feedback|first_name',true)?></label> -->
        <input class="form-control " placeholder="<?=i18n('feedback|first_name')?>" name="name" value="<?=isset($q['name']) ? $q['name'] : ''?>">
    </div>
    
    <div class="form-group">
        <!-- <label for="last_name"><?=i18n('feedback|last_name',true)?></label> -->
        <input class="form-control " placeholder="<?=i18n('feedback|last_name')?>" name="surname" value="<?=isset($q['surname']) ? $q['surname'] : ''?>">
    </div>
    
    <div class="form-group">
        <!-- <label for="phone"><?=i18n('feedback|phone',true)?></label> -->
        <input class="form-control required" placeholder="<?=i18n('feedback|phone')?>" name="phone" value="<?=isset($q['phone']) ? $q['phone'] : ''?>" placeholder="xxx-xxx-xxxx">
    </div>
    
    <div class="form-group">
        <!-- <label for="email"><?=i18n('feedback|email',true)?></label> -->
        <input class="form-control required email" placeholder="<?=i18n('feedback|email')?>" name="email" value="<?=isset($q['email']) ? $q['email'] : ''?>">
    </div>
    
    <div class="form-group">
        <!-- <label for="text"><?=i18n('feedback|text',true)?></label> -->
        <textarea class="form-control required" placeholder="<?=i18n('feedback|text')?>" name="text"><?=isset($q['text']) ? $q['text'] : ''?></textarea>
    </div>
    
    <?=html_array('form/captcha2')?>
    <button
		<?php
		if ($u[1]==$modules['feedback']) {
		?>onclick="ga('send', 'event', 'forSellers', 'Push'); yaCounter34569400.reachGoal('forSellers'); return true;"<?php
		} else {
		?>onclick="ga('send', 'event', 'forBuyers', 'Push'); yaCounter34569400.reachGoal('forBuyers'); return true;"<?php
		}
		?>
	    type="submit" class="btn btn-danger"><?=i18n('feedback|send')?></button>
<?php
/*
echo html_array('form/input',array(
	'caption'	=>	i18n('feedback|email',true),
	'name'		=>	'email',
	'value'		=>	isset($q['email']) ? $q['email'] : '',
	'attr'		=>	'class="required email"',
));
echo html_array('form/input',array(
	'caption'	=>	i18n('feedback|name',true),
	'name'		=>	'name',
	'value'		=>	isset($q['name']) ? $q['name'] : '',
	'attr'		=>	'class="required"',
));
echo html_array('form/textarea',array(
	'name'		=>	'text',
	'caption'	=>	i18n('feedback|text',true),
	'value'		=>	isset($q['text']) ? $q['text'] : '',
	'attr'		=>	'class="required"',
));
echo html_array('form/file',array(
	'caption'	=>	i18n('feedback|attach',true),
	'name'		=>	'attaches[]',
));
echo html_array('form/captcha2');//скрытая капча
echo html_array('form/button',array(
	'name'	=>	i18n('feedback|send'),
));
 
 */
?>
</form>
<?php } ?>