<?php
$q['city_name'] = $config['shop_cities'][$q['city']]['name'];
$q['city_url']	= $config['shop_cities'][$q['city']]['url'];
$img = $q['img'] ? '/files/shop_preconstructions/'.$q['id'].'/img/'.$q['img'] : '/templates/images/no_img.jpg';
$images = $q['imgs'] ? unserialize($q['imgs']) : false;
//$images = $q['imgs'] ? unserialize($q['imgs']) : false;
$title = filter_var($q['name'], FILTER_SANITIZE_STRING);
?>
<div class="row">    
    <div class="col-md-12">    
        <div class="shop-condo-text shop-preconstraction-text">
	        <div class="col-md-12">
		        <div class="page-header"><?=$q['name']?>
			        <span><?=@$q['address']?>, <?= $q['nb_name'] ?>, <?= $q['city_name'] ?><?=$q['zip_code']?', '.$q['zip_code']:''?></span>
		        </div>
	        </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="gallary">
                        <?php if ($q['img']) { ?><a title="<?= $title ?>" onclick="return hs.expand(this, config1)" href="/files/shop_preconstructions/<?= $q['id'] ?>/img/<?= $q['img'] ?>"><?php } ?>
                            <img class="main" src="<?= $img ?>" alt="<?= $title ?>" />
                            <?php if ($q['img']) { ?></a><?php } ?>
							<?php
							if ($images OR $q['video']){
								$n = 0;
								$list = '';
								if ($images) foreach ($images as $k => $v) {
									if (@$v['display'] == 1) {
										$n++;
										$title2 = filter_var($v['name'], FILTER_SANITIZE_STRING);
										$path = '/files/shop_preconstructions/' . $q['id'] . '/imgs/' . $k . '/';
										if (is_file(ROOT_DIR . $path . $v['file'])) {
											$list .= '<li><a title="' . $title2 . '" onclick="return hs.expand(this, {slideshowGroup: \'group1\',transitions: [\'expand\', \'crossfade\']})" href="' . $path . $v['file'] . '"><img src="' . $path . 'p-' . $v['file'] . '" alt="' . $title2 . '" /></a></li>';
										}
									}
								}
								if ($q['video']) {
									$list .= '<li><a data-toggle="modal" data-target="#video" title="video" href="#video"><img src="/templates/images/youtube.png" /></a></li>';
								}
								?>
								<div class="carousel">
									<ul style=""><?= $list ?></ul>
								</div>
							<?php
							}
						?>
                    </div>
                    <div class="control-buttons">
                        <div class="row" style="margin-bottom: 25px;">
                                <div class="col-md-6">
                                    <a target="_blank" href="?print=1" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-print"></span> <?=i18n('shop|btn_print')?></a>
                                </div>
	                        <?php /*
                                <div class="col-md-6">
                                    <a href="#" data-url="<?=$data_url?>" data-name="<?=$data_name?>"  data-toggle="modal" data-target="#saveFavorite" data-trigger="You Saved to Favorite http://<?=$_SERVER['HTTP_HOST']?><?=$_SERVER['REQUEST_URI']?>" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-star"></span> <?=i18n('shop|btn_sent_email')?></a>
                                </div>
 */?>
                        </div>
                        <div class="row">
	                        <?php /*
                                <div class="col-md-6">
                                    <a href="#" data-url="<?=$data_url?>" data-name="<?=$data_name?>" data-toggle="modal" data-target="#sheduleShowing" data-trigger="Shedule a Showing :<?=$q['name']?>" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-calendar"></span> <?=i18n('shop|btn_schedule_showing')?></a>
                                </div>
 */?>
                                <? /*if(@$q['booklet']){
                                    $booklet_url = "files/shop_preconstructions/{$q['id']}/booklet/{$q['booklet']}";
                                    ?>
                                    <?if(is_file(ROOT_DIR.$booklet_url)){?>
                                        <div class="col-md-6">
                                            <a href="/<?=$booklet_url?>" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-save"></span> Download booklet</a>
                                        </div>
                                    <?}?>
                                <?} */?>
		                        <?php if ($q['booklet']) {?>
			                        <div class="col-md-6"><a href="<?=$q['booklet']?>" target="_blank" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-download"></span>Brochure with floor plans </a></div>
		                        <?php } ?>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="h4 social-header">Share and discuss with your friends!</div>
                            </div>
                        </div>
                        <?
                            $share_url = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
                            $share_text = $q['name'];
                        ?>
                        <div class="row">                            
                            <div class="col-md-3">
                                <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?=$share_url?>" class="btn btn-default btn-lg btn-fb">Facebook</a>
                            </div>
                            <div class="col-md-3">
                                <a target="_blank" href="https://www.linkedin.com/cws/share?url=<?=$share_url?>" class="btn btn-default btn-lg btn-li">LinkedIn</a>
                            </div>
                            <div class="col-md-3">
                                <a target="_blank" href="http://twitter.com/share?url=<?=$share_url?>&text=<?=$share_text?>&via=wilkre" class="btn btn-default btn-lg btn-tw">Twitter</a>
                            </div>
                            <div class="col-md-3">
                                <a target="_blank" href="http://plus.google.com/share?url=<?=$share_url?>" class="btn btn-default btn-lg btn-gl">Google+</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="info">

<?php /*
                        <div class="send-request" style="margin-bottom: 20px;">
                            <?=i18n('condos|send_request_1')?>
                            <?=i18n('condos|send_request_2')?>   <a class="btn btn-warning" data-toggle="modal" data-target="#sendRequest" data-trigger="<?=$q['name']?>">Send Request</a>
                        </div>
                        */?>
                        <div class="price_box">
	                        <?php
	                        $parameters = unserialize($q['parameters']);
	                        //print_r($parameters);
	                        if (is_array($parameters) AND count($parameters>0)) {
	                        ?>
	                        <table class=" table parameters">
		                        <?php
		                        foreach ($parameters as $k=>$v) {
			                        ?>
			                        <tr>
				                        <td class="param"><?=$k?>:</td>
				                        <td><?=$v?></td>
			                        </tr>
		                        <?php
		                        }
		                        ?>
	                        </table>
							<?php } ?>
                        </div>
                        <?//= html_array('common/share') ?>                    
                    </div>
                </div>
            </div>

            <?if(@$q['text']){?>
                <div class="row">
                    <div class="col-md-12">
                      <?php /*  <div class="h3"><span>Description</span></div>*/?>
                        <div class="text">
                            <?= $q['text'] ?>
                        </div>
                    </div>
                </div>
            <?}?>

            <div class="row">
                <div class="col-md-12">
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#map" aria-controls="map" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-map-marker"></span> Map location</a></li>                            
                            <!--
                            <li role="presentation"><a href="#street" aria-controls="street" role="tab" data-toggle="tab">Street view</a></li>          
                            
                            <li role="presentation"><a href="#schools" aria-controls="schools" role="tab" data-toggle="tab">Schools</a></li>
                            <li role="presentation"><a href="#walkscore" aria-controls="walkscore" role="tab" data-toggle="tab">Walk Score</a></li>
                            <li role="presentation"><a href="#calculator" aria-controls="calculator" role="tab" data-toggle="tab">Mortgage Calculator</a></li>
                            -->
                            <?/*if(@$q['video']){?>
                                <li role="presentation"><a href="#video" aria-controls="video" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-facetime-video"></span> Video</a></li>                            
                            <?}*/?>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="map">                                
                                 <?=html_array('common/widgets/map',$q)?>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="street">
                                <div id="map_street_view" style="width: 100%; height: 500px;"></div>
                            </div>  
                            <div role="tabpanel" class="tab-pane fade" id="schools">
                                ...
                            </div> 
                            <div role="tabpanel" class="tab-pane fade" id="walkscore">
                                ...
                            </div> 
                            <div role="tabpanel" class="tab-pane fade" id="calculator">
                                <?//=html_array('common/widgets/calc', array('price'=>$q['price']))?>
                            </div>   
                            <?php /*if(@$q['video']){
                                $video_url = YouTubeURLs($q['video']);
                                ?>
                                <div role="tabpanel" class="tab-pane fade" id="video">
                                    <?//var_dump($video_url)?>
                                    <iframe style="width: 100%; height: 420px; margin: 0 auto; display: inline-block;" src="//www.youtube.com/embed/<?=$video_url?>" frameborder="0" allowfullscreen></iframe>
                                </div> 
                            <?}*/?>
                        </div>
                    </div>
                </div>
            </div>

        </div>    
    </div>
</div>

<?php
if(@$q['video']) {
	$video_url = YouTubeURLs($q['video']);?>
	<div class="modal fade wilkModal" id="video" tabindex="-1" role="dialog" aria-labelledby="video" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				<div class="modal-body">
					<iframe style="width: 100%; height: 420px; margin: 0 auto; display: inline-block;"
					        src="//www.youtube.com/embed/<?= $video_url ?>" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
<?php
}
?>

<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function () {
		$('.shop-preconstraction-text .text h4').each(function() {
			$(this).html('<span>' + $(this).text() + '</span>');
		});

        $('body').on('click', '#video .close',function(){
            var src = $(this).closest(".modal-dialog").find("iframe").attr("src");
            $(this).closest(".modal-dialog").find("iframe").attr("src","").attr("src",src);
        });
    });
</script>