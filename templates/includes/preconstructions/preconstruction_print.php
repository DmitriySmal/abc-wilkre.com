<?php
$img = $q['img'] ? '/files/shop_preconstructions/'.$q['id'].'/img/m-'.$q['img'] : '/templates/images/no_img.jpg';
$images = $q['imgs'] ? unserialize($q['imgs']) : false;
$title = filter_var($q['name'], FILTER_SANITIZE_STRING);
?>
<div class="row">
	<div class="col-md-12">
		<div class="shop-condo-text">
			<h3><?=$q['name']?></h3>
			<?=@$q['address']?>, <?= $q['nb_name'] ?>, <?= $q['city_name'] ?><?=$q['zip_code']?', '.$q['zip_code']:''?>
			<div class="row">
				<div class="col-xs-6">
					<div class="gallary">
						<img class="main" src="<?= $img ?>" alt="<?= $title ?>" />
						<?php
							if ($images) {
								$n = 0;
								$list = '';
								foreach ($images as $k => $v)
									if (@$v['display'] == 1) {
										$n++;
										$title2 = filter_var($v['name'], FILTER_SANITIZE_STRING);
										$path = '/files/shop_preconstructions/' . $q['id'] . '/imgs/' . $k . '/';
										if(is_file(ROOT_DIR.$path.$v['file'])){
											$list.= '<li><img src="' . $path . 'p-' . $v['file'] . '" alt="' . $title2 . '" /></li>';
										}
									}
								?>
								<div class="carousel">
									<ul style=""><?= $list ?></ul>
								</div>
							<?php
							}
						?>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="info">

					</div>
				</div>
			</div>

			<?if(@$q['text']){?>
				<div class="row">
					<div class="col-md-12">
						<div class="h3"><span>Description</span></div>
						<div class="text">
							<?= $q['text'] ?>
						</div>
					</div>
				</div>
			<?}?>

		</div>
	</div>
</div>
