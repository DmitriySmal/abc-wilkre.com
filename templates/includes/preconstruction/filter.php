<?
$is_map = $html['is_map'];
$per_page = array(12,24,48);;
?>
<div class="page-filter">
    <div class="row">    
        <div class="col-md-4">
            <div class="control-group">
                <label for="sorted">Sorted by:</label>
                <select name="sorted" <?=($is_map) ? 'disabled class="disabled"' :''?> >
                    <option value="0">Preferred Listings</option>
                </select>
            </div>
        </div>
        <div class="col-md-8 text-right">
            <div class="per-page <?=($is_map) ? 'disabled' :''?>">
                <span>Per page</span>
                <ul>
                    <?
                        $get = $_GET;
                        unset($get['u'],$get['count']);
                        $bquery = http_build_query($get);
                    ?>
                    <?foreach($per_page as $v){                            
                        $url = '/' . $modules['shop'] . '/' . (($bquery) ? '?'.$bquery.'&' : '?').'count='.$v;
                        ?>
                        <li class="<?=(($v == @$_GET['count']) || (($v == reset($per_page)) && !@$_GET['count']) ) ? 'active' : '' ?>"><a href="<?=$url?>"><?=$v?></a></li>
                    <?}?>                    
                </ul>
            </div>
            <a href="/<?=$modules['shop']?>/" class="<?=(!$is_map) ? 'active' :''?> btn btn-light-gray-bordered">Photo view</a>
            <a href="?view=map" class="<?=($is_map) ? 'active' :''?> btn btn-light-gray-bordered">Map view</a>        
        </div>
    </div>
</div>