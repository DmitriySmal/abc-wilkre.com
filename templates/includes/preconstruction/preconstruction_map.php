<?php
if ($i==1) {
	?>
<div id="map_view_content" style="display:block"></div>
<script src="https://maps.googleapis.com/maps/api/js?sensor=true&language=ru"></script>
<script src="/templates/scripts/markerclusterer.js"></script>
<script type="text/javascript">
	var	marker = [],
		map = new google.maps.Map(document.getElementById('map_view_content'), {
			center: new google.maps.LatLng(<?=$q['lat']?>,<?=$q['lng']?>),
			zoom: 12
		});
	var markers = [
	<?php
	}
?>
		['<?=$q['id']?>',<?=$q['lat']?>,<?=$q['lng']?>],
<?php
if ($i==$num_rows) {
	?>
	];
	markers.forEach(function(item, i) {
		marker[i] = new google.maps.Marker({
			position: new google.maps.LatLng(markers[i][1],markers[i][2]),
			map: map,
			id:markers[i][0]
		});
		google.maps.event.addListener(marker[i], 'click', function() {
			infowindow = new google.maps.InfoWindow();
			infowindow.open(map,marker[i]);
			$.get('/ajax.php?file=map&type=preconstruction', {'id':marker[i]['id']},function(data){
				infowindow.setContent(data);
			});

		});
	});
	new MarkerClusterer(map, marker);
</script>
	<?php
}