<?php
$img = $q['img'] ? '/files/shop_preconstruction/'.$q['id'].'/img/m-'.$q['img'] : '/templates/images/no_img.jpg';
//$img = '/templates/images/product-empty.jpg';
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$alt = $q['img'] ? 'p-'.$q['img'] : i18n('common|wrd_no_photo');
//$url = '/'.$modules['shop'].'/'.$q['category'].'-'.$q['category_url'].'/'.$q['id'].'-'.$q['url'].'/';
$url = "/{$modules['preconstruction']}/{$q['city_url']}/{$q['id']}/";//get_product_url($q);
?>
<?if (($i==1)){?>
    <div class="row">
<?}?>
<div class="col-md-4">
    <div class="shop-condo-list">
            <div class="img">
                <a href="<?=$url?>" rel="nofollow"><img src="<?=$img?>" alt="<?=$title?>" /></a>
            </div>
            <div class="info">
                <a class="name" href="<?=$url?>" title="<?=$title?>"><?=implode(', ', array($q['name'], $q['city_name']))?></a>               
                <div class="text">                    
                    <?/*= ((@$q['beds']) ? $q['beds'].' Beds': '') ?> <?= ((@$q['baths']) ? '/ '.$q['baths'].' Baths': '') ?> <?= ((@$q['square']) ? '/ '.$q['square'].' Sq. Ft.': '') ?>  / Condo MLS: <?=$q['mln']*/?>
                </div>                
            </div>
    </div>
</div>

<?if (($i==$num_rows) || (!fmod($i,3))){?>
    </div>
<?}?>

<?if (($i!=$num_rows) && (!fmod($i,3))){?>
    <div class="row">
<?}?>