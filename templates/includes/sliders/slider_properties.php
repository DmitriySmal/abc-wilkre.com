<?
// TODO: не забыть добавить сортировку по языкам сюда и в админку
$path = (@$q['path']) ? $q['path'] : 'shop_products';
//$where = ($state['id']!=9999) ? " AND sp.city IN (".implode(', ', $_cities_ids).")" : '';
$where = (@$q['featured']) ? " AND sp.special = 1 " : '';
$query =    "
	SELECT sp.*,
		neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
	FROM shop_products AS sp
	LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
	WHERE sp.display = 1  AND sp.img!='' {$where}
";
foreach ($config['global_property_type'] as $k=>$v) {
	//ньюйорк
	$where3 = " AND sp.base=3";
	if ($k==1) $where3.= " AND sp.property_type=1 AND sp.property_type2 IN (2,3)";
	if ($k==2) $where3.= " AND sp.property_type=1 AND sp.property_type2 IN (1,4)";
	if ($k==3) $where3.= " AND sp.property_type=3";
	if ($k==4) $where3.= " AND sp.property_type=2";

	//флорида
	$where2 = " AND sp.base=2 AND sp.status = 'A'";
	if ($k==1) $where2.= " AND sp.category=2";
	if ($k==2) $where2.= " AND sp.category IN (1,3)";
	if ($k==3) $where2.= " AND sp.category IN (4,5)";
	if ($k==4) $where2.= " AND sp.category IN (7,8)";

	$order = " ORDER BY sp.id DESC LIMIT 1";

	$items[$k][1] = mysql_select($query.$where3.$order,'row',60*60);
	$items[$k][2] = mysql_select($query.$where2.$order,'row',60*60);
}
?>
<?if (count($items) && $items){?> 
<div class="slider-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="h3">
                    <span><?=@$q['header']?></span>
                </div>
            </div>
        </div>

        <div class="row">
	        <div class="col-md-12 text-center">
		        <div class="switch-types btn-group text-center" role="group" aria-label="...">
		        <?php foreach($config['global_property_type'] as $k=>$v) {?><button data-id="<?=$k?>" type="button" class="btn btn-default <?=$k==1?'active':''?>"><?=$v?></button><?php } ?>
		        </div>
	        </div>

        </div>    
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                $(".switch-types button").on('click',function(){
                    $(this).closest(".switch-types").find('button').removeClass('active');
                    $(this).addClass('active');
                    
                    var slide_num = 0;
	                slide_num = $(this).data('id');
                        
                    $(this).closest('.slider-container').find(".tab-content .tab-pane.active").removeClass('in active');
                    $(this).closest('.slider-container').find(".tab-content #slider"+slide_num).addClass('in active');
                    var sl = $(this).closest('.slider-container').find(".tab-content #slider"+slide_num).find('.swiper-slides');
                    sl.swiper({
                        pagination: $(sl).find('.swiper-pagination'),
                        nextButton: $(sl).find('.swiper-button-next'),
                        prevButton: $(sl).find('.swiper-button-prev'),
                        slidesPerView: 2,            
                        paginationClickable: true,
                        spaceBetween: 0,
                        autoplayDisableOnInteraction: false
                    });                    
                });
            });
        </script>
    </div>

    <div class="tab-content">
        <?foreach($items as $k=>$slides) {?>
            <div role="tabpanel" class="tab-pane fade <?=($k==1)?'in active':''?>" id="slider<?=$k?>">
                <div class="container swiper-slides swiper-container">        
                    <div class="row swiper-wrapper">
                        <?foreach ($slides as $s) if ($s) {
	                        $s['category_name'] = $config['shop_categories'][$s['category']]['name'];
	                        $s['category_url']	= $config['shop_categories'][$s['category']]['url'];
	                        $s['city_name'] = $config['shop_cities'][$s['city']]['name'];
	                        $s['city_url']	= $config['shop_cities'][$s['city']]['url'];
                            $img = "/files/{$path}/{$s['id']}/img/m-{$s['img']}";
	                        if (is_file(ROOT_DIR . $img)==false) {
		                        //отключаем объект и на повторный парсинг
		                        mysql_fn('update','shop_products',array('id'=>$s['id'],'display'=>0,'date'=>date('Y-m-d H:i:s')));
	                        }
                            $img = (is_file(ROOT_DIR . $img)) ? $img : '/templates/images/no_img.jpg';
                            $url = get_product_url($s);
                            $name = implode(', ', array($s['name'], $s['city_name'], $s['neighborhood_name']));
                            //$img = "/templates/images/tmp1.png";                        
                            ?>
                            <div class="col-md-6 swiper-slide">
                                <div class="card">
                                    <div class="img">
                                        <a href="<?= $url ?>" rel="nofollow"><img src="<?= $img ?>"></a>
                                        <div class="city"><?= $s['city_name'] ?></div>
                                    </div>
                                    <div class="info">
                                        <div class="name"><a href="<?= $url ?>" title="<?= strip_tags($name) ?>"><?= $name ?></a></div>
                                        <div class="desc">MLS: <?= $s['mln'] ?></div>
	                                    <div class="courtesy">Listing courtesy of <?=$s['office_name']?></div>
                                        <a class="btn btn-warning" href="<?= $url ?>" rel="nofollow">more details</a>
                                    </div>
                                </div>
                            </div>
                        <?}?>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>  
                    <div class="swiper-pagination"></div>
                </div> 
            </div>
        <?}?>    
    </div>     
</div> 
    
<?}?>