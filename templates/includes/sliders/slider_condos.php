<?
$q['city_name'] = $config['shop_cities'][$q['city']]['name'];
$q['city_url']	= $config['shop_cities'][$q['city']]['url'];
// TODO: не забыть добавить сортировку по языкам сюда и в админку
//$slides = mysql_select("SELECT * FROM slider WHERE display = 1");
$slides = (@$q['items']) ? $q['items'] : array();
$path = (@$q['path']) ? $q['path'] : 'shop_products';
//$slides = (@$q['items']) ? $q['items'] : array();

?>
<?if (count($slides)){?> 

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="h3">
                <span><?=@$q['header']?></span>
            </div>
        </div>
    </div>
</div>


            <div class="container swiper-slides swiper-container condos-container">        
                <div class="row swiper-wrapper">
                    <?foreach ($slides as $s) {
                        $img = "/files/{$path}/{$s['id']}/img/m-{$s['img']}";
                        //var_dump($img);
                        $img = (is_file(ROOT_DIR.$img)) ? $img : '/templates/images/no_img.jpg';
                        //$url = get_product_url($s);
                        //$url = "/{$s['state_url']}/{$modules['condos']}/{$config['condo_statuses'][$s['type']]}/{$s['city_url']}/{$s['id']}-{$s['url']}/";
	                    $url = get_url('condo',$s);
	                    $name = $s['name'];//implode(', ', array($s['name'],$s['city']));
                        //$img = "/templates/images/tmp1.png";                        
                        ?>
                        <div class="col-md-6 swiper-slide">                            
                            <div class="card">
                                <div class="info">
                                    <div class="name nowrap toupper"><a href="<?=$url?>" title="<?=strip_tags($name)?>"><?=$name?></a></div>
                                </div>                                
                                <div class="img">
                                    <a href="<?=$url?>" rel="nofollow"><img src="<?=$img?>"></a>
                                    <div class="city"><?=$s['city_name']?></div>
                                </div>
                                <div class="info">
                                    <?if(@$s['address']){?><div class="name"><a href="<?=$url?>" title="<?=strip_tags($name)?>"><?=$s['address']?></a></div><?}?>
                                    <div class="desc">
                                        <?=($s['min_price'] && $s['max_price']) ? "Price: {$s['min_price']} to {$s['max_price']}<br>" :''?>
                                    </div>                                
                                    <a class="btn btn-warning" href="<?=$url?>" rel="nofollow">more details</a>
                                </div>
                            </div>
                        </div>
                    <?}?>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>  
                <div class="swiper-pagination"></div>
            </div> 

   
    <script>
        /*
        var slider = new Swiper('.swiper-slides', {
            pagination: '.swiper-slides .swiper-pagination',
            nextButton: '.swiper-slides .swiper-button-next',
            prevButton: '.swiper-slides .swiper-button-prev',
            slidesPerView: 2,            
            centeredSlides: true,
            paginationClickable: true,
            spaceBetween: 0,
            //autoplay: 2500,
            autoplayDisableOnInteraction: false
        });*/
    </script>
    
<?}?>