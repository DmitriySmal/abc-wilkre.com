<?php

define('ROOT_DIR', __DIR__ . '/');
include_once ROOT_DIR . 'vendor/autoload.php';

//
$config['rets_url'] = 'https://miamire.apps.retsiq.com/contact/rets/login';
$config['rets_login'] = 'WiSkR';
$config['rets_pw1'] = 'gm83xpt5118';
$config['rets_pw2'] = '';

define('FIELD_UID', 'Matrix_Unique_ID'); // uid номер отдельный для Open House и листинга. Т.е. это разные объекты потому номера могут совпадать
define('FIELD_UID_LISTING', 'Listing_MUI'); // поле в опен хаусе, где содержится ссылка на UID объекта листинга
// логика других связей
// OpenHouse.Listing_MUI = Listing.Matrix_Unique_ID
// Rooms.Listing_MUI = Listing.Matrix_Unique_ID
// Listing.ListAgent_MUI = Agent.Matrix_Unique_ID
// Media.Table_MUI = Listing.Matrix_Unique_ID (media is where photos are stored)

$config['MiamiPropertyTypes'] = [
    'RE1' => 'Single Family',
    'RE2' => 'Condo/Co-op/Villa/Townhouse',
    'RIN' => 'Residential Income',
    'RLD' => 'Residential Land/Boat Docks',
    'CLD' => 'Commercial/Business/Agricultural/Industrial Land',
    'RNT' => 'Residential Rental',
    'COM' => 'Commercial/Industrial',
    'BUS' => 'Business Opportunity',
];

$config['MiamiStatuses'] = [
    'A' => 'Active',
    'B' => 'Backup Contract',
    'C' => 'Cancelled',
    'CS' => 'Closed Sale',
    'PS' => 'Pending Sale',
    'Q' => 'Terminated',
    'R' => 'Rented',
    'T' => 'Temp Off Market',
    'W' => 'Withdrawn',
    'X' => 'Expired',
];

// Matrix_Unique_ID for test 26931874, 26933794
echo '5 items list<br>';
$records = miami_list('(Status=A),(PostalCode=33139)',1000); //'(Status=A),(City=Miami)'
$property_type = array();
echo '<table>';
echo '<tr>';
echo '<td>MLSNumber</td>';
echo '<td>PropertyType</td>';
echo '<td>TypeofProperty</td>';
//echo '<td>PropertySubType</td>';
//echo '<td>StyleofProperty</td>';
echo '<td>PropTypeTypeofBuilding</td>';
echo '<td>City</td>';
echo '</tr>';
foreach ($records as $r) {
	echo '<tr>';
    echo '<td>'.$r['MLSNumber'].'</td>';
    echo '<td>'.$r['PropertyType'].'</td>';
	echo '<td>'.$r['TypeofProperty'].'</td>';
	//echo '<td>'.$r['PropertySubType'].'</td>';
	//echo '<td>'.$r['StyleofProperty'].'</td>';
	echo '<td>'.$r['PropTypeTypeofBuilding'].'</td>';
	echo '<td>'.$r['City'].'</td>';
	echo '</tr>';
    /**/
	if ($r['PropTypeTypeofBuilding']) $property_type[$r['PropertyType']][$r['PropTypeTypeofBuilding']] = $r['PropTypeTypeofBuilding'];
	else $property_type[$r['PropertyType']][$r['TypeofProperty']] = $r['TypeofProperty'];
}
echo '</table>';
foreach ($property_type as $k=>$v) {
	echo $k.'<br>';
	foreach ($v as $k1=>$v1) {
		echo '-' . $v1 . '<br>';
	}
}


/*
echo '<br><br>1 item for matrix ID 26931874<br>';
$record = miami_id(26931874);
echo '<pre>';
print_r($record);
echo '</pre>';
echo $record['Matrix_Unique_ID'] . '<br>';                 
    
echo '<br><br>1 item IMG for matrix ID 26931874<br>';
$imgs = miami_img(26931874);
foreach ($imgs as $img) { // $img содержит объект \PHRETS\Models\Object    
    echo "<img src='data:image/jpg;base64,".base64_encode($img->getContent())."' width='150' style='margin-right: 15px;'>";        
}
*/

function miami_id ($id,$type='Matrix_Unique_ID') {
	global $config;	
	if ($type=='MLSNumber') $query = "(MLSNumber=".$id.")";
	else $query = "(Matrix_Unique_ID=".$id.")";
	//	        
        try {
            $rets_config = new \PHRETS\Configuration;
            $rets_config->setLoginUrl($config['rets_url'])
                    ->setUsername($config['rets_login'])
                    ->setPassword($config['rets_pw1'])
                    ->setRetsVersion('1.7.2')
                    ->setOption('use_post_method', true);
            //
            $rets = new \PHRETS\Session($rets_config);       
            $rets->Login();
            
            if ($records = $rets->Search("Property", 'Listing', $query, array('Limit' => 1, 'Offset' => 1, 'Count' => 1))) {                                
                $rets->Disconnect();
                return isset($records[0]) ? $records[0] : [];
            }
            $rets->Disconnect();
	} catch (Exception $e) {
            echo '<br>'.$e->getMessage().'</br>';
	}
	return false;
}


function miami_list ($query='', $limit=5000, $offset = 1) {
	global $config;
	$query = $query ? $query : "(Matrix_Unique_ID=0+)";	
        //
        try {
            $rets_config = new \PHRETS\Configuration;
            $rets_config->setLoginUrl($config['rets_url'])
                    ->setUsername($config['rets_login'])
                    ->setPassword($config['rets_pw1'])
                    ->setRetsVersion('1.7.2')
                    ->setOption('use_post_method', true);
            //
            $rets = new \PHRETS\Session($rets_config);       
            $rets->Login();
            
            if ($records = $rets->Search("Property", 'Listing', $query, ['Limit' => $limit, 'Offset' => $offset])) {                                
                $rets->Disconnect();
                return $records;
            }
            $rets->Disconnect();
	} catch (Exception $e) {
            echo '<br>'.$e->getMessage().'</br>';
	}	
	return false;
}


function miami_img ($id) {
	global $config;
	
        try {
            $rets_config = new \PHRETS\Configuration;
            $rets_config->setLoginUrl($config['rets_url'])
                    ->setUsername($config['rets_login'])
                    ->setPassword($config['rets_pw1'])
                    ->setRetsVersion('1.7.2')
                    ->setOption('use_post_method', true);
            //
            $rets = new \PHRETS\Session($rets_config);       
            $rets->Login();
            
            $collection = $rets->GetObject('Property', 'LargePhoto', $id);      
            // в $collection возращается коллекция \Illuminate\Support\Collection привет Ларавел :)
            // технически коллекция имплементирует иттератор и должна через foreach() проходить, но матерится, может я чего недопонял :) потому не парился и просто воспользовался функциями коллекции
            // для общего развития https://laravel.com/api/5.5/Illuminate/Support/Collection.html
            if ($collection->count()) {
                    $rets->Disconnect();
                    return $collection->all();
            }
            //
            $rets->Disconnect();
	} catch (Exception $e) {
            echo '<br>'.$e->getMessage().'</br>';
	}
			
	return [];
}