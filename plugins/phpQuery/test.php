<?php

require('phpQuery/phpQuery.php');

$url = 'http://www.shinservice.ru/catalog/bridgestone/summer/ECOPIA+P-EP150/';

$results = file_get_contents($url);

$document = phpQuery::newDocument($results);

$data = $document->find('#prdct tr[valign=top]');

foreach ($data as $el) {
	$post = array(
		'url' => pq($el)->find('.float-th a')->attr('href'),
		'name' => pq($el)->find('.float-th a')->html(),
		'speed' => pq($el)->find('.float-th')->next('td')->html(),
		'weight' => pq($el)->find('.float-th')->next('td')->next('td')->html(),
		'price' => pq($el)->find('.p5 b')->html(),
		'count'=> pq($el)->find('.info-availability')->html(),
	);
	$post['count'] = preg_replace('~[^0-9]+~u', '', $post['count']);
	print_r($post);
	echo '<br/>';
}
?>