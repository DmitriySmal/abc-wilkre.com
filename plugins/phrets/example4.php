<?php @include_once('login.php'); ?>
<pre>
<?php

$rets = new PHRETS;

$connect = $rets->Connect($login, $un, $pw, $pw_ua);

if($connect) {
	$sysid = '130730782';
	$n = 1;
	$dir = 'photos/'.$sysid;
	if(!is_dir($dir)) mkdir($dir,0777,true); // Remember: this can only make one directory at a time
	$photos = $rets->GetObject('Property', 'Photo', $sysid);
	foreach($photos as $photo) {
		file_put_contents($dir.'/'.$n.'.jpg', $photo['Data']);
		$n++;
	}
	$rets->FreeResult($photos);
	$rets->Disconnect();
} else {
	$error = $rets->Error();
	print_r($error);
}

?>
</pre>