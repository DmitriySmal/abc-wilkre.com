<?php

$object = array();
define('ROOT_DIR', dirname(__FILE__).'/');
require_once(ROOT_DIR.'_config_ny.php');
require_once(ROOT_DIR.'functions/rets2_func.php');

$content = '<div id="filter">
        <form>
            <div class="filter">
            <input type="text" name="object" value="'.@$_GET['object'].'" placeholder="382985" style="top: -4px; position: relative;">
            <button type="submit" style="top: -3px; right: -4px; position: relative;">FIND</button>
            </div>
        </form>
</div>
<div class="clear"></div>';

if (@$_GET['object']) {
	if ($data = matrix_id ($_GET['object'],'MLSNumber')) {
		$content.= '<div class="object_information" style = "padding: 30px 0;" >';
		$content.= '<ul style="list-style: none; width: 100%; overflow: hidden; display: block; ">';
		if ($open_houses = matrix_open_houses (array('uid'=>$data['Matrix_Unique_ID']),true)) {
			foreach ($open_houses as $k=>$v) {
				$content.= '<li class="value" style="display: block; margin-right: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 800px;">';
				$content.= 'from '.$v['from'];
				$content.= '<br>to '.$v['to'];
				$content.= '<br>date '.$v['date'];
				$content.= '<br>OpenHouseID '.$v['OpenHouseID'];
				$content.= '<br>agent '.$v['agent'];
				$content.= '</li>';
			}
		}


		foreach ($data as $k=>$v) {
			$content.= '<li class="value" style="display: block; margin-right: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 800px;"><span class="key" style="margin-right: 10px; min-width: 250px; display: inline-block;">'.$k.'</span>'.@$v.'</li>';
		}

		$content.= '</ul>';
		if ($data2 = matrix_img ($data['Matrix_Unique_ID'])) {
			foreach ($data2 as $k => $v) {
				$img = base64_encode($v['Data']);
				$img = 'data: ' . $v['Content-Type'] . ';base64,' . $img;
				$content .= '<img style="width: 100px" src="' . $img . '">';
			}
		}
		else $content.= 'no photo';



	}
	else $content.= 'no object';
}

echo $content;
