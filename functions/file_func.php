<?php

//рекурсивное удаление папок с содержимым
function delete_all($dir,$i = true,$log = true) {
	if ($log == true) {
		//@trigger_error('delete '.$dir);
	}
	//удаление
	$dir_delete = $dir;
	$dir_delete = preg_replace('~[^_a-z0-9]+~u', '', $dir_delete);
	$dir_delete = substr($dir_delete, -13);
	if ($dir_delete=='shop_products') {
		@trigger_error('delete shop_products '.$dir);
		return false;
	}
	//заменяем все обратные слеши на нормальные
	$dir = str_replace('\\','/',$dir);
	//если есть подряд два слеша то удаления не будет так как есть вероятность что пропущена папка
	if (strpos($dir, '//')) return false;
	//если заканчивается на один слеш то удаляем его
	if (substr($dir, -1)=='/') $dir = substr($dir, 0, -1);
	//удаление файла
	if (is_file($dir)) return unlink($dir);
	if (!is_dir($dir)) return false;
	//запускаем рекурсию
	$dh = opendir($dir);
	while (false!==($file = readdir($dh))) {
		if ($file=='.' || $file=='..') continue;
		delete_all($dir.'/'.$file,true,false);
	}
	closedir($dh);
	//удаляем саму папку
	if ($i==true) return rmdir($dir);
}

//копирование папок с файлами
function rcopy($src, $dst) {
	if (file_exists($dst)) delete_all($dst);
	if (is_dir($src)) {
		mkdir($dst);
		$files = scandir($src);
		foreach ($files as $file)
			if ($file != "." && $file != "..") rcopy("$src/$file", "$dst/$file");
	}
	else if (file_exists($src)) copy($src, $dst);
}

?>