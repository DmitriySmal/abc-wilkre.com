<?php
/*
 * flex
 */

/** Property types
 *  A - Residential - Residential
 *  B - Residential Income - Residential Income
 *  C - Land/Docks - Land/Docks
 *  D - Business - Business
 *  E - Comm/Industry - Comm/Industry
 *  F - Rental - Rental
 */


$config['rets_flex'] = array(
	'login_url' => "http://retsgw.flexmls.com:80/rets2_3/Login",
	'username' => "fl.rets.631102147",
	'password' => "churr-poietic52",
);

require_once(ROOT_DIR."functions/phrets.php");

function flex_id ($id,$type) {
	global $config;
	$rets = new phRETS;
	$query = "(LIST_3=".$id.")"; //105
	$connect = $rets->Connect($config['rets_flex']['login_url'], $config['rets_flex']['username'], $config['rets_flex']['password']);        
	//print_r($connect);
	if ($connect) {
		$offset = 1;
		$limit = 1;
		if ($data = $rets->Search("Property", $type, $query, array('Limit' => $limit, 'Offset' => $offset,
			'Format' => 'COMPACT-DECODED', 'Count' => 1, "UsePost" => 1))) {
                        //var_dump($data);
			$rets->Disconnect();
			return $data[0];
		}
	}
	$rets->Disconnect();
	return false;
}

/**
 *  Тип запроса
 * 'Photo' => 300x192
 * 'Thumbnail' => 65x42
 * '640x480' => 640x480
 * 'HiRes' => 1024x768 и более
 *  часто бывает такая вот ошибка RETS Server: Data has not changed since last request. при работе с фотками 
 *  потому надо быть внимательными, если один раз не достанешь то, что пришло, как достать повторно я хз.
 *  Если ошибка, то она приходит в поле ReplyText.
 */
function flex_img ($id) {
	global $config;
	$rets = new phRETS;
	$connect = $rets->Connect($config['rets_flex']['login_url'], $config['rets_flex']['username'], $config['rets_flex']['password']);
	if ($connect) {
		//$media = $rets->GetObject('Property', 'Photo', $id);
		echo 'HiRes';
		$media = $rets->GetObject('Property', 'HiRes', $id);
		/*echo '<pre>';
		print_r($media);
		echo '</pre>';*/
		if ($media[0]['Success']==1) {
			$rets->Disconnect();
			return $media;
		}
	}
	$rets->Disconnect();
	return false;
}

/*
 * EVENT100 - startTime
 * EVENT200 - endTime
 * EVENT6 - Modification Timestamp
 * LIST1  - id mls в системе flex
 */
function flex_open_houses ($product,$test=false) {
	global $config;
	$date = date("Y-m-d");
	$oh_dates = @$product['oh_dates'] ? serialize($product['oh_dates']) : array();
	$rets = new phRETS;
	$connect = $rets->Connect($config['rets_flex']['login_url'], $config['rets_flex']['username'], $config['rets_flex']['password']);
	if ($connect) {
		$query = "(LIST105={$product['uid']})";
		$offset = 1;
		$limit = 100;  
                /* // пример вытащить пачку по датам
                 * var_dump($rets->Search("OpenHouse", 'OpenHouse', "(EVENT100=2016-01-01T00:00:00+)", array('Limit' => 5, 'Offset' => $offset,
			'Format' => 'COMPACT-DECODED', 'Count' => 1, "UsePost" => 1)));*/                
		if ($records = $rets->Search('OpenHouse', 'OpenHouse', $query, array('Limit' => $limit, 'Offset' => $offset,
			'Format' => 'COMPACT-DECODED', 'Count' => 1, "UsePost" => 1))) {                                                
			$oh_dates = array();
			/*echo '<pre>';
			print_r($records);
			echo '</pre>';*/
			foreach ($records as $k=>$v) {
				if ($test==true) print_r($v);
				$StartTime = date('H:i', strtotime($v['EVENT100']));				
				$EndTime = date('H:i', strtotime($v['EVENT200']));
				$OpenHouseDate = date('Y-m-d', strtotime($v['EVENT200']));
				//чтобы даты не повторялись
				$y = true;
				foreach($oh_dates as $k1=>$v1) {
					if (@$v1['OpenHouseID']==$v['EVENT0']) {
						$oh_dates[$k1] = array(
							'from' => $StartTime,
							'to' => $EndTime,
							'date' => $OpenHouseDate,
							'OpenHouseID' => $v['EVENT0'],
							'agent' => @$product['agent'],
						);
						$y = false;
						break;
					}
				}
				if ($y==true) {
					$oh_dates[] = array(
						'from' => $StartTime,
						'to' => $EndTime,
						'date' => $OpenHouseDate,
						'OpenHouseID' => $v['EVENT0'],
						'agent' => @$product['agent'],
					);
				}
			}
			/*echo '<pre>';
			print_r($oh_dates);
			echo '</pre>';*/
		}
		//else echo '<br>нет опенхаусов1';
	}
	if ($test==true) {
		return $oh_dates;
	}
	//else echo '<br>нет опенхаусов2';
	$data = array(
		'oh_date' => '0000-00-00',
		'oh_dates'=>'',
		'oh_display'=>''
	);
	if($oh_dates) {
		usort($oh_dates, "sort2");
		foreach ($oh_dates as $k=>$v) {
			echo '<br>'.$v['date'].' - '.$date;
			if (strtotime($v['date'])<strtotime($date)) unset($oh_dates[$k]);
			else {
				echo ' - ';
				if ($data['oh_date'] == '0000-00-00' OR strtotime($v['date']) > strtotime($data['oh_date'])) {
					$data['oh_date'] = $v['date'];
				}
				else echo 'x'.$data['oh_date'];
			}
		}
		if ($oh_dates) {
			$data['oh_dates'] = serialize($oh_dates);
			$data['oh_display'] = 1;
		}
	}
	$rets->Disconnect();
	return $data;
}


function flex_list ($query='',$type='A',$limit=5000) {
	global $config;
	//$query = $query ? $query : "(LIST_87=2016-01-01T00:00:00+)";
	$offset = 1;
	//$limit = 5000;
	$rets = new phRETS;
	$connect = $rets->Connect($config['rets_flex']['login_url'], $config['rets_flex']['username'], $config['rets_flex']['password']);
	if ($connect) {
		if ($records = $rets->Search("Property", $type, $query, array('Limit' => $limit, 'Offset' => $offset,
			'Format' => 'COMPACT-DECODED', 'Count' => 1, "UsePost" => 1))) {
			$rets->Disconnect();
			return $records;
		}
	}
	$rets->Disconnect();
	return false;
}