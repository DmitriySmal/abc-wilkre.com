<?php
ob_start("ob_gzhandler");
session_start();
        
@include_once(ROOT_DIR.'plugins/phrets/lib/phrets.php');
/*
$phrets_config  ->setLoginUrl('http://sef.rets.interealty.com/Login.asmx/Login  ')
                ->setUsername('WiSkR')
                ->setPassword('gm83xpt5118')
                ->setRetsVersion('1.7.2')
                ->setUserAgent('PHRETS/1.0')
                ->setUserAgentPassword('fghi4921');
*/

class PHRETS_CONTROLLER {
    
    private $_is_connected = false;
    /**
     *
     * @var PHRETS 
     */
    public $_rets;
    public $_headers = array();
    /**
     * 
     * @param type $url Login page url
     * @param type $login Login
     * @param type $pw1 User Password
     * @param type $pw2 User Agent Password
     * 
     */
    /*public function __construct($url, $login, $pw1, $pw2) {
        $this->_rets = new PHRETS;*/
        //$this->_rets->AddHeader("Accept", "*/*");
	/*
		$this->_rets->AddHeader("RETS-Version", "RETS/1.7.2");
		$this->_rets->AddHeader("User-Agent", "PHRETS/1.0");
		//$this->_rets->SetParam("cookie_file", $GLOBALS['cookie_file_name']);
	        $this->_rets->SetParam("cookie_file", ROOT_DIR.'rest_cookie.txt');
		$this->_rets->SetParam("compression_enabled", true);
        $this->_rets->SetParam("force_basic_authentication", true);
        //$this->_rets->SetParam("offset_support", true);
        
        $this->_is_connected = $this->_rets->Connect($url, $login, $pw1, $pw2);        
        
        if (! $this->_is_connected) {
            $error = $this->_rets->Error();
            die($error['text']);
        }
    }*/
	public function __construct($url, $login, $pw1, $pw2, $force=true, $rets_version="1.7.2", $user_agent="PHRETS/1.0") {
		$this->_rets = new PHRETS;
		$this->_rets->AddHeader("Accept", "*/*");
		$this->_rets->AddHeader("RETS-Version", "RETS/".$rets_version);
		$this->_rets->AddHeader("User-Agent", $user_agent);
		//$this->_rets->SetParam("cookie_file", $GLOBALS['cookie_file_name']);
		$this->_rets->SetParam("cookie_file", ROOT_DIR.'rest_cookie.txt');
		$this->_rets->SetParam("compression_enabled", true);
		if($force)
			$this->_rets->SetParam("force_basic_authentication", true);
		//$this->_rets->SetParam("offset_support", true);

		$this->_is_connected = $this->_rets->Connect($url, $login, $pw1, $pw2);

		if (! $this->_is_connected) {
			$error = $this->_rets->Error();
			die($error['text']);
		}
	}
    
    function __destruct() {
        if ($this->_is_connected) $this->_rets->disconnect();
     }
    
    function is_connected () {
        return $this->_is_connected;
    }
    
    function getObjectPhotos($object_id, $resource = 'Property', $type = 'Photo'){               
        $photos = ($this->_is_connected) ? $this->_rets->GetObject($resource, $type, $object_id) : array();
	/*foreach($photos as $photo) {
		//file_put_contents($dir.'/'.$n.'.jpg', $photo['Data']);
		$n++;
	}*/      
        return $photos;
    }
    
    function getEncodedImages($imgs_resources = array(), $first = false){
        $imgs = array();
        foreach($imgs_resources as $k=>$v)
        {
            $imgs[$k] = base64_encode($imgs_resources[$k]['Data']);
            $imgs[$k] = 'data: '.$imgs_resources[$k]['Content-Type'].';base64,'.$imgs[$k];
        }        
        return (count($imgs) && $first) ? $imgs[0] : $imgs;
    }
    
    function getObjectById($object_id, $resource = 'Property', $class = 1){                 
        //if (! $this->_is_connected) die($this->_rets->Error());//return array();
        $this->getFieldsNames($resource, $class); // заголовки        
        $object = (intval($object_id)) ? $this->_rets->Search($resource, $class, 'sysid='.intval($object_id)) : array();                
        $object = (count($object)) ? $object[0] : array();        
        foreach ($object as $k=>$v)
        {
            $object[$k] = array(
                'name' => (isset($this->_headers[$resource][$class][$k])) ? $this->_headers[$resource][$class][$k] : 'unknown',
                'value' => $v
            );
        }
        $object['photos'] = $this->getObjectPhotos($object_id);
        return $object;
    }
    
    function getObjectByML($object_id, $resource = 'Property', $class = 1){                                 
        $this->getFieldsNames($resource, $class); // заголовки           
        $object = ($object_id) ? $this->_rets->Search($resource, $class, '157='.$object_id) : array();                
        $object = (count($object)) ? $object[0] : array();                
        foreach ($object as $k=>$v)
        {   // имена параметров
            $object[$k] = array(
                'name' => (isset($this->_headers[$resource][$class][$k])) ? $this->_headers[$resource][$class][$k] : 'unknown',
                'value' => $v
            );
        }            
        $object['photos'] = (($object['sysid']['value'])) ? $this->getObjectPhotos($object['sysid']['value']) : array();
        return $object;
    }
    
    function getFieldsNames($resource = 'Property', $class = 1){
        if (!isset($this->_headers[$resource][$class])){
            $details = $this->_rets->GetMetadata($resource,$class);
            //$error = $this->_rets->Error();  die($error['text']);       
            //foreach ($details as $k=>$v) $this->_headers[$resource][$class][$k] = $v->getLongName();                    
            foreach ($details as $k=>$v) $this->_headers[$resource][$class][@$v['SystemName']] = @$v['LongName'];                    
        }   
        return $this->_headers[$resource][$class];
    }
    
    function getMetadataTable($resource, $class){
        return $this->_rets->GetMetadataTable($resource, $class);
    }
    
    function getLookupValues($resource, $lookup){
        return $this->_rets->GetLookupValues($resource, $lookup);
    }
    
    function getMetadataTypes(){
        return $this->_rets->GetMetadataTypes();
    }
    
    
    function getObjects ($resource = 'Property', $class = 1, $where = ''){
        $types = array(
            1 => 'RE1',
            2 => 'RE2',
            3 => 'RIN',
            4 => 'RLD',
            5 => 'CLD',
            6 => 'RNT',
            7 => 'COM',
            8 => 'BUS',
        );
        //$years = date('Y-m-d\TH:i:s', time()-31556926*4); 
        //$objects = $this->_rets->Search($resource, $class, '((131='.$years.'+),(924=Florida))', array('Limit'=>2,'Select'=>'sysid','Count'=>1));
        //$objects = $this->_rets->Search($resource, $class, '924=Florida');
        //$objects = $this->_rets->Search($resource, $class, '((sysid=130730782))', array('Limit'=>2,'Select'=>'sysid,1,18,183','Count'=>1));
        //$objects = $this->_rets->Search($resource, $class, '((137=100+))', array('Select'=>'sysid,1,137,18,183','Count'=>1, 'Limit'=>10,)); //'Limit'=>2,
        $objects = $this->_rets->Search($resource, $class, "1={$types[$class]}", array( 'Select'=>'','Limit'=>10, 'Offset' => 0, 'Format' => 'COMPACT-DECODED', 'Count' => 1 )); 
        
        //$objects = $this->_rets->Search($resource, $class, '1=RE1', array( 'Select'=>'sysid','Limit'=>1, 'Offset' => 0, 'Format' => 'COMPACT-DECODED', 'Count' => 1 )); 
        //var_dump($objects); die();
        return $objects;
    }
    
    function getObjects2 ($resource = 'Property', $class = 1, $where = '',$limit=0,$offset=0){
        $types = array(
            1 => 'RE1',
            2 => 'RE2',
            3 => 'RIN',
            4 => 'RLD',
            5 => 'CLD',
            6 => 'RNT',
            7 => 'COM',
            8 => 'BUS',
        );      
        $params = array( 
            'Select'=>'',  
            'Limit' => $limit,
            //'Offset' => $offset,             
            //'StandardNames'=>0,
            'Format' => 'COMPACT-DECODED', 
            'Count' => 1  
        );
        
        $objects = $this->_rets->Search(
                $resource, 
                $class, 
                $where, 
                $params
            );
        
        return $objects;
    }
	function getObjects3 ($resource = 'Property', $class = 1, $where = '',$limit=0,$offset=0){
		$types = array(
			1 => 'RE1',
			2 => 'RE2',
			3 => 'RIN',
			4 => 'RLD',
			5 => 'CLD',
			6 => 'RNT',
			7 => 'COM',
			8 => 'BUS',
		);
		$params = array(
			'Select'=>'',//sysid,49,112,175,9,2302,2304',//'1,370,922,246,157',
			'Limit' => $limit,
			//'Offset' => $offset,
			//'StandardNames'=>0,
			'Format' => 'COMPACT-DECODED',
			'Count' => 1
		);

		$objects = $this->_rets->Search(
			$resource,
			$class,
			$where,
			$params
		);

		return $objects;
	}
    
}
