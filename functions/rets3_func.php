<?php
/*
 * flex
 */

/** Property types
 *  A - Residential - Residential
 *  B - Residential Income - Residential Income
 *  C - Land/Docks - Land/Docks
 *  D - Business - Business
 *  E - Comm/Industry - Comm/Industry
 *  F - Rental - Rental
 */


$config['rets_flex'] = array(
	'login_url' => "http://retsgw.flexmls.com:80/rets2_3/Login",
	'username' => "fl.rets.631102147",
	'password' => "churr-poietic52",
);

require_once(ROOT_DIR."functions/phrets.php");

function flex_id ($id,$type) {
	global $config;
	$rets = new phRETS;
	$query = "(LIST_3=".$id.")"; //105
	echo '<br>';
	echo '<b>'.$query.'-'.$type.'</b>';
	echo '<br>';
	$connect = $rets->Connect($config['rets_flex']['login_url'], $config['rets_flex']['username'], $config['rets_flex']['password']);        
	//print_r($connect);
	if ($connect) {
		$offset = 1;
		$limit = 1;
		if ($data = $rets->Search("Property", $type, $query, array('Limit' => $limit, 'Offset' => $offset,
			'Format' => 'COMPACT-DECODED', 'Count' => 1, "UsePost" => 1))) {
                        //var_dump($data);
			$rets->Disconnect();
			return $data[0];
		}
	}
	$rets->Disconnect();
	return false;
}

/**
 *  Тип запроса
 * 'Photo' => 300x192
 * 'Thumbnail' => 65x42
 * '640x480' => 640x480
 * 'HiRes' => 1024x768 и более
 *  часто бывает такая вот ошибка RETS Server: Data has not changed since last request. при работе с фотками 
 *  потому надо быть внимательными, если один раз не достанешь то, что пришло, как достать повторно я хз.
 *  Если ошибка, то она приходит в поле ReplyText.
 */
function flex_img ($id) {
	global $config;
	$rets = new phRETS;
	$connect = $rets->Connect($config['rets_flex']['login_url'], $config['rets_flex']['username'], $config['rets_flex']['password']);
	if ($connect) {
		//$media = $rets->GetObject('Property', 'Photo', $id);
		//echo 'HiRes';
		//$media = $rets->GetObject('Property', 'HiRes', $id);
		$media = $rets->GetObject('Property', 'HiRes', $id, '*', 1);
		/*echo '<pre>';
		print_r($media);
		echo '</pre>';*/
		if ($media[0]['Success']==1) {
			$rets->Disconnect();
			return $media;
		}
	}
	$rets->Disconnect();
	return false;
}

/*
 * EVENT100 - startTime
 * EVENT200 - endTime
 * EVENT6 - Modification Timestamp
 * LIST1  - id mls в системе flex
 */
function flex_open_houses ($product,$test=false) {
	global $config;
	$date = date("Y-m-d");
	$oh_dates = @$product['oh_dates'] ? serialize($product['oh_dates']) : array();
	$rets = new phRETS;
	$connect = $rets->Connect($config['rets_flex']['login_url'], $config['rets_flex']['username'], $config['rets_flex']['password']);
	if ($connect) {
		$query = "(LIST105={$product['uid']})";
		$offset = 1;
		$limit = 100;  
                /* // пример вытащить пачку по датам
                 * var_dump($rets->Search("OpenHouse", 'OpenHouse', "(EVENT100=2016-01-01T00:00:00+)", array('Limit' => 5, 'Offset' => $offset,
			'Format' => 'COMPACT-DECODED', 'Count' => 1, "UsePost" => 1)));*/                
		if ($records = $rets->Search('OpenHouse', 'OpenHouse', $query, array('Limit' => $limit, 'Offset' => $offset,
			'Format' => 'COMPACT-DECODED', 'Count' => 1, "UsePost" => 1))) {                                                
			$oh_dates = array();
			/*echo '<pre>';
			print_r($records);
			echo '</pre>';*/
			foreach ($records as $k=>$v) {
				if ($test==true) print_r($v);
				$StartTime = date('H:i', strtotime($v['EVENT100']));				
				$EndTime = date('H:i', strtotime($v['EVENT200']));
				$OpenHouseDate = date('Y-m-d', strtotime($v['EVENT200']));
				//чтобы даты не повторялись
				$y = true;
				foreach($oh_dates as $k1=>$v1) {
					if (@$v1['OpenHouseID']==$v['EVENT0']) {
						$oh_dates[$k1] = array(
							'from' => $StartTime,
							'to' => $EndTime,
							'date' => $OpenHouseDate,
							'OpenHouseID' => $v['EVENT0'],
							'agent' => @$product['agent'],
						);
						$y = false;
						break;
					}
				}
				if ($y==true) {
					$oh_dates[] = array(
						'from' => $StartTime,
						'to' => $EndTime,
						'date' => $OpenHouseDate,
						'OpenHouseID' => $v['EVENT0'],
						'agent' => @$product['agent'],
					);
				}
			}
			/*echo '<pre>';
			print_r($oh_dates);
			echo '</pre>';*/
		}
		//else echo '<br>нет опенхаусов1';
	}
	if ($test==true) {
		return $oh_dates;
	}
	//else echo '<br>нет опенхаусов2';
	$data = array(
		'oh_date' => '0000-00-00',
		'oh_dates'=>'',
		'oh_display'=>''
	);
	if($oh_dates) {
		usort($oh_dates, "sort2");
		foreach ($oh_dates as $k=>$v) {
			echo '<br>'.$v['date'].' - '.$date;
			if (strtotime($v['date'])<strtotime($date)) unset($oh_dates[$k]);
			else {
				echo ' - ';
				if ($data['oh_date'] == '0000-00-00' OR strtotime($v['date']) > strtotime($data['oh_date'])) {
					$data['oh_date'] = $v['date'];
				}
				else echo 'x'.$data['oh_date'];
			}
		}
		if ($oh_dates) {
			$data['oh_dates'] = serialize($oh_dates);
			$data['oh_display'] = 1;
		}
	}
	$rets->Disconnect();
	return $data;
}


function flex_list ($query='',$type='A',$limit=5000) {
	global $config;
	//$query = $query ? $query : "(LIST_87=2016-01-01T00:00:00+)";
	$offset = 1;
	//$limit = 5000;
	$rets = new phRETS;
	$connect = $rets->Connect($config['rets_flex']['login_url'], $config['rets_flex']['username'], $config['rets_flex']['password']);
	if ($connect) {
		if ($records = $rets->Search("Property", $type, $query, array('Limit' => $limit, 'Offset' => $offset,
			'Format' => 'COMPACT-DECODED', 'Count' => 1, "UsePost" => 1))) {
			$rets->Disconnect();
			return $records;
		}
	}
	$rets->Disconnect();
	return false;
}

function flex_object($q,$l) {
	$date = date("Y-m-d H:i:s");
	$status = (string)$q['LIST_15'];
	$price = (string)$q['LIST_22'];
	$residential = 1;//rent

	//property_types
	$property_type = 0;
	$property_type2 = '';
	//исключения
	if ($q['LIST_9']=='Special Use') $q['LIST_9'] = 'Special Purpose';
	if ($q['LIST_9']=='Hotel/Motel/Hospitality') $q['LIST_9'] = 'Hotel/Motel';
	if ($q['LIST_9']=='Office') $q['LIST_9'] = 'Office Space';
	if ($q['LIST_9']=='Single Family Detached') $q['LIST_9'] = 'Single';
	if ($q['LIST_9']=='Condo/Coop') $q['LIST_9'] = 'Condo';
	$property_type2 = $q['LIST_9'];
	//Residential
	if ($l=='A') {
		$property_type = 3;
		$residential = 2;//sale
	}
	//Residential Income
	elseif ($l=='B') {
		$property_type = 6;
		$residential = 1;//rent
	}
	//Land
	elseif ($l=='C') {
		$property_type = 5;
		$residential = 1;//????
	}
	//Commercial
	elseif ($l=='D') {
		$property_type = 4;
		$property_type2 = 'Business Only';
		$residential = 2;//sale ????
	}
	//Commercial
	elseif ($l=='E') {
		$property_type = 4;
		if ($q['LIST_9']=='Rental')  $residential = 1;//rent
		else $residential = 2;//sale
	}
	//Residential
	elseif ($l=='F') {
		$property_type = 3;
		$residential = 1;//rent
	}
	//[4.762] Выявил ошибку
	if ($property_type2 == 'Single') $property_type = 1;
	if ($property_type2 == 'Townhouse') $property_type = 1;
	if ($property_type2 == 'Villa') $property_type = 1;

	if ($property_type2) $property_type2 = '"'.$property_type2.'"';
	echo '<br>';
	echo $l.'-'.$status.' ';
	echo $property_type.' / '.$property_type2 .' / '.$q['LIST_9'];
	//echo '<br>';

	//name
	$name = $q['LIST_31']; //StreetNumber
	if ($q['LIST_33']) $name .= ' ' . $q['LIST_33']; // StreetDirPrefix
	if ($q['LIST_34']) $name .= ' ' . $q['LIST_34']; //StreetName
	//StreetDirSuffix LIST_36
	if ($q['LIST_37']) $name .= ' ' . $q['LIST_37']; //StreetSuffix
	if ($q['LIST_35']) $name .= ' #' . $q['LIST_35']; //UnitNumber

	//адрес
	$address = $q['LIST_31'];
	if ($q['LIST_33']) $address .= ' ' . $q['LIST_33']; // StreetDirPrefix
	if ($q['LIST_34']) $address .= ' ' . $q['LIST_34']; //StreetName
	if ($q['LIST_37']) $address .= ' ' . $q['LIST_37']; //StreetSuffix

	//агент
	$agent = 0;
	if ($q['LIST_142']) {//Agent Email
		$agent = mysql_select("
					SELECT id FROM users
					WHERE LOWER(email2) = '" . mysql_res(strtolower($q['LIST_142'])) . "'
				", 'string');
	}
	$office_name = $q['listing_office_name'];
	//$office_agent = '';
	//$office_phone = '';
	$special = 0;
	//[4.646] В Our Listing добавить Broker Information
	if ($office_name == 'Wilk Real Estate I LLC.') {
		$special = 1;
		if ($agent == 0) $agent = 34;
		//$office_agent = $q['ListAgentFullName'];
		//$office_phone = $q['ListAgentDirectWorkPhone'];
	}

	$date_change = str_replace(array('T', '.000'), array(' ', ''), $q['LIST_134']);//$q['LIST_87']);
	$date_add = str_replace(array('T', '.000'), array(' ', ''), $q['LIST_10']);
	if ($date_change == '') $date_change = $date_add;

	foreach ($q as $k => $v) {
		if ($v == '') unset($q[$k]);
	}

	$data = array(

		'uid' => $q['LIST_3'],
		'mln' => $q['LIST_105'],
		'base' => 8,
		//'display'=>1,

		'agent' => $agent,
		'office_name' => $office_name,
		'office_agent' => (string)$q['listing_member_name'],
		'office_phone' => (string)$q['listing_office_phone'],
		//'office_agent_phone' => $q['ListAgentDirectWorkPhone'],
		'office_agent_email' => (string)$q['LIST_142'],
		'special' => $special,

		'city' => 6,
		//'neighborhood' => $neighborhood,
		'residential' => $residential,

		'property_type' => $property_type,
		'property_type2' => $property_type2,

		'price' => $price,
		'name' => $name,
		'url' => trunslit($address),
		'address' => $address,
		'text' => (string)$q['LIST_78'],
		'title' => $name,
		'keywords' => keywords($q['LIST_78']),
		'description' => description($q['LIST_78']),
		'parameters' => serialize($q),
		'beds' => intval(@$q['LIST_66']),
		'baths' => intval(@$q['LIST_67']),
		'square' => (string)@$q['LIST_49'],
		'zip_code' => (string)$q['LIST_43'],
		'unit' => (string)@$q['LIST_35'],

		'status' => $status,
		'date_change' => $date_change,
		'date_add' => $date_add,
		'display'=>1,
		'date'=>$date
	);
	return $data;
}