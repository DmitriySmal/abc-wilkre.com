<?php

function mls_yn ($str) {
	if ($str=='Yes') return 1;
	if ($str=='No') return 2;
}

function mls_mx_yn ($str) {
	if ($str=='') return '';
	if ($str=='1') return 1;
	if ($str=='0') return 2;
}

function mls_multi ($str) {
	if ($str) {
		return explode(',',$str);
	}
	else return '';
}

function save_mls_ny ($post) {
//echo '<pre>';
	$id = $post['id'];
	unset($post['id']);
	$post['base'] = 4;
	$post['uid'] = mysql_select("SELECT uid FROM shop_products WHERE base=" . $post['base'] . " ORDER BY uid DESC LIMIT 1", 'string');
	$post['uid']++;
//print_r($post);
	$params = unserialize($post['parameters']);
//print_r($params);
	$parameters = array(
		3 => $post['status'],
		4 => @$params['ListPrice'],
		5 => @$params['DateNewListing'],
		7 => mls_yn($params['ExclusiveAgency']),
		9 => @$params['Block'],
		10 => @$params['LotNum'],
		11 => @$params['StreetNoIDX'],
		12 => name_search(@$params['StreetNameIDX']),
		13 => name_search(@$params['CrossStreet']),
		14 => name_search(@$params['CrossStreet2Name']),
		16 => mls_yn(@$params['RealtorDotComYN']),
		17 => mls_yn(@$params['ShowAddrYN']),
		20 => @$params['TotalRooms'],
		21 => @$params['Beds'],
		22 => @$params['Baths'],
		23 => @$params['BathsHalf'],
		24 => @$params['YearBuilt'],
		25 => @$params['BuildingWidth'],
		26 => @$params['BuildingLength'],
		27 => @$params['Stores'],
		29 => @$params['UnitNumber'],
		78 => @$params['TotalSqFt'],
		28 => @$params['FloorNumber'],
		30 => mls_yn(@$params['WaterFrontPresent']),
		31 => @$params['ConstructionType'],
		32 => mls_yn(@$params['HandicapAccessYN']),
		33 => @$params['LotSizeWidth'],
		34 => @$params['LotSizeLength'],
		35 => @$params['LotSqFt'],
		88 => @$params['TotalSqFt'],
		36 => mls_yn(@$params['EstWaterSewerDolYr']),
		38 => mls_yn(@$params['EstFuelDolYr']),
		//39=>@$params['EstTaxesDolYr'],
		40 => @$params['SalariesYr'],
		41 => @$params['LeaseTerms'],
		42 => @$params['ListPrice'] * @$params['Stores'], // 4*27
		46 => mls_yn(@$params['HandicapAccessYN']),
		47 => @$params['Zoning'],
		48 => @$params['BasementType'],
		49 => @$params['ResidentialUnits'],
		52 => @$params['FlipTaxFee'],
		56 => @$params['MaintExpenses'],
		76 => $post['property_type2'] ? array(str_replace('"','',$post['property_type2'])) : '',
		80 => @$params['IndustrialUnits'],
		81 => @$params['Stories'],
		82 => @$params['HeatUnits'],
		83 => @$params['ManufacturingUnits'],
		84 => @$params['NonConformingUnits'],
		85 => @$params['MiscCost'],
		91 => @$params['TaxRealEstate'],
		92 => @$params['CommonAreaFees'],
		93 => @$params['VirtualTourURL'],
	);
//if ($parameters[42]==0) $parameters[42] = '';
	foreach ($parameters as $k => $v) {
		if ($v == '') unset($parameters[$k]);
	}
	/*echo '<pre>';
	print_r($params);
	print_r($parameters);
	die(); /* */

	$post['parameters'] = serialize($parameters);
	$post['plate'] = null;
	$post['id'] = mysql_fn('insert', 'shop_products', $post);
	if ($post['id']) {
		mysql_fn('update', 'shop_products', array('id' => $id, 'hide' => $post['id'], 'display' => 0));
		rcopy(ROOT_DIR . 'files/shop_products/' . $id.'/', ROOT_DIR . 'files/shop_products/' . $post['id'].'/');
	}
	return $post;
}

function save_mls_mx ($post,$update=false) {
	global $config;
//echo '<pre>';
	$id = $post['id'];
	unset($post['id']);
	$post['base'] = 7;
	$post['uid'] = mysql_select("SELECT uid FROM shop_products WHERE base=" . $post['base'] . " ORDER BY uid DESC LIMIT 1", 'string');
	$post['uid']++;
//print_r($post);
	$params = unserialize($post['parameters']);
//print_r($params);
	$parameters = array(
		3 => $post['status'],
		4 => @$params['ListPrice'],
		5 => @$params['LastChangeTimestamp'] ? str_replace(array('T','.000'), array(' ',''), $params['LastChangeTimestamp']):'',
		//7 => mls_yn($params['ExclusiveAgency']),
		9 => @$params['Block'],
		10 => @$params['LotNumber'],
		11 => @$params['StreetNumberIDX'],
		12 => name_search(@$params['StreetNameIDX']),
		13 => name_search(@$params['CrossStreetName']),
		14 => name_search(@$params['CrossStreetName2']),
		102 => @$params['StreetDirPrefix'],
		19 => @$params['NumberOfFamilies'],
		20 => @$params['TotalRooms'] ? $params['TotalRooms']:@$params['RoomCount'],
		21 => @$params['BedsTotal'],
		22 => @$params['BathsFull'],//@$params['BathsTotal'],
		23 => @$params['BathsHalf'],
		24 => @$params['YearBuilt'],
		25 => @$params['BuildingWidth'],
		26 => @$params['BuildingLength'],
		27 => @$params['NumberOfStores'],
		//29 => @$params['UnitNumber'],
		28 => @$params['Flooring'],
		30 => mls_mx_yn(@$params['WaterFrontPresentYN']),//mls_yn(@$params['WaterFrontPresentYN']),
		//31 => @$params['ConstructionType'],
		//32 => mls_yn(@$params['HandicapAccessYN']),
		32=> @$params['HandicapAccessYNU'],
		33 => @$params['LotSizeWidth'],
		34 => @$params['LotSizeLength'],
		35 => @$params['LotSqFt'],
		//88 => @$params['TotalSqFt'],
		//41 => @$params['LeaseTerms'],
		//42 => @$params['ListPrice'] * @$params['Stores'], // 4*27
		45 => @$params['PetsAllowed'],
		//46 => mls_yn(@$params['HandicapAccessYN']),
		47 => @$params['Zoning'],
		//48 => @$params['BasementType'],
		//49 => @$params['ResidentialUnits'],
		50=> mls_multi(@$params['ParkingFeatures']),
		//52 => @$params['FlipTaxFee'],
		55=>@$params['DownPaymentRequired'],
		//56 => @$params['MaintExpenses'],
		76 => $post['property_type2'] ? array(str_replace('"','',$post['property_type2'])) : '',
		//80 => @$params['IndustrialUnits'],
		//81 => @$params['Stories'],
		//82 => @$params['HeatUnits'],
		//83 => @$params['ManufacturingUnits'],
		//84 => @$params['NonConformingUnits'],
		//85 => @$params['MiscCost'],
		88 => @$params['BuildingFootprintSqFt'],
		//91 => @$params['TaxRealEstate'],
		//92 => @$params['CommonAreaFees'],
		//93 => @$params['VirtualTourURL'],
		94 => mls_multi(@$params['ResidentialFeatures']),
		95 => mls_multi(@$params['Utilities']),
		96 => @$params['ProjectName'],
		98 => mls_mx_yn(@$params['SubLeaseYN']),
		100=> @$params['SqFtTotal'],
		101=> @$params['OwnerOccupied'],
		103=> @$params['TenantLeaseEndDate'],
		215=> @$params['FuelExpense'],
		216=>@$params['TaxAmount'],
		315=>@$params['HotWater'],
		316=>@$params['InsuranceExpense'],
		401=> @$params['CoOpShares'],
		402=> @$params['FlipTaxFee'],
		405=> mls_mx_yn(@$params['StockCertificateYN']),
		406=> mls_mx_yn(@$params['CertificateOfOccupancyYN']),
		407=> @$params['OwnerOccupiedUnitsSold'],
		408=> @$params['MaintenanceCommonFee'],
		410=> @$params['TenantLeaseEndDate'],
		502=> @$params['PayrollAnnualExpenses'],
		602 => mls_multi(@$params['Style']),
		603=> @$params['ACUnits'],
		613=> @$params['BasementAmenities'],
		614=> @$params['NumberOfVacantUnits'],
	);
//if ($parameters[42]==0) $parameters[42] = '';
	foreach ($parameters as $k => $v) {
		if ($v == '') unset($parameters[$k]);
	}
	/*echo '<pre>';
	print_r($post);
	echo '<pre>';
	//print_r($parameters);
	//die(); /* */

	$post['parameters'] = serialize($parameters);
	/*echo '<pre>';
	print_r($post);
	echo '<pre>';*/
	echo 'insert';
	$config['global_debug'] = 1;
	$post['plate'] = null;
	//[4.743] plate - сохранение объекта
	//если не обновление
	if ($update==false) {
		$post['id'] = mysql_fn('insert', 'shop_products', $post);
		/*echo '<pre>';
		print_r($post);
		echo '<pre>';*/
		if ($post['id']) {
			//echo mysql_error();
			//die();
			mysql_fn('update', 'shop_products', array('id' => $id, 'hide' => $post['id'], 'display' => 0));
			rcopy(ROOT_DIR . 'files/shop_products/' . $id . '/', ROOT_DIR . 'files/shop_products/' . $post['id'] . '/');
			return $post;
		} else return false;
	}
	else {
		$post['id'] = $id;
		mysql_fn('update', 'shop_products', $post);
	}
}

//получить список проперти тайп
function get_property_types ($post) {
	global $config;
	//echo $post['base'].$post['property_type'];
	//$post['residential']
	//1 - rent
	//2 - sale
	//$post['base']
	//1 - NY
	//2 - Florida
	$data = false;
	$data = array();
	//флорида матрикс
	if ($post['base']==9) {
		$data = mysql_select("
			SELECT uid id, name
			FROM shop_property_types
			WHERE base=9 AND parent=0 AND display=1
			ORDER BY rank DESC
		",'array');
	}
	//флорида bridge
	if ($post['base']==10) {
		$data = mysql_select("
			SELECT uid id, name
			FROM shop_property_types
			WHERE base=10 AND parent=0 AND display=1
			ORDER BY rank DESC
		",'array');
	}
	//florida - уже нету
	if (in_array($post['base'],array(2,8))) {
		//rent = [3,4],
		//sale = [1,2,4,5,6];
		foreach ($config['object_groups'] as $k => $v) {
			if ($post['residential']==1 AND !in_array($k,array(3,4))) continue;
			if ($post['residential']==2 AND !in_array($k,array(1,2,4,5,6))) continue;
			$data[$k] = $v['name'];
		}
	}
	//NY
	//1 тут осталось старое, раньше было просто 1 ньюйорк а 2 флорида
	if (in_array(@$post['base'],array(1,6,7))) {
		require_once(ROOT_DIR.'_config_ny.php');
		$data = $config['ny_property_types'];
	}
	if ($data) {
		$data2 = array();
		foreach ($data as $k=>$v) {
			$data2[] = array(
				'id'=>(string)$k,
				'name'=>$v,
			);
		}
		return $data2;
	}
}

//получить конкретный проперти тайп
function get_property_type ($post) {
	global $config;
	//echo $post['base'].$post['property_type'];
	//$post['residential']
	//1 - rent
	//2 - sale
	//$post['base']
	//1 - NY
	//2 - Florida
	$data = false;
	$data = array();
	//florida
	if (in_array($post['base'],array(2,8))) {
		//rent = [3,4],
		//sale = [1,2,4,5,6];
		foreach ($config['object_groups'] as $k => $v) {
			if ($post['residential']==1 AND !in_array($k,array(3,4))) continue;
			if ($post['residential']==2 AND !in_array($k,array(1,2,4,5,6))) continue;
			$data[$k] = $v['name'];
		}
	}
	//NY
	if (@$post['base']==1) {
		require_once(ROOT_DIR.'_config_ny.php');
		$data = $config['ny_property_types'];
	}
	if ($data) {
		return @$data[$post['property_type']];
	}
}

//получить список проперти тайп2
function get_property_types2 ($post) {
	global $config;
	//echo $post['base'].$post['property_type'];
	//$post['residential']
	//1 - rent
	//2 - sale
	//$post['base']
	//1 - NY
	//2 - Florida
	$data = false;
	//запросы к базе


	if ($post['property_type']) {
		$where = '';
		//florida matrix
		if (@$post['base'] == 9) {
			$where = '';
			if (@$post['residential'] == 1) {
				$where .= " AND rent=1";
			}
			if (@$post['residential'] == 2) {
				$where .= " AND sale=1";
			}
			$query = "
				SELECT uid id,name
				FROM shop_property_types
				WHERE base=9 AND display=1 AND parent=" . $post['property_type'] . " ".$where."
				ORDER BY rank DESC
			";
			//echo $query;
			return mysql_select($query, 'rows_id');
		}
		//флорида bridge
		if (@$post['base'] == 10) {
			$where = '';
			if (@$post['residential'] == 1) {
				$where .= " AND rent=1";
			}
			if (@$post['residential'] == 2) {
				$where .= " AND sale=1";
			}
			$query = "
				SELECT uid id,name
				FROM shop_property_types
				WHERE base=10 AND display=1 AND parent=" . $post['property_type'] . " ".$where."
				ORDER BY rank DESC
			";
			//echo $query;
			return mysql_select($query, 'rows_id');
		}

		if (@$post['base'] == 2) {
			$where .= " AND fl_property_type=" . $post['property_type'];
		}
		//NY - 1 осталось старое (1 ньюйорк 2 флорида)
		if (in_array(@$post['base'],array(1,6,7))) {
			$where .= " AND ny_property_type=" . $post['property_type'];
		}
		if (@$post['residential'] == 1) {
			$where .= " AND rent=1";
		}
		if (@$post['residential'] == 2) {
			$where .= " AND sale=1";
		}
		if (@$post['residential'] == 0) {
			$where.= " AND (sale=1 OR rent=1)";
		}
		$query = "SELECT id,name FROM shop_property_types2 WHERE 1 " . $where . " ORDER BY rank DESC, name";
		//echo $query;
		return mysql_select($query, 'rows_id');
	}
	return '';

	//florida
	if (in_array($post['base'],array(2,8))) {
		if ($post['property_type'] == 3 AND $post['residential']!=2) $data = $config['object_groups_3'];
		if ($post['property_type'] == 4) $data = $config['object_groups_4'];
		if ($post['property_type'] == 6 AND $post['residential']!=1) $data = $config['object_groups_6'];
	}
	//NY
	if (@$post['base']==1) {
		require_once(ROOT_DIR.'_config_ny.php');
		if ($post['property_type']==1/* AND $post['residential']!=1*/) $data = $config['ny_property_types_1'];
		if ($post['property_type']==2) {
			if ($post['residential']==1) $data = $config['ny_property_types_2_1'];
			elseif ($post['residential']==2) $data = $config['ny_property_types_2_2'];
			else {
				$data = array_merge($config['ny_property_types_2_2'],$config['ny_property_types_2_1']);
				sort($data);
			}
		}
	}
	//print_R($post);
	if ($data) {
		$data2 = array();
		foreach ($data as $k=>$v) {
			$data2[] = array(
				//'id'=>$k,
				'name'=>$v,
			);
		}
		return $data2;
	}
}

//получить список городов
function get_cities ($post) {
	global $config;
	if ($post['base'] == 9) $post['base'] = 2;
	if ($post['base'] == 6) $post['base'] = 1;
	$data = mysql_select("SELECT id,name FROM shop_cities WHERE state=".$post['base']." AND display=1 ORDER BY rank",'rows');
	if ($data) {
		return $data;
	}
}

//получить список районов
function get_neighborhoods ($post) {
	global $config;
	if ($post['base'] == 9) $post['base'] = 2;
	if ($post['base'] == 6) $post['base'] = 1;
	if ($cities = mysql_select("SELECT id,name FROM shop_cities WHERE state=".$post['base']." AND display=1 ORDER BY rank",'array')) {
		$keys = array_keys($cities);
		if (@$post['city'] AND array_key_exists($post['city'], $cities)) {
			$keys = array($post['city']);
		}
		$query = "
			SELECT id,name FROM shop_neighborhoods
			WHERE id>0 AND display=1 AND city IN ('" . implode("','", $keys) . "')
			ORDER BY rank DESC, name ASC
		";
		//echo $query;
		$data = mysql_select($query, 'rows');
		if ($data) {
			return $data;
		}
	}
}

//массив параметров
function object_parameters ($q) {
	global $config;
	$parameters = $q['parameters'] ? unserialize($q['parameters']) : false;
	//print_R($parameters);
	//echo $q['base'];
	$params = array();
	if (is_array($parameters) AND count($parameters>0)) {
		if (in_array($q['base'],array(6,8,9))) {
			$shop_parameters = mysql_select("
				SELECT * FROM shop_parameters
				WHERE base = ".$q['base']." AND display=1
				ORDER BY rank DESC
			",'rows');
			if (access('user admin')) {
				//dd($parameters);
				//dd($shop_parameters);
			}
			foreach ($shop_parameters as $k=>$v) if (@$parameters[$v['key']]) {
				$str = $parameters[$v['key']];
				if (@$v['type'] == 0 AND $v['template']!='') {
					$str = template($v['template'],$parameters);
					//[4.684] убрать .00
					$str = str_replace('.00','',$str);
				}
				if (@$v['type'] == 1) $str.= ' Sq./Ft.';//'FT&sup2;';
				if (@$v['type'] == 2) {
					$str = preg_replace('~[^0-9.]+~u', '', $str);    //удаление лишних символов
					if ($str) $str ='$'.number_format($str, 0, '.', ',');
				}
				if (@$v['type'] == 3) $str = number_format($str, 0, '.', ',');
				if (@$v['type'] == 4) {
					if ($str==1) $str = 'Yes';
					if ($str==2) $str = 'No';
				}
				if (@$v['type'] == 5) {
					//$str = '<a href="' . $str . '" target="_blank">view</a>';
				}
				if ($str) {
					$params[] = array(
						'name'=>$v['name'],
						'value'=>$str
					);
				}
			}
		}
		//собственные объекты
		elseif (in_array($q['base'], array(1, 4))) {
			//исключение для блок и лот
			if (@$parameters[9] AND @$parameters[10]) {
				$config['shop_params'][910] = array(
					'name' => 'Block/Lot',
				);
				$parameters[910] = $parameters[10] . '/' . $parameters[10];
				unset($config['shop_params'][9], $config['shop_params'][10]);
			}
			if (@$parameters[25] AND @$parameters[26]) {
				$config['shop_params'][2526] = array(
					'name' => 'Building Width/Length',
				);
				$parameters[2526] = $parameters[25] . '*' . $parameters[26];
				unset($config['shop_params'][26], $config['shop_params'][26]);
			}
			if (@$parameters[33] AND @$parameters[34]) {
				$config['shop_params'][3334] = array(
					'name' => 'Lot Width и Lot Length',
				);
				$parameters[3334] = $parameters[33] . '*' . $parameters[34];
				unset($config['shop_params'][33], $config['shop_params'][34]);
			}
			foreach ($config['shop_params'] as $k=>$v) if (@$v['hide']!=1 AND @$parameters[$k]) {
			//foreach ($config['shop_params_sort'] as $a => $k) if (@$parameters[$k]) {/*@$v['display']==1 AND*/
				$str = '';
				if (is_array($parameters[$k])) {
					$str = implode(', ', $parameters[$k]);
				} else {
					$str = $parameters[$k];
				}
				$v = $config['shop_params'][$k];
				if (@$v['type'] == 'square') $str.= ' Sq./Ft.';//'FT&sup2;';
				if (@$v['type'] == 'price') {
					$str = preg_replace('~[^0-9]+~u', '', $str);    //удаление лишних символов
					if ($str) $str ='$'.number_format($str, 0, '.', ',');
				}
				if (@$v['type'] == 'thousand') $str = number_format($str, 0, '.', ',');
				if (@$v['type']== 'wl') {
					$l = $k+1;
					$v['name'].= ' / Length';
					$str = $parameters[$k].' x '.$parameters[$l];
				}
				if (@$v['type'] == 'yn') {
					if ($str==1) $str = 'Yes';
					if ($str==2) $str = 'No';
				}
				if (@$v['type'] == 'checkbox') {
					if ($str==1) $str = 'Yes';
					if ($str==0) $str = 'No';
				}
				if (@$v['type'] == 'uid') {
					if ($q['base'] == 4) $str = 'WRF' . $str;
					else $str = 'WR' . $str;
				}

				/*
				if (@$v['type'] == 'square') $str .= 'FT&sup2;';
				if (@$v['type'] == 'price') $str = '$' . number_format($str, 0, '.', ',');
				if (@$v['type'] == 'yn') {
					if ($str == 1) $str = 'Yes';
					if ($str == 2) $str = 'No';
				}
				if (@$v['type'] == 'checkbox') {
					if ($str == 1) $str = 'Yes';
					if ($str == 0) $str = 'No';
				}
				if (@$v['type'] == 'uid') {
					if ($q['base'] == 4) $str = 'WRF' . $str;
					else $str = 'WR' . $str;
				}
				*/
				/*if (@$v['type'] == 'link') {
					$str = '<a href="' . $str . '" target="_blank">view</a>';
				}*/
				$params[] = array(
					'name' => $v['name'],
					'value' => $str
				);
			}
		}
		else {
			//print_r($config['shop_parameters']);
			$shop_parameters = $q['base'] == 2 ? $config['shop_parameters'] : $config['ny_shop_parameters'];
			$parameters['Listing'] = 'Co-broke';
			if ($q['special']) $parameters['Listing'] = 'Exclusive';
			foreach ($shop_parameters as $k => $v) if (@$parameters[$k]) {
				$str = '';
				if (@$v['type'] == 'link') {
					$str = $parameters[$k];
				} elseif (@$v['type'] == 'price') {
					$str = '$' . number_format($parameters[$k], 0, '.', ',');
				} elseif (@$v['type'] == 'date') {
					$str = substr($parameters[$k], 0, 10);
				} elseif (@$v['type'] == 'square') {
					$str = number_format($parameters[$k], 0, '.', ',') . 'FT&sup2;';
				} elseif (@$v['type'] == 'listing') {
					$array = explode('","', trim($parameters[$k], '"'));
					$str = implode(', ', $array);
				} elseif (@$k == 'BuildingLength') {
					$str = $parameters['BuildingLength'] . ' x ' . $parameters['BuildingWidth'];
				} elseif (@$k == 'LotSizeLength') {
					$str = $parameters['LotSizeLength'] . ' x ' . $parameters['LotSizeWidth'];
				} else {
					$str = $parameters[$k];
					if ($str == 'Y') $str = 'Yes';
					if ($str == 'N') $str = 'No';
					$str .= @$v['ending'];
				}
				//echo $v['name'].': '.$str.'<br>';
				$params[] = array(
					'name' => $v['name'],
					'value' => $str
				);
			}
		}
	}
	//print_r($params);
	return $params;
}

//возвращает рейтинг объекта
function object_rank ($q) {
	global $config;
	//статус новый получают те кому месяц
	if (!isset($config['date_new'])) $config['date_new'] = date('Y-m-d',time()-60*60*24*30);
	$rank = 0;
	if ($q['status']=='New') $rank = 10;
	elseif ($q['status']=='Active') $rank = 9;
	elseif ($q['status']=='A') $rank = 9;
	elseif ($q['status']=='Pending') $rank = 8;
	elseif ($q['status']=='Contract') $rank = 8;
	elseif ($q['status']=='Incomplete') $rank = 6;
	elseif ($q['status']=='Leased') $rank = 4;
	elseif ($q['status']=='R') $rank = 4;
	elseif ($q['status']=='Sold') $rank = 2;
	elseif ($q['status']=='CS') $rank = 2;
	elseif ($q['status']=='Withdrawn') $rank = 0;
	elseif ($q['status']=='Expired') $rank = 0;
	//статус new
	if (@$q['date_add']=='') $q['date_add'] = @$q['date_change'];
	if (@$q['date_add']>$config['date_new']) {
		$rank = 12;
	}
	//сортировка если млс ньюйорк
	if ($q['base']==6 AND $q['special']==1) {
		if ($q['residential']==2 AND $q['property_type']==1) {
			$rank = 21;
			if (strripos($q['property_type2'], 'mixed use/store & family')!==false) $rank = 22;
			if (strripos($q['property_type2'], 'co-op')!==false) $rank = 23;
			if (strripos($q['property_type2'], 'condo')!==false) $rank = 24;
			if (strripos($q['property_type2'], 'multi-family')!==false) $rank = 25;
			if (strripos($q['property_type2'], 'single family')!==false) $rank = 26;
		}
	}
	return $rank;
}

function object_status ($q) {
	$status = '';
	//флорида
	//[4.667] плашки во флориде
	if ($q['base']==10) {
		if ($q['rank']==0 AND $q['hide']==2) $status = 'Off Market';
	}
	elseif ($q['base']<3) {
		if ($q['status']=='R') $status = 'Rented';
		if ($q['status']=='CS') $status = 'Closed Sale';
	}
	elseif ($q['base']==5) $status = 'Sold';
	//матрикс
	elseif ($q['base']==6 OR $q['base']==7) {
		if ($q['status']=='Pending') $q['status']='Contract';
		if (in_array($q['status'],array('Sold','Leased','Incomplete','Leased','Withdrawn','Expired','Contract'))) {
			$status = $q['status'];
		}
	}
	//[4.732] Flex - статусы
	elseif ($q['base']==8) {
		if ($q['status']=='Pending') $q['status']='Contract';
		if ($q['status']=='Backup') $q['status']='Contract';
		if ($q['status']=='Contingent')  $q['status']='Pre Contract';
		if (in_array($q['status'],array('Active','Contract','Pre Contract'))) {
			$status = $q['status'];
		}
	}
	elseif (in_array($q['status'],array('New','Sold','Contract','Reserved','Leased','Expired'))) {
		$status = $q['status'];
	}
	if ($q['date_add']=='0000-00-00') $q['date_add'] = $q['date_change'];
	//[4.74] статус недвижимости
	if (($status=='' OR $status=='Active') AND $q['hide']==0) {
		if ($q['date_add'] != '0000-00-00') $status = (strtotime($q['date_add']) > (time() - 60 * 60 * 24 * 30)) ? 'New' : $status;
	}
	return $status;
}

//[2.23] флорида
//функция для н1 для объекта и названия в списке
function object_name ($q) {
	global $config;
	/*if (access('user admin')) {
		dd($q);
		die();
	}*/
	$names = array();
	$names[] = $q['name'];
	if ($q['neighborhood_name']) $names[] = $q['neighborhood_name'];
	if (!in_array($q['base'],array(9,10,11)) AND $q['city_name']) $names[] = $q['city_name'];
	$names[] = $config['object_base_state'][$q['base']] . ' ' . $q['zip_code'];
	$name = implode(', ',$names);
	return $name;
}

function seo_description($post,$seo,$base,$random) {
	$text = '';
	$text.= random_template('find', $random);
	if ($seo['property_type']) {
		$seo['property_type'] = str_replace('Single Family',random_template('single family', $random),$seo['property_type']);
		$seo['property_type'] = str_replace('Multi-Family',random_template('multi family', $random),$seo['property_type']);
		$seo['property_type'] = str_replace('Condo',random_template('condo', $random),$seo['property_type']);
		$seo['property_type'] = str_replace('Co-op',random_template('co-op', $random),$seo['property_type']);
		$text.= ' '.$seo['property_type'];
	}
	$text.= ' listings';
	if ($post['residential']==1) $text.= ' '.random_template('for rent', $random);
	if ($post['residential']==2) $text.= ' '.random_template('for sale', $random);
	if ($seo['city'] OR $seo['neighborhoods']) {
		$text.= ' in';
		if ($seo['neighborhoods']) {
			$text.= ' '.implode(', ',$seo['neighborhoods']).',';
		}
		if ($seo['city']) {
			$text.= ' '.$seo['city'].',';
		}
		if ($seo['base']==1) {
			$text.= ' '.random_template('ny', $random);
		}
		if ($seo['base']==2) {
			$text.= ' '.random_template('sf', $random);
		}
	}
	$text.= ' and ';
	$text.= random_template('select', $random);
	$text.= ' '.random_template('offers', $random);
	if ($post['baths']) {
		$text.= ' '.$post['baths'].' ';
		$text.= random_template('baths', $random);
	}
	if ($post['beds']) {
		if ($post['baths']) $text.= ' and';
		if ($post['beds']=='studio') $text.= ' Studio ';
		else {
			$text .= ' ' . $post['beds'] . ' ';
			$text .= random_template('beds', $random);
		}
	}
	if ($post['beds'] AND $seo['property_type']) {
		$text.= ' '.$seo['property_type'];
	}
	if ($post['price_min'] OR $post['price_max']) {
		$text .= ' and the ';
		$text .= random_template('value', $random);
		$text .= $seo['from'].$seo['to'];
	}
	$text.= ' at the ';
	$text.= random_template('site', $random);
	$text.= ' of ';
	$text.= in_array($base,array(1,6)) ? 'New York':'South Florida';
	$text.= "'s ";
	$text.= random_template('realtor_agency', $random);
	$text.= ' - Wilk Real Esate I LLC.';
	return $text;
}

function seo_text($post,$seo,$base,$random) {
	global $config;
	if ($seo['property_type']) {
		$seo['property_type1'] = str_replace('Single Family',random_template('single family', $random),$seo['property_type']);
		$seo['property_type1'] = str_replace('Multi-Family',random_template('multi family', $random),$seo['property_type1']);
		$seo['property_type1'] = str_replace('Condo',random_template('condo', $random),$seo['property_type1']);
		$seo['property_type1'] = str_replace('Co-op',random_template('co-op', $random),$seo['property_type1']);

		$seo['property_type2'] = str_replace('Single Family',random_template('single family', $random+1),$seo['property_type']);
		$seo['property_type2'] = str_replace('Multi-Family',random_template('multi family', $random+1),$seo['property_type2']);
		$seo['property_type2'] = str_replace('Condo',random_template('condo', $random+1),$seo['property_type2']);
		$seo['property_type2'] = str_replace('Co-op',random_template('co-op', $random+1),$seo['property_type2']);

		$seo['property_type3'] = str_replace('Single Family',random_template('single family', $random+2),$seo['property_type']);
		$seo['property_type3'] = str_replace('Multi-Family',random_template('multi family', $random+2),$seo['property_type3']);
		$seo['property_type3'] = str_replace('Condo',random_template('condo', $random+2),$seo['property_type3']);
		$seo['property_type3'] = str_replace('Co-op',random_template('co-op', $random+2),$seo['property_type3']);

		$seo['property_type4'] = str_replace('Single Family',random_template('single family', $random+3),$seo['property_type']);
		$seo['property_type4'] = str_replace('Multi-Family',random_template('multi family', $random+3),$seo['property_type4']);
		$seo['property_type4'] = str_replace('Condo',random_template('condo', $random+3),$seo['property_type4']);
		$seo['property_type4'] = str_replace('Co-op',random_template('co-op', $random+3),$seo['property_type4']);

	}
	$text = 'At the ';
	$text.= random_template('wilkre.com', $random);
	$text.= ' ';
	$text.= random_template('you can', $random);
	$text.= ' find and ';
	$text.= random_template('select', $random);
	$text.= ' ';
	$text.= random_template('estate', $random);
	if ($post['residential']==1) $text.= ' '.random_template('for rent', $random);
	if ($post['residential']==2) $text.= ' '.random_template('for sale', $random);
	if ($seo['city'] OR $seo['neighborhoods']) {
		$text.= ' in';
		if ($seo['neighborhoods']) {
			$text.= ' '.implode(', ',$seo['neighborhoods']).',';
		}
		if ($seo['city']) {
			$text.= ' '.$seo['city'].',';
		}
		if ($seo['base']==1) {
			$text.= ' '.random_template('ny', $random);
		}
		if ($seo['base']==2) {
			$text.= ' '.random_template('sf', $random);
		}
	}
	$text.= '. At  the ';
	$text.= random_template('section', $random);
	$text.= ' ';
	//$text.= $seo['property_type'];
	if ($seo['property_type']) {
		$text.= ' '.$seo['property_type1'];
	}
	if ($post['residential']==1) $text.= ' '.random_template('for rent', $random+1);
	if ($post['residential']==2) $text.= ' '.random_template('for sale', $random+1);
	$text.= ' of ';
	$text.= random_template('different sizes', $random);
	$text.='.';
	if ($post['baths'] OR $post['beds']) {
		$text .= ' In the ';
		$text .= random_template('proposed options', $random);
		$text .= ' of ';
		if ($post['baths']) {
			$text .= ' ' . $post['baths'] . ' ';
			$text .= random_template('baths', $random);
		}
		if ($post['beds']) {
			if ($post['beds'] == 'studio') $text .= ' Studio';
			else {
				$text .= ' ' . $post['beds'] . ' ';
				$text .= random_template('beds', $random);
			}
		}
		if ($post['beds'] AND $seo['property_type']) {
			$text .= ' ' . $seo['property_type2'];
		}
		$text .= '.';
	}
	if ($seo['from'] OR $seo['to']) {
		$text .= ' ';
		$text .= random_template('property value', $random);
		if ($seo['from'] AND $seo['to']) {
			$text .= str_replace('from','is between',$seo['from'] . $seo['to']);
		}
		else $text .= $seo['from'] . $seo['to'];
		$text.='.';
	}
	$text.=' This ';
	$text.= random_template('section', $random);
	$text.=' ';
	$text.= random_template('provides', $random);
	$text.=' you {num_rows} ';
	$text.= random_template('proposals', $random);
	$text.= '.<br><br>';
	$text.= 'The ';
	$text.= random_template('value', $random);
	$text.= ' of the ';
	if ($seo['property_type']) {
		$text.= $seo['property_type3'];
	}
	else $text.= random_template('real estate', $random);
	if ($post['residential']==1) $text.= ' '.random_template('for rent', $random+2);
	if ($post['residential']==2) $text.= ' '.random_template('for sale', $random+2);
	$text.= ' is ';
	$text.= random_template('affected', $random);
	$text.= ' by the following ';
	$text.= random_template('factors', $random);
	$text.= ': ';
	$text.= random_template('estate', $random);
	$text.= ' ';
	$text.= random_template('location', $random);
	if ($seo['city'] OR $seo['neighborhoods']) {
		$text.= ' in';
		if ($seo['neighborhoods']) {
			$text.= ' '.implode(', ',$seo['neighborhoods']).',';
		}
		if ($seo['city']) {
			$text.= ' '.$seo['city'].',';
		}
		if ($seo['base']==1) {
			$text.= ' '.random_template('ny', $random+1);
		}
		if ($seo['base']==2) {
			$text.= ' '.random_template('sf', $random+1);
		}
	}
	$text.= ', ';
	$text.= random_template('estate', $random);
	$text.= ' ';
	$text.= random_template('type', $random);
	$text.= ', ';
	$text.= random_template('estate', $random);
	$text.= ' square footage and ';
	$text.= random_template('estate', $random);
	$text.= ' ';
	$text.= random_template('condition', $random);
	$text.= '.';
	$text.= '<br><br>';
	$text.= random_template('if you', $random);
	$text.= ' find ';
	$text.= random_template('your dream', $random);
	if ($seo['property_type']) {
		$text.= ' '.$seo['property_type4'];
	}
	$text.= ' ';
	$text.= random_template('compare', $random);
	$text.= ' ';
	$text.= random_template('price', $random);
	$text.= ' at ';
	$text.= random_template('our website', $random);
	$text.= '. There is a big chance that we ';
	$text.= random_template('might', $random);
	$text.= random_template('offer', $random);
	$text.= ' you a ';
	$text.= random_template('better', $random);
	$text.= ' ';
	$text.= random_template('price', $random);
	$text.= '. To learn ';
	$text.= random_template('a lot of', $random);
	$text.= ' about the ';
	$text.= random_template('estate', $random);
	if ($post['residential']==1) $text.= ' '.random_template('for rent', $random+3);
	if ($post['residential']==2) $text.= ' '.random_template('for sale', $random+3);
	if ($seo['city'] OR $seo['neighborhoods']) {
		$text.= ' in';
		if ($seo['neighborhoods']) {
			$text.= ' '.implode(', ',$seo['neighborhoods']).',';
		}
		if ($seo['city']) {
			$text.= ' '.$seo['city'].',';
		}
		if ($seo['base']==1) {
			$text.= ' '.random_template('ny', $random+2);
		}
		if ($seo['base']==2) {
			$text.= ' '.random_template('sf', $random+2);
		}
	}
	$text.= ' of your choice, please ';
	$text.= random_template('contact', $random);
	$text.= ' us at 718-376-0606 or by using a feedback form.';
	return $text;
}

//[4.630] Поменять Title для листингов
function seo_title ($page) {
	global $config;
	$state = in_array($page['base'],array(1,2,8,9,10)) ? 'FL':'NY';
	$p = unserialize($page['parameters']);
	//$base = $page['base']<2 ? 2:1;
	$page['title'] = '';
	if ($state=='NY') {
		if ($page['mln']) $page['title'] .= 'MLS#' . $page['mln'].' | ';
	}
	//FL
	if ($page['base']<3) {
		$page['title'].= @$p[248].' ';
		$page['title'].= @$p[247].' ';
		$page['title'].= @$p[17] ? '#'.$p[17]:'';
	}
	//матрикс
	elseif ($page['base']==6) {
		$page['title'].= $page['name'];
	}
	//флекс
	elseif ($page['base']==8) {
		$page['title'].= $page['name'];
	}
	//bridge
	elseif ($page['base']==10) {
		$page['title'].= $page['name'];
	}
	//свои [4.808] Проблемы с щtitle
	elseif ($page['base']==4 OR $page['base']==7) {
		$page['title'].= @$p[11].' ';
		$page['title'].= @$p[12].' ';
		$page['title'].= @$p[29] ? '#'.$p[29]:'';
	}
	//свои
	/*elseif ($page['base']==7) {
		$page['title'].= @$p[11].' ';
		$page['title'].= @$p[102].' ';
		$page['title'].= @$p[12] ? '#'.$p[12]:'';
	}*/
	else {
		$page['title'].= $page['address'].' ';
		$page['title'].= $page['unit'] ? '#'.$page['unit']:'';
	}
	$page['title'] = trim($page['title']);
	//флорида без города
	if ($page['base']!=9) {
		if ($page['city']) {
			$page['title'] .= ', ' . mysql_select("SELECT name FROM shop_cities WHERE id =" . $page['city'], 'string') . '';
		}
	}
	//флорида с районом
	else {
		if ($page['neighborhood']) {
			$page['title'] .= ', ' . mysql_select("SELECT name FROM shop_neighborhoods WHERE id =" . $page['neighborhood'], 'string') . '';
		}
	}

	$page['title'].= ', '.$state.' ';

	$page['title'].= $page['zip_code'];
	//[4.803] В title каждого объекта добавить MLS#
	//[2.12] млс номер
	if ($state=='FL') {
		if ($page['mln']) $page['title'] .= ' | MLS#' . $page['mln'];
	}
	$page['title'].=' | Wilk Real Estate';
	return $page['title'];
}
function seo_title_old ($page) {
	global $config;
	//if (access('user admin')) print_r($page);
	$random = random_hash($page);
	$base = $page['base']<2 ? 2:1;
	$page['title'] = '';
	//FL
	if ($base==2) {
		$page['title'] .= 'Look at ';
		//property_type
		if ($page['property_type2']) {
			$page['title'].= str_replace(array('"',','),array(' ','/'),$page['property_type2']);
		}
		else {
			$page['title'].= ' '.$config['object_groups'][$page['property_type']]['name'];
		}
		//добавочное слово
		if ($page['property_type']==1 AND $page['property_type2']=='') {
			//в хомс без проперти тайп нед добавочного слова чтобы не дублировалось
		}
		else {
			if (in_array($page['property_type'], array(1,6))) {
				$page['title'] .= ' ' . random_template('house', $random);
			}
			elseif (in_array($page['property_type'], array(2))) {
				$page['title'] .= ' Apartment';
			}
			//residential
			elseif (in_array($page['property_type'],array(3))) {
				if (in_array($page['property_type2'],array('"efficiency"','"condo"'))) {
					$page['title'] .= ' Apartment';
				}
				elseif (in_array($page['property_type2'],array('"multifamily"','"single"'))) {
					$page['title'] .= ' ' . random_template('house', $random); //' Houses';
				}
			}
			else {
				$page['title'] .= ' ' . random_template('property', $random);
			}
		}
	}
	else {
		//property_type
		if ($page['property_type2']) {
			$page['title'].= str_replace(array('"',','),array(' ','/'),$page['property_type2']);
		}
		else {
			$page['title'] .= ' ' . $config['ny_property_types'][$page['property_type']];
		}
		//добавочное слово
		if (in_array($page['property_type'],array(1))) {
			if (in_array($page['property_type2'], array('"Condo"', '"Co-Op"'))) {
				$page['title'] .= ' Apartment';
			}
			else {
				$page['title'] .= ' ' . random_template('house', $random);
			}
		}
		else {
			$page['title'] .= ' ' . random_template('property', $random);
		}
	}
	$page['title'] .= ' at ';
	$page['title'] .= $page['name'];
	$page['title'].= ' in ';
	//location
	$page['title'].= $page['nb_name'];
	//FL
	if ($base==2) {
		$page['title'].= ', FL ';
	}
	//NY
	else {
		if ($page['city']) {
			$page['title'] .= ', '.mysql_select("SELECT name FROM shop_cities WHERE id =" . $page['city'], 'string') . '';
		}
		$page['title'].= ', NY ';
	}
	$page['title'].= $page['zip_code'];
	//[4.550] Title сделать хвост | WilkRealEstate I LLC
	//$page['title'].= ' - WilkRealEstate I LLС';
	//[4.569] Сделать хвост в Title другим
	$page['title'].= ' - Wilk Real Estate Agency';
	//$page['title'].= ' - Wilk Real Estate I LLC';
	return $page['title'];
}

function random_property_type ($q,$true=false) {
    global $config;
    $base = in_array($q['base'],array(1,2,8)) ? 2 :1;
    $random = random_hash(array(
        $q['id']
    ));
    $name['property_type'] = '';
	//ny
    if ($base==1) {
        $property_type2 = trim(str_replace('"',' ',$q['property_type2']));
	    //echo '1'.$property_type2;
        if ($q['property_type']==1 AND $q['residential']==2) {
            if (in_array($property_type2,array('Condo','CoOp','Co-Op'))) {
                $name['property_type'] = $property_type2.' Apartment';
            }
            elseif ($property_type2!='0') {
                $random = random_hash(array(
                    $property_type2
                ));
                if ($property_type2=='Single Family') {
                    $name['property_type'] = random_template('single family', $random).' '.random_template('house', $random);;
                }
                elseif ($property_type2=='Multi-Family') {
	                //[4.567] Важно сделать кое-какое изменение
	                $name['property_type'] = 'Multi-Family';
                    //$name['property_type'] = random_template('multi family', $random).' '.random_template('house', $random);
                }
                else $name['property_type'] = $property_type2.' '.random_template('house', $random);
                //$name['property_type'] .= $property_type2;
            }
	        else {
		        if ($q['property_type']==1) {
			        $name['property_type'] = $config['ny_property_types'][$q['property_type']].' '.random_template('house', $random);
		        }
		        else $name['property_type'] = $config['ny_property_types'][$q['property_type']]. ' '.random_template('property', $random);
	        }
        }
        else {
            if ($q['property_type']==1) {
	            if ($q['residential']==1) {
		            $name['property_type'] = @$config['ny_property_types'][$q['property_type']];//.' '.random_template('house', $random);
	            }
                else $name['property_type'] = @$config['ny_property_types'][$q['property_type']].' '.random_template('house', $random);
            }
            else $name['property_type'] = @$config['ny_property_types'][$q['property_type']]. ' '.random_template('property', $random);
        }
    }
    //fl
    else {
	    //[4.753] На карточках вывести параметры
        if ($q['property_type']==1) {
	        $name['property_type'] = $config['object_groups'][$q['property_type']]['name'].' '.random_template('house', $random);
        }
        else {
	        $name['property_type'] = $config['object_groups'][$q['property_type']]['name']. ' '.random_template('property', $random);
        }
    }
    return $name['property_type'];
}

function push ($ids,$data,$debug=false,$api_key=false) {
	if ($api_key==false) $api_key = 'AIzaSyBWBhH0BKQ-emsvauM8_wWLH9eNTHuqcmo';
	//29.07.2019 - поменяли апи кей
	$api_key = 'AIzaSyC4Kic9LfXXGMT2uiqfh8ldDAe_plrFSJc';
	//$ids = array(':APA91bF27zjJGI4ak55G8pUrBWxwnZDGCBYgSKenQF2rzYz76uLAsrmyVbdLA59VshajD8NkuCegEcMfN8mz57Vg1hjY32q2bvy7oePghrY6S8oq4fWlcZenVmZ8iFo3UQ9flCsw5HE7');
	log_add('push.txt',date('Y-m-d H:i:s').' '.$api_key);
	// Set POST variables
	$url = 'https://android.googleapis.com/gcm/send';
	$url = 'https://fcm.googleapis.com/fcm/send';
	$fields = $data;
	//$fields['registration_ids'] = $ids;
	$fields['to'] = (string)$ids[0];
	//$fields['to'] = 'feZYNsJ0Zqs:APA91bFMmTxVKlmyIUxRMzvPFfGSB9PiyRF5vimFcxstx6qhkXv2wN5OW48QXZrrGX7l3Fh4C1iqVFwU3fXC-v3cdTWn0CwIOmFInTZ5zwjpiGIS4qGh-zFyv9Cgv14qrlzOKsOjKC0n';
	/*$fields = array(
		'registration_ids' => $ids,
		'data' => $data,
	);*/
	if ($debug) {
		echo $api_key;
		echo '<br>Запрос к гуглу';
		echo '<pre>';
		print_r($fields);
		echo '</pre>';
	}
	log_add('push.txt',$fields);
	$headers = array(
		'Authorization: key=' . $api_key,
		'Content-Type: application/json'
	);
	// Open connection
	$ch = curl_init();
	// Set the url, number of POST vars, POST data
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// Disabling SSL Certificate support temporarly
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	// Execute post
	$result = curl_exec($ch);
	log_add('push.txt','result: '.$result);
	if ($result === FALSE) {
		log_add('push.txt','error');
		die('Curl failed: ' . curl_error($ch));
	}
	// Close connection
	//echo 'Ответ гугла<br>';
	curl_close($ch);
	$data = json_decode($result,true);
	//log_add('push.txt',$data);
	if ($debug) {
		echo '<br>Ответ гугла';
		echo '<pre>';
		print_r($data);
		echo '</pre>';
	}
	return $data;
	/*echo '<pre>';
	print_r($data);
	echo '</pre>';*/
}

function push2 ($ids,$data,$debug=false,$api_key=false) {
	$api_key = 'AIzaSyBWBhH0BKQ-emsvauM8_wWLH9eNTHuqcmo';
	push ($ids,$data,$debug,$api_key);
}

function mailchimp_add ($email,$name='') {
	global $config;
	$name = explode(' ', $name, 2);
	$url = 'http://us13.api.mailchimp.com/1.3/';
	$url .= '?method=listSubscribe';
	$url .= '&apikey=' . $config['mailchimp_api_key'];
	$url .= '&id=' . $config['mailchimp_id'];
	$url .= '&email_address=' . $email;
	$url .= '&merge_vars[FNAME]=' . $name[0];
	if (isset($name[1])) $url .= '&merge_vars[LNAME]=' . $name[1];
	$url .= '&double_optin=0';
	$url .= '&send_welcome=1';
	//return file_get_contents($url);
}