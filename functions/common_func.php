<?php

//обрезание обратных слешев в $_REQUEST данных
function stripslashes_smart($post) {
	if (get_magic_quotes_gpc()) {
		if (is_array($post)) {
			foreach ($post as $k=>$v) {
				$q[$k] = stripslashes_smart($v);
			}
		}
		else $q = stripslashes($post);
	}
	else $q = $post;
	return $q;
}

//создание урл из $_GET
function build_query($key = '') {
	$get = $_GET;
	if ($key) {
		$array = explode(',',$key);
		foreach ($array as $k=>$v) unset($get[$v]);
	}
	return http_build_query($get);
}

function log_error($str) {
	//return false;
	$log_file_name = 'error_'.date('Y-m').'.txt';
	$str.= "\r\n";
	$str.= "\thttp://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	$str.= "\r\n";
	$fp = fopen($log_file_name, 'a');
	fwrite($fp,$str);
	fclose($fp);
}

//создание файла лога в папке logs
function log_add($file,$string,$debug=false) {
	global $config;
	//логи с пометкой дебаг не создаются при выключеном $config['debug']
	if ($debug==false OR $config['debug'] == true) {
		if (!is_dir(ROOT_DIR . 'logs')) mkdir(ROOT_DIR . 'logs');
		$fp = fopen(ROOT_DIR . 'logs/' . $file, 'a');
		//если в лог передан массив то делаем из него строку
		if (is_array($string)) {
			$content = '';
			foreach ($string as $k=>$v) {
				if (is_array($v)) $content.= $k.':'.serialize($v)."\t";
				else $content.= $k.':'.$v."\t";
			}
			$string = $content;
		}
		fwrite($fp, $string . PHP_EOL);
		fclose($fp);
	}
}

function html_minify ($body) {
	//remove redundant (white-space) characters
	$replace = array(
		//remove tabs before and after HTML tags
		'/\>[^\S ]+/s'   => '>',
		'/[^\S ]+\</s'   => '<',
		//shorten multiple whitespace sequences; keep new-line characters because they matter in JS!!!
		'/([\t ])+/s'  => ' ',
		//remove leading and trailing spaces
		'/^([\t ])+/m' => '',
		'/([\t ])+$/m' => '',
		// remove JS line comments (simple only); do NOT remove lines containing URL (e.g. 'src="http://server.com/"')!!!
		'~//[a-zA-Z0-9 ]+$~m' => '',
		//remove empty lines (sequence of line-end and white-space characters)
		'/[\r\n]+([\t ]?[\r\n]+)+/s'  => "\n",
		//remove empty lines (between HTML tags); cannot remove just any line-end characters because in inline JS they can matter!
		'/\>[\r\n\t]+\</s'    => '><',
		//все пробелы между тегами нельзя удалять
		'/\>[ ]+\</s'    => '> <',
		//remove "empty" lines containing only JS's block end character; join with next line (e.g. "}\n}\n</script>" --> "}}</script>"
		'/}[\r\n\t ]+/s'  => '}',
		'/}[\r\n\t ]+,[\r\n\t ]+/s'  => '},',
		//remove new-line after JS's function or condition start; join with next line
		'/\)[\r\n\t ]?{[\r\n\t ]+/s'  => '){',
		'/,[\r\n\t ]?{[\r\n\t ]+/s'  => ',{',
		//remove new-line after JS's line end (only most obvious and safe cases)
		'/\),[\r\n\t ]+/s'  => '),',
		//remove quotes from HTML attributes that does not contain spaces; keep quotes around URLs!
		'~([\r\n\t ])?([a-zA-Z0-9]+)="([a-zA-Z0-9_/\\-]+)"([\r\n\t ])?~s' => '$1$2=$3$4', //$1 and $4 insert first white-space character found before/after attribute
	);
	$body = preg_replace(array_keys($replace), array_values($replace), $body);

	//remove optional ending tags (see http://www.w3.org/TR/html5/syntax.html#syntax-tag-omission )
	$remove = array(
		'</option>', '</li>', '</dt>', '</dd>', '</tr>', '</th>', '</td>'
	);
	$body = str_ireplace($remove, '', $body);

	return $body;
}

/**
 * функция создает массивы данных чтобы не повторять запросы к базе
 * @param $table - название табклицы
 * @param $id - ИД записи
 * @param string $label - поля, которые нужны в нтмл
 * @return string|array
 * @version v1.2.23
 * v.1.2.23 - добавлена
 */
function get_data($table,$id=true,$label='') {
	global $config;
	//возвращает всю таблицу в виде массива
	if ($id===true) {
		if (!isset($config['_'.$table])) {
			$config['_' . $table] = mysql_select("SELECT * FROM `" . $table . "` WHERE 1", 'rows_id');
		}
		return $config['_' . $table];
	}
	//возвращает конкретную запись по ИД
	if (!isset($config['_'.$table][$id])) {
		if ($config['_'.$table][$id] = mysql_select("SELECT * FROM `".$table."` WHERE id=".intval($id),'row')) {
			//значение по умолчанию
			$config['_'.$table][0] = array(
				'name'=>'',
			);
		}
		else return false;
	}
	if ($label) {
		$array = explode(' ',$label);
		$content = '';
		foreach ($array as $k=>$v) {
			$content.= $config['_'.$table][$id][$v].' ';
		}
		return trim($content);
	}
	else return $config['_'.$table][$id];
}

/**
 * функция для тестирования скриптов, выводить в удобочитаемом виде информацию
 * @param $data - массив значений для вывода на экран
 * @param bool $die - опция умирать или нет
 * добавлана v.1.1.30
 */
function dd($data,$die=false) {
	echo '<pre>';
	print_r($data);
	echo '</pre>';
	if ($die) die();
}

if (!function_exists('sys_getloadavg')) {
	function sys_getloadavg () {
		$load = null;
		if (stristr(PHP_OS, "win")) {//echo 5;
			$cmd = "wmic cpu get loadpercentage /all";
			@exec($cmd, $output);

			if ($output)
			{
				foreach ($output as $line)
				{
					if ($line && preg_match("/^[0-9]+\$/", $line))
					{
						$load = $line;
						break;
					}
				}
			}
		}
		else
		{
			if (is_readable("/proc/stat"))
			{
				// Collect 2 samples - each with 1 second period
				// See: https://de.wikipedia.org/wiki/Load#Der_Load_Average_auf_Unix-Systemen
				$statData1 = _getServerLoadLinuxData();
				sleep(1);
				$statData2 = _getServerLoadLinuxData();

				if
				(
					(!is_null($statData1)) &&
					(!is_null($statData2))
				)
				{
					// Get difference
					$statData2[0] -= $statData1[0];
					$statData2[1] -= $statData1[1];
					$statData2[2] -= $statData1[2];
					$statData2[3] -= $statData1[3];

					// Sum up the 4 values for User, Nice, System and Idle and calculate
					// the percentage of idle time (which is part of the 4 values!)
					$cpuTime = $statData2[0] + $statData2[1] + $statData2[2] + $statData2[3];

					// Invert percentage to get CPU time, not idle time
					$load = 100 - ($statData2[3] * 100 / $cpuTime);
				}
			}
		}

		return $load;
	}
}

function push_telegram ($chat_id,$message) {
	global $config;
	$query = http_build_query(
		array(
			'chat_id' => $chat_id,
			'text' => $message
		)
	);
	$url = 'https://api.telegram.org/bot'.$config['telegram_token'].'/sendMessage?'.$query;
	//$proxy = 'http://demo.abc-cms.com/_/get_content.php';
	//$result = file_get_contents ($proxy.'?url='.base64_encode($url));
	$result = @file_get_contents ($url);
	return $result;
	//return file_get_contents($url);

}