<?php

/**
 * функции для работы со строками
 */

/**
 * генерация ключевых слов
 * @param string $str - html код с текстом
 * @return string - ключевые слова через запятую
 */
function keywords($str) {
	$keywords = '';
	if (strlen($str)>0) {
		$str = preg_replace("/&[\w]+;/", ' ',$str);	//замена символов типа &nbsp; на пробел
		$str = mb_strtolower(trim(strip_tags($str)),'UTF-8');
		$str = preg_replace('~[^-їієа-яa-z0-9 ]+~u', ' ', $str);
		$token = strtok($str, ' ');
		$array = array();
		while ($token) {
			$token = trim($token);
			if (strlen($token)>=4) {
				if (!isset($array[$token])) $array[$token]=0;
				$array[$token]++;
			}
			$token = strtok(' ');
		}
		if (count($array)>0) {
			arsort ($array);
			foreach ($array as $key=>$value) {
				if (strlen($keywords.', '.$key)>255) break;
				$keywords.= ', '.$key;
			}
			return substr($keywords, 2);
		}
	}
}

/**
 * генерирует описание из текста
 * @param string $str - html код с текстом
 * @return string текст около 255 символов длиной
 */
function description($str) {
	$description = '';
	$str = preg_replace("/&[\w]+;/", ' ',$str);	//замена символов типа &nbsp; на пробел
	$str = trim(strip_tags($str));
	$token = strtok($str, ' ');
	while ($token) {
		$token = trim($token);
		if ($token!='') {
			if (strlen($description.' '.$token)>255) break;
			$description.= trim($token).' ';
		}
		$token = strtok(' ');
	}
	return trim($description);
}

/**
 * преобразование кирилицы в транслит
 * @param string $str - строка текста, обычно название
 * @return string - транлит
 */
function trunslit($str,$type=false){
	if ($type=='Url') {
		$str = trim(strip_tags($str));
		$str = str_replace(
			array('a', 'o', 'u', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ь', 'ы', 'ъ', 'э', 'ю', 'я', 'і', 'ї', 'є'),
			array('a', 'o', 'u', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'zh', 'z', 'i', 'i', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'shch', '', 'y', '', 'e', 'yu', 'ya', 'i', 'yi', 'e'),
			$str
		);
		$str = str_replace(
			ARRAY('A', 'O', 'U', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ь', 'Ы', 'Ъ', 'Э', 'Ю', 'Я', 'І', 'Ї', 'Є'),
			ARRAY('A', 'O', 'U', 'A', 'B', 'V', 'G', 'D', 'E', 'E', 'ZH', 'Z', 'I', 'I', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'TS', 'CH', 'SH', 'SHCH', '', 'Y', '', 'E', 'YU', 'YA', 'I', 'YI', 'E'),
			$str
		);
		$str = preg_replace('~[^A-Za-z0-9]+~u', '', $str);    //удаление лишних символов
	}
	else {
		$str = mb_strtolower(trim(strip_tags($str)), 'UTF-8');
		$str = str_replace(
			array('a', 'o', 'u', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ь', 'ы', 'ъ', 'э', 'ю', 'я', 'і', 'ї', 'є'),
			array('a', 'o', 'u', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'zh', 'z', 'i', 'i', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'shch', '', 'y', '', 'e', 'yu', 'ya', 'i', 'yi', 'e'),
			$str
		);
		$str = preg_replace('~[^-a-z0-9_.]+~u', '-', $str);    //удаление лишних символов
		$str = preg_replace('~[-]+~u', '-', $str);            //удаление лишних -
		$str = trim($str, '-');                                //обрезка по краям -
		$str = trim($str, '.');
	}
	return $str;
}

//зaмена функции strtolower
if (!function_exists('mb_strtolower')) {
	function mb_strtolower($str,$enc = 'UTF-8') {
		$large = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','A','A','A','A','A','?','C','E','E','E','E','I','I','I','I','?','N','O','O','O','O','O','O','U','U','U','U','Y','А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я','Є');
		$small = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','a','a','a','a','a','a','?','c','e','e','e','e','i','i','i','i','?','n','o','o','o','o','o','o','u','u','u','u','y','а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я','є');
		return str_replace($large,$small,$str);
	}
}
//оставлена для совместимости
function strtolower_utf8($str){
	return mb_strtolower($str,'UTF-8');
}

/**
 * корректировка часового пояса
 * @param $date - дата
 * @param int $time_zone - временной пояс
 * @return string - дата с учетом временного пояса
 */
function time_zone($date,$time_zone = false) {
	if ($time_zone==false) {
		if (access('user auth')==false) return $date;
		global $user;
		$time_zone = $user['time_zone'];
	}
	if ($time_zone==4) return $date;
	return strftime('%Y-%m-%d %H:%M:%S',(strtotime($date)+($time_zone-4)*60*60));
}

/**
 * доставляет нули к числу
 * @param int $number - число
 * @param int $n - количество цифр в числе
 * @return string - число с нулями - 00067
 */
function zerofill($number,$n = 7) {
	return str_pad($number,$n,'0',STR_PAD_LEFT);
}

/**
 * конвертация даты
 * @param datetime $date - время
 * @param string $type - формат даты
 * @return string - отформатировання дана
 * todo
 * добавить месяца в словарь
 */
function date2($date,$type) {
	$months = array(
		'01'	=>	'января',
		'02'	=>	'февраля',
		'03'	=>	'марта',
		'04'	=>	'апреля',
		'05'	=>	'мая',
		'06'	=>	'июня',
		'07'	=>	'июля',
		'08'	=>	'августа',
		'09'	=>	'сентября',
		'10'	=>	'октября',
		'11'	=>	'ноября',
		'12'	=>	'декабря',
	);
	if ($type=='d month y') {
		if (is_string($date)) $date = strtotime($date);
		$d = strftime('%d',$date);
		$m = strftime('%m',$date);
		$y = strftime('%Y',$date);
		return $d.' '.$months[$m].' '.$y;
	}
	elseif ($type=='month') {
		if (is_string($date)) $date = strtotime($date);
		$m = strftime('%m',$date);
		return $months[$m];
	}
	else return strftime($type,strtotime($date));
}

/**
 * 12:00 - 12 PM
 */
function date_time ($time) {
	$data = explode(':',$time);
	$h = intval($data[0]);
	if ($h==12) $str = $h.':'.$data[1].' PM';
	elseif ($h>12) $str = ($h-12).':'.$data[1].' PM';
	else $str = $h.':'.$data[1].' AM';
	return $str;
}

/**
 * обрезание текста
 * @param $text нтмл - код с текстом
 * @param int $lenght - длина результирующего текста
 * @param string $strip_tags - какие теги оставляем
 * @param string $end - постфикс если обрезали строку
 * @return string - обрезанная строка
 */
function about($text,$lenght = 1000,$strip_tags = '<br><img>',$end = '..') {
	$text  = strip_tags($text,$strip_tags);
	if (strlen($text)>$lenght) {
		$text = iconv_substr($text,0,$lenght,"UTF-8");
		$text.= $end;
	}
	return $text;
}

/**
 * функция читабельности текста
 * @param string $text - текст
 * @return string - оформленный текст
 */
function readability($text) {
	//замена - на &mdash
	$text = str_replace(" - ", "&nbsp;&mdash; ", $text);
	//замена пробелов на &nbsp возле кортких слов (1 и 2 буквы)
	$text = preg_replace('/(^|\s+)([0-9A-Za-zA-Zа-яЇї]{1,2})\s+/ui', '$1$2&nbsp;', $text);
	return $text;
}

/**
 * аналог explode только возвращает не массив а нужный елемент
 * @param string $delimiter - разделитель
 * @param string $str - строка для разделения
 * @param int $number -
 * @param int $count
 * @return mixed
 */
function explode2($delimiter,$str,$number = 1,$count = 2) {
	$array = explode($delimiter,$str,$count);
	$n = $number-1;
	if (isset($array[$n])) return $array[$n];
}

/**
 * ножественное число слова
 * @param int $number - число
 * @param string $str1 - строка один ...
 * @param string $str2 - строка два ...
 * @param string $str5 - строка пять ...
 * @return string
 */
function plural($number, $str1, $str2, $str5)
{
    return $number % 10 == 1 && $number % 100 != 11 ? $str1 : ($number % 10 >= 2 && $number % 10 <= 4 && ($number % 100 < 10 || $number % 100 >= 20) ? $str2 : $str5);
}

//генерирует число для рандомизации из разного набора фильтра
function random_hash($array) {
	$hash = md5(serialize($array));
	$hash = preg_replace('~[^0-9]+~u', '', $hash);
	//echo $hash;
	$data = preg_split('//u',$hash,-1,PREG_SPLIT_NO_EMPTY);
	$total = 0;
	foreach ($data as $i) $total+=$i;
	//echo $total;
	return $total;
}
//рандомизация
function random_template($key,$int) {
	global $config;
	$i = fmod($int,count(@$config['random'][$key]));
	return @$config['random'][$key][$i];
}
//рандомизация
function random_template2($array,$int) {
	global $config;
	$i = fmod($int,count(@$array));
	return $array[$i];
}


/**
 *
 * @param array $object
 * @return string 
 * 
 * Генерирует URL Вида /1-модуль_каталога/2-рут-директория-тип/3-категория/4-город/5-район/6-тип-недвижимости/ID/
 */
/*
function get_product_url($object){
    global $config; global $modules;
    /* TODO: убрать сессию и использовать встроенный кэш в mysql_select - указывается в секундах
     *
    $cities = $_SESSION['cities'] = (@$_SESSION['cities']) ? $_SESSION['cities'] : mysql_select("SELECT * FROM shop_cities ORDER BY rank",'rows_id');
    $neighborhoods = $_SESSION['neighborhood'] = (@$_SESSION['neighborhood']) ? $_SESSION['neighborhood'] : mysql_select("SELECT * FROM shop_neighborhoods ORDER BY rank",'rows_id');
    $categories = $_SESSION['categories'] = (@$_SESSION['categories']) ? $_SESSION['categories'] : mysql_select("SELECT * FROM shop_categories ORDER BY left_key",'rows_id');
    //$root_category = $_SESSION['root_category'] = (@$_SESSION['root_category']) ? $_SESSION['root_category'] : mysql_select("SELECT * FROM shop_categories WHERE level < {$categories[$object['category']]['parent']} AND level > 0 AND right_key >= {$categories[$object['category']]['right_key']} ORDER BY left_key LIMIT 1",'row');    
    if(isset($_SESSION['root_categories'][$object['category']]))
    {
        $root_category = $_SESSION['root_categories'][$object['category']];
    }
    else
    {
        $root_category = mysql_select("SELECT * FROM shop_categories WHERE level < {$categories[$object['category']]['parent']} AND level > 0 AND right_key >= {$categories[$object['category']]['right_key']} ORDER BY left_key LIMIT 1",'row');        
        $_SESSION['root_categories'][$object['category']] = $root_category;
    }    
    $url = "/{$modules['shop']}/{$root_category['url']}-property-for-{$config['object_types'][$object['type']]}/{$categories[$object['category']]['url']}/{$cities[$object['city']]['url']}/{$neighborhoods[$object['neighborhood']]['url']}/{$object['id']}/";    
    return mb_strtolower($url);
}*/

function get_product_url2($object){
    global $config; global $modules; //global $state;   
    //$cities = $_SESSION['cities'] = (@$_SESSION['cities']) ? $_SESSION['cities'] : mysql_select("SELECT * FROM shop_cities ORDER BY rank",'rows_id');    
    //$neighborhoods = $_SESSION['neighborhood'] = (@$_SESSION['neighborhood']) ? $_SESSION['neighborhood'] : mysql_select("SELECT * FROM shop_neighborhoods ORDER BY rank",'rows_id');
    //$categories = $_SESSION['categories'] = (@$_SESSION['categories']) ? $_SESSION['categories'] : mysql_select("SELECT * FROM shop_categories ORDER BY left_key",'rows_id');     
    $states = (isset($_SESSION['_states'])) ? $_SESSION['_states'] : mysql_select("SELECT * FROM shop_states",'rows_id',60*60);
    $cities = (isset($_SESSION['_cities'])) ? $_SESSION['_cities'] : mysql_select("SELECT * FROM shop_cities ORDER BY rank",'rows_id',60*60);
    $neighborhoods = (isset($_SESSION['_neighborhoods'])) ? $_SESSION['_neighborhoods'] : mysql_select("SELECT * FROM shop_neighborhoods ORDER BY rank",'rows_id',60*60);    
    $categories = (isset($_SESSION['_categories'])) ? $_SESSION['_categories'] : mysql_select("SELECT * FROM shop_categories ORDER BY left_key",'rows_id',60*60);      
    
    $_SESSION['_states'] = $states;
    $_SESSION['_cities'] = $cities;
    $_SESSION['_neighborhoods'] = $neighborhoods;
    $_SESSION['_categories'] = $categories;
    
    $state_url = (isset($cities[$object['city']]['state']) && isset($states[$cities[$object['city']]['state']])) ? $states[$cities[$object['city']]['state']]['url'] : 'other';    
    $city = (isset($cities[$object['city']]['url']) && $cities[$object['city']]['url']) ? $cities[$object['city']]['url'] : 'other';
    $neighborhood = (isset($neighborhoods[$object['neighborhood']]['url']) && $neighborhoods[$object['neighborhood']]['url']) ? $neighborhoods[$object['neighborhood']]['url'] : 'other';
    $category = (isset($categories[$object['category']]['url']) && $categories[$object['category']]['url']) ? $categories[$object['category']]['url'] : 'other';
    $url = "/{$state_url}/{$modules['shop']}/property-for-{$config['object_types'][$object['type']]}/{$category}/{$city}/{$neighborhood}/{$object['id']}-{$object['url']}/";
    return mb_strtolower($url);
}
function get_product_url($q){
	global $config; global $modules; //global $state;
	if ($q['base']==6) $m = $modules['shop_mx'];
	elseif ($q['base']<3) $m = $modules['shop'];
	else $m = $modules['shop_ny'];
	$url = '/'.$m.'/'.$config['object_groups'][$q['property_type']]['url'].'/'.$q['neighborhood_url'].'/'.$q['id'].'-'.$q['url'].'/';
	return $url;
}
function get_url($type,$q){
	global $config,$u,$modules,$lang; //global $state;
	if (!isset($lang)) $lang = lang(1);
	if (!isset($modules)) $modules = mysql_select("SELECT url name,module id FROM pages WHERE module!='pages' AND language=".$lang['id']." AND display=1",'array',60*60);
	$url = '';
	//урл для районов и городов
	if (@$q['neighborhood_url']=='' AND isset($q['neighborhood'])) {
		if (@$u[3]) $q['neighborhood_url'] = $u[3];
		else {
			if (!isset($config['neighborhood_urls'])) {
				$config['neighborhood_urls'] = mysql_select("SELECT id,url name FROM shop_neighborhoods",'array');
				$config['city_urls'] = mysql_select("SELECT id,url name FROM shop_cities",'array');
			}
			$q['neighborhood_url'] = &$config['neighborhood_urls'][$q['neighborhood']];
			$q['city_url'] = &$config['city_urls'][$q['city']];
		}
	}
	if (@$q['neighborhood_url']=='') $q['neighborhood_url'] = 'neighborhood';
	if ($type=='product') {
		if ($q['base']==5) {
			$url = '/' . $q['url'] . '/';
		}
		//[4.629] Сделать по другому URL для объектов недвижимости
		else {
			if ($q['id']>1061877) {
				if (!in_array($q['base'], array(10))) {
					//убрал проперти дитейл
					//return get_url('product2', $q);
				}
			}
			//if (access('user admin')) {
				return get_url('product3', $q);
			//}
			//новые урл
			if (1) {//@$_GET['new_url'])
				//new york
				if (in_array($q['base'],array(6,7))) $m = $modules['shop_mx'];
				//флорида
				else $m = $modules['shop_flmx'];
				$url = '/';
				if ($q['neighborhood_url']!='neighborhood') $url.=$q['neighborhood_url'].'-';
				$url.= $q['url'].'-';
				if ($q['zip_code']) $url.= $q['zip_code'].'-';
				$url.= $q['id'] . '/';
			}
			else {
				//флорида
				if ($q['base'] < 3) {
					$m = $modules['shop'];
					$property_type = trunslit($config['object_groups'][$q['property_type']]['url']);
					$state = 'fl';
				} //ny matrix
				elseif (in_array($q['base'], array(4, 6, 7))) {
					$m = $modules['shop_mx'];
					if (isset($config['ny_property_types'][$q['property_type']]))
						$property_type = trunslit($config['ny_property_types'][$q['property_type']]);
					else    $property_type = 'real_estate';
					$state = 'ny';
				} //flex
				elseif (in_array($q['base'], array(8))) {
					$m = $modules['shop'];
					$property_type = 'real_estate';
					$property_type = trunslit($config['object_groups'][$q['property_type']]['url']);
					$state = 'sf';
					$state = 'fl';
				} //florida matrix
				elseif (in_array($q['base'], array(9))) {
					$m = $modules['shop_flmx'];
					if (!isset($config['property_types_9'])) {
						$config['property_types_9'] = mysql_select("
						SELECT uid id, name,url
						FROM shop_property_types
						WHERE base=9 AND parent=0 AND display=1
					", 'rows_id');
					}
					if (isset($config['property_types_9'][$q['property_type']])) {
						$property_type = $config['property_types_9'][$q['property_type']]['url'];
					} else $property_type = 'real_estate';
					$state = 'fl';
				} //ньюйорк
				else {
					$m = $modules['shop_ny'];
					if (isset($config['ny_property_types'][$q['property_type']]))
						$property_type = trunslit($config['ny_property_types'][$q['property_type']]);
					else $property_type = 'real_estate';
					$state = 'ny';
				}

				//во флорида матрикс не добавляем
				if ($q['base'] != 9) {
					if ($q['property_type2']) {
						$property_type2 = explode(',', $q['property_type2']);
						$property_type .= '-' . trunslit(trim($property_type2[0]), '"');
					}
				}
				$residential = 'sale-rent';
				if ($q['residential'] == 1) $residential = 'for-rent';
				elseif ($q['residential'] == 2) $residential = 'for-sale';

				if (!isset($config['cities'])) $config['cities'] = mysql_select("SELECT id,name,url FROM shop_cities", 'rows_id');
				$city = isset($config['cities'][$q['city']]) ? $config['cities'][$q['city']]['url'] : 'city';

				$mls = '';
				if ($q['zip_code']) $mls = '-' . $q['zip_code'];
				if ($q['mln']) $mls .= '-mls' . $q['mln'];

				$url = '/' . $m . '/' . $residential . '/' . $property_type . '/' . $city . '/' . $q['url'] . '-' . $city . '-' . $state . $mls . '-' . $q['id'] . '/';
			}
			/*if ($q['base']==6 OR $q['base']==7) {
				$m = $modules['shop_mx'];
				$m .= '/';
				if (isset($config['ny_property_types'][$q['property_type']]))
					$m .= $config['ny_property_types'][$q['property_type']];
				else
					$m .= 'real_estate';
			}
			elseif ($q['base'] < 3) {
				$m = $modules['shop'];
				$m .= '/';
				$m .= $config['object_groups'][$q['property_type']]['url'];
			}
			else {
				$m = $modules['shop_ny'];
				$m .= '/';
				if (isset($config['ny_property_types'][$q['property_type']]))
					$m .= $config['ny_property_types'][$q['property_type']];
				else
					$m .= 'real_estate';
			}
			$url = '/' . $m . '/' . $q['neighborhood_url'] . '/' . $q['id'] . '-' . $q['url'] . '/';
			*/
		}
	}
	if ($type=='product2') {
		$url = '/property-detail/';
		$url.= $q['url'].'-';
		if (!isset($config['neighborhood_urls'])) {
			$config['neighborhood_urls'] = mysql_select("SELECT id,url name FROM shop_neighborhoods",'array');
			$config['city_urls'] = mysql_select("SELECT id,url name FROM shop_cities",'array');
		}
		$q['neighborhood_url'] = &$config['neighborhood_urls'][$q['neighborhood']];
		$q['city_url'] = &$config['city_urls'][$q['city']];
		if ($q['neighborhood_url']!='neighborhood') $url.=$q['neighborhood_url'].'-';
		if (@$q['city_url']) $url.=$q['city_url'].'-';
		$url.= ($q['base']!=10) ? 'ny-':'fl-';
		if ($q['zip_code']) $url.= $q['zip_code'].'-';
		$url.= $q['id'] . '/';
	}
	if ($type=='product3') {
		$url = '/';
		$url.= $q['url'].'-';
		if ($q['neighborhood_url']!='neighborhood') $url.=$q['neighborhood_url'].'-';
		if ($q['base']!=10) {
			if (@$q['city_url']) $url .= $q['city_url'] . '-';
			$url.= 'ny-';
		}
		else {
			$url.= 'fl-';
		}

		if ($q['zip_code']) $url.= $q['zip_code'].'-';
		$url.= $q['id'] . '/';
	}
	if ($type=='agent') {
		$url = '/'.$q['url'].'/';
	}
	if ($type=='preconstruction') {
		if ($q['base']==1) $m = $modules['shop'];
		elseif ($q['base']==2) $m= $modules['shop_mx'];
		else $m = $modules['shop'];

		$url = '/' . $m . '/' . $modules['preconstruction'] . '/'. $q['neighborhood_url'] . '/' . $q['id'] . '-' . $q['url'] . '/';
	}
	if ($type=='condo') {
		$url = '/' . $modules['shop'] . '/' . $modules['condos'] . '/'. $q['neighborhood_url'] . '/' . $q['id'] . '-' . $q['url'] . '/';
	}
	if ($type=='landing') {
		$url = '/'.$q['url'].'/';
		$url = '/'.$q['id'].'-'.$q['url'].'-'.$q['nb_url'].'-'.$config['object_base_state'][$q['base']].($q['zip_code']?'-'.$q['zip_code']:'').'/';
	}
	if ($type=='shop') {
		if (isset($modules['shop'])) $url = '/'.$modules['shop'].'/';
		if (isset($q['base'])) {
			//ньюйорк
			if (in_array($q['base'],array(6,7))) {
				if (isset($modules['shop_mx'])) $url = '/'.$modules['shop_mx'].'/';
			}
			//флорида
			if (in_array($q['base'],array(9))) {
				if (isset($modules['shop_mx'])) $url = '/'.$modules['shop_flmx'].'/';
			}
			//флорида матрикс
			if (in_array($q['base'],array(10))) {
				if (isset($modules['shop_mls10'])) $url = '/'.$modules['shop_mls10'].'/';
			}
		}
	}
	//return $url;
	//[4.781] Настроить регистронезависимость
	return mb_strtolower($url, 'UTF-8');
}

function YouTubeURLs($url) {
    $url = preg_replace('~
        # Match non-linked youtube URL in the wild. (Rev:20130823)  
        
        https?://         # Required scheme. Either http or https.
        (?:[0-9A-Z-]+\.)? # Optional subdomain.
        (?:               # Group host alternatives.
          youtu\.be/      # Either youtu.be,
        | youtube\.com    # or youtube.com followed by
          \S*             # Allow anything up to VIDEO_ID,
          [^\w\-\s]       # but char before ID is non-ID char.
        )                 # End host alternatives.
        ([\w\-]{11})      # $1: VIDEO_ID is exactly 11 chars.
        (?=[^\w\-]|$)     # Assert next char is non-ID or EOS.
        (?!               # Assert URL is not pre-linked.
          [?=&+%\w.-]*    # Allow URL (query) remainder.
          (?:             # Group pre-linked alternatives.
            [\'"][^<>]*>  # Either inside a start tag,
          | </a>          # or inside <a> element text contents.
          )               # End recognized pre-linked alts.
        )                 # End negative lookahead assertion.
        [?=&+%\w.-]*        # Consume any URL (query) remainder.
        ~ix', 
        //'<a href="http://www.youtube.com/watch?v=$1">YouTube link: $1</a>',
       //'<iframe width="350" height="240" src="//www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>',
        '$1',
        $url);
    return $url;
}

function hours ($hours=0,$minutes=0,$int=false) {
	if (is_array($hours)) {
		$minutes = (int)@$hours['minutes'];
		$hours = (int)@$hours['hours'];
	}
	if (!$hours) $hours = 0;
	if ($minutes>59) {
		$hours+=floor($minutes/60);
		$minutes = fmod($minutes,60);
	}
	if ($int) {
		return $hours+$minutes/60;
	}
	else {
		if (strlen($minutes)==1) $minutes = '0'.$minutes;
		return $hours.':'.$minutes;
	}
}


function name_search($str) {
	global $config;
	if (!isset($config['shop_search']))
		$config['shop_search'] = mysql_select("SELECT LOWER(search) id, keyword name FROM shop_search",'array');

	$arr = explode(' ',$str);
	$adr = array();
	foreach ($arr as $k=>$v) {
		$str = strtolower($v);
		if (isset($config['shop_search'][$str])) $adr[] = $config['shop_search'][$str];
		else $adr[] = $v;
	}
	$address = implode(' ',$adr);
	return $address;
}
function name_search2($str) {
	global $config;
	if (!isset($config['shop_search']))
		$config['shop_search'] = mysql_select("SELECT search name, LOWER(keyword) id FROM shop_search2",'array');

	$arr = explode(' ',$str);
	$adr = array();
	foreach ($arr as $k=>$v) {
		$str = strtolower($v);
		if (isset($config['shop_search'][$str])) $adr[] = $config['shop_search'][$str];
		else $adr[] = $v;
	}
	$address = implode(' ',$adr);
	return $address;
}

function clear_text ($str) {
	$str = str_replace(
		array('ä', 'ö', 'ü', 'ß','ż','ę','É','ñ','é'),
		array('ae','oe','ue','ss','z','e','E','n','e'),
		$str);
	//$str = preg_replace('~[^-їієЇІЄА-Яа-яA-Za-z0-9 ();,<>"\'/:&_]+~u', '', $str);
	return $str;
	//return strstr($str, "(") ? trim(strstr($str,'(',true)):$str;
}