<?php
/*
 * florida matrix
 */

$config['rets_url'] = 'https://miamire.apps.retsiq.com/contact/rets/login';
$config['rets_url'] = 'https://miamire.apps.retsiq.com/contact/rets/login';
$config['rets_login'] = 'WiSkR';
$config['rets_pw1'] = 'gm83xpt5118';
$config['rets_pw2'] = '';

define('FIELD_UID', 'Matrix_Unique_ID'); // uid номер отдельный для Open House и листинга. Т.е. это разные объекты потому номера могут совпадать
define('FIELD_UID_LISTING', 'Listing_MUI'); // поле в опен хаусе, где содержится ссылка на UID объекта листинга
// логика других связей
// OpenHouse.Listing_MUI = Listing.Matrix_Unique_ID
// Rooms.Listing_MUI = Listing.Matrix_Unique_ID
// Listing.ListAgent_MUI = Agent.Matrix_Unique_ID
// Media.Table_MUI = Listing.Matrix_Unique_ID (media is where photos are stored)

$config['MiamiPropertyTypes'] = [
	'RE1' => 'Single Family',
	'RE2' => 'Condo/Co-op/Villa/Townhouse',
	'RIN' => 'Residential Income',
	'RLD' => 'Residential Land/Boat Docks',
	'CLD' => 'Commercial/Business/Agricultural/Industrial Land',
	'RNT' => 'Residential Rental',
	'COM' => 'Commercial/Industrial',
	'BUS' => 'Business Opportunity',
];

$config['MiamiStatuses'] = [
	'A' => 'Active',
	'B' => 'Backup Contract',
	'C' => 'Cancelled',
	'CS' => 'Closed Sale',
	'PS' => 'Pending Sale',
	'Q' => 'Terminated',
	'R' => 'Rented',
	'T' => 'Temp Off Market',
	'W' => 'Withdrawn',
	'X' => 'Expired',
];

require_once(ROOT_DIR."plugins/rets2.5/vendor/autoload.php");

function miami_id ($id,$type='Matrix_Unique_ID') {
	global $config;
	if ($type=='MLSNumber') $query = "(MLSNumber=".$id.")";
	else $query = "(Matrix_Unique_ID=".$id.")";
	//
	try {
		$rets_config = new \PHRETS\Configuration;
		$rets_config->setLoginUrl($config['rets_url'])
			->setUsername($config['rets_login'])
			->setPassword($config['rets_pw1'])
			->setRetsVersion('1.7.2')
			->setOption('use_post_method', true);
		//
		$rets = new \PHRETS\Session($rets_config);
		$rets->Login();

		if ($records = $rets->Search("Property", 'Listing', $query, array('Limit' => 1, 'Offset' => 1, 'Count' => 1))) {
			$rets->Disconnect();
			return isset($records[0]) ? $records[0] : [];
		}
		$rets->Disconnect();
	} catch (Exception $e) {
		echo '<br>'.$e->getMessage().'</br>';
	}
	return false;
}


function miami_list ($query='', $limit=5000, $offset = 1) {
	global $config;
	$query = $query ? $query : "(Matrix_Unique_ID=0+)";
	//
	try {
		$rets_config = new \PHRETS\Configuration;
		$rets_config->setLoginUrl($config['rets_url'])
			->setUsername($config['rets_login'])
			->setPassword($config['rets_pw1'])
			->setRetsVersion('1.7.2')
			->setOption('use_post_method', true);
		//
		$rets = new \PHRETS\Session($rets_config);
		$rets->Login();

		if ($records = $rets->Search("Property", 'Listing', $query, ['Limit' => $limit, 'Offset' => $offset])) {
			$rets->Disconnect();
			return $records;
		}
		$rets->Disconnect();
	} catch (Exception $e) {
		echo '<br>'.$e->getMessage().'</br>';
	}
	return false;
}


function miami_img ($id) {
	global $config;

	try {
		$rets_config = new \PHRETS\Configuration;
		$rets_config->setLoginUrl($config['rets_url'])
			->setUsername($config['rets_login'])
			->setPassword($config['rets_pw1'])
			->setRetsVersion('1.7.2')
			->setOption('use_post_method', true);
		//
		$rets = new \PHRETS\Session($rets_config);
		$rets->Login();

		$collection = $rets->GetObject('Property', 'LargePhoto', $id);
		// в $collection возращается коллекция \Illuminate\Support\Collection привет Ларавел :)
		// технически коллекция имплементирует иттератор и должна через foreach() проходить, но матерится, может я чего недопонял :) потому не парился и просто воспользовался функциями коллекции
		// для общего развития https://laravel.com/api/5.5/Illuminate/Support/Collection.html
		if ($collection->count()) {
			$rets->Disconnect();
			return $collection->all();
		}
		//
		$rets->Disconnect();
	} catch (Exception $e) {
		echo '<br>'.$e->getMessage().'</br>';
	}

	return [];
}

function miami_property_type ($q) {
	global $miami_property_type;
	if (empty($miami_property_type)) {
		foreach (array(1, 2, 3, 6) as $k => $v) {
			$miami_property_type[$v] = mysql_select("
				SELECT uid id, mls name FROM shop_property_types WHERE base=9 AND parent=" . $v . " AND display=1
			", 'array');
		}
	}

	//residential
	//todo проверить
	$residential = 0;
	if ($q['PropertyType'] == 'Residential Rental') $residential = 1;
	else $residential = 2;
	//if (@$q['ForLeaseYN']==1) $residential = 1;

	//todo
	$property_type = 0;
	$property_type2 = array();
	if ($q['PropertyType'] == 'Single Family') {
		$property_type = 1;
	}
	if ($q['PropertyType'] == 'Condo/Co-Op/Villa/Townhouse') {
		$property_type = 1;
	}
	if ($q['PropertyType'] == 'Residential Rental') {
		$property_type = 1;
	}
	if ($q['PropertyType'] == 'Business Opportunity') {
		$property_type = 2;
	}
	if ($q['PropertyType'] == 'Commercial/Industrial') {
		$property_type = 2;
	}
	if ($q['PropertyType'] == 'Commercial/Business/Agricultural/Industrial Land') {
		//это все рент
		$residential = 1;
		$property_type = 2;
		$property_type2[] = 221;
	}
	if ($q['PropertyType'] == 'Residential Income') {
		$property_type = 6;
	}
	if ($q['PropertyType'] == 'Residential Land/Boat Docks') {
		$property_type = 3;
	}

	//property_type2
	//!Single Family !Residential Rental +Commercial/Industrial
	if (@$q['PropTypeTypeofBuilding']) {
		$property_type2 = miami_property_type_search($property_type2,$miami_property_type[$property_type],$q['PropTypeTypeofBuilding']);
	}
	//!Single Family !Residential Rental
	if (@$q['TypeofBuilding']) {
		$property_type2 = miami_property_type_search($property_type2,$miami_property_type[$property_type],$q['TypeofBuilding']);
	}
	//!Single Family !Residential Rental !Condo/Co-Op/Villa/Townhouse
	if (@$q['Style']) {
		$property_type2 = miami_property_type_search($property_type2,$miami_property_type[$property_type],$q['Style']);
	}
	//!Commercial/Industrial +Condo/Co-Op/Villa/Townhouse +Residential Rental
	if (@$q['TypeofProperty']) {
		$property_type2 = miami_property_type_search($property_type2,$miami_property_type[$property_type],$q['TypeofProperty']);
	}
	if ($property_type2) {
		$property_type2 = array_unique($property_type2);
		$property_type2 = $property_type2 ? implode(',', $property_type2) : '';

	}
	else $property_type2 = '0';

	return array(
		'property_type'=>$property_type,
		'property_type2'=>$property_type2,
		'residential'=>$residential,
	);
}


function miami_property_type_search ($property_type2,$array,$str) {
	foreach ($array as $k => $v) {
		$val = explode(',',$v);
		foreach ($val as $k1=>$v1) {
			if (stristr($str, $v1) !== FALSE) {
				$property_type2[] = $k;
			}
		}
	}
	return $property_type2;
}