<?php

/*
 * matrix
 */

$config['rets_matrix'] = array(
	'login_url' => "http://matrixrets.bnymls.com/rets/login.ashx",
	'username' => "albert27",
	'password' => "oeYyLexaO7CWBqemvYCN",
);

require_once(ROOT_DIR."functions/phrets.php");

function matrix_id ($id,$type='Matrix_Unique_ID') {
	global $config;
	$rets = new phRETS;
	if ($type=='MLSNumber') $query = "(MLSNumber=".$id.")";
	else $query = "(Matrix_Unique_ID=".$id.")";
	//echo $query;
	$connect = $rets->Connect($config['rets_matrix']['login_url'], $config['rets_matrix']['username'], $config['rets_matrix']['password']);
	if ($connect) {
		$offset = 1;
		$limit = 1;
		if ($data = $rets->Search("Property", 'Listing', $query, array('Limit' => $limit, 'Offset' => $offset,
			'Format' => 'COMPACT-DECODED', 'Count' => 1, "UsePost" => 1))) {
			$rets->Disconnect();
			return $data[0];
		}
		$rets->Disconnect();
	} else {
		echo '<br>Не удаось соедениться с MATRIX сервером</br>';
	}	
	return false;
}

function matrix_img ($id) {
	global $config;
	$rets = new phRETS;
	$connect = $rets->Connect($config['rets_matrix']['login_url'], $config['rets_matrix']['username'], $config['rets_matrix']['password']);
	
	if ($connect) {
		$media = $rets->GetObject('Property', 'LargePhoto', $id);
		if ($media[0]['Success']==1) {
			$rets->Disconnect();
			return $media;
		}
		$rets->Disconnect();
	} else {
		echo '<br>Не удаось соедениться с MATRIX сервером</br>';
	}	
	return false;
}

function matrix_open_houses ($product,$test=false) {
	global $config;
	$date = date("Y-m-d");
	$oh_dates = @$product['oh_dates'] ? serialize($product['oh_dates']) : array();
	$rets = new phRETS;
	$connect = $rets->Connect($config['rets_matrix']['login_url'], $config['rets_matrix']['username'], $config['rets_matrix']['password']);
	if ($connect) {
		$query = "(Listing_MUI={$product['uid']})";
		$offset = 1;
		$limit = 100;
		if ($records = $rets->Search('OpenHouse', 'OpenHouse', $query, array('Limit' => $limit, 'Offset' => $offset,
			'Format' => 'COMPACT-DECODED', 'Count' => 1, "UsePost" => 1))) {
			$oh_dates = array();
			//dd($records);
			foreach ($records as $k=>$v) {
				$n = preg_split('//u',$v['StartTime'],-1,PREG_SPLIT_NO_EMPTY);
				if (isset($n[3])) $StartTime = $n[0].$n[1].':'.$n[2].$n[3];
				else $StartTime = $n[0].':'.$n[1].$n[2];
				$n = preg_split('//u',$v['EndTime'],-1,PREG_SPLIT_NO_EMPTY);
				if (isset($n[3])) $EndTime = $n[0].$n[1].':'.$n[2].$n[3];
				else $EndTime = $n[0].':'.$n[1].$n[2];
				$OpenHouseDate = str_replace(array('T','.000'), array(' ',''), $v['OpenHouseDate']);
				//чтобы даты не повторялись
				$y = true;
				/*foreach($oh_dates as $k1=>$v1) {
					if (@$v1['OpenHouseID']==$OpenHouseDate) {
						$y = false;
						break;
					}
				}*/
				if ($y==true) {
					$oh_dates[] = array(
						'from' => $StartTime,
						'to' => $EndTime,
						'date' => $OpenHouseDate,
						'OpenHouseID' => $v['matrix_unique_id'],
						'agent' => $product['agent'],
					);
				}
			}
			/*echo '<pre>';
			print_r($oh_dates);
			echo '</pre>';*/
		}
		//else echo '<br>нет опенхаусов1';
	} else {
		echo '<br>Не удаось соедениться с MATRIX сервером</br>';
	}
	if ($test==true) {
		return $oh_dates;
	}
	//else echo '<br>нет опенхаусов2';
	$data = array(
		'oh_date' => '0000-00-00',
		'oh_dates'=>'',
		'oh_display'=>''
	);
	if($oh_dates) {
		usort($oh_dates, "sort2");
		foreach ($oh_dates as $k=>$v) {
			echo '<br>'.$v['date'].' - '.$date;
			if (strtotime($v['date'])<strtotime($date)) unset($oh_dates[$k]);
			else {
				echo ' - ';
				if ($data['oh_date'] == '0000-00-00' OR strtotime($v['date']) > strtotime($data['oh_date'])) {
					$data['oh_date'] = $v['date'];
				}
				else echo 'x'.$data['oh_date'];
			}
		}
		if ($oh_dates) {
			$data['oh_dates'] = serialize($oh_dates);
			$data['oh_display'] = 1;
		}
	}
	$rets->Disconnect();
	return $data;
}

function sort2($a, $b) {
	return strcmp($a['date'], $b['date']);
}

function matrix_list ($query='') {
	global $config;
	$query = $query ? $query : "(MatrixModifiedDT=2014-01-01T00:00:00+)";
	$offset = 1;
	$limit = 5000;
	$rets = new phRETS;
	//echo $query;
	$connect = $rets->Connect($config['rets_matrix']['login_url'], $config['rets_matrix']['username'], $config['rets_matrix']['password']);
	//var_dump($connect);die();
	if ($connect) {
		if ($records = $rets->Search("Property", 'Listing', $query, array('Limit' => $limit, 'Offset' => $offset,
			'Format' => 'COMPACT-DECODED', 'Count' => 1, "UsePost" => 1))) {
			$rets->Disconnect();
			//var_dump($records); die();
			return $records;
		}
		$rets->Disconnect();
	} else {
		echo '<br>Не удаось соедениться с MATRIX сервером</br>';
	}	
	return false;
}

// тут как обычно, либо можно задать уточняющий query по параметрам из класса ресурса, по умолчению дата от 01 января 14 иначе не выдаст результаты
// лимиты соответсвующе, что бы вытянуть все, то надо запросить следующие $limit со сдвигом $limit * страницу пагинации
function matrix_agents($query='', $limit=5000) {
	global $config;
	$rets = new phRETS;	
	$query = $query ? $query : "(MatrixModifiedDT=2014-01-01T00:00:00+)";
	$offset = 0;
	//$limit = 5000;
	$connect = $rets->Connect($config['rets_matrix']['login_url'], $config['rets_matrix']['username'], $config['rets_matrix']['password']);	
	if ($connect) {
		if ($records = $rets->Search("Agent", 'Agent', $query, array('Limit' => $limit, 'Offset' => $offset,
			'Format' => 'COMPACT-DECODED', 'Count' => 1, "UsePost" => 1))) {
			$rets->Disconnect();
			return $records;
		}	
		$rets->Disconnect();
	} else {
		echo '<br>Не удаось соедениться с MATRIX сервером</br>';
	}
	return false;
}