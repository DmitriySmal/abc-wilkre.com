<?php

/*
 * florida
 */

function sf_object($object,$category) {
	global $config;
	$status = $object[246];
	$status = array_search($status, $config['object_statuses']);
	//[4.740] флорида - ресурсы
	/*
	if ($object[131]) {
		if ($object[245]) {
			$date_change = $object[131] > $object[245] ? $object[131] : $object[245];
		}
		else $date_change = $object[131];
	}
	else $date_change = $object[245];
	*/
	//дата обновления фото в млс
	$date_change = $object[1329] ? str_replace('T',' ',$object[1329]) : '';
	//дата добавления в млс
	$date_add = str_replace('T',' ',@$object[80]);
	//дата обновления на сайте
	$date = date('Y-m-d H:i:s');

	$state = $object[924];
	$city = $object[922];
	//имя - полный адрес
	$name = (isset($object[881]) && $object[881]) ? $object[881] : '';
	$name = name_search($name);
	//адрес без квартиры #
	$address = substr($name, 0, strpos($name, '#') - 1);
	//$name = $city . ' ' . $address;
	$description = $object[214];

	$type = isset($object[583]) ? $object[583] : 'yes';
	$type = array_search($type, $config['for_sale']);
	$residental = (in_array($category, $config['residental']) ? 1 : 0);
	$ml_number = $object[157];
	$unit_number = (@$object[17]) ? $object[17] : 0;
	//$zip = $object[10] . ((isset($object[11]) && $object[11]) ? '-'.$object[11] : '');
	$zip = isset($object[10]) ? $object[10] : '';
	if (!isset($config['sf_shop_neighborhoods'])) {
		$config['sf_shop_neighborhoods'] = mysql_select("SELECT name id,id name FROM shop_neighborhoods WHERE city=6",'array');
	}
	$nb['id'] = (int)@$config['sf_shop_neighborhoods'][$city];

	//чтобы не хранить лишние параметры
	$parameters = array();
	foreach ($object as $k=>$v) {
		if ($v) $parameters[$k] = $v;
	}
	$square = 0;
	if (@$object[129]) $square = $object[129];
	elseif (@$object[232]) $square = $object[232];
	elseif (@$object[520]) $square = $object[520];


	$post = array(
		'uid' => $object['sysid'],
		'base'=>2,
		'mln' => $ml_number,
		//'display' => 1,
		'price' => $object[137],
		'name' => $name,
		'url' => trunslit($name),
		'address' => $address, //$state.' '.$city.' '.$address,
		'text' => $description,
		'title' => $name,
		'keywords' => keywords($description),
		'description' => description($description),
		'category' => $category,
		'city' => 6,
		'neighborhood' => $nb['id'],
		'zip_code' => $zip,
		'status' => $status,
		'type' => $type,
		//'residential' => $residental,
		'date' => $date,
		'date_change'=>$date_change,
		'date_add'=>$date_add,
		'unit' => $unit_number,
		'beds'=> @$object['25'],
		'baths'=> @$object['92'],
		'square'=> $square,
		'office_name'=> $object[165],
		'office_agent'=>$object[144],
		'office_phone'=>$object[167],
		'complex_name'=> @$object['326'],
		'development_name'=> @$object['69'],
		'parameters'=> serialize($parameters),
		'errors'=>0,//обнуляем ошибки
		//'img' => '',
		//'imgs' => ''
	);

	if (strtolower($post['office_name'])==strtolower('Wilk Real Estate I LLC')) {
		$post['special'] = 1;
		$post['agent'] = 34; //wilk
	}
	//property_type
	$str = '';
	$post['residential'] = 2; //sale
	if ($post['category']==1) $post['property_type'] = 1; //sale
	elseif ($post['category']==2) $post['property_type'] = 2; //sale
	elseif ($post['category']==3) {//sale
		$post['property_type'] = 6;
		if (@$parameters['446']) $str.= '"'.mb_strtolower($parameters['446'],'UTF-8').'"';
	}
	elseif ($post['category']==4) $post['property_type'] = 5; //sale
	elseif ($post['category']==5) $post['property_type'] = 5; //sale
	elseif ($post['category']==6) {
		$post['property_type'] = 3;
		$post['residential'] = 1;
		if (@$parameters['588']) $str.= '"'.mb_strtolower($parameters['588'],'UTF-8').'"';
	}
	elseif ($post['category']==7) {
		$post['property_type'] = 4;
		if (@$parameters['594']) $str.= mb_strtolower($parameters['594'],'UTF-8');
		if (@$parameters['672']) {
			if ($parameters['672'] == 'Lease') $post['residential'] = 1;
			//if ($str) $str.=',';
			//$str.= '"'.mb_strtolower($parameters['672'],'UTF-8').'"';
		}
	}
	elseif ($post['category']==8) {//sale
		$post['property_type'] = 4;
		if (@$parameters['716']) $str.= mb_strtolower($parameters['716'], 'UTF-8');
		//if (@$parameters['750']) $post['residential'] = 1;
	}
	if ($str) $post['property_type2'] = $str;
	$post['rank'] = object_rank ($post);

	return $post;
}