<?php

include(ROOT_DIR.'_config_ny.php');

$html['module'] = 'shop';

$html['submenu'] = html_query('menu/list', "
	SELECT id,name,url,module,1 level,1 submenu
	FROM pages
	WHERE display=1 AND menu = 1 AND parent=".$page['id']."
	ORDER BY left_key
", '', 60 * 60, 'json');

//вложенные модули
$submodule = false;
if ($submenu = mysql_select("SELECT id,name,url,module,level,1 submenu
	FROM pages
	WHERE display=1 AND menu = 1 AND parent=".$page['id']."
	ORDER BY left_key",'rows',60 * 60))
{
	foreach ($submenu as $k=>$v) {
		if ($v['url']==$u[2]) {
			$page = mysql_select("SELECT * FROM pages WHERE id=".$v['id'],'row');
			$breadcrumb['page']	= array_merge(
				array(
					array(
						$page['name'],
						'/'.$u[1].'/'.$u[2].'/'
					)
				),
				$breadcrumb['page']
			);
			$submodule = $v['module'];
			include(ROOT_DIR.'modules/'.$submodule.'.php');
			break;
		}
	}
}

//если нет вложенного модуля
if ($submodule == false) {
	$html['is_product'] = false;
	$html['is_map'] = false;
	$where = $order_by = ' ';
	$view_order = array('asc', 'desc');
	$view_type = array('price');//,'date'

	$zips = array();
	$where = $join = '';

	$html['is_quick'] = $quick = mysql_real_escape_string(@$_GET['quick']);

	if ($u[4]) {
		//$html['content'] = html_array('shop/product_text',$page);
		$id = intval(explode2('-', $u[4]));
		//запрос на товар и на категорию
		$product = mysql_select("
			SELECT sp.*, sn.name nb_name
			FROM shop_products sp
			LEFT JOIN shop_neighborhoods AS sn ON sn.id = sp.neighborhood
			WHERE /*sp.display = 1 AND */sp.id = '" . $id . "'
			LIMIT 1
		", 'row');
		if ($product) {
			//$breadcrumb['page'] = array();
			$breadcrumb['module'][] = array(
				$product['name'],
				$_SERVER['REQUEST_URI']
			);
			$breadcrumb['module'][] = array(
				$product['nb_name'],
				'/' . $modules['shop_ny'] . '/?property_type=' . $product['property_type'].'&location[]=' . $product['neighborhood']
			);
			$breadcrumb['module'][] = array(
				$config['ny_property_types'][$product['property_type']],
				'/' . $modules['shop_ny'] . '/?property_type=' . $product['property_type']
			);

			$html['is_product'] = true;
			$html['search'] = html_array('shop/search_ny');

			unset($product['bimg']);
			$page = array_merge($page, $product);

			//формировка title
			$page['title'] = $page['name'];
			$page['title'].= ', ';
			//property_type
			if (isset($config['ny_property_types'][$page['property_type']])) {
				$page['title'].= $config['ny_property_types'][$page['property_type']];
				if ($page['property_type2'] AND isset($config['ny_property_types_'.$page['property_type']])) {
					if (in_array($page['property_type2'],$config['ny_property_types_'.$page['property_type']])) {
						$page['title'].= ' '.$page['property_type2'];
					}
				}
			}
			else {
				$page['title'].= 'Real Estate Properties';
			}
			//location
			$page['title'].= ' in ';
			if ($page['city']) {
				$page['title'].= mysql_select("SELECT name FROM shop_cities WHERE id =".$page['city'],'string').' ';
			}
			else $page['title'].= ' New York ';
			$page['title'].= $page['nb_name'];
			$page['title'].= ' - Wilk Real Estate I LLC';

			$html['content'] = (!isset($_GET['print'])) ? html_array('shop/product_text', $page) : html_array('shop/product_print', $page);
		} else
			$error++;
	}
	else {
		//фильтр поиска
		$fields = array(
			'residential'=>'int',
			'property_type' => 'int',
			'property_type_1'=>'int',
			'property_type_2'=>'int',
			'quick' => 'text',
			'city' => 'int',
			'location' => 'array_int',
			'beds' => 'int',
			'baths' => 'int',
			'price_min' => 'price',
			'price_max' => 'price',
			'quick'=>'text',
			'view'=>'text'
		);
		$post = form_smart($fields, stripslashes_smart($_GET)); //print_r($post);
		$where = '';

		//property_type
		if ($post['property_type']) {
			$where .= " AND sp.property_type = ".$post['property_type'];
			if ($post['property_type']==1) {
				if ($post['property_type_1']) $where .= " AND sp.property_type2 = '".$post['property_type_1']."'";
			}
			if ($post['property_type']==2) {
				if ($post['property_type_2']) $where .= " AND sp.property_type2 = '".$post['property_type_2']."'";
			}
		}

		//количество кроватей, ванн, цена
		$where .= $post['beds'] ? " AND sp.beds = " . $post['beds'] . " " : '';
		$where .= $post['baths'] ? " AND sp.baths = " . $post['baths'] . " " : '';
		if ($post['price_min']) $where .= " AND sp.price >= " . $post['price_min'];
		if ($post['price_max']) $where .= " AND sp.price <= " . $post['price_max'];

		//neighborhoods
		if ($post['city']) $where .= " AND sp.city = ".$post['city'];
		$neighborhoods = false;
		if (count($post['location']) > 0) {
			$neighborhoods = mysql_select("SELECT id,name FROM shop_neighborhoods WHERE id IN (" . implode(',', $post['location']) . ")", 'array', 60 * 60);
			$where .= $neighborhoods ? " AND sp.neighborhood IN (" . implode(',', array_keys($neighborhoods)) . ") " : '';
		}
		//если не выбран location то показываем всю флориду
		//if ($neighborhoods==false) $where.=" AND sp.city=6";

		//сортировка только по цене
		if (@$_GET['sort']) {
			$view = explode('-', $_GET['sort']);
			if (in_array($view[0], $view_type)) {
				$order_by = " sp.{$view[0]} " . ((in_array($view[1], $view_order)) ? strtoupper($view[1]) : 'DESC') . ', ';
			}
		}

		//аренда или продажа
		if ($post['residential']) $where.=" AND sp.residential=".$post['residential'];

		//быстрый поиск
		if ($quick) {
			$quick2 = mb_strtolower($quick);
			$where.= ' AND (';
			$where .= " LOWER (sp.name) LIKE '%{$quick2}%' ";
			$where .= " OR LOWER (sp.address) LIKE '%{$quick2}%' ";
			$where .= " OR LOWER (sp.zip_code) LIKE '{$quick2}' ";
			$where .= " OR LOWER (neighborhood.name) LIKE '%{$quick2}%' ";
			$where .= " OR LOWER (neighborhood.name2) LIKE '%{$quick2}%' ";
			$where .= ')';
		}

		//формировка H1
		if ($quick) {
			$page['name'] = "Search '{$quick}' in " . $state['name'];
			$page['subname'] = '';
		}
		else {
			$page['name'] = '';
			if ($post['beds']) $page['name'] .= $post['beds'] . ' bedrooms';
			if ($post['baths']) {
				$page['name'] .= $page['name'] ? ' and ' : '';
				$page['name'] .= $post['beds'] . ' baths';
			}
			if ($post['price_min']) {
				$page['name'] .= ' from ' . $post['price_min'] . '$';
			}
			if ($post['price_max']) {
				$page['name'] .= ' under ' . $post['price_max'] . '$';
			}

			if ($post['group'] AND isset($config['object_groups'][$post['group']])) {
				$page['name'] .= $page['name'] ? ' ' : '';
				$page['name'] .= $config['object_groups'][$post['group']]['name'];
			}
			if ($neighborhoods) {
				$page['subname'] = 'in ' . implode(', ', $neighborhoods);
			}
		}
		if ($page['name'] == '') {
			if ($page['subname'] == '') $page['name'] = 'Sold Properties';
			else $page['name'] = 'All Properties';
		}

		//если без гет параметров то только с галками
		$select = '';
		if ($where == '') {
			//$where.= " AND sp.city=6";
			//$where.= ' AND sp.special=1';
			//$select = ', IF (property_type=1 && property_type2=4,1,0) sort ';
			//$order_by = ' sort DESC,';
			//property_type=1&property_type_1=4
		}




		$limit = '';
		//$html['search'] = html_array('shop/search_ny', $post);
		$html['filter'] = html_array('shop/filter', $page);
		if ($post['view']=='map') {
			$where.= " AND sp.lat!=''";
			$limit = ' LIMIT 1000';
		}
		$query = "
	            SELECT sp.* ".$select." ,
		            neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
	            FROM shop_products AS sp
	            LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
	            WHERE sp.display = 1 AND sp.base=5 {$where}
	            ORDER BY {$order_by} sp.special DESC, sp.date DESC
	            $limit
	    	";
		//echo $query;
		if ($post['view']=='map') {
			$html['content'] = html_query('shop/product_map', $query, false, 60 * 60);
		}
		else {
			$html['content'] = html_query('shop/product_list shop', $query, false, 60 * 60);
			if (@$_GET['action'] == 'pagination') die($html['content']);
		}
		//echo mysql_error();

		$breadcrumb['page'][0] = array(
			$page['name'],
			'/' . $modules['shop_ny'] . '/'
		);
	}
}