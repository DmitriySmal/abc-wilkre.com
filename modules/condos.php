<?php

$html['critical'] = 'critical_shop';

$html['filter'] = '';
$html['is_product'] = false;
$html['is_map'] = false;
$where = $order_by = ' ';
$view_order = array('asc','desc');
$view_type = array('name','year');

$product_url = $u[4];
if ($u[1]==$modules['condos']) $error++;
elseif ($product_url) {
    $html['is_product'] = true;
    //$html['content'] = html_array('shop/product_text',$page);    
        $id = intval(explode2('-', $product_url));
        //запрос на товар и на категорию
        $product = mysql_select("
			SELECT sp.*,sn.name nb_name
			FROM shop_condos sp
 			LEFT JOIN shop_neighborhoods AS sn ON sn.id = sp.neighborhood
			WHERE sp.display = 1 AND sp.id = '" . $id . "'
			LIMIT 1
		", 'row');
        if ($product) {
	        $html['search'] = html_array('condos/search');
	        $breadcrumb['module'][] = array(
		        $product['name'],
		        $_SERVER['REQUEST_URI']
	        );
	        $breadcrumb['module'][] = array(
		        $product['nb_name'],
		        '/'.$modules['shop'].'/' . $modules['condos'] . '/?location[]='.$product['neighborhood']
	        );
	        $product['zip_code'] = trim($product['zip_code']);

            $page = array_merge($page, $product);
            $page['subname'] = $page['address'];
            //$page['parameters'] = $category['parameters'];
            //переадреация на корректный урл
            /*
            if (explode2('-', $u[3], 2) != $page['url'])
                die(header('location: /' . $modules['shop_products'] . '/' . $page['id'] . '-' . $page['url'] . '/', true, 301));
            */
	        $state = 'South Florida';
	        $page['title'] =  $product['name'].', '.@$product['address'].', '.$product['nb_name'].', '.$config['shop_cities'][$product['city']]['name'].' '.($product['zip_code']?', '.$product['zip_code']:'');
	        $page['title'].= ' - Wilk Real Estate I LLC';

	        $html['content'] = (!isset($_GET['print'])) ? html_array('condos/condo_text', $page) : html_array('condos/condo_print', $page);
            //$breadcrumb['page'] = array();
	        $html['critical'] = 'critical_object';
        }
        else
            $error++;    
}
else {
	//фильтр поиска
	$fields = array(
		'residential'=>'int',
		'quick' => 'text',
		'location' => 'array_int',
		'beds' => 'int',
		'baths' => 'int',
		'price_min' => ' int',
		'price_max' => ' int',
		'quick'=>'text',
		'view' =>'text'
	);
	$post = form_smart($fields, stripslashes_smart($_GET)); //print_r($post);

	$html['search'] = html_array('condos/search',$post);
    $html['filter'] = html_array('condos/filter',$page);

	$where = '';
	//количество кроватей, ванн, цена
	$where .= $post['beds'] ? " AND sp.beds_min <=" . $post['beds'] . " AND sp.beds_max >=" . $post['beds'] . "" : '';
	$where .= $post['baths'] ? " AND sp.baths_min <= " . $post['baths'] . " AND sp.baths_max <= " . $post['baths'] .  "" : '';
	if ($post['residential']==1) {
		if ($post['price_min']) $where .= " AND sp.price_min >= " . $post['price_min'];
		if ($post['price_max']) $where .= " AND sp.price_max <= " . $post['price_max'];
	}
	elseif ($post['residential']==2) {
		if ($post['price_min']) $where .= " AND sp.price_min2 >= " . $post['price_min'];
		if ($post['price_max']) $where .= " AND sp.price_max2 <= " . $post['price_max'];
	}
	else {
		if ($post['price_min']) $where .= " AND sp.price_min >= " . $post['price_min'];
		if ($post['price_max']) $where .= " AND sp.price_max2 <= " . $post['price_max'];
	}
	//neighborhoods
	$neighborhoods = false;
	if (count($post['location']) > 0) {
		$neighborhoods = mysql_select("SELECT id,name FROM shop_neighborhoods WHERE id IN (" . implode(',', $post['location']) . ")", 'array', 60 * 60);
		$where .= $neighborhoods ? " AND sp.neighborhood IN (" . implode(',', array_keys($neighborhoods)) . ") " : '';
		if (count($post['location'])==1) {
			$page['text'] = mysql_select("
				SELECT `text_condo` FROM shop_neighborhoods WHERE id IN (" . implode(',', $post['location']) . ")
			", 'string');
		}
	}
	//если не выбран location то показываем всю флориду
	if ($neighborhoods==false) $where.=" AND sp.city=6";

	//аренда или продажа
	if ($post['residential']) $where.=" AND sp.residential=".$post['residential'];

	//быстрый поиск
	if ($post['quick']) {
		$quick2 = mb_strtolower($post['quick']);
		$where.= ' AND (';
		$where .= " LOWER (sp.name) LIKE '%{$quick2}%' ";
		$where .= " OR LOWER (neighborhood.name) LIKE '%{$quick2}%' ";
		$where .= ')';
	}
    
    if (@$_GET['sort']){        
        $view = explode('-', $_GET['sort']);
        if (in_array($view[0], $view_type)){
            $order_by = " sp.{$view[0]} ".( (in_array($view[1], $view_order)) ? strtoupper($view[1]) : 'DESC' ).', ';
        }
    }
    if ($post['view']=='map') {
	    $where.= " AND sp.lat!=''";
	    $tpl = 'condos/condo_map';
    }
	elseif ($post['view']=='list') $tpl = 'condos/condo_listview shop';
	else $tpl = 'condos/condo_list shop';
    $html['content'] = html_query($tpl,"
                SELECT sp.*, neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
                FROM shop_condos AS sp 
                LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
                WHERE sp.display = 1 {$where} 
                ORDER BY sp.img DESC, {$order_by} sp.date DESC
        "); //TODO: убрать костыль с изображениями
    
    if(@$_GET['location'] && is_array($_GET['location'])){
        if ($nbs = mysql_select("SELECT sn.* FROM shop_neighborhoods AS sn WHERE sn.id IN (".implode(',',$_GET['location']).") LIMIT 1",'row'))
        {
            $page['name'] = $page['name'] ." in ".$nbs['name'];
        }
        else
        {
            $page['name'] = "All ". $page['name'];
        }
    }
    else
    {
        $page['name'] = "All ". $page['name'];
    }
    
    
    $breadcrumb['page'][0] = array(
        $page['name'], 
        '/'.$state['url'].'/' . $modules['condos'] . '/'.((@$u[2])?$u[2]:'')
    );
}

?>