<?php

$html['submenu'] = html_query('menu/list', "
	SELECT id,name,url,module,1 level,1 submenu
	FROM pages
	WHERE display=1 AND menu = 1 AND parent=".$page['id']."
	ORDER BY left_key
", '', 60 * 60, 'json');

//вложенные модули
$submodule = false;
if ($submenu = mysql_select("SELECT id,name,url,module,level,1 submenu
	FROM pages
	WHERE display=1 AND menu = 1 AND parent=".$page['id']."
	ORDER BY left_key",'rows',60 * 60))
{
	foreach ($submenu as $k=>$v) {
		if ($v['url']==$u[2]) {
			$page = mysql_select("SELECT * FROM pages WHERE id=".$v['id'],'row');
			$breadcrumb['page']	= array_merge(
				array(
					array(
						$page['name'],
						'/'.$u[1].'/'.$u[2].'/'
					)
				),
				$breadcrumb['page']
			);
			$submodule = $v['module'];
			include(ROOT_DIR.'modules/'.$submodule.'.php');
			break;
		}
	}
}

//если нет вложенного модуля
if ($submodule == false) {
	$html['is_product'] = false;
	$html['is_map'] = false;
	$where = $order_by = ' ';
	$view_order = array('asc', 'desc');
	$view_type = array('price');//,'date'

	$zips = array();
	$where = $join = '';

	$html['is_quick'] = $quick = mysql_real_escape_string(@$_GET['quick']);

	if ($u[4]) {
		//$html['content'] = html_array('shop/product_text',$page);
		$id = intval(explode2('-', $u[4]));
		//запрос на товар и на категорию
		$product = mysql_select("
			SELECT sp.*, sn.name nb_name
			FROM shop_products sp
			LEFT JOIN shop_neighborhoods AS sn ON sn.id = sp.neighborhood
			WHERE sp.display = 1 AND sp.id = '" . $id . "'
			LIMIT 1
		", 'row');
		if ($product) {

			if (@$_GET['action']=='pdf') {
				$html = clear_text(html_array('shop/product_pdf', $product));
				$html = iconv('UTF-8', "windows-1251//IGNORE", $html);
				require_once ROOT_DIR . "plugins/MPDF56/mpdf.php"; // Подключаем класс рендера
				$mpdf = new mPDF('utf-8', 'A4', '8', '', 5, 0, 5, 0, 10, 10); // Создаем экземпляр класса
				$mpdf->charset_in = 'cp1251'; // Входная кодировка html
				//$css = file_get_contents(ROOT_DIR . 'templates/css/print.css');// Получаем css файл
				//$mpdf->WriteHTML($css, 1);// Добавляем наш css в поток
				$mpdf->WriteHTML($html, 2); // В переменной $html хранится наш html
				$mpdf->Output('mpdf.pdf', 'I'); // Вывод нашего pdf
				//$file = ROOT_DIR.'files/shop_products/'.$product['id'].'/'.$product['id'].'.pdf';
				//$mpdf->Output($file, 'F'); // сохранение нашего pdf
				die();
			}

			//$breadcrumb['page'] = array();
			$breadcrumb['module'][] = array(
				$product['name'],
				$_SERVER['REQUEST_URI']
			);
			$breadcrumb['module'][] = array(
				$product['nb_name'],
				'/' . $modules['shop'] . '/?property_type=' . $product['property_type'].'&location[]=' . $product['neighborhood']
			);
			$breadcrumb['module'][] = array(
				$config['object_groups'][$product['property_type']]['name'],
				'/' . $modules['shop'] . '/?property_type=' . $product['property_type']
			);

			$html['is_product'] = true;
			$html['search'] = html_array('shop/search');

			unset($product['bimg']);
			$page = array_merge($page, $product);

			//формировка title
			$page['title'] = $page['name'];
			$page['title'].= ', ';
			//property_type
			if (isset($config['object_groups'][$page['property_type']])) {
				$page['title'].= $config['object_groups'][$page['property_type']]['name'];
				if ($page['property_type2'] AND isset($config['object_groups_'.$page['property_type']])) {
					if (in_array($page['property_type2'],$config['object_groups_'.$page['property_type']])) {
						$page['title'].= ' '.$page['property_type2'];
					}
				}
			}
			else {
				$page['title'].= 'Real Estate Properties';
			}
			//location
			$page['title'].= ' in ';
			$page['title'].= $page['nb_name'];
			$page['title'].= ' - Wilk Real Estate I LLC';

			$html['content'] = (!isset($_GET['print'])) ? html_array('shop/product_text', $page) : html_array('shop/product_print', $page);
		}
		else $error++;
	}
	elseif ($u[2] AND strpos($u[2],'=')==false) $error++;
	else {
		if (isset($_GET['property_type'])) $_GET['group'] = $_GET['property_type'];
		//фильтр поиска
		$fields = array(
			'residential'=>'int',
			'property_type' => 'int',
			'property_type2'=>'text',
			'quick' => 'text',
			'location' => 'array_int',
			'beds' => 'text',
			'baths' => 'int',
			'price_min' => 'price',
			'price_max' => 'price',
			'quick'=>'text',
			'view'=>'text',
			'source'=>'text'
		);
		if ($u[2]) {
			$post = form_smart($fields, form_url($u[2]));
		}
		else $post = form_smart($fields, stripslashes_smart($_GET));
		$where = $select = '';
		if ($post['source']=='subscribe') {
			$html['source'] = 'subscribe';
			$today = date('Y-m-d');
			$where.= " AND sp.date_change + interval 5 day > '".$today."'";
		}

		//property_type
		if ($post['property_type']) {
			$where .= " AND sp.property_type = ".$post['property_type'];
			//property_type2
			if ($post['property_type2']) {
				$where .= " AND sp.property_type2 LIKE '%" . mysql_res(mb_strtolower($post['property_type2'])) . "%'";
			}

		}

		//количество кроватей, ванн, цена
		if ($post['beds']=='studio') $where .= " AND sp.beds = 0 ";
		else {
			//$post['beds'] = intval($post['beds']);
			if($post['beds'] == 5) $where .= " AND sp.beds >=5 ";
			elseif ($post['beds']) $where .= " AND sp.beds = " . intval($post['beds']);
		}
		$where .= $post['baths'] ? " AND sp.baths = " . $post['baths'] . " " : '';
		if ($post['price_min']) $where .= " AND sp.price >= " . $post['price_min'];
		if ($post['price_max']) $where .= " AND sp.price <= " . $post['price_max'];

		//neighborhoods
		$neighborhoods = false;
		if (count($post['location']) > 0) {
			$neighborhoods = mysql_select("SELECT id,name FROM shop_neighborhoods WHERE id IN (" . implode(',', $post['location']) . ")", 'array', 60 * 60);
			$where .= $neighborhoods ? " AND sp.neighborhood IN (" . implode(',', array_keys($neighborhoods)) . ") " : '';
		}
		if (count($neighborhoods)==1) {
			$page['text'] = mysql_select("SELECT text FROM shop_neighborhoods WHERE id=".key($neighborhoods),'string');
		}
		//если не выбран location то показываем всю флориду
		//if ($neighborhoods==false) $where.=" AND sp.city=6";

		//сортировка только по цене
		if (@$_GET['sort']) {
			$view = explode('-', $_GET['sort']);
			if (in_array($view[0], $view_type)) {
				$order_by = " sp.{$view[0]} " . ((in_array($view[1], $view_order)) ? strtoupper($view[1]) : 'DESC') . ', ';
			}
		}

		//аренда или продажа
		if ($post['residential']) $where.=" AND sp.residential=".$post['residential'];

		//быстрый поиск
		if ($quick) {
			$quick2 = mb_strtolower($quick);
			$quick3 = mb_strtolower(name_search($quick));
			$where.= ' AND (';
			$where .= " LOWER (sp.name) LIKE '%{$quick3}%' ";
			//$where .= " OR LOWER (sp.address) LIKE '%{$quick2}%' ";
			$where .= " OR LOWER (sp.mln) LIKE '%{$quick2}%' ";
			$where .= " OR LOWER (sp.zip_code) LIKE '{$quick2}' ";
			$where .= " OR LOWER (sp.complex_name) LIKE '%{$quick2}%' ";
			$where .= " OR LOWER (sp.development_name) LIKE '%{$quick2}%' ";
			$where .= " OR LOWER (neighborhood.name) LIKE '%{$quick2}%' ";
			$where .= " OR LOWER (neighborhood.name2) LIKE '%{$quick2}%' ";
			$where .= ')';
		}

		//формировка H1
		if ($quick) {
			$page['name'] = "Search '{$quick}' in " . $state['name'];
			$page['subname'] = '';
		}
		else {
			$page['name'] = '';
			if ($post['beds']) {
				if ($post['beds'] == 'studio') $page['name'] .= $post['beds'];
				else $page['name'] .= $post['beds'] . ' bedrooms';
			}
			if ($post['baths']) {
				$page['name'] .= $page['name'] ? ' and ' : '';
				$page['name'] .= $post['beds'] . ' baths';
			}
			if ($post['price_min']) {
				$page['name'] .= ' from ' . $post['price_min'] . '$';
			}
			if ($post['price_max']) {
				$page['name'] .= ' under ' . $post['price_max'] . '$';
			}

			if ($post['property_type']AND isset($config['object_groups'][$post['property_type']])) {
				$page['name'] .= $page['name'] ? ' ' : '';
				$page['name'] .= $config['object_groups'][$post['property_type']]['name'];
			}
			if ($neighborhoods) {
				$page['subname'] = 'in ' . implode(', ', $neighborhoods);
			}
		}
		if ($page['name'] == '') {
			if ($page['subname'] == '') $page['name'] = 'Featured Properties';
			else $page['name'] = 'All Properties';
		}

		//формировка title
		$page['title'] = '';
		//residential
		if (isset($config['object_residential'][$post['residential']])) {
			if ($post['residential']==1) $page['title'] = 'Rent';
			else $page['title'] = 'Sale';
		}
		else {
			$page['title'].= 'Sale and Rental';
		}
		//property_type
		$page['title'].= ' of ';
		if (isset($config['object_groups'][$post['property_type']])) {
			$page['title'].= $config['object_groups'][$post['property_type']]['name'];
			if ($post['property_type2'] AND isset($config['object_groups_'.$post['property_type']])) {
				if (in_array($post['property_type2'],$config['object_groups_'.$post['property_type']])) {
					$page['title'].= ' '.$post['property_type2'];
				}
			}
		}
		else {
			$page['title'].= 'Real Estate Properties';
		}
		//beds or baths
		if ($post['beds'] OR $post['baths']) {
			$page['title'].= ' with ';
			if ($post['beds']) {
				if ($post['beds']=='studio') $page['title'].= ' Studio ';
				else $page['title'].= $post['beds'].' Bedrooms ';
			}
			if ($post['baths']) {
				if ($post['beds']) $page['title'].= '/ ';
				$page['title'].= $post['baths'].' Baths ';
			}
		}
		//location
		$page['title'].= ' in ';
		$page['title'].= ' South Florida ';
		if ($neighborhoods) $page['title'].= implode(' / ',$neighborhoods);
		//price
		if ($post['price_min'] OR $post['price_max']) {
			$page['title'] = trim($page['title']);
			$page['title'].= '. Priced ';
			if ($post['price_min']) $page['title'].= ' from $'.number_format($post['price_min'],0,'.',',');
			if ($post['price_max']) $page['title'].= ' to $'.number_format($post['price_max'],0,'.',',');
		}
		$page['title'].= ' - Wilk Real Estate I LLC';

		//если без гет параметров то только с галками
		if ($where == '') {
			$where.= " AND sp.city=6";
			$where.= ' AND sp.special=1';
			$select = ' IF (property_type=6,1,property_type) sort, ';
			$order_by = ' sort, property_type DESC, sp.date_change DESC, ';
		}


		// если есть, то получаем данные для СЕО категория / район
		/*
		if (isset($_GET['location']) && is_array($_GET['location']) && (count($_GET['location']) == 1)) {
			$location_id = intval($_GET['location'][0]);
			if ($category) // если есть категория, ищем связку категория - район
				$seo_text = mysql_select("SELECT s.name, s.text, s.title, s.keywords, s.description FROM shop_neighborhoods_seo AS s WHERE s.category = {$category['id']} AND s.neighborhood = {$location_id} AND s.display = 1 LIMIT 1", 'row');
			else // если нет, берем данные из района
				$seo_text = mysql_select("SELECT s.name, s.text, s.title, s.keywords, s.description FROM shop_neighborhoods AS s WHERE s.id = {$location_id} AND s.display = 1 LIMIT 1", 'row');
			if ($seo_text) {
				$page = array_merge($page, $seo_text);
			}
		}*/


		$limit = ''; //print_R($post);
		if (@$html['source']!= 'subscribe') $html['search'] = html_array('shop/search', $post);
		$html['filter'] = html_array('shop/filter', $page);
		if ($post['view']=='map') {
			$where.= " AND sp.lat!=''";
			$limit = ' LIMIT 1000';
		}
		$query = "
	            SELECT sp.*, $select
		            neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
	            FROM shop_products AS sp
	            LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
	            WHERE sp.display = 1 AND ( (sp.base=2 AND sp.status = 'A') OR sp.base=1) {$where}
	            ORDER BY {$order_by} sp.special DESC, sp.date_change DESC
	            $limit
	    	";
		//echo $query;
		if ($post['view']=='map') {
			$html['content'] = html_query('shop/product_map', $query, false, 60 * 60);
		}
		else {
			$html['content'] = html_query('shop/product_list shop', $query, false, 60 * 60);
			if (@$_GET['action'] == 'pagination') die($html['content']);
		}
		//echo mysql_error();

		$breadcrumb['page'][0] = array(
			$page['name'],
			'/' . $state['url'] . '/' . $modules['shop'] . '/'
		);
	}
}