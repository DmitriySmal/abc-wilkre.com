<?php

$base = $u[1]==$modules['shop'] ? 1:2;

$html['filter'] = '';
$html['is_product'] = false;
$html['is_map'] = false;
$where = $order_by = ' ';
$view_order = array('asc','desc');
$view_type = array('name','year');

$product_url = $u[4];
if ($product_url) {
	$html['is_product'] = true;
	//$html['content'] = html_array('shop/product_text',$page);
	$id = intval(explode2('-', $product_url));
	//запрос на товар и на категорию
	$product = mysql_select("
			SELECT sp.*,sn.name nb_name
			FROM shop_preconstructions sp
 			LEFT JOIN shop_neighborhoods AS sn ON sn.id = sp.neighborhood
			WHERE sp.base=".$base." AND sp.display = 1 AND sp.id = '" . $id . "'
			LIMIT 1
		", 'row');
	if ($product) {
		$html['search'] = html_array('preconstructions/search');
		$breadcrumb['module'][] = array(
			$product['name'],
			$_SERVER['REQUEST_URI']
		);
		$breadcrumb['module'][] = array(
			$product['nb_name'],
			'/'.$u[1].'/' . $u[2] . '/?location[]='.$product['neighborhood']
		);

		$page = array_merge($page, $product);
		$page['subname'] = $page['address'];
		//$page['parameters'] = $category['parameters'];
		//переадреация на корректный урл
		/*
		if (explode2('-', $u[3], 2) != $page['url'])
			die(header('location: /' . $modules['shop_products'] . '/' . $page['id'] . '-' . $page['url'] . '/', true, 301));
		*/
        $state = $base==1 ? 'FL':'NY';
        $page['title'] =  $product['name'].', '.@$product['address'].', '.$product['nb_name'].', '.$config['shop_cities'][$product['city']]['name'].', '.$state.($product['zip_code']?', '.$product['zip_code']:'');
        $page['title'].= ' - Wilk Real Estate I LLC';
        $html['content'] = (!isset($_GET['print'])) ? html_array('preconstructions/preconstruction_text', $page) : html_array('preconstructions/preconstruction_print', $page);
		//$breadcrumb['page'] = array();

	}
	else
		$error++;
}
else {
	//фильтр поиска
	$fields = array(
		'quick' => 'text',
		'location' => 'array_int',
		'view' =>'text'
	);
	$post = form_smart($fields, stripslashes_smart($_GET)); //print_r($post);

	$html['search'] = html_array('preconstructions/search',$post);
	$html['filter'] = html_array('preconstructions/filter',$page);

	$where = '';

	//neighborhoods
	$neighborhoods = false;
	if (count($post['location']) > 0) {
		$neighborhoods = mysql_select("SELECT id,name FROM shop_neighborhoods WHERE id IN (" . implode(',', $post['location']) . ")", 'array', 60 * 60);
		$where .= $neighborhoods ? " AND sp.neighborhood IN (" . implode(',', array_keys($neighborhoods)) . ") " : '';
		if (count($post['location'])==1) {
			$page['text'] = mysql_select("
				SELECT `text_pre-constraction` FROM shop_neighborhoods WHERE id IN (" . implode(',', $post['location']) . ")
			", 'string');
		}
	}
	//если не выбран location то показываем всю флориду
	//if ($neighborhoods==false) $where.=" AND sp.city=6";

	//быстрый поиск
	if ($post['quick']) {
		$quick2 = mb_strtolower($post['quick']);
		$where = ' AND (';
		$where .= " LOWER (sp.name) LIKE '%{$quick2}%' ";
		$where .= " OR LOWER (neighborhood.name) LIKE '%{$quick2}%' ";
		$where .= ')';
	}

	if (@$_GET['sort']){
		$view = explode('-', $_GET['sort']);
		if (in_array($view[0], $view_type)){
			$order_by = " sp.{$view[0]} ".( (in_array($view[1], $view_order)) ? strtoupper($view[1]) : 'DESC' ).', ';
		}
	}

	if ($post['view']=='map') {
		$where.= " AND sp.lat!=''";
		$tpl = 'preconstructions/preconstruction_map';
	}
	else $tpl = 'preconstructions/preconstruction_list shop';
	$query = "
                SELECT sp.*, neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
                FROM shop_preconstructions AS sp
                LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
                WHERE sp.base=".$base." AND sp.display = 1 {$where}
                ORDER BY sp.img DESC, {$order_by} sp.date DESC
        "; //echo $query;
	$html['content'] = html_query($tpl,$query); //TODO: убрать костыль с изображениями

	if(@$_GET['location'] && is_array($_GET['location'])){
		if ($nbs = mysql_select("SELECT sn.* FROM shop_neighborhoods AS sn WHERE sn.id IN (".implode(',',$_GET['location']).") LIMIT 1",'row'))
		{
			$page['name'] = $page['name'] ." in ".$nbs['name'];
		}
		else
		{
			$page['name'] = "All ". $page['name'];
		}
	}
	else
	{
		$page['name'] = "All ". $page['name'];
	}


	$breadcrumb['page'][0] = array(
		$page['name'],
		'/'.$u[1].'/' . $u[2] . '/'
	);
}

?>