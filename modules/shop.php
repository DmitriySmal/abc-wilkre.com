<?php

//print_R($u);

$html['critical'] = 'critical_shop';

if (!isset($config['_api'])) {
	//FL
	if (!isset($base)) {
		$base = 2;
		$module_url = $modules['shop'];
		header('location: /'.$modules['shop_flmx'].'/');
		die();
	}
	$config['state'] = $base;
	//NY неактивный
	if ($base == 1) {
		include(ROOT_DIR . '_config_ny.php');
		$html['module'] = 'shop';
		$module_url = $modules['shop_ny'];
	}
	//матрикс - ньюйорk
	if ($base == 6) {
		include(ROOT_DIR . '_config_ny.php');
		$html['module'] = 'shop';
		$module_url = $modules['shop_mx'];
		$config['state'] = 1;
	}
	//матрикс - ньюйорk - свои
	if ($base == 7) {
		include(ROOT_DIR . '_config_ny.php');
		$html['module'] = 'shop';
		$module_url = $modules['shop_mx'];
		$config['state'] = 1;
	}
	//неактивный
	if ($base == 8) {
		$html['module'] = 'shop';
		$module_url = $modules['shop_flex'];
		$config['state'] = 2;
	}
	//матрикс флорида
	if ($base == 9) {
		$html['module'] = 'shop';
		$module_url = $modules['shop_flmx'];
		$config['state'] = 2;
	}
	//bridge флорида
	if ($base == 10) {
		$html['module'] = 'shop';
		$module_url = $modules['shop_mls10'];
		$config['state'] = 2;
	}
	//флорида свои объекты
	if ($base == 11) {
		$html['module'] = 'shop';
		$module_url = $modules['shop_mls10'];
		$config['state'] = 2;
	}
	if (!isset($module_url)) {
		$module_url = $modules['shop'];
	}
	$page['base'] = $base;
}

$submodule = false;
if (!isset($config['_api'])) {
	$html['submenu'] = html_query('menu/list', "
		SELECT id,name,url,module,1 level,1 submenu
		FROM pages
		WHERE display=1 AND menu = 1 AND parent=" . $page['id'] . "
		ORDER BY left_key
	", '', 60 * 60, 'json');

	//вложенные модули

	if ($submenu = mysql_select("SELECT id,name,url,module,level,1 submenu
		FROM pages
		WHERE display=1 AND menu = 1 AND parent=" . $page['id'] . "
		ORDER BY left_key", 'rows', 60 * 60)
	) {
		foreach ($submenu as $k => $v) {
			if ($v['url'] == $u[2] AND $v['url']!='for-sale' AND $v['url']!='for-rent') {
				$page = mysql_select("SELECT * FROM pages WHERE id=" . $v['id'], 'row');
				$breadcrumb['page'] = array_merge(
					array(
						array(
							$page['name'],
							'/' . $u[1] . '/' . $u[2] . '/'
						)
					),
					$breadcrumb['page']
				);
				$submodule = $v['module'];
				include(ROOT_DIR . 'modules/' . $submodule . '.php');
				break;
			}
		}
	}
}

//если нет вложенного модуля
if ($submodule == false) {
	$html['is_product'] = false;
	$html['is_map'] = false;
	$where = $order_by = ' ';
	$view_order = array('asc', 'desc');
	$view_type = array('price');//,'date'

	$zips = array();
	$where = $join = '';

	$html['is_quick'] = $quick = mysql_res(@$_GET['quick']);
	//редиректы со старого урл
	//[4.629] Сделать по другому URL для объектов недвижимости
	if ($u[4]) {
		$id = intval(explode2('-', $u[4]));
		if ($id) {
			if ($product = mysql_select("
				SELECT sp.*, sn.name nb_name, sn.url nb_url
				FROM shop_products sp
				LEFT JOIN shop_neighborhoods AS sn ON sn.id = sp.neighborhood
				WHERE sp.display = 1 AND sp.id = '" . $id . "'
				LIMIT 1
			", 'row')) {
				header('location: '.get_url('product',$product));
				die();
			}
		}
	}
	//print_R($u);


	$new_url = @$new_url ?  $new_url : false;
	$u2 = explode('-', $u[2]);
	$id = intval(array_pop($u2));
	if ($id>0) $new_url = true;

	//[4.629] Сделать по другому URL для объектов недвижимости
	if ($u[5] OR (isset($product) AND $product) OR $new_url) {

		//$html['content'] = html_array('shop/product_text',$page);
		if ($new_url==false) {
			$u5 = explode('-', $u[5]);
			$id = array_pop($u5);
		}
		//echo $id; die();
		//запрос на товар и на категорию
		$product = (isset($product) AND $product) ? $product :  mysql_select("
			SELECT sp.*, sn.name nb_name, sn.url nb_url, sc.name city_name
			FROM shop_products sp
			LEFT JOIN shop_neighborhoods AS sn ON sn.id = sp.neighborhood
			LEFT JOIN shop_cities AS sc ON sc.id = sp.city
			WHERE sp.display = 1 AND sp.id = '" . $id . "'
			LIMIT 1
		", 'row');



		if ($product) {
			if (access('user admin')) {
				if (@$_GET['to_rss']) {
					$product['date_rss'] = '';
					mysql_fn('update','shop_products',array(
						'id'=>$product['id'],
						'date_rss'=>$product['date_rss']
					));
				}
				//die('234');
				//dd($html);
				//dd($product);
				//die();
			}
			if (!isset($product['neighborhood_name'])) {
				$neighborhood = get_data('shop_neighborhoods',$product['neighborhood']);
				$product['neighborhood_name'] = $neighborhood['name'];
			}
			if (!isset($product['city_name'])) {
				$product['city_name'] = mysql_select("
					SELECT name FROM shop_cities WHERE id=".$product['city'],'string');
			}
			$page['base'] = $product['base'];
			$product_url = get_url('product', $product);
			$html['canonical'] = $product_url;
			if ($u[2] AND $u[2]!='mailchimp' AND $u[1]!='property-detail') {
				//if (access('user admin')) die('123');
				header('location: '.$product_url,true,301);
				die();
			}
			//урл 5 базы состоит из 1 уровня урл только
			/*if ($product['base']!=5 AND $new_url==false) {
				//echo 2;
				if ('/' . $u[1] . '/' . $u[2] . '/' . $u[3] . '/' . $u[4] . '/' . $u[5] . '/' != $product_url) {
					//echo $product_url;
					header('location: ' . $product_url);
					die();
				}
			}*/
			//print_R($product);
			if (@$_GET['action']=='pdf') {
				$html = clear_text(html_array('shop/product_pdf', $product));
				$html = iconv('UTF-8', "windows-1251//IGNORE", $html);
				require_once ROOT_DIR . "plugins/MPDF56/mpdf.php"; // Подключаем класс рендера
				$mpdf = new mPDF('utf-8', 'A4', '8', '', 5, 0, 5, 0, 10, 10); // Создаем экземпляр класса
				$mpdf->charset_in = 'cp1251'; // Входная кодировка html
				//$css = file_get_contents(ROOT_DIR . 'templates/css/print.css');// Получаем css файл
				//$mpdf->WriteHTML($css, 1);// Добавляем наш css в поток
				$mpdf->WriteHTML($html, 2); // В переменной $html хранится наш html
				$mpdf->Output('mpdf.pdf', 'I'); // Вывод нашего pdf
				//$file = ROOT_DIR.'files/shop_products/'.$product['id'].'/'.$product['id'].'.pdf';
				//$mpdf->Output($file, 'F'); // сохранение нашего pdf
				die();
			}

			//$breadcrumb['page'] = array();
			//[4.629] Сделать по другому URL для объектов недвижимости
			$breadcrumb['module'][] = array(
				object_name($product),
				$_SERVER['REQUEST_URI']
			);
			$bc = explode('/',$product_url);
			if ($product['base']!=5 AND $new_url==false) {
				$breadcrumb['module'][] = array(
					$product['city_name'],
					'/' . $bc[1] . '/' . $bc[2] . '/' . $bc[3] . '/' . $bc[4] . '/'
				);
				$breadcrumb['module'][] = array(
					in_array($product['base'], array(1, 2, 8)) ? $config['object_groups'][$product['property_type']]['name'] : $config['ny_property_types'][$product['property_type']],
					'/' . $bc[1] . '/' . $bc[2] . '/' . $bc[3] . '/'
				);
				$breadcrumb['module'][] = array(
					$product['residential'] == 1 ? 'For rent' : 'For sale',
					'/' . $bc[1] . '/' . $bc[2] . '/'
				);
			}
			elseif ($new_url==true) {
				$breadcrumb['module'][] = array(
					!in_array($product['base'],array(9,10,11))?'New York':'Florida',
					'/' . $module_url . '/'
				);
			}

			/*
			$breadcrumb['module'][] = array(
				$product['nb_name'],
				'/' . $module_url . '/?property_type=' . $product['property_type'].'&location[]=' . $product['neighborhood']
			);
			if ($base==2) {
				$breadcrumb['module'][] = array(
					$config['object_groups'][$product['property_type']]['name'],
					'/' . $module_url . '/?property_type=' . $product['property_type']
				);
			}
			else {
				$breadcrumb['module'][] = array(
					$config['ny_property_types'][$product['property_type']],
					'/' . $modules['shop_ny'] . '/?property_type=' . $product['property_type']
				);
			}*/

			$html['is_product'] = true;
			$html['search'] = html_array('shop/search');

			unset($product['bimg']);
			$page = array_merge($page, $product);
			//формировка title
			//$page['title'] = seo_title($page);
			$page['title'] = object_name($page);

			$html['critical'] = 'critical_object';

			if (access('user admin') AND @$_GET['action'] == 'template') {
				die(html_array('shop/product_mailchimp', $page));
			}

			if (@$_GET['mailchimp']==1) {
				if (access('user admin')) {
					$html['content'] = html_array('shop/mailchimp',$page);
				}
				else $error++;
			}
			else {
				if (!isset($_GET['print'])) {
					$html['content'] = html_array('shop/product_text', $page);
				}
				else {
					//$page['noindex'] = 1;
					$html['content'] = html_array('shop/product_print', $page);
				}
			}

			$page['description'] = 'For more information click on the URL '.$page['description'];

			//канонический урл
            $request_canonical = explode('?',$_SERVER['REQUEST_URI']);
            $html['canonical'] = $request_canonical[0];
            //dd($html['canonical']);

		}
		else {
			//[4.438] Исправить ошибки страниц (гугл отлавливает огромное количество страниц с 404 ошибкой)
			//делаем 301 редирект на страницу каталога
			header('HTTP/1.1 301 Moved Permanently');
			//NY
			$post = array();
			if ($base==1) {
				$post['property_type'] = array_search($u[2],$config['ny_property_types']);

			}
			if ($base==2) {
				$post['property_type'] = array_search($u[2],$config['sf_property_type_url']);
			}
			if ($neighborhood = mysql_select("SELECT id FROM shop_neighborhoods WHERE url='".mysql_res($u[3])."'",'string')) {
				$post['location'][] = $neighborhood;
			}
			$url = http_build_query($post);
			die(header('location:/'.$module_url.'/?'.$url));
			//$error++;
		}
	}
	//elseif ($u[3]) $error++;
	else {
		//if (isset($_GET['property_type'])) $_GET['group'] = $_GET['property_type'];
		//фильтр поиска
		$fields = array(
			'residential'=>'int',
			'property_type' => 'int',
			'property_type2'=>'int',
			'quick' => 'text',
			'city'=>'int',
			'location' => 'array_int',
			'beds' => 'text',
			'baths' => 'int',
			'price_min' => 'price',
			'price_max' => 'price',
			'view'=>'text',
			'source'=>'text'
		);
		//print_r($u);
		if ($u[2]) {
			//[4.629] Сделать по другому URL для объектов недвижимости
			$post = form_smart($fields, stripslashes_smart($_GET));
			if ($u[2]=='for-sale') $post['residential'] = 2;
			elseif ($u[2]=='for-rent') $post['residential'] = 1;
			else $error++;

			if ($error==0 AND $u[3]) {
				$property_type = explode('-', $u[3],2);
				//print_r($property_type);
				$post['property_type'] = 0;
				if ($u[1]==$modules['shop']) {
					foreach ($config['object_groups'] as $k=>$v) {
						if (trunslit($v['url'])==$property_type[0]) $post['property_type'] = $k;
					}
				}
				else {
					foreach ($config['ny_property_types'] as $k=>$v) {
						if (trunslit($v)==$property_type[0]) $post['property_type'] = $k;
					}
				}
				if ($post['property_type']) {
					if ($property_type[1]) {
						$property_type2 = preg_replace('~[^a-z]+~u', '', $property_type[1]);
						$post['property_type2'] = mysql_select("SELECT id FROM shop_property_types2 WHERE LOWER(url) = '".mysql_res($property_type2)."'",'string');
						if ($post['property_type2']==0) $error++;
					}
				}
				else $error++;
			}

			if ($error==0 AND $u[4]) {
				$post['city'] = mysql_select("SELECT id FROM shop_cities WHERE url='".mysql_res($u[4])."'",'string');
			}

			//$post = form_smart($fields, form_url($u[2]));
			/*
			$data = explode('-',$u[2]);
			//print_r($data);
			if (count($data)==3 OR count($data)==4) {
				if (in_array($data[0],$config['object_residential_url'])) {
					$post['residential'] = array_search($data[0],$config['object_residential_url']);
					if ($post['location'][] = mysql_select("
							SELECT id FROM shop_neighborhoods WHERE url2='".mysql_res($data[1])."'
						",'string')) {
						if (
							($base==1 AND in_array($data[2],$config['ny_property_types']))
							OR
							($base==2 AND in_array($data[2],$config['sf_property_type_url']))
						) {
							if ($base==1) {
								$post['property_type'] = array_search($data[2],$config['ny_property_types']);
								if ($data[3]) {
									if (array_key_exists($data[3],$config['ny_property_types_1_url'])) {
										$post['property_type2'] = $config['ny_property_types_1_url'][$data[3]];
									}
									else {
										//echo 4;
										$error++;
									}
								}
							}
							else $post['property_type'] = array_search($data[2],$config['sf_property_type_url']);
						}
						else {
							//echo 3;
							$error++;
						}
					}
					else {
						//echo 2;
						$error++;
					}
				}
				else {
					$error++;
					//echo 1;
				}

			}
			else {
				$error++;
				//echo '0'.count($data);
			}
			*/
		}
		else {
			if (@$seo_shop) {
				//пост указан в index.php
			}
			else $post = form_smart($fields, stripslashes_smart($_GET));
		}
		//print_R($post); //echo 1; die('2');

		$random = random_hash(@$post);
		//if ($base==2) $post['city'] = 6;

		$where = $select = $join = '';

		//neighborhoods
		if ($post['city']) $where .= " AND sp.city = ".$post['city'];
		$neighborhoods = $zip_codes = false;
		if (count($post['location']) > 0) {
			$neighborhoods2 = mysql_select("SELECT id,name,zip_codes FROM shop_neighborhoods WHERE id IN (" . implode(',', $post['location']) . ")", 'rows_id');
			//$where .= $neighborhoods2 ? " AND sp.neighborhood IN (" . implode(',', array_keys($neighborhoods2)) . ") " : '';
			if ($neighborhoods2) foreach ($neighborhoods2 as $k=>$v) {
				$neighborhoods[$k] = $v['name'];
				$zip_codes.= ' '.$v['zip_codes'];
			}
			if ($zip_codes) {
				$zip_codes = trim($zip_codes);
				$zip_codes = str_replace(' ',',',$zip_codes);
				$zip_codes = str_replace(',,',',',$zip_codes);
				$zip_codes = str_replace(',,',',',$zip_codes);
				//$zip_codes = str_replace(',','|',$zip_codes);
			}
			if ($zip_codes) {
				$where.= " AND (sp.neighborhood IN (" . implode(',', array_keys($neighborhoods2)).") ";
				$zip_codes = explode(',',$zip_codes);
				foreach ($zip_codes as $k=>$v) {
					$where.= " OR sp.zip_code LIKE '".intval($v)."' ";
				}
				$where.= " ) ";
			}
			else $where .= $neighborhoods2 ? " AND sp.neighborhood IN (" . implode(',', array_keys($neighborhoods2)) . ") " : '';
		}

		//property_type
		if ($post['property_type']) {
			$where .= " AND sp.property_type = ".$post['property_type'];
			//property_type2
			if ($post['property_type2']) {
				if ($base==9) {
					$property_type2 = mysql_select("SELECT name FROM shop_property_types WHERE base=9 AND parent=".$post['property_type']." AND uid=" . $post['property_type2'], 'string');
					$where .= " AND FIND_IN_SET(".$post['property_type2'].",sp.property_type2) ";
				}
				elseif ($base==10 OR $base==11) {
					$property_type2 = mysql_select("SELECT name FROM shop_property_types WHERE base=10 AND parent=".$post['property_type']." AND uid=" . $post['property_type2'], 'string');
					$where .= " AND FIND_IN_SET(".$post['property_type2'].",sp.property_type2) ";
				}
				else {
					//$post['property_type2'] = str_replace(array('%2F','&amp;'),array('/','&'),$post['property_type2']);
					if ($property_type2 = mysql_select("SELECT name FROM shop_property_types2 WHERE id=" . $post['property_type2'], 'string')) {
						$where .= " AND sp.property_type2 LIKE '%" . mysql_res(mb_strtolower($property_type2)) . "%'";
					}
				}
			}

		}

		//количество кроватей, ванн, цена
		if ($post['beds']=='studio') $where .= " AND sp.beds = 0 ";
		else {
			//$post['beds'] = intval($post['beds']);
			if($post['beds'] == 5) $where .= " AND sp.beds >=5 ";
			//[4.588] Дим, нужно сделать так, чтобы когда выбираешь beds или baths, то результаты были от, а не точные
			elseif ($post['beds']) $where .= " AND sp.beds >= " . intval($post['beds']);
		}
		$where .= $post['baths'] ? " AND sp.baths >= " . $post['baths'] . " " : '';
		if ($post['price_min']) $where .= " AND sp.price >= " . $post['price_min'];
		if ($post['price_max']) $where .= " AND sp.price <= " . $post['price_max'];

		if (@$seo_shop==false) $page['text'] = '';
		//если в урл только район то показываем текст для района
		if (!isset($config['_api']) AND @$seo_shop==false) {
			if (count($neighborhoods) == 1) {
				$i = 0;
				foreach ($fields as $k => $v) {
					if (!in_array($k, array('location', 'city', 'source', 'view'))) {
						if ($post[$k]) $i++;
					}
				}
				if ($i == 0) {
					if ($neighborhoods) {
						$page['text'] = mysql_select("SELECT text FROM shop_neighborhoods WHERE id=" . key($neighborhoods), 'string');
					}
				}
			}

			//если не выбран location то показываем всю флориду
			//if ($neighborhoods==false) $where.=" AND sp.city=6";

			//сортировка только по цене
			if (@$_GET['sort']) {
				$view = explode('-', $_GET['sort']);
				if (in_array($view[0], $view_type)) {
					$order_by = " sp.{$view[0]} " . ((in_array($view[1], $view_order)) ? strtoupper($view[1]) : 'DESC') . ', ';
				}
			}
		}
		//аренда или продажа
		if ($post['residential']) $where.=" AND sp.residential=".$post['residential'];

		//быстрый поиск
		if ($quick) {
			$quick2 = mb_strtolower($quick);
			$quick3 = mb_strtolower(name_search($quick));
			$int = preg_replace('~[^0-9]+~u','', $quick2);//делаем только цифры
			//если ввели только цифры то ищем по зип и по млн
			/*if ($int==$quick2) {
				$where.= ' AND (';
				$where .= " LOWER (sp.zip_code) LIKE '{$quick2}' ";
				$where .= " OR LOWER (sp.mln) LIKE '%{$quick2}%' ";
				$where .= " OR LOWER (sp.uid) LIKE '%{$quick2}%' ";
				$where .= ')';
			}*/
			//поиск по WR и WRS
			if ('wr'.$int==$quick2 OR 'wrs'.$int==$quick2) {
				$where.= ' AND (';
				$where .= "  sp.uid = '{$int}' ";
				$where .= ')';
			}
			//поиск по словам
			else {
				//[4.789] Ошибка после глюка
				$nbds = mysql_select("
					SELECT id,name FROM shop_neighborhoods
					WHERE LOWER (name) LIKE '{$quick2}' AND display=1
				", 'array');
				if ($nbds AND count($nbds)==1) {
					foreach ($nbds as $k=>$v) {
						$where.= " AND sp.neighborhood = ".$k;
					}
				}
				else {
					$where .= ' AND (';
					$where .= " LOWER (sp.name) LIKE '%{$quick3}%' ";
					if ($base == 2) {
						$where .= " OR LOWER (sp.complex_name) LIKE '%{$quick2}%' ";
						$where .= " OR LOWER (sp.development_name) LIKE '%{$quick2}%' ";
					}
					//[4.580] Поиск по сокращениям не работает!!!
					//если число
					if ($int == $quick2) {
						$where .= ' OR (';
						$where .= " LOWER (sp.zip_code) LIKE '{$quick2}' ";
						$where .= " OR LOWER (sp.mln) LIKE '{$quick2}' ";
						$where .= " OR LOWER (sp.uid) LIKE '{$quick2}' ";
						$where .= ')';
					} //если не число
					else {
						/*$join = "LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood";
						$where .= " OR LOWER (neighborhood.name) LIKE '%{$quick2}%' ";
						$where .= " OR LOWER (neighborhood.name2) LIKE '%{$quick2}%' ";*/
						$where .= " OR LOWER (sp.parameters) LIKE '%{$quick2}%' ";
					}
					$where .= ')';
				}
			}
			//[4.729] Неправильно работает поиск
			$quick2 = trim($quick2);
			/*
			if ($logs_shop = mysql_select("SELECT * FROM logs_shop WHERE LOWER(search) = '".mysql_res($quick2)."'",'row')) {
				$logs_shop['count']++;
				mysql_fn('update','logs_shop',$logs_shop);
			}
			else {
				$logs_shop = array(
					'search'=>$quick2,
					'count'=>1
				);
				mysql_fn('insert','logs_shop',$logs_shop);
			}
			*/

		}

		//формировка title
		if (!isset($config['_api']) AND @$seo_shop==false) {
			$seo = array(
				'property_type' => false,
				'base' => $base,
				'city' => false,
				'neighborhoods' => false,
				'from' => false,
				'to' => false,
			);
			//4.742] Неправильно пишется Title
			//[4.569] Сделать хвост в Title другим
			$page['title'] = $page['name'] = $page['description'] = '';
			//[4.797] В H1 на всем сайте убрать приписку
			//$page['name'] = random_template('to choose', $random);
			//$page['name'] .= ' Your ';
			//$page['name'] .= random_template('beautiful', $random + 1) . ' ';
			//location
			//$page['title'].= ' in ';
			if ($neighborhoods) {
				//$page['title'] .= implode(' / ', $neighborhoods);
				$seo['neighborhoods'] = $neighborhoods;
			}
			//SF
			if ($base == 9) {
				//property_type
				if ($post['property_type']) {
					if ($post['property_type2']) {
						//die($property_type2);
						$seo['property_type'] = $property_type2;//$post['property_type2'];
					}
					else {
						if ($property_type1 = mysql_select("
							SELECT name FROM shop_property_types WHERE base=9 AND uid=".$post['property_type']." AND parent=0
						",'string')) {
							$seo['property_type'] .= ' ' . $property_type1;
						}
						else $seo['property_type'] .= ' ' . random_template('real estate', $random);
					}
					$page['title'] .= ' ' . $seo['property_type'];
					$page['name'] .= ' ' . $seo['property_type'];
				}
				else {
					$page['title'] .= 'Real Estate Properties';
					$page['name'] .= 'Real Estate Properties';
				}
			}
			elseif ($base == 10 OR $base==11) {
				//dd($post);
				//die($page['name']);
				//property_type
				if ($post['property_type']) {
					if ($post['property_type2']) {
						//die($property_type2);
						$seo['property_type'] = $property_type2;//$post['property_type2'];
					}
					else {
						if ($property_type1 = mysql_select("
							SELECT name FROM shop_property_types WHERE base IN (10,11) AND uid=".$post['property_type']." AND parent=0
						",'string')) {
							$seo['property_type'] .= ' ' . $property_type1;
							//dd($seo);
						}
						else $seo['property_type'] .= ' ' . random_template('real estate', $random);
					}
					$page['title'] .= ' ' . $seo['property_type'];
					$page['name'] .= ' ' . $seo['property_type'];
				}
				else {
					$page['title'] .= 'Real Estate Properties';
					$page['name'] .= 'Real Estate Properties';
				}
				//die($page['name']);
			}
			//SF
			elseif ($base ==2 OR $base==8) {
				/*if (!$neighborhoods) $page['title'] .= ' Florida ';
				else $page['title'] .= ' FL ';
				$page['title'] .= '{zip_code}';*/
				//property_type
				if (isset($config['object_groups'][$post['property_type']])) {
					if ($post['property_type2']
						//AND isset($config['object_groups_'.$post['property_type']])
						//AND in_array($post['property_type2'],$config['object_groups_'.$post['property_type']])
					) {
						//$page['title'].= ' '.$post['property_type2'];
						//$page['name'].= ' '.$post['property_type2'];
						$seo['property_type'] = $property_type2;//$post['property_type2'];
						//добавочное слово
						if (in_array($post['property_type'], array(2))) {
							//$page['title'] .= ' Apartments';
							//$page['name'] .= ' Apartments';
							$seo['property_type'] .= ' Apartments';
						} //residential
						elseif (in_array($post['property_type'], array(3))) {
							if (in_array($property_type2, array('Efficiency', 'Condo'))) {
								//$page['title'] .= ' Apartments';
								//$page['name'] .= ' Apartments';
								$seo['property_type'] .= ' Apartments';
							} elseif (in_array($property_type2, array('Multifamily', 'Single'))) {
								//$page['title'] .= ' '.random_template('houses',$random); //' Houses';
								//$page['name'] .= ' '.random_template('houses',$random); //' Houses';
								$seo['property_type'] .= ' ' . random_template('houses', $random); //' Houses';
							}
						} //commercial
						elseif (in_array($post['property_type'], array(4))) {
							//$page['title'] .= ' ' . random_template('real estate', $random);
							//$page['name'] .= ' ' . random_template('real estate', $random);
							$seo['property_type'] .= ' ' . random_template('real estate', $random);
						} else {
							//$page['title'] .= ' '.random_template('houses',$random); //' Houses';
							//$page['name'] .= ' '.random_template('houses',$random); //' Houses';
							$seo['property_type'] .= ' ' . random_template('houses', $random); //' Houses';
						}

					} else {
						//$page['title'].= ' '.$config['object_groups'][$post['property_type']]['name'];
						//$page['name'].= ' '.$config['object_groups'][$post['property_type']]['name'];
						$seo['property_type'] = $config['object_groups'][$post['property_type']]['name'];
						if ($post['property_type'] == 2) {
							//$page['title'] .= ' Apartments';
							//$page['name'] .= ' Apartments';
							$seo['property_type'] .= ' Apartments';
						}
						if (in_array($post['property_type'], array(3, 4, 5, 6))) {
							//$page['title'] .= ' ' . random_template('real estate', $random);
							//$page['name'] .= ' ' . random_template('real estate', $random);
							$seo['property_type'] .= ' ' . random_template('real estate', $random);
						}
					}
					$page['title'] .= ' ' . $seo['property_type'];
					$page['name'] .= ' ' . $seo['property_type'];
				}
				else {
					$page['title'] .= 'Real Estate Properties';
					$page['name'] .= 'Real Estate Properties';
				}
			} //NY
			else {
				if ($post['city']) {
					$seo['city'] = mysql_select("SELECT name FROM shop_cities WHERE id =" . $post['city'], 'string');
					//$page['title'] .= ' ' . $seo['city'] . ' ';
				}
				//if (!$neighborhoods AND !$post['city']) $page['title'] .= ' New York ';
				//else $page['title'] .= ' NY ';
				//$page['title'] .= '{zip_code}';
				//property_type
				//$page['title'].= ' of ';
				if (isset($config['ny_property_types'][$post['property_type']])) {
					if ($post['property_type2']
						//AND isset($config['ny_property_types_' . $post['property_type']])
						//AND in_array($post['property_type2'], $config['ny_property_types_' . $post['property_type']])
					) {
						//$page['title'] .= ' ' . $post['property_type2'];
						//$page['name'] .= ' ' . $post['property_type2'];
						$seo['property_type'] = $property_type2;//$post['property_type2'];
						//commercial + land
						if (in_array($post['property_type'], array(2, 3))) {
							//$page['title'] .= ' ' . random_template('real estate', $random);
							//$page['name'] .= ' ' . random_template('real estate', $random);
							$seo['property_type'] .= ' ' . random_template('real estate', $random);
						}
						else {
							if (in_array($property_type2, array('Condo', 'Co-Op'))) {
								//$page['title'] .= ' Apartments';
								//$page['name'] .= ' Apartments';
								$seo['property_type'] .= ' Apartments';
							} else {
								//$page['title'] .= ' ' . random_template('houses', $random); //' Houses';
								//$page['name'] .= ' ' . random_template('houses', $random); //' Houses';
								$seo['property_type'] .= ' ' . random_template('houses', $random); //' Houses';
							}
						}
					} else {
						//$page['title'] .= ' ' . $config['ny_property_types'][$post['property_type']];
						//$page['name'] .= ' ' . $config['ny_property_types'][$post['property_type']];
						$seo['property_type'] = $config['ny_property_types'][$post['property_type']];
						//if (in_array($post['property_type'],array(2,3))) {
						//$page['title'] .= ' ' . random_template('real estate', $random);
						//$page['name'] .= ' ' . random_template('real estate', $random);
						$seo['property_type'] .= ' ' . random_template('real estate', $random);
						//}
					}
					$page['title'] .= ' ' . $seo['property_type'];
					$page['name'] .= ' ' . $seo['property_type'];
				} else {
					$page['title'] .= ' Real Estate Properties';
					$page['name'] .= ' Real Estate Properties';
				}
			}

			//beds or baths
			if ($post['beds'] OR $post['baths']) {
				$page['title'] .= ' with ';
				$page['name'] .= ' with ';
				if ($post['beds']) {
					$seo['beds'] = $post['beds'];
					if ($post['beds'] == 'studio') {
						$page['title'] .= ' Studio ';
						$page['name'] .= ' Studio ';
					} else {
						$page['title'] .= $post['beds'] . ' Bedrooms ';
						$page['name'] .= $post['beds'] . ' Bedrooms ';
					}
				}
				if ($post['baths']) {
					$seo['baths'] = $post['baths'];
					if ($post['beds']) {
						$page['title'] .= '/ ';
						$page['name'] .= '/ ';
					}
					$page['title'] .= $post['baths'] . ' Baths ';
					$page['name'] .= $post['baths'] . ' Baths ';
				}
			}

			//residential
			if (isset($config['object_residential'][$post['residential']])) {
				$seo['residential'] = $post['residential'];
				if ($post['residential'] == 1) {
					$page['title'] .= ' For Rent';
					$page['name'] .= ' For Rent';
				} else {
					$page['title'] .= ' For Sale';
					$page['name'] .= ' For Sale';
				}
			} else {
				$page['title'] .= ' For Sale and Rental';
				$page['name'] .= ' For Sale and Rental';
			}
			//price
			if ($post['price_min'] OR $post['price_max']) {
				$page['title'] = trim($page['title']);
				$page['name'] = trim($page['name']);
				//$page['title'].= '. Priced ';
				if ($post['price_min']) {
					$page['title'] .= ' from $' . number_format($post['price_min'], 0, '.', ',');
					$page['name'] .= ' from $' . number_format($post['price_min'], 0, '.', ',');
					$seo['from'] = ' from $' . number_format($post['price_min'], 0, '.', ',');
				}
				if ($post['price_max']) {
					$page['title'] .= ' to $' . number_format($post['price_max'], 0, '.', ',');
					$page['name'] .= ' to $' . number_format($post['price_max'], 0, '.', ',');
					$seo['to'] = ' to $' . number_format($post['price_max'], 0, '.', ',');
				}
			}

			if ($neighborhoods) {
				$page['name'] .= ' in ' . implode(' / ', $neighborhoods);
				$page['title'] .= ' '.implode(' / ', $neighborhoods);
			}
			//echo $base;
			if (in_array($base,array(2,8,9,10,11))) {
				if (!$neighborhoods) $page['name'] .= ' in Florida ';
				else $page['name'] .= ', Florida ';
				if (!$neighborhoods) $page['title'] .= ' Florida ';
				else $page['title'] .= ' FL ';
				//$page['title'] .= '{zip_code}';
			}
			else {
				if ($post['city']) {
					if (!$neighborhoods) $page['name'] .= ' in ';
					$page['name'] .= ' ' . $seo['city']  . '';
					$page['title'] .= ' ' . $seo['city'] . ' ';
				}
				if (!$neighborhoods AND !$post['city']) $page['name'] .= ' in New York ';
				else $page['name'] .= ', New York';
				if (!$neighborhoods AND !$post['city']) $page['title'] .= ' New York ';
				else $page['title'] .= ' NY ';
				//$page['title'] .= '{zip_code}';
			}

			if (@$post['source']=='subscribe') {
				$html['source'] = 'subscribe';
				$today = date('Y-m-d');
				$where.= " AND sp.date_change + interval 5 day > '".$today."'";
			}


			//$page['title'] .= ' - Wilk Real Estate I LLC';
			//[4.569] Сделать хвост в Title другим
			$page['title'].= ' - Wilk Real Estate';

			if ($where == '') {
				//[4.759] Заменить H1 на страницах
				//$page['name'] = 'Featured Properties';
				if (in_array($base,array(2,8,9,10,11))) $page['name'] = 'Properties for Sale and Rent in Florida';
				else $page['name'] = 'Property for Sale and Rent in New York. Real Estate Company in Brooklyn, New York';
			}

			require_once(ROOT_DIR . 'functions/mls_func.php');
			//description
			$page['description'] = seo_description($post, $seo, $base, $random);
			//if (access('user admin'))	echo $page['description'];

			//text
			if ($page['text'] == '' AND $where != '' AND $post['view'] != 'map') {
				//$page['text'] .= seo_text($post, $seo, $base, $random);
			}
		}

		//если без гет параметров то только с галками
		$links = true; //[4.514] Для Florida сделать дополнительную перелицовку с картинками
		//сортировка
		//[4.612] Коммерческая недвижимость должна быть перед In Contract
		//матрикс - активный (ньюйорк)
		if ($base==6) {
			//[4.623] matrix - статусы
			if (!isset($config['_api'])) {
				if ($where == '' AND @$_GET['sort'] == '') {
					$where = ' AND sp.special=1'.$where;
					$links = false;
				}
			}
			$where = " sp.base IN (/*4,*/6,7) ".$where;
			//[4.623] matrix - статусы
			//[4.670] Сортировка
			//$select = ', IF (sp.rank>8 AND sp.special=1,1,0) sort ';
			//$order_by = ' sort DESC, ';
		}
		//флекс - походу не работает
		elseif ($base==8) {
			//[4.623] matrix - статусы
			if (!isset($config['_api'])) {
				if ($where == '' AND @$_GET['sort'] == '') {
					//$where .= ' AND sp.special=1';
					$links = false;
				}
			}
			$where = " sp.base IN (8)";
			//[4.623] matrix - статусы
			//[4.670] Сортировка
			$select = ', IF (sp.rank>8 AND sp.special=1,1,0) sort ';
			$order_by = ' img DESC, sort DESC, ';
			if (@$_GET['sort']=='price-asc') {
				$select = '';
				$order_by = ' price ASC, ';
			}
			if (@$_GET['sort']=='price-desc') {
				$select = '';
				$order_by = ' price DESC, ';
			}
		}
		//флорида - активный
		elseif ($base == 2) {
			if (!isset($config['_api'])) {
				if ($where == '' AND @$_GET['sort'] == '') {
					//$where .= " AND sp.city=6";
					//[4.758] флорида - свои объекты
					//$where .= ' AND sp.special=1';
					//$where .= ' AND sp.special=1';
					/*
					$select = ", IF (sp.img='' OR sp.img IS NULL,1,0) sort1 ";
					$select.= ', IF (property_type=6,1,property_type) sort ';
					$order_by = ' sp.base DESC, sort1, sort, property_type DESC, sp.date_change DESC, ';
					*/
					$links = false;
				}
			}
			//[4.740] флорида - ресурсы
			else $order_by = " sp.base DESC, ";
			$where = " AND sp.base IN (1,2,8) ".$where;
		}
		//флорида матрикс
		elseif ($base==9) {
			if (!isset($config['_api'])) {
				if ($where == '' AND @$_GET['sort'] == '') {
					$links = false;
				}
			}
			else $order_by = " sp.base DESC, ";
			$where = " sp.base IN (9) ".$where;
		}
		//флорида матрикс
		elseif ($base==10 OR $base==11) {
			if (!isset($config['_api'])) {
				if ($where == '' AND @$_GET['sort'] == '') {
					$links = false;
				}
			}
			else $order_by = " sp.base DESC, ";
			$where = " sp.base IN (10,11) ".$where;
		}
		//ньюйорк - походу отключен
		else {
			if (!isset($config['_api'])) {
				if ($where == '' AND @$_GET['sort'] == '') {
					//$where.= " AND sp.city=6";
					$where = ' AND sp.special=1'.$where;
					//$select = ', IF (property_type=1 && property_type2=4,1,0) sort ';
					/*$select .= ", IF (status='Contract',1,0) sort1 ";
					$select .= ", IF (property_type2 LIKE '%Single Family%',0,1) sort2 ";
					$select .= ", IF (property_type2 LIKE '%Multi-Family%',0,1) sort3 ";
					$select .= ", IF (property_type2 LIKE '%Condo%',0,1) sort4 ";
					$select .= ", IF (property_type2 LIKE '%Co-Op%',0,1) sort5 ";
					$order_by = ' sp.property_type, sort1, sort2, sort3, sort4, sort5, sp.date_change DESC,';
					*/
					//property_type=1&property_type_1=4
					$links = false;
				}
				else {
					//[4.401] Сортировка по цене не работает
					if ($order_by == '') {
						//$select = ", IF (status='Contract' && special=0,0,1) sort2 ";
						//$order_by = ' sort2 DESC, ';
					}
				}
			}
			//[4.836] Пропали объекты на всех посадочных страницах!
			//$where .= " AND (sp.base=3 OR sp.base=4) ";
			$where = " sp.base IN (4,6,7) ".$where;
		}



		// если есть, то получаем данные для СЕО категория / район
		/*
		if (isset($_GET['location']) && is_array($_GET['location']) && (count($_GET['location']) == 1)) {
			$location_id = intval($_GET['location'][0]);
			if ($category) // если есть категория, ищем связку категория - район
				$seo_text = mysql_select("SELECT s.name, s.text, s.title, s.keywords, s.description FROM shop_neighborhoods_seo AS s WHERE s.category = {$category['id']} AND s.neighborhood = {$location_id} AND s.display = 1 LIMIT 1", 'row');
			else // если нет, берем данные из района
				$seo_text = mysql_select("SELECT s.name, s.text, s.title, s.keywords, s.description FROM shop_neighborhoods AS s WHERE s.id = {$location_id} AND s.display = 1 LIMIT 1", 'row');
			if ($seo_text) {
				$page = array_merge($page, $seo_text);
			}
		}*/


		$limit = ''; //print_R($post);
		if (!isset($config['_api'])) {
			if (@$html['source'] != 'subscribe') $html['search'] = html_array('shop/search', $post);
			$html['filter'] = html_array('shop/filter', $page);
			if ($post['view'] == 'map') {
				$where .= " AND sp.lat!=''";
				$limit = ' LIMIT 1000';
			}
		}
		$query = "/*1*/
			SELECT sp.id,
					sp.rank,
					sp.hide,
					sp.name,
					sp.residential,
					sp.special,
					sp.city,
					sp.neighborhood,
					sp.property_type,
					sp.property_type2,
					sp.date_change,
					sp.mln,
					sp.uid,
					sp.status,
					sp.price,
					sp.beds,
					sp.baths,
					sp.square,
					sp.zip_code,
					sp.img,
					sp.base,
					sp.office_name,
					sp.oh_date,
					sp.oh_dates,
					sp.oh_display,
					sp.url,
					sp.date_add,
					sp.lat,
					sp.lng/*,
					sn.url neighborhood_url*/
				$select
			FROM shop_products AS sp
			/*LEFT JOIN shop_neighborhoods AS sn ON sn.id = sp.neighborhood*/
			$join
			WHERE {$where} AND sp.display = 1

		";

		$query2 = "/*2*/
			SELECT COUNT(sp.id)
				$select
			FROM shop_products AS sp
			$join
			WHERE {$where} AND sp.display = 1

		";
		$num_rows = mysql_select($query2,'string');

		if (!isset($config['_api'])) {
			$query.= "ORDER BY {$order_by} sp.special DESC, sp.rank DESC, sp.date_change DESC";
			$query.= $limit;
			//echo $query;
			//echo $page['text'];
			if ($post['view'] == 'map') {
				$page['noindex'] = 1;
				//$query.= " LIMIT 1000";
				$html['content'] = html_query('shop/product_map', $query, false);
			}
			else {
				$html['content'] = html_query('shop/product_list shop '.$num_rows, $query, false);
				//if (access('user admin')) dd($page);
				$page['text'] = template($page['text'], array('num_rows' => $config['_num_rows']));
				if (@$_GET['action'] == 'pagination') die($html['content']);
			}
			//[4.514] Для Florida сделать дополнительную перелицовку с картинками
			if ($config['_num_rows']==0) $links = false;
			if (@$post['source']=='subscribe') $links = false;
			//if ($u[1])
			if ($links)	{
				$post['state'] = $base;
				//[2.25] поиск кондо в гугле
				//отключил перелинковку
				//$html['content'].= html_array('shop/links',$post);
			}
			$config['_zip_code'] = isset($config['_zip_code']) ? $config['_zip_code'] . ' ' : '';
			//echo mysql_error();
			$page['title'] = template($page['title'], array('zip_code' => $config['_zip_code']));

			$breadcrumb['page'][0] = array(
				$page['name'],
				'/' . $state['url'] . '/' . $module_url . '/'
			);
		}
	}
}

if (access('user admin')) {
	//dd($html);
	//die();
}