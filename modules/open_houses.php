<?php

$html['critical'] = 'critical_shop';

$config['date'] = date('Y-m-d');


$where = " AND (sp.base=3 OR sp.base=4)";
if ($u[1]==$modules['shop_mx']) {
	$where = " AND base IN (3,4,6,7)";
}

$query = "
	SELECT sp.*,
	neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
	FROM shop_products AS sp
	LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
	WHERE sp.display = 1 ".$where." AND oh_display=1 AND oh_date>='".date('Y-m-d')."'
	ORDER BY sp.oh_date
";
//$html['content'] = '<h1 style="text-align:center">'.$page['name'].'</h1>';
$html['content'] = $page['text'];
$html['content'].= html_query('shop/product_list shop', $query, false, 60 * 0);