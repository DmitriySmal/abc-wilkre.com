<?php
/*
 * Скрипт для проставления property_type
 *
 */

define('ROOT_DIR', dirname(__FILE__).'/../');
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек
include_once (ROOT_DIR.'functions/file_func.php');
require_once(ROOT_DIR.'functions/mysql_func.php');
require_once(ROOT_DIR.'functions/string_func.php');
require_once(ROOT_DIR.'functions/image_func.php');

mysql_connect_db();
require(ROOT_DIR.'plugins/phpQuery/phpQuery/phpQuery.php');

$config['neighborhoods'] = mysql_select("SELECT id,LOWER(name) name FROM shop_neighborhoods WHERE city=6",'array',60*60);

//echo ROOT_DIR;
$query = 'SELECT * FROM shop_preconstructions WHERE display=0 ORDEr BY id';
//$query = 'SELECT * FROM shop_preconstructions WHERE id=1';
if ($q = mysql_select($query,'row')) {

	$url = $q['url'];
	if (mb_strlen($url)<45) $url = 'http://www.miamiproperties.com/info/about/'.$url;
	else echo mb_strlen($url);
	echo $url . '<br><br>';

	$results = file_get_contents($url);

	$document = phpQuery::newDocument($results);

	$data = $document->find('.text_section');
	$img_box = pq($data)->find('img[width=630]');
//echo pq($img_box)->attr('src');
	$imgs_box = pq($img_box)->parent('p')->next('table');
	$images = pq($imgs_box)->find('a');

	$iframe = pq($data)->find('iframe');
	$url = parse_url(pq($iframe)->attr('src'));
//print_r($url); echo '<br>';
	parse_str($url['query'], $str);
//print_r($str); echo '<br>';
	$coords = explode(',', $str['ll']);

	//$booklet = pq($data)->find('a[href^=https://]')->attr('href');

//print_r($coords); echo '<br>';
//echo pq($iframe)->attr('src').'<br>';
	$text = pq($data);
	pq($text)->find('style')->remove();
	pq($text)->find('table')->remove();
	pq($text)->find('iframe')->remove();
	pq($text)->find('.bread_crumb')->remove();
	pq($text)->find('img')->remove();
	pq($text)->find('h1:first-child')->remove();

	$booklet = pq($text)->find('a[href^=https://]')->attr('href');

	$txt = trim(pq($text)->html());
	$txt = str_replace(
		array('<p><br><br></p>',' style="color: #333333;"', ' style="color: #0099ff;"', ' style="text-align: justify;"', '<p>  </p>', '<p> </p>', '<p></p>', 'h1'),
		array('','', '', '', '', '', '', 'h2'),
		$txt);
	$txt = trim($txt);

	$start = strpos($txt, "<h2><span>Price List");
	//$stop = strpos($txt, "-->");
	//echo $start.'-'.$stop;
	$txt = substr($txt,0,$start);
	echo '<textarea>';
	echo $txt;
	echo '</textarea>';


	$result		= stristr($txt,'Location: </span>');
	$s			= strpos($result, '</h2>');
	$address	= substr($result,18,$s-18);
	echo '<textarea>';
	echo $address;
	echo '</textarea>';

	$post = array(
		'id' => $q['id'],
		'url' => basename(trim($q['url'], '/')),
		//'img'=>basename(pq($img_box)->find('img')->attr('src')),
		//'imgs'=>count($imgs)?serialize($imgs):'',
		'coordinates' => count($coords) > 1 ? '(' . implode(',', $coords) . ')' : '',
		'lat' => count($coords) > 1 ? $coords[0] : '',
		'lng' => count($coords) > 1 ? $coords[1] : '',
		'text' => $txt,
		'keywords' => keywords($txt),
		'description' => description($txt),
		'booklet'=>$booklet,
		'address'=>$address,
		'display' => 1
	);
	foreach ($config['neighborhoods'] as $k=>$v) {
		if (strpos(strtolower($address),$v)) {
			$post['neighborhood'] = $k;
			echo $v;
			break;
		}
		else {
			//echo strtolower($address).' - '.$v.'<br>';
		}
	}

	$root = ROOT_DIR . 'files/shop_preconstructions/' . $q['id'] . '/';
	if (is_dir($root)) delete_all($root, false); //удаление старого файла
	$root1 = $root . 'img/';
	$root2 = $root . 'imgs/';
	mkdir($root1, 0755, true);
	mkdir($root2, 0755, true);

	$img = pq($img_box)->attr('src');
	$img = str_replace(' ', '%20', $img);
	echo '<br>' . $img;
//echo file_get_contents($img);
	if (@fopen($img, "r")) {
		$post['img'] = basename($img);
		echo '<br>';
		echo $post['img'] . '<br>';
		$path = $root1 . $post['img'];
		copy($img, $path);
		img_process('resize', $path, '100x100', $root1 . '/a-' . $post['img']);
		img_process('cut', $path, '536x320', $root1 . '/m-' . $post['img']);
		img_process('resize', $path, '293x117', $root1 . '/p-' . $post['img']);
	}

	$imgs = array();
	$i = 0;
	foreach ($images as $li) {
		$file = pq($li)->attr('href');
		$file = str_replace(' ', '%20', $file);
		if (@fopen($file, "r")) {
			$i++;
			echo $file . '<br>';
			$imgs[$i] = array(
				'name' => basename($file),
				'file' => basename($file),
				'display' => 1
			);
			mkdir($root2 . $i, 0755, true);
			$path = $root2 . $i . '/' . $imgs[$i]['file'];
			copy($file, $path);
			img_process('resize', $path, '100x100', $root2 . $i . '/a-' . $imgs[$i]['file']);
			img_process('cut', $path, '536x320', $root2 . $i . '/m-' . $imgs[$i]['file']);
			img_process('resize', $path, '293x117', $root2 . $i . '/p-' . $imgs[$i]['file']);
		} else echo 'not ' . $file . '<br>';
	}
	$post['imgs'] = count($imgs) ? serialize($imgs) : '';

	mysql_fn('update','shop_preconstructions',$post);
	echo '<pre>';
	unset($post['text']);
	print_r($post);
	echo '<pre><br>';
	echo '<script type="text/javascript">location.reload();</script>';
}
else echo 'ok';
