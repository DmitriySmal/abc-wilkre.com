<?php

$config['cms_version'] = '1.0.4-4';

$config['http'] = 'https';
$config['domain'] = 'www.wilkrealestate.com';

$config['store_info'] = array(
	'type'=>array(
		1=>'Store',
		2=>'Apt',
	),
	'lease'=>array(
		1=>'Yes',
		2=>'No'
	)
);

$config['plate'] = array();
for($i=2; $i<51; $i++) {
	$config['plates'][$i] = $i;
}

$config['mailchimp_list'] = array(
		'MLS agents'=>'b415717882',
		'MLS Realtors'=> '325b0508ea',
		'Wilk Clients'=>'ca0fda5fe7',
		'Wilk Agents'=> '9370b48556',
		'Trullia'=>'af3b7502cd',
		'Zillow'=>'4b20e3efc8',
		"New Wilk's Clients"=>'2cc1f6567d'
	//'test'=> '2e618eac63'
);
$config['mailchimp_api_key'] = '604e1ac8b92a95b206427aeb84b3ca03-us13';
$config['mailchimp_id'] = '2cc1f6567d';

$config['random'] = array(
	'houses'=>array('Houses','Homes'),
	'house'=>array('House','Home'),
	'to choose'=>array('To Choose','To Select','Look At','Take a look at'),
	'real estate'=>array('Real Estate','Properties'),
	'property'=>array('Real Estate','Property'),
	'beautiful'=>array('Beautiful','Posh','Respectability','Luxury','Wishful','Favourite','Interested','Wanted'),
	'single family'=>array('Single Family','One Family'),
	'multi family'=>array('Multi-Family','Two Family','Three Family','Four Family','Five Family'),
	4=>array(1,2,3,4),

	'find'=>array('Find','Search'),
	'site'=>array('site','service','website','platform'),
	'realtor_agency'=>array('Realtor Agency','Real Estate Agency','Brocker Agency','Real Estate Company','Brocker Company'),
	'baths'=>array('Bathrooms','Baths','Bath Rooms'),
	'beds'=>array('Bedrooms','Sleeping Rooms'),
	'condo'=>array('Condo','Condominium'),
	'coo-op'=>array('Co-op','Coop','Cooperative'),
	'for sale'=>array('For Sale','Sales'),
	'for rent'=>array('For Rent','Rental'),
	'sf'=>array('Florida','Miami','FL'),
	'ny'=>array('New York','NY'),
	'value'=>array('value','cost','worth'),
	'select'=>array('select','choose'),
	'offers'=>array('offers','proposes'),

	'wilkre.com'=>array('Wilk Real Estate site','site of Wilk Real Estate','Wilk Real Estate’s site','site of Albert Wilk','WilkRealEstate.com'),
	'you can'=>array('you can','you could'),
	'realty'=>array('property','real estate','realty','estate'),
	'different sizes'=>array('different sizes','various sizes'),
	'proposed options'=>array('proposed options','suggested options','offered options'),
	'property value'=>array('Property value','Property market value','Realty market value','Realty value','Estate value','Estate market value'),
	'section'=>array('section','division','category'),
	'provides'=>array('provides','gives','tenders','furnishes'),
	'proposals'=>array('proposals','offers','suggestions'),
	'affected'=>array('affected','influenced'),
	'factors'=>array('factors','moments','points'),
	'estate'=>array('property','real estate property','real estate','estate'),
	'location'=>array('location','position','state'),
	'type'=>array('type','kind','class'),

	'condition'=>array('condition', 'state', 'status','position'),
	'if you'=>array('If you','When you'),
	'your dream'=>array('your dream','your favorite','your lovely'),
	'compare'=>array('compare','match','collate'),
	'price'=>array('price','cost','value','worth'),
	'our website'=>array('our website','the Wilk’s site','our site','our service','our platform'),
	'might'=>array('might','can'),
	'offer'=>array('offer','suggest','propose'),
	'better'=>array('better','best'),
	'a lot of'=>array('a lot of','more'),

	'contact'=>array('contact','call'),



);

$config['object_base'] = array(
	1=>'SF own',//to delete
	2=>'SF mls',//to delete
	3=>'NY mls',//delete
	4=>'NY own',//delete
	5=>'NY old',//delete
	6=>'MX mls',
	7=>'MX own',
	8=>'FX mls'
);
$config['object_base_state'] = array(
	1=>'SF',
	2=>'SF',
	3=>'NY',
	4=>'NY ',
	5=>'NY',
	6=>'NY',
	7=>'NY',
	8=>'SF'
);

$config['parameter_base'] = array(
	2=>'Florida',
	3=>'New York',
	6=>'Matrix'
);

$config['object_residential'] = array(
	2=>'For Sale',
	1=>'For Rent'
);
$config['object_residential_url'] = array(
	2=>'Sale',
	1=>'Rent'
);

$config['categories'] = array(
	1	=> 'Single Family',
	2	=> 'Condo/Coop/Villa/Twnhse',
	3	=> 'Residential Income',
	4	=> 'ResidentialLand/BoatDocks',
	5	=> 'Comm/Bus/Agr/Indust Land',
	6	=> 'Residential Rental',
	7	=> 'Improved Comm/Indust',
	8	=> 'Business Opportunity',
);

//***
$config['global_property_type'] = array(
	1=>'Condo / Co-op',
	2=>'Homes',
	3=>'Land',
	4=>'Commercial',
);

//урл для пропертитайп флориды
$config['sf_property_type_url'] = array(
	1=>'Homes',
	2=>'CondoCoop',
	3=>'Residential',
	4=>'Commercial',
	5=>'Land',
	6=>'ResidentialIncome',
	//7=>'CondoHotel',
	//8=>'Villa',
	//9=>'MobileManufactured',
	//10=>'Townhouse',
);

//todo сделать в виде таблицы в админке
$config['object_groups'] = array(
	1=>array(
		'name'=>'Homes',
		'url'=>'Homes',
		'categories'=> '1',//sale
	),
	/*7=>array(
		'name'=>'Condo Hotel',
		'url'=>'CondoHotel',
		'categories'=>'0',
	),
	8=>array(
		'name'=>'Villa',
		'url'=>'Villa',
		'categories'=>'0',
	),
	9=>array(
		'name'=>'Mobile/Manufactured',
		'url'=>'MobileManufactured',
		'categories'=>'0',
	),
	10=>array(
		'name'=>'Townhouse',
		'url'=>'Townhouse',
		'categories'=>'0',
	),*/
	6=>array(
		'name'=>'Residential Income',
		'url'=>'ResidentialIncome',
		'categories'=> '3',//sale
	),
	2=>array(
		'name'=>'Condo / Co-op',
		'url'=>'CondoCoop',
		'categories'=> '2',//sale
	),
	3=>array(
		'name'=>'Residential',
		'url'=>'Residential',
		'categories'=> '6',//rent
	),
	4=>array(
		'name'=>'Commercial',
		'url'=>'Commercial',
		'categories'=> '7,8',//sale/rent
	),
	5=>array(
		'name'=>'Land',
		'url'=>'Land',
		'categories'=> '4,5',//sale
	),
);

$config['object_groups_6'] = array(
	//"Single",
	"Duplex",
	"Fourplex",
	"Triplex"
);

$config['object_groups_3'] = array(
	'Apartment',
	'Condo',
	'Efficiency',
	'Multifamily',
	'Single',
	'Townhouse'
);

$config['object_groups_4'] = array(
	'Anchored Center',
	'Apartments',
	'Assisted Living Facility',
	'Automotive',
	'Building Service',
	'Building Trades',
	'Church',
	'Commercial',
	'Commercial Condo',
	'Commercial/Residential Income',
	'Condominium',
	'Consumer Service',
	'Convenience Store',
	'Daycare',
	'Dock Height Building',
	'Free Standing',
	'Heavy Manufacturing',
	'Hotel/Motel',
	'Industrial',
	'Industrial/Manufacturing',
	'Light Manufacturing',
	'Lounge',
	'Mall Enclosed',
	'Marina',
	'Medical Office',
	'Mini Warehouse',
	'Mobile/Rv Parks',
	'Non-Profit',
	'Office Space',
	'Office/Retail',
	'Office/Warehouse Combination',
	'Other Building Type',
	'Other Type',
	'Professional',
	'Recreation Facility',
	'Residential-Multi-Family',
	'Residential-Single Family',
	'Restaurant',
	'Restaurant/Bar',
	'Retail Space',
	'School',
	'Shopping Center',
	'Showroom',
	'Special Purpose',
	'Store/Warehouse Combination',
	'Strip Store',
	'Tavern/Bar',
	'Unimproved Interior',
	'Warehouse Space',
	'Wholesale',
);


$config['condo_statuses'] = array(
    1=> 'constructed-building',
    0=> 'pre-construction',
);

$config['default_state'][9999] = array(
    'id' => 0,
    'name' => 'Home',
    'url' => 'home'
);

$config['object_statuses'] = array(
    'A' => 'Active-Available',
    "B" => 'Backup Contract-Call LA',
    'C' => 'Cancelled',
    'CS' => 'Closed Sale',
    'PS' => 'Pending Sale/Rental',
    'Q' => 'Terminated',
    'T' => 'Temp Off Market',
    'W' => 'Withdrawn',
    'X' => 'Expired',
    'R' => 'Rented'
);

$config['prices_list'] = array(
	500=>'$500',
    1000 => '$1,000',
    1500 => '$1,500',
    2000 => '$2,000',
    2500 => '$2,500',
    3000 => '$3,000',
    4000 => '$4,000',
	4500 => '$4,500',
    5000 => '$5,000',
	5500 => '$5,500',
    6000 => '$6,000',
	6500 => '$6,500',
    7000 => '$7,000',
	7500 => '$7,500',
    8000 => '$8,000',
	8500 => '$8,500',
    9000 => '$9,000',
	9500 => '$9,500',
    10000 => '$10,000',
    20000 => '$20,000',
    30000 => '$30,000',
    40000 => '$40,000',
    50000 => '$50,000',
    60000 => '$60,000',
    70000 => '$70,000',
    80000 => '$80,000',
    90000 => '$90,000',
    100000 => '$100,000',
    200000 => '$200,000',
    300000 => '$300,000',
    400000 => '$400,000',
    500000 => '$500,000',
    600000 => '$600,000',
    700000 => '$700,000',
    800000 => '$800,000',
    900000 => '$900,000',
	1000000 => '$1,000,000',
	2000000 => '$2,000,000',
	3000000 => '$3,000,000',
	4000000 => '$4,000,000',
	5000000 => '$5,000,000',
	10000000 => '$1,000,000',
	20000000 => '$20,000,000',
	30000000 => '$30,000,000',
	50000000 => '$50,000,000',
   /* 1000000 => '$1M',
    2000000 => '$2M',
    3000000 => '$3M',
    4000000 => '$4M',
    5000000 => '$5M',
    10000000 => '$10M',
    20000000 => '$20M',
    30000000 => '$30M',
    50000000 => '$50M',
    //999999999 => '$50M+',
   */
);

$config['bedrooms_list'] = array(
    'studio'=>'Studio',
	'1' => '1',
    '2' => '2',
    '3' => '3',
    '4' => '4',
    '5' => '5+',
);

$config['baths_list'] = array(
    1 => '1',
    2 => '2',
    3 => '3',
    4 => '4',
    5 => '5+',
);
$config['time_list'] = array(
	1 => 'AM',
	2 => 'PM',
);
$config['days_list'] = array(
	1=>'Su',
	2=>'Mo',
	3=>'Tu',
	4=>'We',
	5=>'Th',
	6=>'Fr',
	7=>'Sa',
);

$config['shop_parameters'] = array(
	1223=>array('name'=>'Virtual Tour','type'=>'link'),
	'Listing'=>array('name'=>'Listing'),
	17=>array('name'=>'Unit Number'),
	137=>array('name'=>'Price','type'=>'price'),
	129=>array('name'=>'Living Area',/*'type'=>'square'*/),
	1358=>array('name'=>'Price Per Foot','type'=>'price'),
	675=>array('name'=>'Bulidings'),
	643=>array('name'=>'Floors'),
	651=>array('name'=>'Parking Spaces'),
	25=>array('name'=>'Bedrooms'),
	1032=>array('name'=>'Bedrooms'),
	92=>array('name'=>'Full Baths'),
	396=>array('name'=>'Full Baths'),
	109=>array('name'=>'Half Baths'),
	1040=>array('name'=>'Half Baths'),
	348=>array('name'=>'Maintenance','type'=>'price'),
	347=>array('name'=>'Maintenance Includes','type'=>'listing'),
	274=>array('name'=>'Taxes','type'=>'price'),
	665=>array('name'=>'Taxes','type'=>'price'),
	314=>array('name'=>'Year Built'),
	145=>array('name'=>'Lot Size'),
	181=>array('name'=>'Pets Allowed'),
	295=>array('name'=>'Waterfront'),
	285=>array('name'=>'View'),
	375=>array('name'=>'Unit View','type'=>'listing'),
	1464=>array('name'=>'Days On Market'),
	1473=>array('name'=>'Foreclosure'),
	1465=>array('name'=>'Short Sale'),
	355=>array('name'=>'Parking','type'=>'listing'),
	177=>array('name'=>'Parking','type'=>'listing'),
	417=>array('name'=>'Parking','type'=>'listing'),
	53=>array('name'=>'Construction','type'=>'listing'),
	276=>array('name'=>'Type of Property'),
	446=>array('name'=>'Type of Property'),
	//80=>array('name'=>'Listing Date'),
	131=>array('name'=>'Listing Date','type'=>'date'),
);



/*
$config['object_statuses'] = array(
    0 => 'Active', //продается (отображается в поиске)
    1 => 'Sold', // продана (не отображается в поиске, но есть в карте сайта)
    2 => 'Save Listing', //телефон и кнопка захвата “узнать информацию”
    3 => 'Contract', //на контракте
    4 => 'Of the market' // в продаже
); */

$config['for_sale'] = array(
    0 => 'Yes', // продажа
    1 => 'No', // аренда
);

$config['object_types'] = array(
    0 => 'Sell', // продажа
    1 => 'Rent', // аренда
);

$config['rets_url'] = 'http://sef.rets.interealty.com/Login.asmx/Login';
$config['rets_login'] = 'WiSkR';
$config['rets_pw1'] = 'gm83xpt5118';
$config['rets_pw2'] = 'fghi4921';

//database
/*$config['mysql_server']		= 'db2.unlim.com';
$config['mysql_username']	= 'u11788_test12';
$config['mysql_password']	= 'AcT4K0L00rhL';
$config['mysql_database']	= 'u11788_test12';*/

$config['mysql_server']		= 'wilkreco-base.c3nxcbqoji6s.us-east-2.rds.amazonaws.com';
$config['mysql_username']	= 'wilkreco_user';
$config['mysql_password']	= 'm9pkllss';
$config['mysql_database']	= 'wilkreco_base';

//болгарский хостинг
/*$config['mysql_server']		= 'localhost:3306';
$config['mysql_username']	= 'wilkreal_main';
$config['mysql_password']	= 'Ca{Nt}=&b_!F';
$config['mysql_database']	= 'wilkreal_main';
*/

//исключение для локальной версии
if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' AND $_SERVER['SERVER_ADDR']=='127.0.0.1') {
	$config['mysql_server'] = '198.12.154.3';
	$config['mysql_server'] = 'localhost';
	$config['mysql_username'] = 'root';
	$config['mysql_password'] = '';
	$config['mysql_database'] = 'wilkre';
	/* */
}
$config['mysql_charset']	= 'UTF8';
$config['mysql_connect']	= false; //по умолчанию база не подключена
$config['mysql_error']		= false; //ошибка подключения к базе

//массив всех подключаемых css и js файлов
$config['sources'] = array(
	'css_reset'					=> '/templates/css/reset.css',
	'css_common'				=> '/templates/css/common.css?',
	'css_print'				    => '/templates/css/print.css?',
	'script_common'				=> '/templates/scripts/common.js?',
	'jquery'					=> '/plugins/jquery/jquery-1.11.1.min.js',
	'jquery_cookie'				=> '/plugins/jquery/jquery.cookie.js',
	'jquery_ui'					=> '/plugins/jquery/jquery-ui-1.11.1.custom.min.js',
	'jquery_ui_style'			=> '/plugins/jquery/redmond/jquery-ui-1.8.17.custom.css',
	'jquery_localization'		=> '/plugins/jquery/i18n/jquery.ui.datepicker-{localization}.js',
	'jquery_form'				=> '/plugins/jquery/jquery.form.min.js',
	'jquery_uploader'			=> '/plugins/jquery/jquery.uploader.js',
	'jquery_validate'			=> array(
		'/plugins/jquery/jquery-validation-1.8.1/jquery.validate.min.js',
		'/plugins/jquery/jquery-validation-1.8.1/additional-methods.min.js',
		//'/plugins/jquery/jquery-validation-1.8.1/localization/messages_{localization}.js',
	),
	'jquery_smartbanner'			=> array(
		'/plugins/jquery/jquery.smartbanner-master/jquery.smartbanner.css',
		'/plugins/jquery/jquery.smartbanner-master/jquery.smartbanner.js',
		//'/plugins/jquery/jquery-validation-1.8.1/localization/messages_{localization}.js',
	),
	'jquery_combo_select'=>'/plugins/jquery/jquery.combo.select.js',
	'jquery_multidatespicker'	=> '/plugins/jquery/jquery-ui.multidatespicker.js',
	'highslide'					=> array(
		'/plugins/highslide/highslide.packed.js',
		'/plugins/highslide/highslide.css',
	),
	'highslide_gallery' 		=> array(
		'/plugins/highslide/highslide-with-gallery.js',
		'/templates/scripts/highslide.js?',
		'/plugins/highslide/highslide.css',
	),
	'tinymce'					=> '/plugins/tinymce/tinymce.min.js',
	'editable'					=> '/templates/scripts/editable.js',
		'bootstrap_chosen' => array(
			'/plugins/bootstrap/css/bootstrap-chosen.css?',
			'/plugins/bootstrap/js/bootstrap-chosen.js',
		),
	'landing.css'=>array(
		'/templates/css/landing/font.css?',
		'/templates/css/landing/common.css?',
	),
	'landing.js'=>array(
		'/templates/scripts/landing/common.js?',
		'/templates/scripts/landing/core.min.js',
		'/templates/scripts/landing/script.js?',

	),
	'bootstrap.critical.css'=> '/plugins/bootstrap/css/bootstrap.critical.min.css',
	'bootstrap.css'=> '/plugins/bootstrap/css/bootstrap.min.css',
	'bootstrap.js'=>'/plugins/bootstrap/js/bootstrap.min.js',

        'bootstrap'=> array(
            '/plugins/bootstrap/css/bootstrap.min.css',
            '/plugins/bootstrap/js/bootstrap.min.js',
        ),
        'bootstrap_validator'=>
            '/plugins/bootstrap/js/validator.js',
        'bootstrap_switch'=> array(
            '/plugins/bootstrap/css/bootstrap-switch.css',
            '/plugins/bootstrap/js/bootstrap-switch.js',
        ),
        'bootstrap_multiselect'=> array(
            '/plugins/bootstrap/css/bootstrap-multiselect.css',
            '/plugins/bootstrap/js/bootstrap-multiselect.js',
        ),
        'bootstrap_range_slider'=> array(
            '/plugins/bootstrap/css/bootstrap-slider.css',
            '/plugins/bootstrap/js/bootstrap-slider.js',
        ),
        'swiper' => array(
            '/plugins/swiper/swiper.min.css',
            '/plugins/swiper/swiper.min.js',
        ),
        'purl'=> array(
            '/templates/scripts/purl.js',
        ),
        'gmaps'=>
            //'http://maps.googleapis.com/maps/api/js?key=AIzaSyDg1HaVnTNemgZAhdjAGHCLrjurumdoEzQ&sensor=false"',
            // last worked
            //'<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDg1HaVnTNemgZAhdjAGHCLrjurumdoEzQ&sensor=false"></script>',
            '<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>'
);

//timezone
$config['timezone']			= 'Europe/Moscow';
$config['timezone']			= 'America/New_York';

//папка со стилями
$config['style'] = 'templates';

//charset
$config['charset']			= 'UTF-8';

error_reporting(E_ALL);
//error_reporting(0);
//исключение для локальной версии
if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' AND $_SERVER['SERVER_ADDR']=='127.0.0.1') {
	//set_error_handler('error_handler');
}
else set_error_handler('error_handler');

date_default_timezone_set($config['timezone']);
ini_set('session.cookie_lifetime', 0);
ini_set('magic_quotes_gpc', 0);

header('Content-type: text/html; charset='.$config['charset']);
header('X-UA-Compatible: IE=edge,chrome=1');

//обработчик ошибок
function error_handler($errno,$errmsg,$file,$line) {
	//если тестируем удаление
	$delete = substr($errmsg,0,6);
	if ($delete=='delete') {
		//запись в файл
		$dir = ROOT_DIR.'logs';
		//файл лога
		$log_file_name = $dir.'/delete_'.date('Y-m-d').'.txt';
		$err_str = date('d H:i');
		$err_str.= "\tfile://".$file;
		$err_str.= "\t".$line;
		$err_str.= "\thttp://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
		$err_str.= "\t".$errmsg;
		$err_str.= "\r\n";
		$fp = fopen($log_file_name, 'a');
		fwrite($fp,$err_str);
		fclose($fp);
		//фатальная ошибка
		if ($errno==E_USER_ERROR) exit(1);
		//не запускаем внутренний обработчик ошибок PHP
		return true;
	}
	if ($delete=='test  ') {
		//запись в файл
		$dir = ROOT_DIR.'logs';
		//файл лога
		$log_file_name = $dir.'/error_'.date('Y-m').'.txt';
		$err_str = date('d H:i');
		$err_str.= "\tfile://".$file;
		$err_str.= "\t".$line;
		$err_str.= "\thttp://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
		$err_str.= "\t".$errmsg;
		$err_str.= "\r\n";
		$fp = fopen($log_file_name, 'a');
		fwrite($fp,$err_str);
		fclose($fp);
		//фатальная ошибка
		if ($errno==E_USER_ERROR) exit(1);
		//не запускаем внутренний обработчик ошибок PHP
		return true;
	}


	// Этот код ошибки не включен в error_reporting
	if (in_array($errno,array(8192,8,2))) return;
	//if (!(error_reporting() & $errno)) return;
	//не фиксируем простые ошибки
	if ($errno==E_USER_NOTICE) return true;
	//папка с логами
	$dir = ROOT_DIR.'logs';
	//файл лога
	$log_file_name = $dir.'/error_'.date('Y-m').'.txt';
	//запись в файл или перезапись файла
	$write = 'a'; //запись в файл
	//размер файла лога
	$size = 0;
	//максимальный размер файла лога
	$max_size = 100000; //1 мегабайт
	/*if (file_exists($log_file_name)) {
		$size = filesize($log_file_name);
		if ($size>$max_size) $write = 'w';
	}*/
	//строка лога
	$err_str = date('d H:i');
	$err_str.= "\t".$errno;
	$err_str.= "\tfile://".$file;
	$err_str.= "\t".$line;
	$err_str.= "\thttp://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	$err_str.= "\t".$errmsg;
	$err_str.= "\r\n";
	//создаем папку
	if (!is_dir($dir)) mkdir($dir);
	//запись в файл
	$fp = fopen($log_file_name, $write);
	fwrite($fp,$err_str);
	fclose($fp);
	//фатальная ошибка
	if ($errno==E_USER_ERROR) exit(1);
	//не запускаем внутренний обработчик ошибок PHP
	return true;
}

?>