<?php
//FL
if (!isset($base)) {
	$base = 2;
	$module_url = $modules['shop'];
}
//NY
if ($base==1) {
	include(ROOT_DIR.'_config_ny.php');
	$html['module'] = 'shop';
	$module_url = $modules['shop_ny'];
}

$html['submenu'] = html_query('menu/list', "
	SELECT id,name,url,module,1 level,1 submenu
	FROM pages
	WHERE display=1 AND menu = 1 AND parent=".$page['id']."
	ORDER BY left_key
", '', 60 * 60, 'json');

//вложенные модули
$submodule = false;
if ($submenu = mysql_select("SELECT id,name,url,module,level,1 submenu
	FROM pages
	WHERE display=1 AND menu = 1 AND parent=".$page['id']."
	ORDER BY left_key",'rows',60 * 60))
{
	foreach ($submenu as $k=>$v) {
		if ($v['url']==$u[2]) {
			$page = mysql_select("SELECT * FROM pages WHERE id=".$v['id'],'row');
			$breadcrumb['page']	= array_merge(
				array(
					array(
						$page['name'],
						'/'.$u[1].'/'.$u[2].'/'
					)
				),
				$breadcrumb['page']
			);
			$submodule = $v['module'];
			include(ROOT_DIR.'modules/'.$submodule.'.php');
			break;
		}
	}
}

//если нет вложенного модуля
if ($submodule == false) {
	$html['is_product'] = false;
	$html['is_map'] = false;
	$where = $order_by = ' ';
	$view_order = array('asc', 'desc');
	$view_type = array('price');//,'date'

	$zips = array();
	$where = $join = '';

	$html['is_quick'] = $quick = mysql_real_escape_string(@$_GET['quick']);

	if ($u[4] OR (isset($product) AND $product)) {
		//$html['content'] = html_array('shop/product_text',$page);
		$id = intval(explode2('-', $u[4]));
		//запрос на товар и на категорию
		$product = (isset($product) AND $product) ? $product :  mysql_select("
			SELECT sp.*, sn.name nb_name, sn.url nb_url
			FROM shop_products sp
			LEFT JOIN shop_neighborhoods AS sn ON sn.id = sp.neighborhood
			WHERE sp.display = 1 AND sp.id = '" . $id . "'
			LIMIT 1
		", 'row');
		if ($product) {

			if (@$_GET['action']=='pdf') {
				$html = clear_text(html_array('shop/product_pdf', $product));
				$html = iconv('UTF-8', "windows-1251//IGNORE", $html);
				require_once ROOT_DIR . "plugins/MPDF56/mpdf.php"; // Подключаем класс рендера
				$mpdf = new mPDF('utf-8', 'A4', '8', '', 5, 0, 5, 0, 10, 10); // Создаем экземпляр класса
				$mpdf->charset_in = 'cp1251'; // Входная кодировка html
				//$css = file_get_contents(ROOT_DIR . 'templates/css/print.css');// Получаем css файл
				//$mpdf->WriteHTML($css, 1);// Добавляем наш css в поток
				$mpdf->WriteHTML($html, 2); // В переменной $html хранится наш html
				$mpdf->Output('mpdf.pdf', 'I'); // Вывод нашего pdf
				//$file = ROOT_DIR.'files/shop_products/'.$product['id'].'/'.$product['id'].'.pdf';
				//$mpdf->Output($file, 'F'); // сохранение нашего pdf
				die();
			}

			//$breadcrumb['page'] = array();
			$breadcrumb['module'][] = array(
				$product['name'],
				$_SERVER['REQUEST_URI']
			);
			$breadcrumb['module'][] = array(
				$product['nb_name'],
				'/' . $module_url . '/?property_type=' . $product['property_type'].'&location[]=' . $product['neighborhood']
			);
			if ($base==2) {
				$breadcrumb['module'][] = array(
					$config['object_groups'][$product['property_type']]['name'],
					'/' . $module_url . '/?property_type=' . $product['property_type']
				);
			}
			else {
				$breadcrumb['module'][] = array(
					$config['ny_property_types'][$product['property_type']],
					'/' . $modules['shop_ny'] . '/?property_type=' . $product['property_type']
				);
			}

			$html['is_product'] = true;
			$html['search'] = html_array('shop/search');

			unset($product['bimg']);
			$page = array_merge($page, $product);
			$random = random_hash($page);
			//формировка title
			$page['title'] = '';
			//FL
			if ($base==2) {
				$page['title'] .= 'Look at ';
				//property_type
				if ($page['property_type2']) {
					$page['title'].= str_replace(array('"',','),array(' ','/'),$page['property_type2']);
				}
				else {
					$page['title'].= ' '.$config['object_groups'][$page['property_type']]['name'];
				}
				//добавочное слово
				if ($page['property_type']==1 AND $page['property_type2']=='') {
					//в хомс без проперти тайп нед добавочного слова чтобы не дублировалось
				}
				else {
					if (in_array($page['property_type'], array(1,6))) {
						$page['title'] .= ' ' . random_template('house', $random);
					}
					elseif (in_array($page['property_type'], array(2))) {
						$page['title'] .= ' Apartment';
					}
					//residential
					elseif (in_array($page['property_type'],array(3))) {
						if (in_array($page['property_type2'],array('"efficiency"','"condo"'))) {
							$page['title'] .= ' Apartment';
						}
						elseif (in_array($page['property_type2'],array('"multifamily"','"single"'))) {
							$page['title'] .= ' ' . random_template('house', $random); //' Houses';
						}
					}
					else {
						$page['title'] .= ' ' . random_template('property', $random);
					}
				}
			}
			else {
				//property_type
				if ($page['property_type2']) {
					$page['title'].= str_replace(array('"',','),array(' ','/'),$page['property_type2']);
				}
				else {
					$page['title'] .= ' ' . $config['ny_property_types'][$page['property_type']];
				}
				//добавочное слово
				if (in_array($page['property_type'],array(1))) {
					if (in_array($page['property_type2'], array('"Condo"', '"Co-Op"'))) {
						$page['title'] .= ' Apartment';
					}
					else {
						$page['title'] .= ' ' . random_template('house', $random);
					}
				}
				else {
					$page['title'] .= ' ' . random_template('property', $random);
				}
			}
			$page['title'] .= ' at ';
			$page['title'] .= $page['name'];
			$page['title'].= ' in ';
			//location
			$page['title'].= $page['nb_name'];
			//FL
			if ($base==2) {
				$page['title'].= ', FL ';
			}
			//NY
			else {
				if ($page['city']) {
					$page['title'] .= ', '.mysql_select("SELECT name FROM shop_cities WHERE id =" . $page['city'], 'string') . '';
				}
				$page['title'].= ', NY ';
			}
			$page['title'].= $page['zip_code'];
			//$page['title'].= ' - Wilk Real Estate I LLC';

			$html['content'] = (!isset($_GET['print'])) ? html_array('shop/product_text', $page) : html_array('shop/product_print', $page);
		}
		else {
			//[4.438] Исправить ошибки страниц (гугл отлавливает огромное количество страниц с 404 ошибкой)
			//делаем 301 редирект на страницу каталога
			header('HTTP/1.1 301 Moved Permanently');
			//NY
			$post = array();
			if ($base==1) {
				$post['property_type'] = array_search($u[2],$config['ny_property_types']);

			}
			if ($base==2) {
				$post['property_type'] = array_search($u[2],$config['sf_property_type_url']);
			}
			if ($neighborhood = mysql_select("SELECT id FROM shop_neighborhoods WHERE url='".mysql_res($u[3])."'",'string')) {
				$post['location'][] = $neighborhood;
			}
			$url = http_build_query($post);
			die(header('location:/'.$module_url.'/?'.$url));
			//$error++;
		}
	}
	elseif ($u[3]) $error++;
	else {
		//if (isset($_GET['property_type'])) $_GET['group'] = $_GET['property_type'];
		//фильтр поиска
		$fields = array(
			'residential'=>'int',
			'property_type' => 'int',
			'property_type2'=>'text',
			'quick' => 'text',
			'city'=>'int',
			'location' => 'array_int',
			'beds' => 'text',
			'baths' => 'int',
			'price_min' => 'price',
			'price_max' => 'price',
			'quick'=>'text',
			'view'=>'text',
			'source'=>'text'
		);
		if ($u[2]) {
			//$post = form_smart($fields, form_url($u[2]));
			$data = explode('-',$u[2]);
			//print_r($data);
			if (count($data)==3 OR count($data)==4) {
				if (in_array($data[0],$config['object_residential_url'])) {
					$post['residential'] = array_search($data[0],$config['object_residential_url']);
					if ($post['location'][] = mysql_select("
							SELECT id FROM shop_neighborhoods WHERE url2='".mysql_res($data[1])."'
						",'string')) {
						if (
							($base==1 AND in_array($data[2],$config['ny_property_types']))
							OR
							($base==2 AND in_array($data[2],$config['sf_property_type_url']))
						) {
							if ($base==1) {
								$post['property_type'] = array_search($data[2],$config['ny_property_types']);
								if ($data[3]) {
									if (array_key_exists($data[3],$config['ny_property_types_1_url'])) {
										$post['property_type2'] = $config['ny_property_types_1_url'][$data[3]];
									}
									else {
										//echo 4;
										$error++;
									}
								}
							}
							else $post['property_type'] = array_search($data[2],$config['sf_property_type_url']);
						}
						else {
							//echo 3;
							$error++;
						}
					}
					else {
						//echo 2;
						$error++;
					}
				}
				else {
					$error++;
					//echo 1;
				}

			}
			else {
				$error++;
				//echo '0'.count($data);
			}
		}
		else $post = form_smart($fields, stripslashes_smart($_GET));

		$random = random_hash($post);

		$where = $select = '';
		if ($post['source']=='subscribe') {
			$html['source'] = 'subscribe';
			$today = date('Y-m-d');
			$where.= " AND sp.date_change + interval 5 day > '".$today."'";
		}

		//property_type
		if ($post['property_type']) {
			$where .= " AND sp.property_type = ".$post['property_type'];
			//property_type2
			if ($post['property_type2']) {
				$post['property_type2'] = str_replace(array('%2F','&amp;'),array('/','&'),$post['property_type2']);
				$where .= " AND sp.property_type2 LIKE '%" . mysql_res(mb_strtolower($post['property_type2'])) . "%'";
			}

		}

		//количество кроватей, ванн, цена
		if ($post['beds']=='studio') $where .= " AND sp.beds = 0 ";
		else {
			//$post['beds'] = intval($post['beds']);
			if($post['beds'] == 5) $where .= " AND sp.beds >=5 ";
			elseif ($post['beds']) $where .= " AND sp.beds = " . intval($post['beds']);
		}
		$where .= $post['baths'] ? " AND sp.baths = " . $post['baths'] . " " : '';
		if ($post['price_min']) $where .= " AND sp.price >= " . $post['price_min'];
		if ($post['price_max']) $where .= " AND sp.price <= " . $post['price_max'];

		//neighborhoods
		if ($post['city']) $where .= " AND sp.city = ".$post['city'];
		$neighborhoods = $zip_codes = false;
		if (count($post['location']) > 0) {
			$neighborhoods2 = mysql_select("SELECT id,name,zip_codes FROM shop_neighborhoods WHERE id IN (" . implode(',', $post['location']) . ")", 'rows_id');
			//$where .= $neighborhoods2 ? " AND sp.neighborhood IN (" . implode(',', array_keys($neighborhoods2)) . ") " : '';
			if ($neighborhoods2) foreach ($neighborhoods2 as $k=>$v) {
				$neighborhoods[$k] = $v['name'];
				$zip_codes.= ' '.$v['zip_codes'];
			}
			if ($zip_codes) {
				$zip_codes = trim($zip_codes);
				$zip_codes = str_replace(' ',',',$zip_codes);
				$zip_codes = str_replace(',,',',',$zip_codes);
				$zip_codes = str_replace(',,',',',$zip_codes);
				//$zip_codes = str_replace(',','|',$zip_codes);
			}
			if ($zip_codes) {
				$where.= " AND (sp.neighborhood IN (" . implode(',', array_keys($neighborhoods2)).") ";
				$zip_codes = explode(',',$zip_codes);
				foreach ($zip_codes as $k=>$v) {
					$where.= " OR sp.zip_code LIKE '".intval($v)."' ";
				}
				$where.= " ) ";
			}
			else $where .= $neighborhoods2 ? " AND sp.neighborhood IN (" . implode(',', array_keys($neighborhoods2)) . ") " : '';
		}

		if (count($neighborhoods)==1) {
			$page['text'] = mysql_select("SELECT text FROM shop_neighborhoods WHERE id=".key($neighborhoods),'string');
		}
		//если не выбран location то показываем всю флориду
		//if ($neighborhoods==false) $where.=" AND sp.city=6";

		//сортировка только по цене
		if (@$_GET['sort']) {
			$view = explode('-', $_GET['sort']);
			if (in_array($view[0], $view_type)) {
				$order_by = " sp.{$view[0]} " . ((in_array($view[1], $view_order)) ? strtoupper($view[1]) : 'DESC') . ', ';
			}
		}

		//аренда или продажа
		if ($post['residential']) $where.=" AND sp.residential=".$post['residential'];

		//быстрый поиск
		if ($quick) {
			$quick2 = mb_strtolower($quick);
			$quick3 = mb_strtolower(name_search($quick));
			$int = preg_replace('~[^0-9]+~u','', $quick2);//делаем только цифры
			//если ввели только цифры то ищем по зип и по млн
			if ($int==$quick2) {
				$where.= ' AND (';
				$where .= " LOWER (sp.zip_code) LIKE '{$quick2}' ";
				$where .= " OR LOWER (sp.mln) LIKE '%{$quick2}%' ";
				$where .= " OR LOWER (sp.uid) LIKE '%{$quick2}%' ";
				$where .= ')';
			}
			//поиск по WR и WRS
			elseif ('wr'.$int==$quick2 OR 'wrs'.$int==$quick2) {
				$where.= ' AND (';
				$where .= "  sp.uid = '{$int}' ";
				$where .= ')';
			}
			//поиск по словам
			else {
				$where.= ' AND (';
				$where .= " LOWER (sp.name) LIKE '%{$quick3}%' ";
				$where .= " OR LOWER (neighborhood.name) LIKE '%{$quick2}%' ";
				$where .= " OR LOWER (neighborhood.name2) LIKE '%{$quick2}%' ";
				if ($base==2) {
					$where .= " OR LOWER (sp.complex_name) LIKE '%{$quick2}%' ";
					$where .= " OR LOWER (sp.development_name) LIKE '%{$quick2}%' ";
				}
				$where .= ')';
			}

		}

		//формировка title
		$page['title'] = $page['name'] = '';
		$page['name'] = random_template('to choose',$random);
		$page['name'].= ' Your ';
		$page['name'].= random_template('beautiful',$random+1).' ';
		//location
		//$page['title'].= ' in ';
		if ($neighborhoods) {
			$page['title'] .= implode(' / ', $neighborhoods);
		}
		//SF
		if ($base==2) {
			if (!$neighborhoods) $page['title'] .= ' Florida ';
			else $page['title'] .= ' FL ';
			//property_type
			if (isset($config['object_groups'][$post['property_type']])) {
				if ($post['property_type2']
					//AND isset($config['object_groups_'.$post['property_type']])
					//AND in_array($post['property_type2'],$config['object_groups_'.$post['property_type']])
				) {
					$page['title'].= ' '.$post['property_type2'];
					$page['name'].= ' '.$post['property_type2'];
					//добавочное слово
					if (in_array($post['property_type'],array(2))) {
						$page['title'] .= ' Apartments';
						$page['name'] .= ' Apartments';
					}
					//residential
					elseif (in_array($post['property_type'],array(3))) {
						if (in_array($post['property_type2'],array('Efficiency','Condo'))) {
							$page['title'] .= ' Apartments';
							$page['name'] .= ' Apartments';
						}
						elseif (in_array($post['property_type2'],array('Multifamily','Single'))) {
							$page['title'] .= ' '.random_template('houses',$random); //' Houses';
							$page['name'] .= ' '.random_template('houses',$random); //' Houses';
						}
					}
					//commercial
					elseif (in_array($post['property_type'],array(4))) {
						$page['title'] .= ' ' . random_template('real estate', $random);
						$page['name'] .= ' ' . random_template('real estate', $random);
					}
					else {
						$page['title'] .= ' '.random_template('houses',$random); //' Houses';
						$page['name'] .= ' '.random_template('houses',$random); //' Houses';
					}

				}
				else {
					$page['title'].= ' '.$config['object_groups'][$post['property_type']]['name'];
					$page['name'].= ' '.$config['object_groups'][$post['property_type']]['name'];
					if ($post['property_type']==2) {
						$page['title'] .= ' Apartments';
						$page['name'] .= ' Apartments';
					}
					if (in_array($post['property_type'],array(3,4,5,6))) {
						$page['title'] .= ' ' . random_template('real estate', $random);
						$page['name'] .= ' ' . random_template('real estate', $random);
					}
				}
			}
			else {
				$page['title'].= 'Real Estate Properties';
				$page['name'].= 'Real Estate Properties';
			}
		}
		//NY
		else {
			if ($post['city']) {
				$page['title'] .= ' ' . mysql_select("SELECT name FROM shop_cities WHERE id =" . $post['city'], 'string') . ' ';
			}
			if (!$neighborhoods AND !$post['city']) $page['title'] .= ' New York ';
			else $page['title'] .= ' NY ';
			//property_type
			//$page['title'].= ' of ';
			if (isset($config['ny_property_types'][$post['property_type']])) {
				if ($post['property_type2']
					//AND isset($config['ny_property_types_' . $post['property_type']])
					//AND in_array($post['property_type2'], $config['ny_property_types_' . $post['property_type']])
				) {
					$page['title'] .= ' ' . $post['property_type2'];
					$page['name'] .= ' ' . $post['property_type2'];
					//commercial + land
					if (in_array($post['property_type'],array(2,3))) {
						$page['title'] .= ' ' . random_template('real estate', $random);
						$page['name'] .= ' ' . random_template('real estate', $random);
					}
					else {
						if (in_array($post['property_type2'], array('Condo', 'Co-Op'))) {
							$page['title'] .= ' Apartments';
							$page['name'] .= ' Apartments';
						}
						else {
							$page['title'] .= ' ' . random_template('houses', $random); //' Houses';
							$page['name'] .= ' ' . random_template('houses', $random); //' Houses';
						}
					}
				}
				else {
					$page['title'] .= ' ' . $config['ny_property_types'][$post['property_type']];
					$page['name'] .= ' ' . $config['ny_property_types'][$post['property_type']];
					//if (in_array($post['property_type'],array(2,3))) {
						$page['title'] .= ' ' . random_template('real estate', $random);
						$page['name'] .= ' ' . random_template('real estate', $random);
					//}
				}
			}
			else {
				$page['title'] .= ' Real Estate Properties';
				$page['name'] .= ' Real Estate Properties';
			}
		}
		//beds or baths
		if ($post['beds'] OR $post['baths']) {
			$page['title'].= ' with ';
			$page['name'].= ' with ';
			if ($post['beds']) {
				if ($post['beds']=='studio') {
					$page['title'].= ' Studio ';
					$page['name'].= ' Studio ';
				}
				else {
					$page['title'].= $post['beds'].' Bedrooms ';
					$page['name'].= $post['beds'].' Bedrooms ';
				}
			}
			if ($post['baths']) {
				if ($post['beds']) {
					$page['title'].= '/ ';
					$page['name'].= '/ ';
				}
				$page['title'].= $post['baths'].' Baths ';
				$page['name'].= $post['baths'].' Baths ';
			}
		}
		//residential
		if (isset($config['object_residential'][$post['residential']])) {
			if ($post['residential']==1) {
				$page['title'].= ' For Rent';
				$page['name'].= ' For Rent';
			}
			else {
				$page['title'].= ' For Sale';
				$page['name'].= ' For Sale';
			}
		}
		else {
			$page['title'].= ' For Sale and Rental';
			$page['name'].= ' For Sale and Rental';
		}
		//price
		if ($post['price_min'] OR $post['price_max']) {
			$page['title'] = trim($page['title']);
			$page['name'] = trim($page['name']);
			//$page['title'].= '. Priced ';
			if ($post['price_min']) {
				$page['title'].= ' from $'.number_format($post['price_min'],0,'.',',');
				$page['name'].= ' from $'.number_format($post['price_min'],0,'.',',');
			}
			if ($post['price_max']) {
				$page['title'].= ' to $'.number_format($post['price_max'],0,'.',',');
				$page['name'].= ' to $'.number_format($post['price_max'],0,'.',',');
			}
		}

		if ($neighborhoods) {
			$page['name'] .= ' in '.implode(' / ', $neighborhoods);
		}
		if ($base==2) {
			if (!$neighborhoods) $page['name'] .= ' in Florida ';
			else $page['name'] .= ', Florida ';
		}
		else {
			if ($post['city']) {
				if (!$neighborhoods) $page['name'] .= ' in ';
				$page['name'] .= ' ' . mysql_select("SELECT name FROM shop_cities WHERE id =" . $post['city'], 'string') . '';
			}
			if (!$neighborhoods AND !$post['city']) $page['name'] .= ' in New York ';
			else $page['name'] .= ', New York';
		}

		$page['title'].= ' - Wilk Real Estate I LLC';
		//$page['name'].= ' - Wilk Real Estate I LLC';

		if ($where=='')  $page['name'] = 'Featured Properties';



		//если без гет параметров то только с галками
		if ($base==2) {
			if ($where == '' AND @$_GET['sort']=='') {
				$where .= " AND sp.city=6";
				$where .= ' AND sp.special=1';
				$select = ', IF (property_type=6,1,property_type) sort ';
				$order_by = ' sort, property_type DESC, sp.date_change DESC, ';
			}
			$where.= " AND ( (sp.base=2 AND sp.status = 'A') OR sp.base=1) ";
		}
		else {
			if ($where == '' AND @$_GET['sort']=='') {
				//$where.= " AND sp.city=6";
				$where.= ' AND sp.special=1';
				//$select = ', IF (property_type=1 && property_type2=4,1,0) sort ';
				$select.= ", IF (status='Contract',1,0) sort1 ";
				$select.= ", IF (property_type2 LIKE '%Single Family%',0,1) sort2 ";
				$select.= ", IF (property_type2 LIKE '%Multi-Family%',0,1) sort3 ";
				$select.= ", IF (property_type2 LIKE '%Condo%',0,1) sort4 ";
				$select.= ", IF (property_type2 LIKE '%Co-Op%',0,1) sort5 ";
				$order_by = ' sp.property_type, sort1, sort2, sort3, sort4, sort5, sp.date_change DESC,';
				//property_type=1&property_type_1=4
			}
			else {
				//[4.401] Сортировка по цене не работает
				if ($order_by=='') {
					$select = ", IF (status='Contract' && special=0,0,1) sort2 ";
					$order_by = ' sort2 DESC, ';
				}
			}
			$where.= " AND (sp.base=3 OR sp.base=4) ";
		}


		// если есть, то получаем данные для СЕО категория / район
		/*
		if (isset($_GET['location']) && is_array($_GET['location']) && (count($_GET['location']) == 1)) {
			$location_id = intval($_GET['location'][0]);
			if ($category) // если есть категория, ищем связку категория - район
				$seo_text = mysql_select("SELECT s.name, s.text, s.title, s.keywords, s.description FROM shop_neighborhoods_seo AS s WHERE s.category = {$category['id']} AND s.neighborhood = {$location_id} AND s.display = 1 LIMIT 1", 'row');
			else // если нет, берем данные из района
				$seo_text = mysql_select("SELECT s.name, s.text, s.title, s.keywords, s.description FROM shop_neighborhoods AS s WHERE s.id = {$location_id} AND s.display = 1 LIMIT 1", 'row');
			if ($seo_text) {
				$page = array_merge($page, $seo_text);
			}
		}*/


		$limit = ''; //print_R($post);
		if (@$html['source']!= 'subscribe') $html['search'] = html_array('shop/search', $post);
		$html['filter'] = html_array('shop/filter', $page);
		if ($post['view']=='map') {
			$where.= " AND sp.lat!=''";
			$limit = ' LIMIT 1000';
		}
		$query = "
	            SELECT sp.*,
		            neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
		             $select
	            FROM shop_products AS sp
	            LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
	            WHERE sp.display = 1 {$where}
	            ORDER BY {$order_by} sp.special DESC, sp.date_change DESC
	            $limit
	    	";
		//echo $query;
		if ($post['view']=='map') {
			$html['content'] = html_query('shop/product_map', $query, false, 60 * 60);
		}
		else {
			$html['content'] = html_query('shop/product_list shop', $query, false, 60 * 60);
			if (@$_GET['action'] == 'pagination') die($html['content']);
		}
		//echo mysql_error();

		$breadcrumb['page'][0] = array(
			$page['name'],
			'/' . $state['url'] . '/' . $module_url . '/'
		);
	}
}