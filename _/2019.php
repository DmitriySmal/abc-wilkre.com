<?php

define('ROOT_DIR', dirname(__FILE__).'/../');
require_once(ROOT_DIR.'_config.php');  //динамические настройки
require_once(ROOT_DIR.'_config2.php'); //установка настроек
require_once(ROOT_DIR.'_config_ny.php'); //установка настроек
require_once(ROOT_DIR.'functions/mysql_func.php'); //установка настроек
require_once(ROOT_DIR.'functions/common_func.php'); //установка настроек


$file = ROOT_DIR.'_/2019-2.csv';

$height = 106;
if (@$_GET['height']) $height = $_GET['height'];

$data = array();
$handle = fopen($file, 'r');
while (($value = fgetcsv($handle, 8000, ',')) !== FALSE) {
	$data[] = $value;
}
fclose($handle);

/*
 *
 *
 */

ob_start(); // echo to buffer, not screen
?>
<style>
    @page
    {
        size:  auto;
        margin: 8mm 0mm 0mm 5mm;
    }
</style>
<?php
$i = 0;
foreach ($data as $q) {
	$i++;
	if (fmod($i,3)==0) $padding = '30px 0px 10px 40px';
	else $padding = '30px 20px 10px 20px';
	?>
	<div style="width: 33%; float: left; height: <?=$height?>px">
		<div style=" padding:<?=$padding?>">
			<?=$q[0]?> <?=$q[1]?> <?=$q[2]?> <?=$q[3]?> <?=$q[4]?>
			<br><?=$q[5]?>, <?=$q[6]?> <?=$q[7]?>
		</div>
	</div>
	<?php

    if (fmod($i,30)==0) {
        ?>
        <div style="width:100%; clear:both; page-break-before: always;"></div>
        <?php
    }
}
$content = ob_get_clean(); // get buffer contents

if (@$_GET['pdf']) {
	include(ROOT_DIR . "plugins/MPDF56/mpdf.php");

	//$mpdf = new mPDF();
	$mpdf = new mPDF('utf-8', 'A4', '12', '', 0, 0, 0, 0, 0, 0); // Создаем экземпляр класс
	$mpdf->WriteHTML($content);
	$mpdf->Output();
	exit;
}
else {
    echo $content;
}
