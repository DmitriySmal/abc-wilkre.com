<?php
/*
 * Скрипт для проставления property_type
 *
 */

define('ROOT_DIR', dirname(__FILE__).'/../');
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек
include_once (ROOT_DIR.'functions/file_func.php');
require_once(ROOT_DIR.'functions/mysql_func.php');
require_once(ROOT_DIR.'functions/string_func.php');
require_once(ROOT_DIR.'functions/image_func.php');

mysql_connect_db();
require(ROOT_DIR.'plugins/phpQuery/phpQuery/phpQuery.php');

set_time_limit(360);

$config['neighborhoods'] = mysql_select("SELECT id,LOWER(name) name FROM shop_neighborhoods WHERE city=6",'array',60*60);

//echo ROOT_DIR;
$query = 'SELECT * FROM shop_preconstructions WHERE display=0 ORDEr BY id';
//$query = 'SELECT * FROM shop_preconstructions WHERE id=1';
if ($q = mysql_select($query,'row')) {
	$url = 'http://www.preconstructioncondosofmiami.com/properties/'.$q['url'].'/';
	$results = file_get_contents($url);
	$document = phpQuery::newDocument($results);
	$data = $document->find('.sixprop.propcol');

	$post = array();

//название
	$post['name'] = trim(pq($data)->find('.entry-title-prop')->text());

	$post['lat'] = pq($data)->find('#gmap_wrapper')->attr('data-cur_lat');
	$post['lng'] = pq($data)->find('#gmap_wrapper')->attr('data-cur_long');
	$post['coordinates'] = '(' . $post['lat'].',' . $post['lng'].')';

//адрес
	$post['address'] = trim(pq($data)->find('#prop_des')->text());
//$post['neighborhood'] = trim(pq($data)->find('#prop_des a')->text());
	$address = explode(',', $post['address']);
	$post['address'] = trim($address[0]);
	$neighborhood = trim($address[1]); //echo $neighborhood;
	//$post['neighborhoods'] = $neighborhood;
	foreach ($config['neighborhoods'] as $k=>$v) {
		$str1 = preg_replace('~[^a-z]+~u', '', strtolower($neighborhood));
		$str2 = preg_replace('~[^a-z]+~u', '', strtolower($v));
		if ($str1==$str2) {
			$post['neighborhood'] = $k;
			echo $v;
			break;
		}
		else {
			//echo '-'.$str1.'-'.$str2.'-<br>';
		}
	}
	$post['city'] = 6;

//https://www.youtube.com/watch?v=Q6DCVnLM7nc
//$post['imgs']

	if ($video = pq($data)->find('#main-carusel .video_thumb')) {
		if ($str = pq($video)->attr('data-video_id')) $post['video'] = 'https://www.youtube.com/watch?v=' . $str;
	}
//фото
//$img_box = trim(pq($data)->find('#main-carusel')->text());
	$images = pq($data)->find('#main-carusel a');
	/*foreach ($images as $li) {
		$post['imgs'][] = array(
			'file' => pq($li)->attr('href')
		);
	}*/

//описание
	$post['text'] = '';
	$description = pq($data)->find('.property_description');
	pq($description)->find('ul .regRequiredLink:first-child')->parent('li')->parent('ul')->addClass('delete')->prev()->addClass('delete')->prev()->addClass('delete');
	pq($description)->find('.floorplan-table')->addClass('delete');

	$h5 = pq($description)->find('h5');
	foreach ($h5 as $li) {
		$text = pq($li)->text();
		if (substr($text, 0, 20) == 'For more information') pq($li)->addClass('delete');
	}
	pq($description)->find('.delete')->remove();
	pq($description)->find('h4:last-child')->remove();
	pq($description)->find('hr')->remove();
	pq($description)->find('h4:last-child')->remove();
	pq($description)->find('hr')->remove();

	$post['text'] = pq($description)->html();
	/*pq($description)->find('p')->addClass('p');
	$box = pq($description)->find('h4:first-child');
	//echo pq($h4)->text();
	$stop=0;
	while($stop<10) {
		$box = pq($box)->next();
		if (pq($box)->hasClass('p')) {
			$post['text'].= '<p>'.trim(pq($box)->html()).'</p>';
			$stop++;
		}
		else $stop = 100;
	}*/

	$param = pq($data)->find('.prop_details li');
	foreach ($param as $li) {
		$name = trim(trim(pq($li)->find('span')->text()), ':');
		pq($li)->find('span')->remove();
		$text = trim(pq($li)->text());
		if ($name == 'Zip') $post['zip_code'] = $text;
	}

	$param = pq($data)->find('.prop_details_custom p');
	foreach ($param as $li) {
		$name = trim(trim(pq($li)->find('span')->text()), ':');
		pq($li)->find('span')->remove();
		$text = trim(pq($li)->text());
		if ($text) $post['parameters'][$name] = $text;
	}
	$post['parameters'] = count($post['parameters']) ? serialize($post['parameters']) : '';

	$root = ROOT_DIR . 'files/shop_preconstructions/' . $q['id'] . '/';
	if (is_dir($root)) delete_all($root, false); //удаление старого файла
	$root1 = $root . 'img/';
	$root2 = $root . 'imgs/';
	mkdir($root1, 0755, true);
	mkdir($root2, 0755, true);
	$imgs = array();
	$i = 0;
	foreach ($images as $li) {
		$file = pq($li)->attr('href');
		$file = str_replace(' ', '%20', $file);
		if (@fopen($file, "r")) {
			if ($i==0) {
				$post['img'] = basename($file);
				mkdir($root1 ,0755, true);
				$path = $root1 . $post['img'] ; //echo $path;
				copy($file, $path);
				img_process('resize', $path, '100x100', $root1 . 'a-' . $post['img'] );
				img_process('cut', $path, '536x320', $root1 . 'm-' . $post['img'] );
				img_process('resize', $path, '293x117', $root1. 'p-' . $post['img'] );
			}
			else {
				echo $file . '<br>';
				$imgs[$i] = array(
					'name' => basename($file),
					'file' => basename($file),
					'display' => 1
				);
				mkdir($root2 . $i, 0755, true);
				$path = $root2 . $i . '/' . $imgs[$i]['file'];
				copy($file, $path);
				img_process('resize', $path, '100x100', $root2 . $i . '/a-' . $imgs[$i]['file']);
				img_process('cut', $path, '536x320', $root2 . $i . '/m-' . $imgs[$i]['file']);
				img_process('resize', $path, '293x117', $root2 . $i . '/p-' . $imgs[$i]['file']);
			}
			$i++;
		} else echo 'not ' . $file . '<br>';
	}
	$post['imgs'] = count($imgs) ? serialize($imgs) : '';
	/**/

	$post['keywords'] = $post['name'];
	$post['description'] = $post['name'];
	$post['display'] = 1;
	$post['id'] = $q['id'];
	mysql_fn('update','shop_preconstructions',$post);
	echo '<table>';
	foreach ($post as $k => $v) {
		echo '<tr>';
		echo '<td>' . $k . '</td>';
		echo '<td><textarea style="width:400px">';
		if (is_array($v)) {
			print_r($v);
			echo '
';
		} else echo $v;
		echo '</textarea></td>';
		echo '</tr>';
	}
	echo '</table>';
	echo '<script type="text/javascript">location.reload();</script>';

}
