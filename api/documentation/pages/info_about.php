API предназначено для поиска по каталогам недвижимости
<br>- <a href="http://www.wilkrealestate.com/new-york/" target="_blank">New York</a>
<br>- <a href="http://www.wilkrealestate.com/south-florida/" target="_blank">South Florida</a>
<br>а так же для создания и обработки заявок объектам по недвижимости

<br><br>
Все запросы осуществляются по адресу <kbd>http://wilkre.com/api/{key}</kbd> (key - идентификатор API) с передачей нужных GET или POST параметров и headers описанных в данной документации.