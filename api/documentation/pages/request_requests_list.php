На данной старнице приведено описание запросов и ответов сервера для получения агентом его списка заявок

<h3>Описание запроса на сервер</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td style="color:darkred">token</td>
			<td>string</td>
			<td>Бессрочный одноразовый token для авторизированного действия</td>
		</tr>
	</tbody>
</table>
<small style="color:darkred">* красным выделены обязательные параметры</small>


<h3>Пример запроса</h3>
<pre>http://wilkre.com/api/requests_list?token=d8e25a1da1f12de5f3361181c1e31c2b</pre>


<h3>Описание ответа сервера</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td>error</td>
			<td>tinyint</td>
			<td>ID ошибки
				<br>0 - успешно
				<br>601 - не корректный token
			</td>
		</tr>
		<tr>
			<td style="color:darkblue">token</td>
			<td>string</td>
			<td>Бессрочный одноразовый token для последующего авторизированного действия</td>
		</tr>
		<tr>
			<td>requests</td>
			<td>array</td>
			<td>Массив данных заявки</td>
		</tr>
		<?php
$array = array(
	'id' => array('int','ID заявки'),
	'type'=>array('int','2 - Send Request<br>4 - Schedule a Showing'),
	'processed'=>array('boolean','0 - не обработано<br>1 - обработано'),
	'date'=>array('date','Дата добавления заявки'),
	'date_shedule'=>array('date','Дата просмотра объекта, только для заявок с type=4'),
	'extension'=>array('int','Продление обработки заявки в часах'),

	'name'=>array('string','Имя клиента'),
	'email'=>array('string','Email клиента'),
	'phone'=>array('string','Телефон клиента'),

	'object'=>array('int','ID объекта недвижимости'),
	'mln' => array('string','MLS номер'),
	'property_type' =>array('string','Тип недвижимости'),
	'residential'=>array('int','1 - аренда<br>2 - продажа'),
	'price'=> array('int','Цена'),
	'beds'=> array('int','Количество кроватей'),
	'baths'=> array('int','Количество ванн'),
	'square'=> array('int','Площадь'),
	'city' =>  array('string','Город'),
	'neighborhood' => array('string','Район'),
	'address' => array('string','Адрес'),
	'img' => array('string','Url картинки'),
);

		foreach ($array as $k=>$v) {
		?>
		<tr>
			<td>requests.<?=$k?></td>
			<td><?=$v[0]?></td>
			<td><?=$v[1]?></td>
		</tr>
		<?php } ?>


	</tbody>
</table>
<small style="color:darkblue">* значение token должно быть сохранено на устройстве и использовано для последующих запросов</small>

<h3>Пример ответа</h3>
<pre>
{
	"error":"0",
	"token":"d8e25a1da1f12de5f3361181c1e31c2b",
	"requests" {
		{
			"id":"167",
			"object":"397308",
			"processed":"1",
			"date":"2016-05-16 20:15:01",
			"date_shedule":"0000-00-00",
			"extension":"3",
			"name":"Alejandra ",
			"email":"test@test.com",
			"phone":"12345678",
			"type":"4",
			"residential":"1",
			"price":"1750",
			"beds":"3",
			"baths":"2",
			"square":"1250",
			"city":"South Florida",
			"neighborhood":"Miami",
			"mln":"A10083648",
			"address":"11831 SW 177th Terrace",
			"property_type":"Land Property",
			"img":"http:\/\/www.wilkrealestate.com\/files\/shop_products\/397308\/img\/397308.jpg"
		}
	}
}
</pre>
