На данной старнице приведено описание запросов и ответов сервера для результатов поиска объектов

<h3>Описание запроса на сервер</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td style="color:darkred">base</td>
			<td>tinyint</td>
			<td>ИД каталога, обязательный параметр
				<br>1 - <a href="http://www.wilkrealestate.com/new-york/" target="_blank">New York</a>
				<br>2 - <a href="http://www.wilkrealestate.com/south-florida/" target="_blank">South Florida</a>
			</td>
		</tr>
		<tr>
			<td>open_house</td>
			<td>boolean</td>
			<td>1 - показываем только недвижимость с датами показа</td>
		</tr>
		<tr>
			<td>our_listings</td>
			<td>boolean</td>
			<td>1 - показываем только свою недвижимость</td>
		</tr>
		<tr>
			<td>featured_properties</td>
			<td>boolean</td>
			<td>1 - показываем только особенную недвижимость</td>
		</tr>
		<tr>
			<td>city</td>
			<td>tinyint</td>
			<td>ИД города<br>используется только при base=1</td>
		</tr>
		<tr>
			<td>neighborhoods</td>
			<td>string</td>
			<td>ИД районов через запятую</td>
		</tr>
		<tr>
			<td>status</td>
			<td>string</td>
			<td>Статус: New, Sold, Contract, Reserved</td>
		</tr>
		<tr>
			<td>residential</td>
			<td>tinyint</td>
			<td>Продажа или аренда
				<br>1 - Аренда
				<br>2 - Продажа
			</td>
		</tr>
		<tr>
			<td>property_type</td>
			<td>tinyint</td>
			<td>Основной вид недвижимости</td>
		</tr>
		<tr>
			<td>property_type2</td>
			<td>string</td>
			<td>Подвид недвижимости</td>
		</tr>
		<tr>
			<td>quick</td>
			<td>string</td>
			<td>Строка быстрого поиска</td>
		</tr>
		<tr>
			<td>price_min</td>
			<td>int</td>
			<td>Цена от</td>
		</tr>
		<tr>
			<td>price_max</td>
			<td>int</td>
			<td>Цена до</td>
		</tr>
		<tr>
			<td>beds</td>
			<td>string</td>
			<td>Количество спален
				<br>studio - студия
				<br>1 - 1 спалня
				<br>...
				<br>5 - 5 и больше
			</td>
		</tr>
		<tr>
			<td>baths</td>
			<td>tinyint</td>
			<td>Количество ванн
				<br>1 - 1 ванна
				<br>...
				<br>5 - 5 и больше</td>
		</tr>
		<tr>
			<td>n</td>
			<td>int</td>
			<td>Номер страницы с результатом поиска</td>
		</tr>
		<tr>
			<td>count</td>
			<td>int</td>
			<td>Количество результатов поиска на странице</td>
		</tr>
	</tbody>
</table>
<small style="color:darkred">* красным выделены обязательные параметры</small>

<h3>Пример запроса</h3>
<pre>http://wilkre.com/api/objects_list?base=1&city=7&residential=1&property_type=1</pre>


<h3>Описание ответа сервера</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td>error</td>
			<td>tinyint</td>
			<td>ID ошибки [100-200]
				<br>0 - успешно
				<br>101 - не правильно указан параметр base
			</td>
		</tr>
		<tr>
			<td>num_rows</td>
			<td>int</td>
			<td>Общее количество записей для запроса</td>
		</tr>
		<tr>
			<td>objects</td>
			<td>array</td>
			<td>Данные объекта</td>
		</tr>

		<?php
		$array = array(
			'id'=>array('int','ID объекта недвижимости'),
			'mln' => array('string','MLS номер'),
			'property_type' =>array('string','Тип недвижимости'),
			'residential'=>array('int','1 - аренда<br>2 - продажа'),
			'price'=> array('int','Цена'),
			'beds'=> array('int','Количество кроватей'),
			'baths'=> array('int','Количество ванн'),
			'square'=> array('int','Площадь'),
			'city' =>  array('string','Город'),
			'neighborhood' => array('string','Район'),
			'address' => array('string','Адрес'),
			'img' => array('string','Url картинки'),
		);
		foreach ($array as $k=>$v) {
			?>
			<tr>
				<td>:.. <?=$k?></td>
				<td><?=$v[0]?></td>
				<td><?=$v[1]?></td>
			</tr>
		<?php } ?>
		<tr>
			<td>:.. office_name</td>
			<td>string</td>
			<td>Данные о риелторе</td>
		</tr>
		<tr>
			<td>:.. open_house</td>
			<td>array</td>
			<td>Данные по датам просмотра объкта</td>
		</tr>
		<?php
		$array = array(
			'date'=>array('date','Дата просмотра'),
			'from' => array('string','Время начала просмотра'),
			'to' =>array('string','Время окончания просмотра'),
			'agent_name'=>array('string','Имя агента'),
			'agent_phone'=> array('string','Телуфон агента'),
		);
		foreach ($array as $k=>$v) {
			?>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;:.. <?=$k?></td>
				<td><?=$v[0]?></td>
				<td><?=$v[1]?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>

<h3>Пример ответа</h3>
<pre>
{
	"error": "0",
	"num_rows": "24",
	"objects": {
		{
			"id": "1",
			"mln": "399107",
			"property_type": "Condo",
			"price": "1199000",
			"img": "http://www.wilkrealestate.com/files/shop_products/343702/img/343702.jpg",
			"address": "2040 East 12 Street",
			"city": "Brooklyn",
			"neighborhood": "Homecrest",
			"status": "Contract",
			"beds": "1",
			"baths": "1",
			"square": "3705",
			"office_name": "WILK REAL ESTATE I LLC"
		}
	}
}
</pre>
