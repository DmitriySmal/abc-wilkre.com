На данной старнице приведено описание запросов и ответов сервера для авторизации агента в системе

<h3>Описание запроса на сервер</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td style="color:darkred">email</td>
			<td>string</td>
			<td>Email агента</td>
		</tr>
		<tr>
			<td style="color:darkred">hash</td>
			<td>string</td>
			<td>md5({email}.md5({password}))</td>
		</tr>
		<tr>
			<td style="color:darkred">uid</td>
			<td>string</td>
			<td>ID устройства для дальнейшего PUSH уведомления</td>
		</tr>
	</tbody>
</table>
<small style="color:darkred">* красным выделены обязательные параметры</small>


<h3>Пример запроса</h3>
<pre>http://wilkre.com/api/user_auth?&email=test@test.com&hash=d8e25a1da1f12de5f3361181c1e31c2b</pre>
<?php
/*
 * http://wilkre.com/api/user_auth?email=sepoolcom@gmail.com&hash=362749abf7a7a0ec9efab135ad3a5e7b
*/
?>


<h3>Описание ответа сервера</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td>error</td>
			<td>tinyint</td>
			<td>ID ошибки
				<br>0 - успешно
				<br>501 - не все обязательные параметры переданы
				<br>502 - нет агента в системе
				<br>503 - не правильный пароль
				<br>504 - нет прав доступа
			</td>
		</tr>
		<tr>
			<td>email</td>
			<td>string</td>
			<td>Email агента</td>
		</tr>
		<tr>
			<td>name</td>
			<td>string</td>
			<td>Имя агента</td>
		</tr>
		<tr>
			<td>phone</td>
			<td>string</td>
			<td>Телефон агента</td>
		</tr>
		<tr>
			<td style="color:darkblue">token</td>
			<td>string</td>
			<td>Бессрочный одноразовый token для последующего авторизированного действия</td>
		</tr>

	</tbody>
</table>
<small style="color:darkblue">* значение token должно быть сохранено на устройстве и использовано для последующих запросов</small>

<h3>Пример ответа</h3>
<pre>
{
	"error":"0",
	"email":"test@test.ru",
	"name":"Test Testovich",
	"phone":"12345",
	"token":"d8e25a1da1f12de5f3361181c1e31c2b"
}
</pre>
