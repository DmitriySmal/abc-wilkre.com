На данной старнице приведено описание запросов и ответов сервера для формы обратной связи

<h3>Описание запроса на сервер</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td>name</td>
			<td>string</td>
			<td>Имя клиента</td>
		</tr>
        <tr>
            <td>phone</td>
            <td>string</td>
            <td>Телефон клиента</td>
        </tr>
        <tr>
            <td>email</td>
            <td>string</td>
            <td>Email клиента</td>
        </tr>
        <tr>
            <td>text</td>
            <td>string</td>
            <td>Текстовое сообщение</td>
        </tr>
	</tbody>
</table>

<h3>Пример запроса</h3>
<pre>http://wilkre.com/api/feedback?name=test&email=test@test.com</pre>

<h3>Описание ответа сервера</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td>error</td>
			<td>tinyint</td>
			<td>ID ошибки
				<br>0 - успешно
				<br>401 - не все обязательные поля заполнены
			</td>
		</tr>

	</tbody>
</table>

<h3>Пример ответа</h3>
<pre>
{
	"error":"0"
}
</pre>
