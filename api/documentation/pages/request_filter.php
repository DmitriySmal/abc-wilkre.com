На данной старнице приведено описание запросов и ответов сервера для формирования фильтра поиска по каталогу

<h3>Описание запроса на сервер</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td style="color:darkred">base</td>
			<td>tinyint</td>
			<td>ИД каталога, обязательный параметр
				<br>1 - <a href="http://www.wilkrealestate.com/new-york/" target="_blank">New York</a>
				<br>2 - <a href="http://www.wilkrealestate.com/south-florida/" target="_blank">Florida</a>
			</td>
		</tr>
		<tr>
			<td>city</td>
			<td>tinyint</td>
			<td>ИД города
				<br>влияет на список выводимых районов в фильтре
				<br>используется только при base=1
			</td>
		</tr>
		<tr>
			<td>residential</td>
			<td>tinyint</td>
			<td>Продажа или аренда
				<br>влияет на список выводимых подвидов недвижимости
				<br>1 - Аренда
				<br>2 - Продажа
			</td>
		</tr>
		<tr>
			<td>property_type</td>
			<td>tinyint</td>
			<td>Основной вид недвижимости
				<br>влияет на список выводимых подвидов недвижимости
				<br>для разных баз недвижимости список отличается
			</td>
		</tr>
	</tbody>
</table>
<small style="color:darkred">* красным выделены обязательные параметры</small>

<h3>Пример запроса</h3>
<pre>http://wilkre.com/api/filter?base=1&city=7&residential=1&property_type=1</pre>


<h3>Описание ответа сервера</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td>error</td>
			<td>tinyint</td>
			<td>ID ошибки [100-200]
				<br>0 - успешно
				<br>101 - не правильно указан параметр base
			</td>
		</tr>
		<tr>
			<td>property_types</td>
			<td>array</td>
			<td>Список допустимых значений видов недвижимости
				<br>id - ID
				<br>name - название</td>
		</tr>
		<tr>
			<td>property_types2</td>
			<td>array</td>
			<td>Список допустимых значений подвидов недвижимости<br>id - ID
				<br>name - название</td>
		</tr>
		<tr>
			<td>cities</td>
			<td>array</td>
			<td>Список допустимых значений городов<br>id - ID
				<br>name - название</td>
		</tr>
		<tr>
			<td>neighborhoods</td>
			<td>array</td>
			<td>Список допустимых значений районов<br>id - ID
				<br>name - название</td>
		</tr>
	</tbody>
</table>

<h3>Пример ответа</h3>
<pre>
{
	"error": "0",
	"property_types": [
		{"id": "1","name": "Residential"},
		{"id": "2","name": "Commercial"},
		{"id": "3","name": "Land"}
	],
	"property_types2":[
		{"id":"10","name":"Multi-Family"},
		{"id":"11","name":"Condo"},
		{"id":"12","name":"Co-Op"},
		{"id":"13","name":"Single Family"},
		{"id":"14","name":"Mixed Use\/Store & Family"}
	],
	"neighborhoods": [
		{"id": "61","name": "Chinatown"},
		{"id": "62","name": "Chinatown"},
		{"id": "63","name": ""Harlem"},
	]
}
</pre>
