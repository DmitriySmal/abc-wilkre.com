<h3 class="bg-danger" style="padding:20px;">API работает в режиме тестирования, обработка запроса и генерация ответов происходят корректно, но реальный запрос в базе сайта не создает</h3>

На данной старнице приведено описание запросов и ответов сервера для отправки заявки по объекту

<h3>Описание запроса на сервер</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
        <tr>
            <td style="color:darkred">object</td>
            <td>int</td>
            <td>ID объекта недвижимости</td>
        </tr>
        <tr>
            <td style="color:darkred">type</td>
            <td>int</td>
            <td>2 - Send Request
                <br>4 - Schedule a Showing
            </td>
        </tr>
        <tr>
            <td style="color:darkred">phone</td>
            <td>string</td>
            <td>Телефон клиента</td>
        </tr>
        <tr>
            <td style="color:darkred">email</td>
            <td>string</td>
            <td>Email клиента</td>
        </tr>
        <tr>
	        <td>name</td>
	        <td>string</td>
	        <td>Имя клиента</td>
        </tr>
        <tr>
            <td>date</td>
            <td>date</td>
            <td>Дата просмотра объекта, только для type=2
                <br>Дата в формате YYYY-MM-DD</td>
        </tr>
	</tbody>
</table>
<small style="color:darkred">* красным выделены обязательные параметры</small>

<h3>Пример запроса</h3>
<pre>http://wilkre.com/api/request_new?object=328843&type=2&name=test&email=test@test.com</pre>


<h3>Описание ответа сервера</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td>error</td>
			<td>tinyint</td>
			<td>ID ошибки
				<br>0 - успешно
				<br>201 - нет объекта в базе
				<br>301 - не корректный параметр type
				<br>302 - не заполненны обязательные поля
			</td>
		</tr>

	</tbody>
</table>

<h3>Пример ответа</h3>
<pre>
{
	"error":"0"
}
</pre>
