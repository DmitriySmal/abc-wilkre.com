На данной старнице описан push-запрос для уведомления агента о новой заявке

<h3>Описание массива данных сервера</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<?php
$array = array(
	'id' => array('int','ID заявки'),
	'status'=>array('int','1 - Новая заявка<br>2 - Просроченная заявка'),
	'object'=>array('int','ID объекта недвижимости'),
	'name'=>array('string','Имя клиента'),
	'email'=>array('string','Email клиента'),
	'phone'=>array('string','Телефон клиента'),
	'aps' => array('array','Текстовое сообщение (IOS)'),
	':.. alert' => array('string','Текст сообщения'),
	':.. sound' => array('string','Входящий звук сособщения'),
	'message' => array('string','Текстовое сообщение (Android)'),
);

		foreach ($array as $k=>$v) {
		?>
		<tr>
			<td><?=$k?></td>
			<td><?=$v[0]?></td>
			<td><?=$v[1]?></td>
		</tr>
		<?php } ?>


	</tbody>
</table>

<h3>Пример ответа</h3>
<pre>
{
	"id":"167",
	"object":"397308",
	"status":"1",
	"aps":{"alert":"bla-bla","sound":"default"},
	"message":{"bla-bla"}
}
</pre>
