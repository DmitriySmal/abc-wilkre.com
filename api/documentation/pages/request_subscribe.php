На данной старнице приведено описание запросов и ответов сервера для подписки

<h3>Описание запроса на сервер</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td style="color:darkred">base</td>
			<td>tinyint</td>
			<td>ИД каталога, обязательный параметр
				<br>1 - <a href="http://www.wilkrealestate.com/new-york/" target="_blank">New York</a>
				<br>2 - <a href="http://www.wilkrealestate.com/south-florida/" target="_blank">South Florida</a>
			</td>
		</tr>
		<tr>
			<td>city</td>
			<td>tinyint</td>
			<td>ИД города<br>используется только при base=1</td>
		</tr>
		<tr>
			<td>neighborhoods</td>
			<td>string</td>
			<td>ИД районов через запятую</td>
		</tr>
		<tr>
			<td>residential</td>
			<td>tinyint</td>
			<td>Продажа или аренда
				<br>1 - Аренда
				<br>2 - Продажа
			</td>
		</tr>
		<tr>
			<td>property_type</td>
			<td>tinyint</td>
			<td>Основной вид недвижимости</td>
		</tr>
		<tr>
			<td>property_type2</td>
			<td>string</td>
			<td>Подвид недвижимости</td>
		</tr>
		<tr>
			<td>price_min</td>
			<td>int</td>
			<td>Цена от</td>
		</tr>
		<tr>
			<td>price_max</td>
			<td>int</td>
			<td>Цена до</td>
		</tr>
		<tr>
			<td>beds</td>
			<td>string</td>
			<td>Количество спален
				<br>studio - студия
				<br>1 - 1 спалня
				<br>...
				<br>5 - 5 и больше
			</td>
		</tr>
		<tr>
			<td>baths</td>
			<td>tinyint</td>
			<td>Количество ванн
				<br>1 - 1 ванна
				<br>...
				<br>5 - 5 и больше</td>
		</tr>
		<tr>
			<td style="color:darkred">email</td>
			<td>text</td>
			<td>Email для подписки</td>
		</tr>
		<tr>
			<td>day</td>
			<td>array</td>
			<td>Дни недели для рассылки (1 - пн, 2 - вт ... 7 - вс)</td>
		</tr>
		<tr>
			<td>time</td>
			<td>int</td>
			<td>Время рассылки (0 - без выбора, 1 - AM, 2 - PM)</td>
		</tr>
	</tbody>
</table>
<small style="color:darkred">* красным выделены обязательные параметры</small>

<h3>Пример запроса</h3>
<pre>http://wilkre.com/api/subscribe?base=1&city=7&residential=1&property_type=1&day[]=1&day[]=2&email=test@test.ru</pre>


<h3>Описание ответа сервера</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td>error</td>
			<td>tinyint</td>
			<td>ID ошибки [100-200]
				<br>0 - успешно
				<br>101 - не правильно указан параметр base или email
			</td>
		</tr>
	</tbody>
</table>

<h3>Пример ответа</h3>
<pre>
{
	"error": "0",
}
</pre>
