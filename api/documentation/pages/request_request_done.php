На данной старнице приведено описание запросов и ответов сервера для обработки агентом заявки по объекту

<h3>Описание запроса на сервер</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
        <tr>
            <td style="color:darkred">id</td>
            <td>int</td>
            <td>ID заявки в базе</td>
        </tr>
		<tr>
			<td style="color:darkred">token</td>
			<td>string</td>
			<td>Бессрочный одноразовый token для авторизированного действия</td>
		</tr>
	</tbody>
</table>
<small style="color:darkred">* красным выделены обязательные параметры</small>

<h3>Пример запроса</h3>
<pre>http://wilkre.com/api/request_done?id=328843&token=d8e25a1da1f12de5f3361181c1e31c2b</pre>


<h3>Описание ответа сервера</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td>error</td>
			<td>tinyint</td>
			<td>ID ошибки
				<br>0 - успешно
				<br>601 - не корректный token
				<br>701 - не указана заявка
				<br>702 - нет заявки базе
				<br>703 - нет прав для обработки данной заявки
			</td>
		</tr>
		<tr>
			<td style="color:darkblue">token</td>
			<td>string</td>
			<td>Бессрочный одноразовый token для последующего авторизированного действия</td>
		</tr>

	</tbody>
</table>
<small style="color:darkblue">* значение token должно быть сохранено на устройстве и использовано для последующих запросов</small>

<h3>Пример ответа</h3>
<pre>
{
	"error":"0",
	"token":"d8e25a1da1f12de5f3361181c1e31c2b"
}
</pre>
