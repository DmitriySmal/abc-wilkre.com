На данной старнице приведено описание запросов и ответов сервера для объекта недвижимости

<h3>Описание запроса на сервер</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td style="color:darkred">id</td>
			<td>int</td>
			<td>ИД объекта на сайте</td>
		</tr>
	</tbody>
</table>
<small style="color:darkred">* красным выделены обязательные параметры</small>

<h3>Пример запроса</h3>
<pre>http://wilkre.com/api/object_text?id=328843</pre>


<h3>Описание ответа сервера</h3>
<table class="table">
	<thead>
		<tr>
			<td>Ключ</td>
			<td>Тип</td>
			<td>Описание</td>
		<tr>
	</thead>
	<tbody>
		<tr>
			<td>error</td>
			<td>tinyint</td>
			<td>ID ошибки [100-200]
				<br>0 - успешно
				<br>201 - нет объекта в базе
			</td>
		</tr>
		<tr>
			<td>id</td>
			<td>int</td>
			<td>ID</td>
		</tr>
		<tr>
			<td>mln</td>
			<td>string</td>
			<td>MLS номер</td>
		</tr>
		<tr>
			<td>property_type</td>
			<td>string</td>
			<td>Тип недвижимости</td>
		</tr>
		<tr>
			<td>price</td>
			<td>int</td>
			<td>Цена</td>
		</tr>
		<tr>
			<td>img</td>
			<td>string</td>
			<td>Url картинки</td>
		</tr>
		<tr>
			<td>address</td>
			<td>string</td>
			<td>Адрес</td>
		</tr>
		<tr>
			<td>city</td>
			<td>string</td>
			<td>Город</td>
		</tr>
		<tr>
			<td>neighborhood</td>
			<td>string</td>
			<td>Район</td>
		</tr>
		<tr>
			<td>status</td>
			<td>string</td>
			<td>Статус: New, Sold, Contract, Reserved</td>
		</tr>
		<tr>
			<td>parameters</td>
			<td>array</td>
			<td>Массив параметров</td>
		</tr>
		<tr>
			<td>office_name</td>
			<td>string</td>
			<td>Данные о риелторе</td>
		</tr>
		<tr>
			<td>text</td>
			<td>string</td>
			<td>Текстовое описание в html</td>
		</tr>
		<tr>
			<td>lat</td>
			<td>string</td>
			<td>Координаты - широта</td>
		</tr>
		<tr>
			<td>lng</td>
			<td>string</td>
			<td>Координаты - долгота</td>
		</tr>
		<tr>
			<td>agent_name</td>
			<td>string</td>
			<td>Имя агента</td>
		</tr>
		<tr>
			<td>agent_phone</td>
			<td>string</td>
			<td>Телефон агента</td>
		</tr>
		<tr>
			<td>open_house</td>
			<td>array</td>
			<td>Данные по датам просмотра объкта</td>
		</tr>
		<?php
		$array = array(
			'date'=>array('date','Дата просмотра'),
			'from' => array('string','Время начала просмотра'),
			'to' =>array('string','Время окончания просмотра'),
			'agent_name'=>array('string','Имя агента'),
			'agent_phone'=> array('string','Телефон агента'),
		);
		foreach ($array as $k=>$v) {
			?>
			<tr>
				<td>:.. <?=$k?></td>
				<td><?=$v[0]?></td>
				<td><?=$v[1]?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>

<h3>Пример ответа</h3>
<pre>
{
	"error":"0",
	"object":{
		"id":"148651",
		"mln":"395671",
		"address":"3574 Canal Avenue",
		"city":"Brooklyn",
		"neighborhood":"Coney Island",
		"img":"http:\/\/www.wilkrealestate.com\/files\/shop_products\/148651\/img\/148651.jpg",
		"price":"559000",
		"office_name":"WILK REAL ESTATE I LLC",
		"parameters":[
			{"name":"Listing","value":"Exclusive"},
			{"name":"Price","value":"$559,000"},
			{"name":"Square Feet","value":"1520"},
			{"name":"Lot Sq\/Ft","value":"1800"},
			{"name":"Rooms","value":"8"},
			{"name":"Bedrooms","value":"4"},
			{"name":"Full Baths","value":"2"},
			{"name":"Stories","value":"2"},
			{"name":"Construction Type","value":"Attached"},
			{"name":"Basement Type","value":"None"},
			{"name":"Parking","value":"2 Spaces,Parking Area,Private Drive"},
			{"name":"Zoning","value":"R5"},
			{"name":"MLS Number","value":"395671"},
			{"name":"Cross St #1 ","value":"West 36"},
			{"name":"Cross St #2","value":"West 37"}
		],
		"text":"Completely renovated1 fam  duplex on best block in Coney Island4 bed 2 full bath large master with his and her closetsGranite kitchen, heated Florida roomPrivate parking 2 car, private backyard, nice brickwork on the ground in front and back of the houseExcellent for large family.  Wont last!!!",
		"lat":"40.57882300",
		"lng":"-74.00215900",
		"imgs":[
			"http:\/\/www.wilkrealestate.com\/files\/shop_products\/148651\/imgs\/1\/148651_1.jpg",
			"http:\/\/www.wilkrealestate.com\/files\/shop_products\/148651\/imgs\/2\/148651_2.jpg",
			"http:\/\/www.wilkrealestate.com\/files\/shop_products\/148651\/imgs\/3\/148651_3.jpg",
			"http:\/\/www.wilkrealestate.com\/files\/shop_products\/148651\/imgs\/4\/148651_4.jpg",
			"http:\/\/www.wilkrealestate.com\/files\/shop_products\/148651\/imgs\/5\/148651_5.jpg",
			"http:\/\/www.wilkrealestate.com\/files\/shop_products\/148651\/imgs\/6\/148651_6.jpg",
			"http:\/\/www.wilkrealestate.com\/files\/shop_products\/148651\/imgs\/7\/148651_7.jpg",
			"http:\/\/www.wilkrealestate.com\/files\/shop_products\/148651\/imgs\/8\/148651_8.jpg",
			"http:\/\/www.wilkrealestate.com\/files\/shop_products\/148651\/imgs\/9\/148651_9.jpg",
			"http:\/\/www.wilkrealestate.com\/files\/shop_products\/148651\/imgs\/10\/148651_10.jpg",
			"http:\/\/www.wilkrealestate.com\/files\/shop_products\/148651\/imgs\/11\/148651_11.jpg"
		]
	}
}
</pre>
