<?php

define('ROOT_DIR', dirname(__FILE__).'/../');

$pages = array(
	'Основы' => array(
		'info_about'		=> 'Введение',
		//'info_structure'	=> 'Архитектура',
	),
	'Запросы' => array(
		'request_filter'	=> 'filter',
		'request_objects_list'	=> 'objects_list',
		'request_object_text'	=> 'object_text',
		'request_subscribe'	=> 'subscribe',
		'request_feedback'	=> 'feedback',
		'request_user_auth'	=> 'user_auth',
		'request_request_new'	=> 'request_new',
		'request_requests_list'	=> 'requests_list',
		'request_request_done'	=> 'request_done',
	),
	'PUSH' => array(
		'push_request'	=> 'request',
	),
);

$page = 'info_about';
$name = 'Введение';
foreach ($pages as $k=>$v) {
	if (array_key_exists(@$_GET['page'],$v)) {
		$page = $_GET['page'];
		$name = $v[$page];
	}
}

include('template.php');

?>