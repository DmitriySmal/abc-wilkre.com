<?php
//список

$fields = array(
	'base'=>'int',
	'residential'=>'int',
	'property_type' => 'int',
	'property_type2'=>'int',
	'city'=>'int',
	'neighborhoods' => 'string_int',
	'beds' => 'text',
	'baths' => 'int',
	'price_min' => 'price',
	'price_max' => 'price',
	'quick'=>'text',
	'open_house'=>'boolean',
	'n'=>'int',
	'count'=>'int'
);
$post = form_smart($fields, stripslashes_smart($_REQUEST));
if (in_array($post['base'],array(1,2))) {
	$where = '';
	if ($post['base']==2) {
		$where .= " AND ( (sp.base=2 AND sp.status = 'A') OR sp.base=1) ";
	}
	if ($post['base']==1) {
		$where .= " AND (sp.base=3 OR sp.base=4) ";
	}
	//property_type
	if ($post['property_type']) {
		$where .= " AND sp.property_type = ".$post['property_type'];
		//property_type2
		if ($post['property_type2']) {
			if ($property_type2 = mysql_select("SELECT name FROM shop_property_types2 WHERE id=".$post['property_type2'],'string')) {
				$where .= " AND sp.property_type2 LIKE '%" . mysql_res(mb_strtolower($property_type2)) . "%'";
			}
		}
	}
	//количество кроватей, ванн, цена
	if ($post['beds']=='studio') $where .= " AND sp.beds = 0 ";
	else {
		//$post['beds'] = intval($post['beds']);
		if($post['beds'] == 5) $where .= " AND sp.beds >=5 ";
		elseif ($post['beds']) $where .= " AND sp.beds = " . intval($post['beds']);
	}
	$where .= $post['baths'] ? " AND sp.baths = " . $post['baths'] . " " : '';
	if ($post['price_min']) $where .= " AND sp.price >= " . $post['price_min'];
	if ($post['price_max']) $where .= " AND sp.price <= " . $post['price_max'];

	//neighborhoods
	if ($post['city']) $where .= " AND sp.city = ".$post['city'];
	$neighborhoods = $zip_codes = false;
	if ($post['neighborhoods']) {
		$neighborhoods2 = mysql_select("SELECT id,name,zip_codes FROM shop_neighborhoods WHERE id IN (" . $post['neighborhoods'] . ")", 'rows_id');
		//$where .= $neighborhoods2 ? " AND sp.neighborhood IN (" . implode(',', array_keys($neighborhoods2)) . ") " : '';
		if ($neighborhoods2) foreach ($neighborhoods2 as $k=>$v) {
			$neighborhoods[$k] = $v['name'];
			$zip_codes.= ' '.$v['zip_codes'];
		}
		if ($zip_codes) {
			$zip_codes = trim($zip_codes);
			$zip_codes = str_replace(' ',',',$zip_codes);
			$zip_codes = str_replace(',,',',',$zip_codes);
			$zip_codes = str_replace(',,',',',$zip_codes);
			//$zip_codes = str_replace(',','|',$zip_codes);
		}
		if ($zip_codes) {
			$where.= " AND (sp.neighborhood IN (" . implode(',', array_keys($neighborhoods2)).") ";
			$zip_codes = explode(',',$zip_codes);
			foreach ($zip_codes as $k=>$v) {
				$where.= " OR sp.zip_code LIKE '".intval($v)."' ";
			}
			$where.= " ) ";
		}
		else $where .= $neighborhoods2 ? " AND sp.neighborhood IN (" . implode(',', array_keys($neighborhoods2)) . ") " : '';
	}
	if ($post['open_house']) {
		$where.= " AND sp.oh_display=1 AND sp.oh_date>='".date('Y-m-d')."'";
	}
	if ($post['quick']) {
		$quick2 = mb_strtolower($post['quick']);
		$quick3 = mb_strtolower(name_search($quick));
		$int = preg_replace('~[^0-9]+~u','', $quick2);//делаем только цифры
		//если ввели только цифры то ищем по зип и по млн
		if ($int==$quick2) {
			$where.= ' AND (';
			$where .= " LOWER (sp.zip_code) LIKE '{$quick2}' ";
			$where .= " OR LOWER (sp.mln) LIKE '%{$quick2}%' ";
			$where .= " OR LOWER (sp.uid) LIKE '%{$quick2}%' ";
			$where .= ')';
		}
		//поиск по WR и WRS
		elseif ('wr'.$int==$quick2 OR 'wrs'.$int==$quick2) {
			$where.= ' AND (';
			$where .= "  sp.uid = '{$int}' ";
			$where .= ')';
		}
		//поиск по словам
		else {
			$where.= ' AND (';
			$where .= " LOWER (sp.name) LIKE '%{$quick3}%' ";
			$where .= " OR LOWER (neighborhood.name) LIKE '%{$quick2}%' ";
			$where .= " OR LOWER (neighborhood.name2) LIKE '%{$quick2}%' ";
			if ($base==2) {
				$where .= " OR LOWER (sp.complex_name) LIKE '%{$quick2}%' ";
				$where .= " OR LOWER (sp.development_name) LIKE '%{$quick2}%' ";
			}
			$where .= ')';
		}

	}



	$config['cities'] = mysql_select("SELECT id,name FROM shop_cities WHERE display=1 ORDER BY rank",'array');
	$query = "
		SELECT sp.*,
			neighborhood.name neighborhood_name, neighborhood.url neighborhood_url
			FROM shop_products AS sp
		LEFT JOIN shop_neighborhoods AS neighborhood ON neighborhood.id = sp.neighborhood
		WHERE sp.display = 1 ".$where."
		ORDER BY sp.special DESC, sp.date_change DESC
	";
	//номер страницы с записями
	$n = $post['n']>=1 ? $post['n'] : 1;
	//количество записей на страницу
	$c = $post['count']>=1 ? $post['count'] : 20;
	$begin = $n*$c-$c;
	$query.= ' LIMIT '.$begin.','.$c;
	$products = mysql_select($query,'rows');
	$num_rows = mysql_select("
		SELECT sp.id
		FROM shop_products AS sp
		WHERE sp.display = 1 ".$where."
	",'num_rows');
	$data = array(
		'error'=>'0',
		'num_rows'=>$num_rows,
	);
	if ($products) {
		foreach ($products as $k => $q) {
			$array = array(
				'id' => $q['id'],
				'mln' => $q['mln'],
				'property_type' => random_property_type ($q),
				'residential'=>$q['residential'],
				'address' => $q['address'],
				'city' => $config['cities'][$q['city']],
				'neighborhood' => $q['neighborhood_name'],
				'img' => $q['img'] ? 'http://www.wilkrealestate.com/files/shop_products/' . $q['id'] . '/img/' . $q['img'] : '',
				'price'=> $q['price'],
				'beds'=> $q['beds'],
				'baths'=> $q['baths'],
				'square'=> $q['square'],
				'office_name'=> $q['office_name'],
			);
			$config['date'] = date('Y-m-d');
			if ($q['oh_display'] AND strtotime($q['oh_date'])>=strtotime($config['date'])) {
				$agents = mysql_select("SELECT * FROM users WHERE agent=1",'rows_id');
				$oh = unserialize($q['oh_dates']);
				foreach ($oh as $k1=>$v1) if (strtotime($v1['date'])>=strtotime($config['date'])) {
					$array['open_house'][] = array(
						'date'=>$v1['date'],
						'from'=>$v1['from'],
						'to'=>$v1['to'],
						'agent_name'=>$agents[$v1['agent']]['name'],
						'agent_phone'=>$agents[$v1['agent']]['phone']
					);
				}
				$array['open_house_text'] = $q['oh_text'];
			}
			$data['objects'][] = $array;

		}
	}
}
else $data['error'] = 101;
