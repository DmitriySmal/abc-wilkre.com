<?php
//список

$fields = array(
	'base'=>'int',
	'neighborhoods' => 'string_int',
	'open_house'=>'boolean',
	'our_listings'=>'boolean',
	'featured_properties'=>'boolean',
	'n'=>'int',
	'count'=>'int'
);
$post_api = form_smart($fields, stripslashes_smart($_REQUEST));
if (in_array($post_api['base'],array(1,2))) {

	$config['_api'] = true;
	$base = $post_api['base'];
	//перекллючили ньюйорк на матрикс
	if ($base==1) $base=6;
	//новая флорида
	if ($base==2) $base=10;
	$query = '';
	$u[1] = $u[2] = $u[3] = $u[4] = '';
	$GET = $_REQUEST;
	if ($post_api['neighborhoods']) {
		$neighborhoods = explode(',',$post_api['neighborhoods']);
		foreach ($neighborhoods as $k=>$v) {
			$_GET['location'][] = $v;
		}
	}

	$data2 = $data;
	ob_start(); // echo to buffer, not screen
	include(ROOT_DIR.'modules/shop.php');
	$content = ob_get_clean();
	$data = $data2;

	if ($post_api['open_house']) {
		$query.= " AND sp.oh_display=1 AND sp.oh_date>='".date('Y-m-d')."'";
	}
	if ($post_api['our_listings']) {
		$agents = mysql_select("
					SELECT id, id name FROM users
					WHERE mls!='' AND agent=1"
			,'array');
		$query.= " AND sp.agent IN (".implode(',',$agents).") ";
	}
	if ($post_api['featured_properties']) {
		$query.= " AND sp.special=1 ";
		//$select = ', IF (sp.rank>=8 AND sp.special=1,1,0) sort ';
		//$order_by = ' sort DESC, ';
	}
	$query.= "ORDER BY {$order_by} sp.rank DESC, sp.date_change DESC";
	//$query.= " ORDER BY sp.special DESC, sp.date_change DESC";

	//echo $query; die();

	//номер страницы с записями
	$n = $post_api['n']>=1 ? $post_api['n'] : 1;
	//количество записей на страницу
	$c = $post_api['count']>=1 ? $post_api['count'] : 20;
	$begin = $n*$c-$c;
	$num_rows = mysql_select($query,'num_rows');
	$query.= ' LIMIT '.$begin.','.$c;
	$products = mysql_select($query,'rows');
	$data = array(
		'error'=>'0',
		'num_rows'=>$num_rows,
		'query'=>$query,
	);
	//$data['query'] = $query;
	if ($products) {
		$config['cities'] = mysql_select("SELECT id,name FROM shop_cities WHERE display=1 ORDER BY rank",'array');
		foreach ($products as $k => $q) {
			$array = array(
				'id' => $q['id'],
				'mln' => $q['mln'],
				'property_type' => random_property_type ($q),
				'residential'=>$q['residential'],
				'address' => $q['address'],
				'city' => $config['cities'][$q['city']],
				'neighborhood' => $q['neighborhood_name'],
				'status'=>object_status ($q),
				'img' => $q['img'] ? 'https://www.wilkrealestate.com/files/shop_products/' . $q['id'] . '/img/' . $q['img'] : '',
				'price'=> $q['price'],
				'beds'=> $q['beds'],
				'baths'=> $q['baths'],
				'square'=> $q['square'],
				'office_name'=> $q['office_name'],
			);
			$q['city_name'] = @$config['shop_cities'][$q['city']]['name'];
			if (!isset($q['neighborhood_name'])) {
				$neighborhood = get_data('shop_neighborhoods',$q['neighborhood']);
				$q['neighborhood_name'] = $neighborhood['name'];
			}
			if ($base==10) {
				$array['address'] = $name = implode(', ', array(
					$q['name'],
					$q['neighborhood_name'],
					$config['object_base_state'][$q['base']] . ' ' . $q['zip_code']
				));
			}
			else {
				$array['address'] = $name = implode(', ', array(
					$q['name'],
					$q['neighborhood_name'],
					$q['city_name'],
					$config['object_base_state'][$q['base']] . ' ' . $q['zip_code']
				));
			}
			$array['open_house'] = array();
			$config['date'] = date('Y-m-d');
			if ($q['oh_display'] AND strtotime($q['oh_date'])>=strtotime($config['date'])) {
				$agents = mysql_select("SELECT * FROM users WHERE agent=1",'rows_id');
				$oh = unserialize($q['oh_dates']);
				foreach ($oh as $k1=>$v1) {
					if (strtotime($v1['date'])>=strtotime($config['date'])) {
						$array['open_house'][] = array(
							'date'=>$v1['date'],
							'from'=>$v1['from'],
							'to'=>$v1['to'],
							'agent_name'=>$agents[$v1['agent']]['name'],
							'agent_phone'=>$agents[$v1['agent']]['phone']
						);
					}
				}
				$array['open_house_text'] = $q['oh_text'];
			}
			$data['objects'][] = $array;

		}
	}
}
else $data['error'] = 101;
