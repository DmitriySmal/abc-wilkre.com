<?php

// загрузка настроек *********************************************************
define('ROOT_DIR', dirname(__FILE__).'/../');
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек
require_once(ROOT_DIR.'_config_ny.php');	//установка настроек

// загрузка функций **********************************************************
//require_once(ROOT_DIR.'functions/admin_func.php');	//функции админки
require_once(ROOT_DIR.'functions/auth_func.php');	//функции авторизации
require_once(ROOT_DIR.'functions/common_func.php');	//общие функции
//require_once(ROOT_DIR.'functions/file_func.php');	//функции для работы с файлами
require_once(ROOT_DIR.'functions/html_func.php');	//функции для работы нтмл кодом
require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами
//require_once(ROOT_DIR.'functions/image_func.php');	//функции для работы с картинками
require_once(ROOT_DIR.'functions/lang_func.php');	//функции словаря
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
require_once(ROOT_DIR.'functions/string_func.php');	//функции для работы со строками
require_once(ROOT_DIR.'functions/mls_func.php');	//функции почты

$time = microtime(true);
//определение значений формы
$fields = array(
	'version'	=>	'version',
	'action'	=>	'text',
	'time'		=>	'time',
	'hash'		=>	'text',
);
//создание массива $post
$post = form_smart($fields,stripslashes_smart($_GET)); //print_r($post);

$data = array(
	'error'=>0
);

$post['action'] = preg_replace('~[^_a-z]+~u', '-', $post['action']);    //удаление лишних символов
if (file_exists(ROOT_DIR.'api/actions/'.$post['action'].'.php')) {
	include(ROOT_DIR.'api/actions/'.$post['action'].'.php');
}

if (!$data AND !isset($data['error'])) {
	$data['error'] = 1;
}

$data['time'] = microtime(true)-$time;
if (@$_GET['debug']==1) {
	echo '<pre>';
	print_r($data);
}
else {
	unset ($data['query']);
	header('Content-type: application/json; charset='.$config['charset']);
	echo json_encode($data);
}
