<?php

$config['admin_lang'] = 'en'; //язык админпанели

$config['multilingual'] = false; //многоязычный сайт

$config['merchants'] = array(
	1 => 'наличный рассчет',
	2 => 'robokassa [все платежи]',
	3 => 'robokassa [yandex]',
	4 => 'robokassa [wmr]',
	5 => 'robokassa [qiwi]',
	6 => 'robokassa [терминал]',
	7 => 'robokassa [банковской картой]',
);
$config['payments'] = array(
	1 => 'наличный рассчет',
	2 => 'robokassa',
);

$config['depend'] = array(
	//'shop_products'=>array('categories'=>'shop_products-categories'),
	'users'=>array('cities'=>'users-cities'),
);

$config['boolean'] = array(
	'boolean','display','market','yandex_index',
);

$config['mirrors'] = array(
	'shop_property_types_2'=>'shop_property_types',
	'mls10_shop_products'=>'shop_products',
	'mls11_shop_products'=>'shop_products',
	'flmx_shop_products'=>'shop_products',
	'fx_shop_products'=>'shop_products',
	'mx_shop_products'=>'shop_products',
	'mx_open_houses'=>'shop_products',
	'mx_shop_products_own'=>'shop_products',
	//'shop_products_1'=>'shop_products',
	'sf_shop_products_own'=>'shop_products',
	//'shop_products_exclusive'=>'shop_products',
	'ny_shop_products'=>'shop_products',
	'ny_shop_products_old'=>'shop_products',
	'ny_shop_products_own'=>'shop_products',
	'ny_open_houses'=>'shop_products',
	'ny_preconstructions'=>'shop_preconstructions',
	'requests4'=>'requests',
	'requests5'=>'requests',
	'feedback2'=>'feedback',
	'feedback3'=>'feedback',
	'feedback4'=>'feedback',

	'mx_shop_parameters'=>'shop_parameters',
	'fx_shop_parameters'=>'shop_parameters',
	'flmx_shop_parameters'=>'shop_parameters',
	'mls10_shop_parameters'=>'shop_parameters',
);

$modules_admin = array(
	'pages'			=> 'pages',
	'news'		=> 'news',
	//'agents'		=> 'agents',
	'gallery' => array(
		//'gallery'	=> 'gallery',
		'slider'	=> 'slider',
	),

	'requests' => array(
		'Send Requests'	=> 'requests',
		'Schedule a Showing'	=> 'requests4',
		'Promo SMS clients'	=> 'requests5',
		'For Sellers'	=> 'feedback',
		'For Buyers'	=> 'feedback2',
		'Old Requests'	=> 'feedback3',
		'Feedback'	=> 'feedback4',
		'Import for Sellers/Buyers'	=> 'feedback_import',
	),
	'users' => array(
		'users'	=> 'users',
		'user_types'	=> 'user_types',
		//'user_fields'	=> 'user_fields',
	),

	/*'Finance' => array(
		'banks'	=> 'banks',
		'bank_percents'	=> 'bank_percents',
	),*/

	'catalog' => array(
				//'own objects'	=> 'shop_products_1',
                //'own objects'	=> 'shop_products_exclusive',
		 //'shop_condos'	=> 'shop_condos',
                //'shop_preconstruction'	=> 'shop_preconstruction',

		//'shop_items'	=> 'shop_items',
		//'shop_categories'	=> 'shop_categories',
                'States'           => 'shop_states',
                'Cities'           => 'shop_cities',
                'Neighborhoods'	=> 'shop_neighborhoods',
				'Keywords'	=> 'shop_search',
				'Keywords h2'	=> 'shop_search2',
				'Property Types'	=> 'shop_property_types2',
				'Import Links'	=> 'shop_links_import',
				'Links'	=> 'shop_links',
		'Import Neighborhoods'	=> 'shop_neighborhoods_import',
                //'empty_zip'	=> 'empty_zip',
                //'shop_neighborhoods_seo'	=> 'shop_neighborhoods_seo',
		//'shop_brands'	=> 'shop_brands',
		//'shop_parameters'	=> 'shop_parameters',
		//'shop_reviews'	=> 'shop_reviews',
		'New Property Types'	=> 'shop_property_types',
		'Property SubTypes'	=> 'shop_property_types_2',

	),
	/*'South Florida'=>array(
		//'Our listing'	=> 'sf_shop_products_own',
		'MLS listings'	=> 'shop_products',
		//'Search on MLS'	=> 'shop_mls',
	),
	/*'New York'=>array(
		//'Our listing'	=> 'ny_shop_products_own',
		//'Old listing'	=> 'ny_shop_products_old',
		'MLS listings'	=> 'ny_shop_products',
		'Search on MLS'	=> 'ny_getobject',
		//'Pre-constructions'	=> 'ny_preconstructions',
		'Open Houses'	=> 'ny_open_houses',
		//'shop_condos'	=> 'shop_condos',
		//'preconstructions'	=> 'shop_preconstructions',
	),*/
	'New York'=>array(
		'Our listing'	=> 'mx_shop_products_own',
		//'Old listing'	=> 'ny_shop_products_old',
		'MLS listings'	=> 'mx_shop_products',
		'MLS OLD listings'	=> 'ny_shop_products',
		'Search on MLS'	=> 'mx_getobject',
		'Parameters'	=> 'mx_shop_parameters',
		'Pre-constructions'	=> 'ny_preconstructions',
		'Open Houses'	=> 'mx_open_houses',
	),
	/*'Flex'=>array(
		//'Our listing'	=> 'fx_shop_products_own',
		'MLS listings'	=> 'fx_shop_products',
		'Search on MLS'	=> 'fx_getobject',
		'Parameters'	=> 'fx_shop_parameters',
		//'Open Houses'	=> 'fx_open_houses',
	),*/
	/*
	'Florida'=>array(
		'MLS listings'	=> 'flmx_shop_products',
		'Search on MLS'	=> 'flmx_getobject',
		'Parameters'	=> 'flmx_shop_parameters',
		'Condos'	=> 'shop_condos',
		'Pre-constructions'	=> 'shop_preconstructions',
	),*/
	//Bridge
	'Florida'=>array(
		'Our listing'	=> 'mls11_shop_products',
		'MLS listings'	=> 'mls10_shop_products',
		'Search on MLS'	=> 'mls10_getobject',
		'Parameters'	=> 'mls10_shop_parameters',
		'Statistics'	=> 'statistics',
	),

        /*
	'synchronization'=>array(
		'export'	=> 'shop_export',
		'import'	=> 'shop_import',
	),*/
	/*'shop' => array(
		'orders'		=> 'orders',
		'order_types'		=> 'order_types',
		'order_deliveries'	=> 'order_deliveries',
		'order_payments'	=> 'order_payments',
	),*/


	'subscribe'=>array(
		'subscribers'		=> 'subscribers',
		//'subscribe_letters'	=> 'subscribe_letters',
		'letters'			=> 'letters',
		'MLS Agents'        => 'agents_mls',
		'Zillow clients'    => 'agents_mls2',
		'Trulia clients'    => 'agents_mls3',
		'Agents export'     => 'agents_mls_export',
	),

	'config' => array(
		'config'			=> 'config',
		'dictionary'	=> 'languages',
		'letter_templates'	=> 'letter_templates',
		'logs'				=> 'logs',
		'Logs search'				=> 'logs_shop',
		/*'template_css'	=> 'template_css',
		'template_images'	=> 'template_images',
		'template_includes'	=> 'template_includes',
		'template_scripts'	=> 'template_scripts'*/
	),
	/*'design' => array(

	),*/
	'backup' => array(
		'backup'	=> 'backup',
		'restore'	=> 'restore'
	),
	'seo' => array(
		'redirects'		=> 'redirects',
		'robots.txt'	=> 'seo_robots',
		'sitemap.xml'	=> 'seo_sitemap',
		'.htaccess'		=> 'seo_htaccess',
		'anckors'		=> 'seo_anckors',
		'links'		=> 'seo_shop',
		/*'links'		=> 'seo_links',
		'pages'		=> 'seo_pages',
		'import'		=> 'seo_links_import',
		'export'		=> 'seo_links_export',*/
	),
	'support' => 'support'
);
