<?php
require_once(ROOT_DIR.'_config_ny.php');

$table = array(
	'_delete'=>false,
	//'_edit'=>false,
	'id'	=>	'id rank name',
	'fl_property_type'    =>'::table_fl_property_type',
	'ny_property_type'    =>'::table_ny_property_type',
	//'mx_property_type'    =>'::table_mx_property_type',
	'sale'    =>'boolean',
	'rent'    =>'boolean',
	'name'	=>	'',
	'url'	=> '',
	'rank'	=>	'',
);

$query = "SELECT * FROM shop_property_types2 WHERE 1";

function table_ny_property_type ($q) {
	//print_r($q);
	global $config;
	$content = $q['ny_property_type'] ? $config['ny_property_types'][$q['ny_property_type']].'<br>'.$q['ny_products'] : '-';
	return '<td>' . $content . '</td>';
}
function table_fl_property_type ($q) {
	global $config;
	$content = $q['fl_property_type'] ? ($config['object_groups'][$q['fl_property_type']]['name'].'<br>'.$q['fl_products']) : '-';
	return '<td>'.$content.'</td>';
}
function table_mx_property_type ($q) {
	global $config;
	$content = $q['mx_property_type'] ? $config['mx_property_types'][$q['mx_property_type']].'<br>'.$q['mx_products'] : '-';
	return '<td>'.$content.'</td>';
}

$form[] = array('input td6','name',true);
$form[] = array('input td2','url',true);
$form[] = array('input td2','rank',true);
$form[] = array('input td2','fl_property_type',true);
$form[] = array('input td2','ny_property_type',true);

//$form[] = array('select td2','mx_property_type',array(true,$config['mx_property_types'],''));

