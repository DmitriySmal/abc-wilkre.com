<?php

$categories = mysql_select("SELECT * FROM shop_categories ORDER BY id",'array');
$neighborhoods = mysql_select("SELECT * FROM shop_neighborhoods ORDER BY name",'array');

$table = array(
	'id'		=>	'neighborhood category name title id',
        'category'		=>	$categories,
        'neighborhood'		=>	$neighborhoods,
	'name'		=>	'',
	'title'		=>	'',
	'display'	=>	'boolean'
);


$form[] = array('input td10','name',true);
$form[] = array('checkbox','display',true);
$form[] = array('select td5','neighborhood',array(true, $neighborhoods));
$form[] = array('select td5','category',array(true, $categories));
$form[] = array('tinymce td12','text',true);
$form[] = array('seo','seo title keywords description',true);