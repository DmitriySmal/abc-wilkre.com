<?php

$modules_site = array(
	'pages'			=> 'Page',
	'index'			=> 'Index',
	'news'			=> 'Blog',
	'agents'		=> 'Florida Agents',
	//'gallery'		=> 'Галерея',
	'shop'			=> 'Florida',
	'condos'		=> 'Florida Condos',
	'preconstruction'	=> 'Florida Pre-construction',
	'shop_ny'			=> 'New-York',
	'agents_ny'		=> 'New-York Agents',
	'shop_ny_old'		=> 'New-York Old Listing',
	'shop_mx'			=> 'Matrix',
	'shop_flex'			=> 'Flex',
	'shop_flmx'			=> 'Florida Matrix',
	'shop_mls10'			=> 'Florida bridge',
	'agents_mx'		=> 'Matrix Agents',
	'open_houses'   => 'Open Houses',
	//'basket'		=> 'Корзина',
	'feedback'		=> 'for sellers',
	'feedback2'		=> 'for buyers',
	'request'	=> 'Applications',
	'sitemap'		=> 'Sitemap',
	//'profile'		=> 'Личный кабинет',
	//'login'			=> 'Авторизация',
	//'registration'	=> 'Регистрация',
	//'remind'		=> 'Восстановление пароля',
	'subscribe'		=> 'Unsubscribe'
);
$a18n['menu4'] = 'menu top left';
$a18n['menu2'] = 'menu top right';
$a18n['menu'] = 'submenu';
$a18n['menu3'] = 'menu footer';


if ($get['u']=='form') {
	if (empty($post['module'])) $post['module'] = 'pages';
	foreach ($modules_site as $k=>$v)
		if (!file_exists(ROOT_DIR.'modules/'.$k.'.php'))
			unset($modules_site[$k]);
}

$table = array(
	'_tree'		=> true,
	'_edit'		=> true,
	//'bimg'		=> 'img',
	'id'		=> '',
	'name'		=> '',
	'title'		=> '',
	'url'		=> '',
	'module'	=> $modules_site,
	'menu4'		=> 'boolean',
	'menu2'		=> 'boolean',
	'menu'		=> 'boolean',
	'menu3'		=> 'boolean',
	'display'	=> 'display'
);
/*
if(@$_GET['state']>0){
    $form[] = '<input name="state" type="hidden" value="'.$get['state'].'" />';
	$where .= " AND pages.state = ".intval($_GET['state'])." ";
} else {
    $where .= " AND pages.state = 9999 ";
}
*/
$query = "
		SELECT pages.*
		FROM pages
		WHERE 1 {$where}
	";

//только если многоязычный сайт
if ($config['multilingual']) {
	$languages = mysql_select("SELECT id,name FROM languages ORDER BY rank DESC", 'array');
	//приоритет пост над гет
	if (isset($post['language'])) $get['language'] = $post['language'];
	if ($get['language'] == 0) $get['language'] = key($languages);
	$query = "
		SELECT pages.*
		FROM pages
		WHERE pages.language = '".$get['language']."' {$where}
	";
	$filter[] = array('language', $languages);
	$form[] = '<input name="language" type="hidden" value="'.$get['language'].'" />';
}

//$states = mysql_select("SELECT id,name FROM shop_states ORDER BY rank DESC", 'array');
//$filter[] = array('state', $states);


$delete['confirm'] = array('pages'=>'parent');

$form[] = array('input td7','name',true);
$form[] = array('select td3','module',array(true,$modules_site));
$form[] = array('checkbox','display',true);
$form[] = array('parent td3 td4','parent',true);
$form[] = '<div class="clear"></div>';
$form[] = array('checkbox','menu4',true);
$form[] = array('checkbox','menu2',true);
$form[] = array('checkbox','menu',true);
$form[] = array('checkbox','menu3',true);
$form[] = array('tinymce td12','text',true);//,array('attr'=>'style="height:500px"'));
$form[] = array('seo','seo url title keywords description',true);
$form[] = array('file td6','bimg','Image',array(''=>'resize 1200x100'));
