<?php
$module = 'shop_products';
if (!isset($base)) $base = 3;

if ($get['u']=='') {
	$plates = mysql_select("SELECT plate id, plate id FROM shop_products WHERE plate>0", 'array');
	if ($plates) foreach ($plates as $k => $v) unset($config['plates'][$k]);
}
if ($get['u']=='form' OR $get['id']) {
	$plates = mysql_select("SELECT plate id, plate id FROM shop_products WHERE plate>0 AND id!=".intval(@$get['id']), 'array');
	if ($plates) foreach ($plates as $k => $v) unset($config['plates'][$k]);
}

include(ROOT_DIR.'_config_ny.php');

//var_dump($config['object_statuses']);
$config['hide_add_button'] = true;
$config['save_listing'] = true;
$a18s['hide'] = '#';
//dd($post); die();
//$save_as = true;
if (@$post['city']) $neighborhoods = mysql_select("SELECT * FROM shop_neighborhoods WHERE city = '".intval($post['city'])."' ORDER BY name",'array');
else $neighborhoods = mysql_select("SELECT * FROM shop_neighborhoods WHERE 1 ". ((isset($get['city']) && $get['city']>0) ? " AND city = '".intval($get['city'])."' " : "") ." ORDER BY name",'array');
$cities = mysql_select("SELECT * FROM shop_cities",'array');
$agents = mysql_select("SELECT id,name FROM users WHERE agent=1 ORDER BY name",'array');
//echo $base;
$categories = $config['categories'];
$statuses = $config['ny_object_statuses'];
$property_types = $config['ny_property_types'];
if ($base==6) $statuses = $config['mx_object_statuses'];
if ($base==8) $property_types  = $config['sf_property_type_url'];
$types = $config['for_sale'];

if ($get['u']=='post') {
	if ($get['name']=='plate' AND $get['value']==0) $get['value'] = NULL;
}

if ($get['u']=='edit') {
	$date = date('Y-m-d');
	$post['oh_date'] = '0000-00-00';
	if ($post['plate']==0) $post['plate'] = NULL;
	if(@$post['oh_dates']){
		foreach ($post['oh_dates'] as $k=>$v) {
			if (strtotime($v['date'])<strtotime($date)) unset($post['oh_dates'][$k]);
			else {
				if (strtotime($v['date']) > strtotime($post['oh_date']) OR $post['oh_date'] == '0000-00-00')
					$post['oh_date'] = $v['date'];
			}
		}
		$post['oh_dates'] = serialize($post['oh_dates']);
		//$post['oh_display'] = 1;
	}
	else $post['oh_dates'] = '';
    if($post['neighborhood'] && empty($post['zip_code'])){
        $zip = mysql_select("SELECT * FROM shop_neighborhoods_zips WHERE nb_id = {$post['neighborhood']} LIMIT 1",'row');
        if ($zip)
            $post['zip_code'] = $zip['zip'];
    }
    /*
    if($post['zip_code'] && empty($post['neighborhood'])){
        $zip = mysql_select("SELECT * FROM shop_neighborhoods_zips WHERE zip = {$post['zip_code']} LIMIT 1",'row');
        if ($zip)
            $post['neighborhood'] = $zip['nb_id'];
    }*/

    if($post['coordinates'] && !empty($post['coordinates'])){
        $coordinates = trim($post['coordinates'],' ()');
        $location = explode(',', $coordinates);
        //var_dump($location);
        $post['lat'] = trim($location[0]);
        $post['lng'] = trim($location[1]);
    }
}

if ($get['u'] == 'save_listing') {
	require_once(ROOT_DIR . 'functions/mls_func.php');    //динамические настройки

	//print_r($parameters);
	if ($base==6) {
		$post = save_mls_mx($post);
		die(header('location: /admin.php?m=mx_shop_products_own&id=' . $post['id']));
	}
	else {
		$post = save_mls_ny($post);
		die(header('location: /admin.php?m=ny_shop_products_own&id=' . $post['id']));
	}
}


$config['hide_add_button'] = true;

$table = array(
	'_edit'		=>	true,
	'id'		=>	'special:desc uid date:desc id:desc name price errors unit mln plate property_type status display date_rss',
	'plate'     => '::table_plate',
	'mln'		=>	'<a target="_blank" href="/cron.php?file=ny_photo&id={id}">{mln}</a>',
	'img'       => 'img',
	'zip_code'  => '',
	'uid'=>'',
	'property_type' => $property_types,
	'name'		=>	'::row_object',
	'unit'      => '',
	'city' => $cities,
	'neighborhood' => $neighborhoods,
	//'neighborhood' => '',
	'zip_code' => '',
	'coords' => '{lat}<br>{lng}',
	'date'		=>	'date',
	'price'		=>	'right',
	'errors'    =>  '',
	'status'	=>	'',
	'agent'=>$agents,
	'date_rss'=>'',
	'addurl'	=>	'boolean',
	'special'	=>	'boolean',
	'to_agent'	=>	'boolean',
	'display'	=>	'boolean',
	'addurl'=>'',
	'hide'	=>	''
);
if ($base==6) {
	$table['mln'] = '<a target="_blank" href="/cron.php?file=matrix_photo&id={id}">{mln}</a>';
}
if ($base==8) {
	$table['mln'] = '<a target="_blank" href="/cron.php?file=flex_photo&id={id}">{uid}</a>';
}
if ($base==9) {
	$table['mln'] = '<a target="_blank" href="/cron.php?file=mls4_photo&id={id}">{uid}</a>';
}
if ($base==10) {
	$table['mln'] = '<a target="_blank" href="/cron.php?file=mls10_photo&id={id}">{mln}</a>';
}
function table_plate ($q) {
	global $config;
	$array = $config['plates'];
	$select = select($q['plate'],$array,'');
	if ($q['plate']) $select.= '<option value="'.$q['plate'].'" selected="selected">'.$q['plate'].'</option>';
	return '<td data-id="'.$q['id'].'" data-name="plate"><select>'.$select.'</select></td>';
}

$where = '';
if (isset($get['search']) && $get['search']!='') {
	//[4.726] Сделать поиск по plate, чтобы можно было находить недвижимость по plate
	if (intval($get['search'])>0 AND intval($get['search'])<100) {
		$where.= " AND shop_products.plate = ".intval($get['search']);
	}
	else {
		$where .= "
			AND (
				LOWER(shop_products.mln) like '%" . mysql_real_escape_string(mb_strtolower($get['search'], 'UTF-8')) . "%'
				OR LOWER(shop_products.uid) like '%" . mysql_real_escape_string(mb_strtolower($get['search'], 'UTF-8')) . "%'
				OR LOWER(shop_products.name) like '%" . mysql_real_escape_string(mb_strtolower($get['search'], 'UTF-8')) . "%'
				OR shop_products.id = '" . mysql_real_escape_string($get['search']) . "'
				OR shop_products.office_name  like '%" . mysql_real_escape_string(mb_strtolower($get['search'], 'UTF-8')) . "%'
			)
		";
	}
}
$where.= (@$get['neighborhood'] > 0) ? " AND neighborhood = ".$get['neighborhood'] : '';
$where.= (@$get['city'] > 0) ? " AND city = ".$get['city'] : '';
$where.= (@$get['agent'] > 0) ? " AND agent = ".$get['agent'] : '';
$where.= (@$get['own'] > 0) ? " AND agent >0 AND display=1" : '';
$where.= (@$get['display'] > 0) ? " AND display = 1" : '';
$where.= (@$get['errors']) ? " AND errors = ".$get['errors'] : '';
$where.= (@$get['property_type'] > 0) ? " AND property_type = ".$get['property_type'] : '';
$where.= (@$_GET['status']) ? " AND status = '".mysql_res($_GET['status'])."'" : '';

if (@$_GET['property_type2']) {
	if ($property_type2 = mysql_select("SELECT name FROM shop_property_types2 WHERE id=".intval($_GET['property_type2']),'string')) {
		$where .= " AND property_type2 LIKE '%" . mysql_res(mb_strtolower($property_type2)) . "%'";
	}
}
//echo $query;

$filter[] = array('search');
if ($user['agent']==0) {
	$filter[] = array('agent', $agents, '-agent-');
	$filter[] = array('city', $cities, '-city-');
	$filter[] = array('neighborhood', $neighborhoods, '-neighborhood-');
	//$filter[] = array('category', $categories, '-category-');
//$statuses['A,PS,T,R'] = 'A,PS,T,R';
	$filter[] = array('status', $statuses, '-status-');
	$filter[] = array('property_type', $property_types, '-Real Estate Types-');
	if (@$_GET['property_type']) {
		require_once(ROOT_DIR.'functions/mls_func.php');
		//$property_type2 = select_property_type(2,0,$_GET['property_type']);
		$post2 = array(
			'base'=>1,
			'property_type'=>$_GET['property_type']
		);
		$property_types2 = get_property_types2 ($post2);
		//print_R($property_types2);
		if ($property_types2) {
			$filter[] = array('property_type2', $property_types2, '-Property Type-');
		}
	}
}
else {
	if ($get['search']=='') {
		$where.= ' AND shop_products.agent='.$user['id'];
	}
}

if ($get['m']=='ny_open_houses') {
	$where.= " AND shop_products.base in (3,4)";
	//$where.= " AND shop_products.oh_date>= '".date('Y-m-d')." 00:00:00'";
	$where.= " AND shop_products.oh_display=1 ";
	$table['oh_display'] = 'boolean';
}
elseif ($get['m']=='mx_open_houses') {
	$where.= " AND shop_products.base in (4,6,7)";
	//$where.= " AND shop_products.oh_date>= '".date('Y-m-d')." 00:00:00'";
	$where.= " AND shop_products.oh_display=1 ";
	$table['oh_display'] = 'boolean';
}
else {
	$where.= " AND shop_products.base = ".$base;
}

$query = "
	SELECT
		shop_products.*
	FROM
		shop_products
	WHERE 1 $where
";
//echo $query;

//unset($statuses['A,PS,T,R']);

$delete = array(
	'delete'=>"DELETE FROM shop_reviews WHERE product = '".$get['id']."'"  //удаление отзывов
);

$tabs = array(
	1=>'Main',
	2=>'Settings',
	3=>'Pictures',
	5=>'Map',
	6=>'Open House',
	7=>'Settings'
);

$form[1][] = '<input name="base" value="'.(@$post['base']?$post['base']:$base).'" type="hidden"/>';
if (@$post['hide']==0) {
	//if ($base!=6) {
		$form[1][] = '<a style="position:absolute; top:50px; right:25px" href="?m=' . $get['m'] . '&id=' . @$post['id'] . '&u=save_listing"><b>Save Listing</b></a>';
	//}
}
else {
	$form[1][] = '<a style="position:absolute; top:50px; right:25px" target="_blank" href="?m=ny_shop_products_own&id=' . $post['hide'] . '"><b>Listing '.$post['hide'].'</b></a>';
}
$form[1][] = array('input td3','name',true);
if ($user['agent']==0) {
	$form[1][] = array('select td2','agent',array(true,$agents,'-select-'));
	$form[1][] = array('select td1','plate',array(true,$config['plates'],'-select-'));
}
$form[1][] = array('input td1','hide',true,array('name'=>'save listing'));
$form[1][] = array('checkbox','landing',true);
$form[1][] = array('checkbox','special',true);
$form[1][] = array('checkbox','to_agent',true);
$form[1][] = array('checkbox','display',true);

//$form[1][] = array('select td3','brand',array(true,"SELECT id,name FROM shop_brands ORDER BY name"));
//$form[1][] = array('select td2','type',array(true,$types),array('name'=>'for sale'));

$form[1][] = '<div class="clear"></div>';
$form[1][] = array('select td3','property_type',array(true,$property_types));
$form[1][] = array('select td3','city',array(true,$cities,'-select-'));
$form[1][] = array('select td3','neighborhood',array(true,$neighborhoods,'-select-'));
if(isset($post['zip_code']) && !empty($post['zip_code'])){
    $form[1][] = '<input type="hidden" name="zip_code" value="'.$post['zip_code'].'">';
}

//$form[1][] = array('multicheckbox td3','categories',array(true,'SELECT id,name,level FROM shop_categories ORDER BY left_key'));
$form[1][] = array('input td1 right','price',true);
//$form[1][] = array('input td1 right','price2',true);
//$form[1][] = array('input td1 right','rating',true,@$get['id']>0?array('name'=>'<a target="_blank" href="?m=shop_reviews&product='.@$get['id'].'">оценки</a>'):NULL);
//$form[1][] = array('input td1','article',true);
$form[1][] = array('input td2','date',true);

$form[1][] = array('input td9','property_type2',true);
$form[1][] = array('select td3','residential',array(true,$config['object_residential'],''));

$form[1][] = array('input td3','office_name',true); //ListOfficeName
$form[1][] = array('input td3','office_phone',true); //ListAgentDirectWorkPhone
$form[1][] = array('input td3','office_agent',true); //ListAgentFullName
$form[1][] = array('input td3','office_agent_phone',true); //ListOfficePhone
$form[1][] = array('input td3','office_agent_email',true); //ListAgentEmail

$form[1][] = array('input td3','complex_name',true);
$form[1][] = array('input td3','development_name',true);
$form[1][] = array('input td1','beds',true);
$form[1][] = array('input td1','baths',true);
$form[1][] = array('input td1','square',true);


//$form[1][] = array('checkbox','is_condo',true);
$form[1][] = array('tinymce td12','text',true);
$form[1][] = array('seo','seo url title keywords description',true);

$form[2][] = '';
if ($get['u']=='form' OR ($get['id']>0 AND $get['u']=='')) {
	$parameters = $post['parameters'] ? unserialize($post['parameters']) : false;
	if ($parameters) foreach ($parameters as $k=>$v) {
		if (is_array($v)) {
			foreach ($v as $k1=>$v1) {
				if (is_array($v1)) {
					foreach ($v1 as $k2=>$v2) {
						$form[2][] = $k.'['.$k1.']['.$k2.'] - '.$v2.'<br>';
					}
				}
				else $form[2][] = $k.'['.$k1.'] - '.$v1.'<br>';
			}
		}
		else $form[2][] = $k.' - '.$v.'<br>';
	}
}
$form[2][] = array('input td2','date_photo',true);
$form[2][] = array('input td2','date_change',true);

$form[3][] = array('file td6','img','Main image',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));
$form[3][] = array('file td6','booklet','Booklet',true);
$form[3][] = array('input td12','video',true);
$form[3][] = array('input td12','3d',true);
$form[3][] = array('file_multi','imgs','Additional images',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));

//$form[3][] = array('file_multi_db','shop_items','Additional images',array(''=>'resize 1000x1000','preview'=>'resize 150x150'));

$google = preg_replace('~[^-0-9.,]+~u','',@$post['coordinates']);
$google = $google ? $google : '39.5,-0.38';
//var_dump($google);
$form[5][] = array('input td10','address',@$post['address'],array('name'=>'address'));
$form[5][] = '<a href="#" id="coordinates_search" class="button red" style="margin:15px 0 25px"><span>Search</span></a>';
$form[5][] = '<div style="display:none">';
$form[5][] = array('input td12','coordinates',@$post['coordinates'],array('name'=>'карта'));
$form[5][] = '</div>';
$form[5][] = '<div id="map" style="clear:both; width:790px; height:500px"></div>';


$agents2 = '<option value="0">select</option>';
foreach ($agents as $k=>$v) {
	$agents2.='<option value="'.$k.'" {agent_'.$k.'}>'.$v.'</option>';
}
//$form[6][] = array('input td4','oh_hosted_by',true,array('name'=>'hosted by'));
//$form[6][] = array('input td4','oh_phone',true,array('name'=>'phone'));

//$form[6][] = array('input td4','oh_date',true,array('name'=>'date'));
$form[6][] = array('textarea td9','oh_text',true,array('name'=>'directions & description'));
$form[6][] = array('checkbox td3','oh_display',true,array('name'=>'open houses'));
$form[6][] = @$post['oh_date'];

$form[6][] = '<div style="clear:both; background:#E9E9E9; padding:5px 10px; width:875px; margin:0 -10px">';
$form[6][] = '<table class="oh_dates">';
$form[6][]= '<tr data-i="0">';
$form[6][] = '<th>date</th>';
$form[6][] = '<th>from</th>';
$form[6][] = '<th>to</th>';
$form[6][] = '<th>agent</th>';
$form[6][] = '<th>OpenHouseID</th>';
$form[6][] = '<th><a href="#" style="background:#35B374; display:inline-block; padding:2px; border-radius:10px"><span class="sprite plus"></span></a></th>';
$form[6][] = '</tr>';
$template['oh_dates'] = '
	<tr data-i="{i}">
		<td><input class="date" name="oh_dates[{i}][date]" value="{date}" placeholder="'.date('Y-m-d').'"/></td>
		<td><input name="oh_dates[{i}][from]" value="{from}" placeholder="9:00" /></td>
		<td><input name="oh_dates[{i}][to]" value="{to}" placeholder="16:00"/></td>
		<td><select name="oh_dates[{i}][agent]">'.$agents2.'</select></td>
		<td><input name="oh_dates[{i}][OpenHouseID]" value="{OpenHouseID}" placeholder=""/></td>
		<td><a href="#" class="sprite boolean_0"></a></td>
	</tr>
';
if (isset($post['oh_dates'])) {
	$oh_text = unserialize($post['oh_dates']); //print_r ($basket);
	if (is_array($oh_text)) foreach ($oh_text as $key=>$val) {
		$val['i'] = $key;
		$val['agent_'.$val['agent']] = ' selected="selected" ' ;
		$form[6][] = template($template['oh_dates'],$val);
	}
}
$form[6][] = '</table></div>';

$form[7][] = array('input td12','date_rss',true);


$content.= '<div style="display:none">';
$content.= '<textarea id="template_oh_dates">'.htmlspecialchars($template['oh_dates']).'</textarea>';
$content.= '</div>';
$content.= '<style type="text/css">
.form .oh_dates {width:100%}
.form .oh_dates th {text-align:left; padding:0 0 5px;}
.form .oh_dates td {border-top:1px solid #F3F3F3; padding:5px 0; vertical-align:top;}
.form .oh_dates input {text-align:right; border:1px solid gray; margin:0; padding:0 2px; height:19px; width:70px}
.form .oh_dates .product_name {width:550px; text-align:left;}
.form .oh_dates td td {border:none}
</style>';

$content.= '

<script type="text/javascript" src="https://maps.google.com/maps/api/js?key='.$config['google_maps_key'].'&sensor=false"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(this).on("form.open",".form",function(){
		//alert(1);
		$(".oh_dates input.date").datepicker({dateFormat:"yy-mm-dd"});
	});
	$(document).on("click",".oh_dates th a",function(){
		var i = $(this).parents("table").find("tr:last").data("i");
		i++;
		var content = $("#template_oh_dates").val();
		content = content.replace(/{i}/g,i);
		content = content.replace(/{[\w]*}/g,"");
		$(this).parents("table").append(content);
		$(".oh_dates input.date").datepicker({dateFormat:"yy-mm-dd"});
		return false;
	});
	$(document).on("click",".oh_dates td a",function(){
		$(this).parents("tr").remove();
		return false;
	});

	//замена параметров при смене категории
	$(document).on("change",".form select[name=category]",function(){
		var category = $(this).val(),
			id = $(".form").prop("id").replace(/[^0-9]/g,"");
		$.get(
			"/admin.php",
			{"m":"shop_products","u":"shop_parameters","category":category,"id":id},
			function(data){$("#shop_parameters").html(data)}
		);
	});

	$(document).on("click",".bookmarks a",function(){
		var i = $(this).data("i");
                var coords = $("input[name=coordinates]").val().replace("(","").replace(")","").split(",");
                coords = (coords.length > 1) ? coords : new Array (25.7616798,-80.191);
		if (i==5) {
                        console.log(coords);
			geocoder = new google.maps.Geocoder();
			map = new google.maps.Map(document.getElementById("map"),{
				zoom: 10,
				center: new google.maps.LatLng(coords[0],coords[1]),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
			});
			markersArray = [];
			var marker = new google.maps.Marker({position:new google.maps.LatLng(coords[0],coords[1])});
			marker.setMap(map);
			markersArray.push(marker);
			google.maps.event.addListener(map, "click", function(event) {
				for (i in markersArray) markersArray[i].setMap(null);
				markersArray.length = 0;
				var marker = new google.maps.Marker({position:event.latLng,map:map});
				markersArray.push(marker);
				$("input[name=coordinates]").val(event.latLng);
			});
		}
		return false;
	});

        $(document).on("change",".form select[name=city]",function(){
		// меняем районы
                var cities = $(this);
                var neighborhoods = $(cities).closest(".form").find("select[name=neighborhood]");
                $.ajax({
                    url: "/ajax.php",
                    data: {
                        file: "neighborhoods",
                        city: $(cities).val(),
                    },
                    type: "get",
                    dataType: "json",
                    beforeSend: function(){
                        $(neighborhoods).find("option").slice(1).remove();
                    },
                    success: function(json){
                        if(json.result)
                        {
                            $.each(json.data, function(i, v) {
                                $(neighborhoods).append($("<option>").text(v.name).attr("value", v.id));
                            });
                        }
                    }
                });
	});

        //поиск по гуглу
	$(document).on("click","#coordinates_search",function () {
		marker_search();
		return false;
	});

	function marker_search() {
		var address = $("input[name=\"address\"]").val();
		geocoder.geocode( { "address": address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				for (i in markersArray) markersArray[i].setMap(null);
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});
				markersArray.push(marker);
                                console.log(results[0].geometry.location.toString());
                                $("input[name=coordinates]").val(results[0].geometry.location.toString());
			} else {
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
	}

});
</script>';

