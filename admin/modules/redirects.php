<?php

$a18n['new_url'] = 'new url';
$a18n['old_url'] = 'old url';

$table = array(
	'id'		=>	'id old_url new_url',
	'old_url'		=>	'',
	'new_url'		=>	'',
	'display'	=>	'display'
);

$where = '';
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(old_url) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(new_url) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
	)
";

$filter[] = array('search');

$content = '<div style="margin:10px 0 0; padding:5px 10px; font:12px/14px Arial; background:#DFE0E0; border-radius:3px">Redirects are '.($config['redirects']==1 ? '<b style="color:green">on</b>' : '<b style="color:darkred">off</b>').' <a  target="_blank" href="/admin.php?m=config">config</a></div>';

$form[] = array('input td5','old_url',true);
$form[] = array('input td5','new_url',true);
$form[] = array('checkbox','display',true);