<?php

$a18n['property_type'] = 'real estate type';
$a18n['property_type2'] = 'property type';
$a18n['name'] = 'H1';

include(ROOT_DIR.'_config_ny.php');

if (isset($post['state'])) $get['state'] = $post['state'];
if (@$get['state'] == 0) $get['state'] = 1;

$state = @$get['state'];
$base = $state;//$state==1 ? 3:1;
$states = mysql_select("SELECT id,name FROM shop_states ORDER BY id",'array');
$cities = mysql_select("SELECT id,name FROM shop_cities WHERE state = ".$state,'array');
//echo "SELECT id,name FROM shop_cities WHERE state = ".$state;
$neighborhoods = mysql_select("SELECT * FROM shop_neighborhoods WHERE 1 ". (@$get['city']>0? " AND city = '".intval($get['city'])."' " : " "),'array');
/*if (@$get['property_type']) {
	$property_types2 = mysql_select("SELECT id,name FROM shop_property_types WHERE " . ($state == 1 ? " ny_property_type = '" . intval($get['property_type']) . "' " : " sf_property_type = '" . intval($get['property_type']) . "' "), 'array');
}*/

$table = array(
	'id'		=>	'id',
	'property_type' => '::table_property_type',
	'name'		=>	'',
	'url'=>'',
	'state'      => $states,
	'city' => $cities,
	'neighborhood' => $neighborhoods,
	'price_min'		=>	'right',
	'price_max'		=>	'right',
	'beds'=>'',
	'baths'=>'',
	'display'	=>	'boolean'
);

function table_property_type($q) {
	global $config;
	$content = '';
	if ($q['state']==1) {
		$content = @$config['ny_property_types'][$q['property_type']];
	}
	else {
		$content = $config['object_groups'][$q['property_type']]['name'];
	}
	return '<td>'.$content.'</td>';
}

$where .= (@$get['state'] > 0) ? " AND state = ".$get['state'] : '';
$where .= (@$get['neighborhood'] > 0) ? " AND neighborhood = ".$get['neighborhood'] : '';
$where .= (@$get['city'] > 0) ? " AND city = ".$get['city'] : '';
$where .= (@$get['property_type'] > 0) ? " AND property_type = ".$get['property_type'] : '';
$where .= (@$get['property_type2'] > 0) ? " AND property_type2 = '".$get['property_type']."'" : '';
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(name) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(url) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
	)
";

$query = "
	SELECT
		*
	FROM
		seo_shop
	WHERE 1 $where
";

$filter[] = array('search');
$filter[] = array('state',$states,null);
$filter[] = array('city',$cities,'-city-');
if (@$get['city']) {
	$filter[] = array('neighborhood', $neighborhoods, '-neighborhood-');
}
$filter[] = array('property_type',select_property_type1 ($base,@$get['residential'],@$get['property_type']),null);
if (@$get['property_type']) {
	$filter[] = array('property_type2', select_property_type ($base,@$get['residential'],@$get['property_type'],@$get['property_type2']),null);
}

$form[] = '<input name="state" value="'.$state.'" type="hidden">';

$form[] = array('input td9','name',true);
$form[] = array('checkbox td3','display',true);

//$form[] = array('select td3','state',array(true,$states));
$form[] = array('select td3','city',array(true,$cities,''));
$form[] = array('select td3','neighborhood',array(true,$neighborhoods,''));
$form[] = '<div class="clear"></div>';

$form[] = array('select td3','residential',array(true,$config['object_residential'],''));
$form[] = array('select td3','property_type',array(true,select_property_type1 ($base,@$post['residential'],@$post['property_type'])));
if (@$post['property_type']>0) $form[] = array('select td3','property_type2',array(true,select_property_type ($base,@$post['residential'],@$post['property_type'],@$post['property_type2'])));
else $form[] = array('select td3','property_type2',array(true,array()));
$form[] = '<div class="clear"></div>';

$form[] = array('input td3','beds',true);
$form[] = array('input td3','baths',true);
$form[] = array('input td3','price_min',true);
$form[] = array('input td3','price_max',true);

$form[] = array('textarea td12','names',true);
$form[] = array('tinymce td12','text',true);
$form[] = array('seo','seo url title keywords description',true);
$form[] = array('file td6','img','Main image',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));



$content.= '
<script type="text/javascript">
$(document).ready(function(){

    $(document).on("change",".form select[name=city]",function(){
    //alert(1);
	// меняем районы
            var cities = $(this);
            var neighborhoods = $(cities).closest(".form").find("select[name=neighborhood]");
            $(neighborhoods).html("");
            $.ajax({
                url: "/ajax.php",
                data: {
                    file: "neighborhoods",
                    city: $(cities).val(),
                },
                type: "get",
                dataType: "json",
                beforeSend: function(){
                    //$(neighborhoods).find("option").slice(1).remove();
                },
                success: function(json){
                    if(json.result)
                    {
                    //alert(2);
                    	$(neighborhoods).append($("<option>").text("").attr("value", 0));
                        $.each(json.data, function(i, v) {
                            $(neighborhoods).append($("<option>").text(v.name).attr("value", v.id));
                        });
                    }
                }
            });
	});
	//property_type
	$(document).on("change",".form select[name=property_type],.form select[name=residential]",function(){
		var property_type = $(".form select[name=property_type]").val();
		var residential = $(".form select[name=residential]").val();
		var base = $(".form input[name=base]").val();
		base = base<3 ? 2:1;
		if ((base==1 && property_type==2) || (base==2 && property_type==4)) {
			$(".form select[name=property_type2]").html("");
		}
		else {
			$.get(
				"/ajax.php?file=property_type",
				{"residential": residential, "property_type": property_type, "base": base},
				function (data) {
					$(".form select[name=property_type2]").html(data);
				}
			);
		}
	});
	//listing_details
	$(document).on("change",".form select[name=property_type],.form select[name=property_type2],.form select[name=residential]",function(){
		var property_type = $(".form select[name=property_type]").val();
		var property_type2 = $(".form select[name=property_type2]").val();
		var residential = $(".form select[name=residential]").val();
		var id = $(".form .form_head span").text();
		//alert(id);
		if (property_type==0 || residential==0) {
			$("#listing_details").html("Select Sale/Rent and Real Estate Type");
		}
		else {
			$.get(
				"/ajax.php?file=listing_details",
				{"id":id,"residential": residential, "property_type": property_type, "property_type2": property_type2},
				function (data) {
					$("#listing_details").html(data);
					$("#listing_details .datepicker input").datepicker({dateFormat:"yy-mm-dd"});
				}
			);
		}
	});
	$(this).on("form.open",".form",function(){
		//alert(2);
		$(".form select[name=property_type2]").trigger("change");
	});
	$(".form select[name=property_type2]").trigger("change");
});
</script>';
