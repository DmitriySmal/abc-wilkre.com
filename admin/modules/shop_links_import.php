<?php

$a18n['neighborhood'] = 'neighborhood';
$a18n['residential'] = 'sale|rent';
$a18n['property_type'] = 'real estate type';
$a18n['property_type2'] = 'property type';

$states = mysql_select("SELECT * FROM shop_states",'array');
$property_types2 = mysql_select("SELECT * FROM shop_property_types2",'rows_id');

$table = array(
	'name',
	'neighborhood',
	'residential',
	'property_type',
	'property_type2',
	'img'
);

$content = '<br /><h2>Загрузка файла excel (csv, xls, xlsx)</h2>';
$content.= '<form method="post" enctype="multipart/form-data" action="">';
$content.= '<br /><input type="file" name="i">';
$content.= '<select name="state">'.select(@$_POST['state'],$states).'</select>';
$content.= '<br /><input type="submit" name="upload" value="Загрузить файл">';
$content.= '</form>';

if (count($_POST)>0) {
	$file = $exc = $data = false;
	//загрузка файла
	if (isset($_POST['upload'])) {
		if (is_file($_FILES["i"]["tmp_name"])) {
			$file = strtolower(trunslit($_FILES['i']['name']));
			$arr = explode('.',$file);
			$exc = end($arr);
			$file = ROOT_DIR."files/temp/".$file; //die($file);
			if (is_dir(ROOT_DIR.'files/temp') || mkdir(ROOT_DIR.'files/temp',0755,true)) {
				copy($_FILES["i"]["tmp_name"],$file);
			}
		}
		else $content.= '<br /><b>ошибка загрузки файла</b>';
	}
	//импорт файла
	elseif (isset($_POST['import'])) {
		if (is_file($_POST['file'])) {
			$file = $_POST['file'];
			$arr = explode('.',$file);
			$exc = end($arr);
		}
		else {
			$content.= '<br /><b>ошибка загрузки файла</b>';
		}
	}
	//обработка файла
	if ($file AND is_file($file)) {
		//загрузка csv
		$i = 0;
		if ($exc=='csv') {
			$handle = fopen($file, 'r');
			while (($value = fgetcsv($handle, 8000, ';')) !== FALSE) {
				$i++;
				foreach ($table as $k=>$v) {
					if ($k==0) {
						$data[$i][$k] = iconv("cp1251", "UTF-8",current($value));
						//next($value);
					}
					else $data[$i][$k] = iconv("cp1251", "UTF-8",next($value));
				}
			}
			fclose($handle);
		}
		//загрузка excel
		elseif ($exc=='xls' OR $exc=='xlsx') {
			include ROOT_DIR.'plugins/phpexcel/PHPExcel/IOFactory.php';
			$inputFileName = $file;
			//echo 'Loading file ',pathinfo($inputFileName,PATHINFO_BASENAME),' using IOFactory to identify the format<br />';
			$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
			$data = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		}
	}
	else {
		$content.= '<br /><b>ошибка типа файла</b>';
	}
	//вывод данных на экран
	if (is_array($data)) { //print_r($data);
		if (isset($_POST['upload'])) {
			$content = '<br /><h2>Подтверждение загрузки</h2>';
			$content.= '<form method="post" action="">
				<input name="file" type="hidden" value="'.$file.'">
				<input name="state" type="hidden" value="'.@$_POST['state'].'">
				<input type="submit" name="import" value="Загрузить данные на сайт?"> &nbsp; <a href="">назад</a></form><br />';
			$content.= '<br /><h2>Содержание загруженого файла</h2>';
			$content.= 'Ссылки на зеленом фоне будут обновлены, товары на красном фоне будут добавлены<br /><br />';
			$content.= $states[$_POST['state']];
			$content.= '<table class="table"><tr>';
			foreach ($table as $k=>$v) {
				$content.= '<th>'.$a18n[$v].'</th>';
			}
		}
		else {
			$insert = $update = 0;
			$content = '<h3>Результаты загрузки</h3>';
		}
		//алфавит для экселя
		$a = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		foreach ($data as $key=>$value) {
			//используем алфавит
			if ($value[$a[0]]) {
				$row = mysql_select("SELECT * FROM shop_links WHERE name = '".mysql_res($value[$a[0]])."' LIMIT 1",'row');
				$color = $row==false ? 'darkred' : 'green';
				if (isset($_POST['upload'])) $content.= '<tr class="bg_'.$color.'">';
				$post = array();
				foreach ($table as $k=>$v) {
					if (isset($_POST['upload'])) {
						$content.= '<td>'.$value[$a[$k]].'</td>';
					}
					//формирование массива для обновления БД
					else {
						$post[$v] = $value[$a[$k]];
					}
				}
				if (isset($_POST['import'])) {
					$post['state'] = $_POST['state'];
					if ($post['property_type']==0) {
						if ($post['property_type2']) {
							if ($post['state']==1) $post['property_type'] = $property_types2[$post['property_type2']]['ny_property_type'];
							else $post['property_type'] = $property_types2[$post['property_type2']]['fl_property_type'];
						}
					}

					if ($row==false) {
						$post['rank'] = rand(1,999999);
						mysql_fn('insert','shop_links',$post);
						$insert++;
					}
					else {
						$post['id'] = $row['id'];
						if ($row['rank']==0) $post['rank'] = rand(1,999999);
						mysql_fn('update','shop_links',$post);
						$update++;
					}
				}
				if (isset($_POST['upload']))  $content.= '</tr>';
			}
		}
		if (isset($_POST['upload'])) $content.= '</table>';
		else {
			$content.= '<br />Количество обновленных товаров:'.$update;
			$content.= '<br />Количество добавленных товаров:'.$insert;
		}
	}
	else {
		$content.= '<br /><b>ошибка обработки файла</b>';
	}
}
unset($table);
