<?php

$save_as = true;

$table = array(
	'id'		=> 'keyword search id',
	'search'		=> '',
	'keyword'		=> '',
);

$filter[] = array('search');

$where = '';
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(shop_search2.search) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(shop_search2.keyword) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
	)
";
$query = "
	SELECT
		shop_search2.*
	FROM
		shop_search2
	WHERE 1 $where
";


$form[] = array('input td6','search',true);
$form[] = array('input td6','keyword',true);