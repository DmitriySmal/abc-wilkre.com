<?php

//$config['hide_add_button'] = true;

$base = array(
	1=> 'MLS Agents',
	2=> 'Zillow clients',
	3=> 'Trulia clients',
);

$table = array(
	'id'		=>	'id name email phone',
	'base'      => $base,
	'name'		=>	'',
	'surname'	=>	'',
	'phone'     => '',
	'email'		=>	'',
	'url'		=>	'',
	'firm_id'	=>	''
);

if ($get['m']=='agents_mls2') $post['base'] = 2;
elseif ($get['m']=='agents_mls3') $post['base'] = 3;
else $post['base'] = 1;

$filter[] = array('search');

$where = '';
$where.= " AND base=".$post['base'];
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(name) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(email) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(phone) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
	)
";

$query = "SELECT * FROM agents_mls WHERE 1 $where";
//echo $query;

$form[] = '<input type="hidden" name="base" value="'.$post['base'].'" />';
$form[] = array('input td4','name',true);
$form[] = array('input td4','surname',true);
$form[] = array('input td4','firm_id',true);
$form[] = array('input td12','phone',true);
$form[] = array('input td12','email',true);
$form[] = array('input td12','url',true);

