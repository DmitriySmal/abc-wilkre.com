<?php

$object = array();

$categories = array(
	'A' => 'Residential',
	'B' => 'Residential Income',
	'C' => 'Land/Docks',
	'D' => 'Business',
	'E' => 'Comm/Industry',
	'F' => 'Rental'
);

$content = '<div id="filter">
        <form>
        <div class="filter">
        <select name="type">'.select(@$_GET['type'],$categories).'</select>
        </div>
            <div class="filter">
            <input type="hidden" name="m" value="'.@$_GET['m'].'">

            <input type="text" name="object" value="'.@$_GET['object'].'" placeholder="382985" style="top: -4px; position: relative;">

            <button type="submit" style="top: -3px; right: -4px; position: relative;">FIND</button>
            </div>
        </form>
</div>
<div class="clear"></div>';

if (@$_GET['object']) {
	require_once(ROOT_DIR.'functions/rets3_func.php');
	if ($data = flex_id ($_GET['object'],$_GET['type'])) {
		$content.= '<div class="object_information" style = "padding: 30px 0;" >';
		$content.= '<ul style="list-style: none; width: 100%; overflow: hidden; display: block; ">';
		//$content.= $data['LIST_1'];
		/*if ($open_houses = flex_open_houses (array('uid'=>$_GET['object']),true)) {
			foreach ($open_houses as $k=>$v) {
				$content.= '<li class="value" style="display: block; margin-right: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 800px;">';
				$content.= 'from '.$v['from'];
				$content.= '<br>to '.$v['to'];
				$content.= '<br>date '.$v['date'];
				$content.= '<br>OpenHouseID '.$v['OpenHouseID'];
				$content.= '<br>agent '.$v['agent'];
				$content.= '</li>';
			}
		}
		*/


		foreach ($data as $k=>$v) {
			$content.= '<li class="value" style="display: block; margin-right: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 800px;"><span class="key" style="margin-right: 10px; min-width: 250px; display: inline-block;">'.$k.'</span>'.@$v.'</li>';
		}

		$content.= '</ul>';
		if ($data = flex_img ($data['LIST_2'].'-'.$_GET['object'])) {
			//[9.10] Картинки маленькие
			foreach ($data as $k => $v) {
				//$img = base64_encode($v['Data']);
				//$img = 'data: ' . $v['Content-Type'] . ';base64,' . $img;
				$img = $v['Location'];
				$content .= '<img style="width: 100px" src="' . $img . '">';
			}
		}
		else $content.= 'нет фото';
	}
	else $content.= 'нет объекта';
}
