<?php

include(ROOT_DIR.'functions/mls_func.php');

if ($get['u']=='') {
	$plates = mysql_select("SELECT plate id, plate id FROM shop_products WHERE plate>0", 'array');
	if ($plates) foreach ($plates as $k => $v) unset($config['plates'][$k]);
}
if ($get['u']=='form' OR $get['id']) {
	$plates = mysql_select("SELECT plate id, plate id FROM shop_products WHERE plate>0 AND id!=".intval(@$get['id']), 'array');
	if ($plates) foreach ($plates as $k => $v) unset($config['plates'][$k]);
}

//$save_as = true;
if ($get['m']=='ny_shop_products_own') {
	include(ROOT_DIR.'_config_ny.php');
	$property_type = $config['ny_property_types'];
	$neighborhoods = mysql_select("SELECT * FROM shop_neighborhoods WHERE 1 ". (@$get['city']>0? " AND city = '".intval($get['city'])."' " : " AND city != 6"),'array');
	$cities = mysql_select("SELECT id,name FROM shop_cities WHERE id!=6",'array');
	$base = 4;
	$statuses = $config['ny_object_statuses'];
}
elseif ($get['m']=='mx_shop_products_own') {
	include(ROOT_DIR.'_config_ny.php');
	$property_type = $config['ny_property_types'];
	$neighborhoods = mysql_select("SELECT * FROM shop_neighborhoods WHERE 1 ". (@$get['city']>0? " AND city = '".intval($get['city'])."' " : " AND city != 6"),'array');
	$cities = mysql_select("SELECT id,name FROM shop_cities WHERE id!=6",'array');
	$base = 7;
	$statuses = array_merge($config['mx_object_statuses'],$config['ny_object_statuses']);
}
else {
	$neighborhoods = mysql_select("SELECT * FROM shop_neighborhoods WHERE city = 6",'array');
	$cities = mysql_select("SELECT id,name FROM shop_cities WHERE id=6",'array');
	$property_type = array();
	foreach ($config['object_groups'] as $k=>$v) {
		$property_type[$k]=$v['name'];
	}
	$base = 1;
	$statuses = $config['object_statuses'];
}

$agents = mysql_select("SELECT id,name FROM users WHERE agent=1 ORDER BY name",'array');
$categories = $config['categories'];
$types = $config['for_sale'];

if ($get['u']=='post') {
	if ($get['name']=='plate' AND $get['value']==0) $get['value'] = NULL;
}

if ($get['u']=='edit') {
	$post['date_change'] = date('Y-m-d H:i:s');
	$date = date('Y-m-d');
	$post['oh_date'] = '0000-00-00';
	if ($post['plate']==0) $post['plate'] = NULL;
	if(@$post['oh_dates']){
		foreach ($post['oh_dates'] as $k=>$v) {
			if (strtotime($v['date'])<strtotime($date)) unset($post['oh_dates'][$k]);
			else {
				if (strtotime($v['date']) > strtotime($post['oh_date']) OR $post['oh_date'] == '0000-00-00')
					$post['oh_date'] = $v['date'];
			}
		}
		$post['oh_dates'] = serialize($post['oh_dates']);
		//$post['oh_display'] = 1;
	}
	else $post['oh_dates'] = '';
	if(@$post['store_info']) {
		$store_info = array();
		foreach ($post['store_info'] as $key=>$val) {
			foreach ($val as $k=>$v) {
				if ($v) {
					$store_info[$key] = $val;
					continue;
				}
			}
		}
		if ($store_info) $post['store_info'] = serialize($store_info);
		else $post['store_info'] = '';
	}

    if($post['neighborhood'] && empty($post['zip_code'])) {
	    $zip = mysql_select("SELECT * FROM shop_neighborhoods_zips WHERE nb_id = {$post['neighborhood']} LIMIT 1", 'row');
	    if ($zip)
		    $post['zip_code'] = $zip['zip'];
    }
	//$post['uid'] = $post['parameters'][1];
	//$post['status'] = $post['parameters'][3];
	$post['unit'] = $post['parameters'][29];
	$post['parameters'][12] = name_search($post['parameters'][12]);
	$post['address'] = $post['parameters'][11].' '.$post['parameters'][12];
	$post['name'] = $post['address'];
	if ($post['unit']) $post['name'].= ' #'.$post['unit'];
	$post['title'] = $post['name'];
	$post['beds'] = $post['parameters'][21];
	$post['baths'] = $post['parameters'][22];
	$post['square'] = intval($post['parameters'][100]);
	$post['price'] = $post['parameters'][4];
	$post['url'] = trunslit($post['name']);
	//сохраняем с вкладки комершал
	if (($post['base']<3 AND $post['property_type']==4) OR($post['base']>2 AND $post['property_type']==2)) {
		if ($post['parameters'][76]) {
			$post['property_type2'] = '"'.implode('","', $post['parameters'][76]).'"';
		}
		else $post['property_type2'] = '';
	}
	//добавляем кавычки для проперти тайп
	else {
		if (@$post['property_type2']) {
			$post['property_type2'] = mysql_select("SELECT name FROM shop_property_types2 WHERE id='".intval($post['property_type2'])."'",'string');
			$post['property_type2'] = '"'.$post['property_type2'].'"';
		}
	}
	$post['parameters'] = serialize($post['parameters']);

	$post['rank'] = object_rank ($post);

    /*
    if($post['zip_code'] && empty($post['neighborhood'])){
        $zip = mysql_select("SELECT * FROM shop_neighborhoods_zips WHERE zip = {$post['zip_code']} LIMIT 1",'row');
        if ($zip)
            $post['neighborhood'] = $zip['nb_id'];
    }*/

    if($post['coordinates'] && !empty($post['coordinates'])){
        $coordinates = trim($post['coordinates'],' ()');
        $location = explode(',', $coordinates);
        //var_dump($location);
        $post['lat'] = trim($location[0]);
        $post['lng'] = trim($location[1]);
    }
}

$table = array(
	'_edit'		=>	true,
	'id'		=>	'date_change:desc id:desc name price errors plate',
	'plate'     => '::table_plate',
	//'plate'     => '',
	'uid'		=>	'',
	'mln'		=>	'',
	'property_type' => $property_type,
	'name'		=>	'::row_object',
	'unit'      => '',
	'city' => $cities,
	'neighborhood' => $neighborhoods,
	'zip_code' => '',
	'coords' => '{lat}<br>{lng}',
	'date_change'		=>	'date',
	'price'		=>	'right',
	//'errors'    =>  '',
	'status'	=>	'',
	'landing'	=>	'boolean',
	'special'	=>	'boolean',
	'display'	=>	'boolean'
);

function table_plate ($q) {
	global $config;
	$array = $config['plates'];
	$select = select($q['plate'],$array,'');
	if ($q['plate']) $select.= '<option value="'.$q['plate'].'" selected="selected">'.$q['plate'].'</option>';
	return '<td data-id="'.$q['id'].'" data-name="plate"><select>'.$select.'</select></td>';
}

$where = '';
if ($user['agent']==1) $where = " AND shop_products.agent=".$user['id'];
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(shop_products.mln) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(shop_products.name) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
		OR shop_products.id = '".mysql_real_escape_string($get['search'])."'
	)
";
$where .= (@$get['neighborhood'] > 0) ? " AND neighborhood = ".$get['neighborhood'] : '';
$where .= (@$get['city'] > 0) ? " AND city = ".$get['city'] : '';
$where .= (@$get['display'] > 0) ? " AND display = 1" : '';
//$where .= (@$get['category'] > 0) ? " AND category = ".$get['category'] : '';
$where .= (@$get['errors']) ? " AND errors = ".$get['errors'] : '';
//if (@$_GET['status']=='A,PS,T,R') $where .= " AND status IN ('A','PS','T','R') AND mln LIKE ('A%')" ;
//else
$where.= (@$_GET['status']) ? " AND status = '".mysql_res($_GET['status'])."'" : '';
if ($get['m']=='ny_shop_products_own') $where.= ' AND shop_products.base = 4 ';
elseif ($get['m']=='mx_shop_products_own') {
	$where.= ' AND shop_products.base IN (4,7) ';
}
else $where.= ' AND shop_products.base = 1';
$query = "
	SELECT
		shop_products.*
	FROM
		shop_products
	WHERE shop_products.id>0 $where
";
//echo $query;

$filter[] = array('search');
$filter[] = array('city',$cities,'-city-');
$filter[] = array('neighborhood',$neighborhoods,'-neighborhood-');
$filter[] = array('status',$statuses,'-status-');

$delete = array(
	'delete'=>"DELETE FROM shop_reviews WHERE product = '".$get['id']."'"  //удаление отзывов
);

$tabs = array(
	1=>'Main Settings',
	2=>'Listing Details',
	10=>'Remarks',
	11=>'Features',
	3=>'Owner Information',
	4=>'Buyer Information',
	//5=>'Commercial Information',
	//3=>'Property Information',
	//4=>'Financial Information',
	7=>'Photo and Video',
	8=>'Map',
	//6=>'Share',
	9=>'Open House'
);

if ($get['u']=='form' OR $get['id']>0) {
	$parameters = @$post['parameters'] ? unserialize($post['parameters']) : array();
	$parameters[1] = @$post['uid'];
	if (@$parameters[1]==0) {
		$parameters[1] = mysql_select("SELECT uid FROM shop_products WHERE base=".$base." ORDER BY uid DESC LIMIT 1",'string');
		$parameters[1] ++;
	}
}

//$form[1][] = '-'.@$post['name'].'-';
$form[1][] = '<input name="base" value="'.($post['base']?$post['base']:$base).'" type="hidden"/>';
$form[1][] = '<input name="office_name" value="WILK REAL ESTATE I LLC" type="hidden"/>';

$form[1][] = array('select td3','residential',array(true,$config['object_residential'],''),array('name'=>'For Sale / For Rent'));
$form[1][] = array('select td3','property_type',array(true,$property_type),array('name'=>'Real Estate Types'));

$form[1][] = '<div class="field select td3"><label><span>Property Types</span></label><div><select name="property_type2">';
if ((@$post['base']<3 AND @$post['property_type']==4) OR(@$post['base']>2 AND @$post['property_type']==2)) {}
else {
	if (@$post['property_type2']) {
		if ($base < 3)
			$property_type2 = mysql_select("SELECT id FROM shop_property_types2
			WHERE fl_property_type='" . $post['property_type'] . "'
			AND name = '" . mysql_res(trim($post['property_type2'], '"')) . "'", 'string');
		else
			$property_type2 = mysql_select("SELECT id FROM shop_property_types2
			WHERE ny_property_type='" . $post['property_type'] . "'
			AND name = '" . mysql_res(trim($post['property_type2'], '"')) . "'", 'string');
	}

	$form[1][] = select_property_type($base, @$post['residential'], @$post['property_type'], @$property_type2);
}
$form[1][] = '</select></div></div>';
if ($user['agent']==0) {
	$form[1][] = array('input td2', 'mln', true);
	$form[1][] = array('select td1', 'plate', array(true, $config['plates'], '-select-'));
}
else $form[1][] = array('input td3', 'mln', true);
$form[1][] = array('select td3','city',array(true,$cities,'-select-'));
$form[1][] = array('select td3','neighborhood',array(true,$neighborhoods,'-select-'));
$form[1][] = array('input td3', 'zip_code', true);
$form[1][] = array('input td3', 'date', true);
$form[1][] = array('input td3', 'date_change', true,array('attr'=>'class="date"'));

$form[1][] = array('input td3', 'uid', true,array('name'=>'Web ID/ MLS#'));

$form[1][] = array('select td3','status',array(true,$statuses,'-select-'));

$form[1][] = array('input td3','office_name',true); //ListOfficeName
$form[1][] = array('input td3','office_phone',true); //ListAgentDirectWorkPhone
$form[1][] = array('input td3','office_agent',true); //ListAgentFullName
$form[1][] = array('input td3','office_agent_phone',true); //ListOfficePhone
$form[1][] = array('input td3','office_agent_email',true); //ListAgentEmail

$array = array();
foreach ($config['shop_params'][2]['data'] as $k1=>$v1) $array[$v1] = $v1;
$form[1][] = array('select td3', 'parameters[2]', array(@$parameters[2],$array,'--'),array('name'=>$config['shop_params'][2]['name']));

$form[1][] = array('checkbox','landing',true);
$form[1][] = array('checkbox','special',true);
$form[1][] = array('checkbox','display',true);
$form[1][] = '<div class="clear"></div>';

$form[2][] = '<div id="listing_details">';
$form[2][] = '</div>';

//$form[2][] = $post['property_type2'];
//$pt = trim(@$post['property_type2'],'"');

$form[7][] = array('file td6','img','Main image',array(''=>'resize 1000x1000','m-'=>'!cut 536x320','p-'=>'resize 293x117'));
$form[7][] = array('input td12','booklet',true);
$form[7][] = array('input td12','video',true);
$form[7][] = array('input td12','3d',true);
$form[7][] = array('file_multi','imgs','Additional images',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));

$form[10][] = array('tinymce td12','text',true,array('name'=>'Remarks'));

foreach ($config['shop_params_groups'] as $key=>$val) {
	foreach ($config['shop_params'] as $k=>$v) if (@$v['group']==$key) {
		if ($k==28) {
			if ($user['agent']==1) $form[$key][] = '<input type="hidden" name="agent" value="'.$user['id'].'"/>';
			else $form[$key][] = array('select td3','agent',array(true,$agents,'-select-'));
			$form[$key][] = '<h2>Condo/Coop/House/Commercial</h2>';
		}

		$td = @$v['td'] ? $v['td'] : 'td3';
		if (@$v['hide']==1) $v['name'] = '<strong style="color:#999">'.$v['name'].'</strong>';
		if (@$v['type']=='yn') {
			$form[$key][] = array('select '.$td, 'parameters[' . $k . ']', array(@$parameters[$k],array(1=>'Yes',2=>'No'),'--'),array('name'=>$v['name']));
		}
		elseif (@$v['type']=='date') {
			$form[$key][] = array('input datepicker '.$td, 'parameters[' . $k . ']', @$parameters[$k],array('name'=>$v['name']));
		}
		elseif (@$v['type']=='multicheckbox') {
			$array = array();
			foreach ($v['data'] as $k1=>$v1) $array[$v1] = $v1;
			$form[$key][] = array('multicheckbox '.$td, 'parameters[' . $k . ']',  array(@$parameters[$k],$array),array('name'=>$v['name']));
		}
		elseif (@$v['type']=='checkbox') {
			$form[$key][] = array('checkbox td3', 'parameters[' . $k . ']', @$parameters[$k],array('name'=>$v['name']));
		}
		elseif (isset($v['data'])) {
			$array = array();
			foreach ($v['data'] as $k1=>$v1) $array[$v1] = $v1;
			$form[$key][] = array('select '.$td, 'parameters[' . $k . ']', array(@$parameters[$k],$array,'--'),array('name'=>$v['name']));
		}
		else $form[$key][] = array('input '.$td, 'parameters[' . $k . ']', @$parameters[$k],array('name'=>$v['name']));
		if (@$v['clear']==1) $form[$key][] = '<div class="clear"></div>';
	}
}

$form[10][] = array('seo','seo url title keywords description',true);


//$form[3][] = array('file_multi_db','shop_items','Additional images',array(''=>'resize 1000x1000','preview'=>'resize 150x150'));

$google = preg_replace('~[^-0-9.,]+~u','',@$post['coordinates']);
$google = $google ? $google : '39.5,-0.38';
//var_dump($google);
$form[8][] = array('input td10','address',@$post['address'],array('name'=>'address'));
$form[8][] = '<a href="#" id="coordinates_search" class="button red" style="margin:15px 0 25px"><span>Search</span></a>';
$form[8][] = '<div style="display:none">';
$form[8][] = array('input td12','coordinates',@$post['coordinates'],array('name'=>'карта'));
$form[8][] = '</div>';
$form[8][] = '<div id="map" style="clear:both; width:790px; height:500px"></div>';

//$form[9][] = array('input td4','oh_hosted_by',true,array('name'=>'hosted by'));
//$form[9][] = array('input td4','oh_phone',true,array('name'=>'phone'));

//$form[9][] = array('input td4','oh_date',true,array('name'=>'date'));
$form[9][] = array('textarea td9','oh_text',true,array('name'=>'directions & description'));
$form[9][] = array('checkbox td3','oh_display',true,array('name'=>'open houses'));

$form[9][] = '<div style="clear:both; background:#E9E9E9; padding:5px 10px; width:875px; margin:0 -10px">';
$form[9][] = '<table class="oh_dates">';
$form[9][]= '<tr data-i="0">';
$form[9][] = '<th>date</th>';
$form[9][] = '<th>from</th>';
$form[9][] = '<th>to</th>';
$form[9][] = '<th>agent</th>';
$form[9][] = '<th><a href="#" style="background:#35B374; display:inline-block; padding:2px; border-radius:10px"><span class="sprite plus"></span></a></th>';
$form[9][] = '</tr>';
$agents2 = '<option value="0">select</option>';
foreach ($agents as $k=>$v) {
	$agents2.='<option value="'.$k.'" {agent_'.$k.'}>'.$v.'</option>';
}
$template['oh_dates'] = '
	<tr data-i="{i}">
		<td><input class="date" name="oh_dates[{i}][date]" value="{date}" placeholder="'.date('Y-m-d').'"/></td>
		<td><input name="oh_dates[{i}][from]" value="{from}" placeholder="9:00" /></td>
		<td><input name="oh_dates[{i}][to]" value="{to}" placeholder="16:00"/></td>
		<td><select name="oh_dates[{i}][agent]">'.$agents2.'</select></td>
		<td><a href="#" class="sprite boolean_0"></a></td>
	</tr>
';
if (isset($post['oh_dates'])) {
	$oh_text = unserialize($post['oh_dates']); //print_r ($basket);
	if (is_array($oh_text)) foreach ($oh_text as $key=>$val) {
		$val['i'] = $key;
		$val['agent_'.$val['agent']] = ' selected="selected" ' ;
		$form[9][] = template($template['oh_dates'],$val);
	}
}
$form[9][] = '</table></div>';


$content.= '<div style="display:none">';
$content.= '<textarea id="template_oh_dates">'.htmlspecialchars($template['oh_dates']).'</textarea>';
$content.= '</div>';
$content.= '<style type="text/css">
.form .oh_dates {width:100%}
.form .oh_dates th {text-align:left; padding:0 0 5px;}
.form .oh_dates td {border-top:1px solid #F3F3F3; padding:5px 0; vertical-align:top;}
.form .oh_dates input {text-align:right; border:1px solid gray; margin:0; padding:0 2px; height:19px; width:70px}
.form .oh_dates .product_name {width:550px; text-align:left;}
.form .oh_dates td td {border:none}
.form .store_info {width:100%}
.form .store_info th {text-align:left; padding:0 0 5px;}
.form .store_info td {border-top:1px solid #F3F3F3; padding:5px 0; vertical-align:top;}
.form .store_info input {text-align:right; border:1px solid gray; margin:0; padding:0 2px; height:19px; width:70px}
.form .store_info .product_name {width:550px; text-align:left;}
.form .store_info td td {border:none}
</style>';


$content.= '

<script type="text/javascript" src="https://maps.google.com/maps/api/js?key='.$config['google_maps_key'].'"></script>
<script type="text/javascript">
$(document).ready(function(){
	//open house
	$(this).on("form.open",".form",function(){
		$("input.date").datepicker({dateFormat:"yy-mm-dd"});
		//$(".form select[name=property_type2]").triger("change");
	});
	$(document).on("click",".oh_dates th a",function(){
		var i = $(this).parents("table").find("tr:last").data("i");
		i++;
		var content = $("#template_oh_dates").val();
		content = content.replace(/{i}/g,i);
		content = content.replace(/{[\w]*}/g,"");
		$(this).parents("table").append(content);
		$(".oh_dates input.date").datepicker({dateFormat:"yy-mm-dd"});
		return false;
	});
	$(document).on("click",".oh_dates td a",function(){
		$(this).parents("tr").remove();
		return false;
	});
	//APT. & STORE INFORMATION
	$(document).on("click",".store_info td a",function(){
		$(this).parents("tr").remove();
		return false;
	});
	$(document).on("click",".store_info th a",function(){
		var i = $(this).parents("table").find("tr:last").data("i");
		i++;
		var content = $("#template_store_info").val();
		//alert(content);
		content = content.replace(/{i}/g,i);
		content = content.replace(/{[\w]*}/g,"");
		$(this).parents("table").append(content);
		return false;
	});

	$(document).on("change",
		".form input[name=\'parameters[4]\']," +
		".form input[name=\'parameters[25]\']," +
		".form input[name=\'parameters[26]\']," +
		".form input[name=\'parameters[27]\']"
	,function(){
		total_bulding ();
	});

	function total_bulding () {
		var p27 = $(".form input[name=\'parameters[27]\']").val(),
			p26 = $(".form input[name=\'parameters[26]\']").val(),
			p25 = $(".form input[name=\'parameters[25]\']").val(),
			p78 = p27*p26*p25,
			p4 = $(".form input[name=\'parameters[4]\']").val();
			//p42 = p27*p4;
		$(".form input[name=\'parameters[78]\']").val(p78);
		//$(".form input[name=\'parameters[42]\']").val(p42); //[4.415] убрать GROSS RENT YR
	}


	$(document).on("click",".bookmarks a",function(){
		var i = $(this).data("i");
                var coords = $("input[name=coordinates]").val().replace("(","").replace(")","").split(",");
                coords = (coords.length > 1) ? coords : new Array (25.7616798,-80.191);
		if (i==8) {
                        console.log(coords);
			geocoder = new google.maps.Geocoder();
			map = new google.maps.Map(document.getElementById("map"),{
				zoom: 10,
				center: new google.maps.LatLng(coords[0],coords[1]),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
			});
			markersArray = [];
			var marker = new google.maps.Marker({position:new google.maps.LatLng(coords[0],coords[1])});
			marker.setMap(map);
			markersArray.push(marker);
			google.maps.event.addListener(map, "click", function(event) {
				for (i in markersArray) markersArray[i].setMap(null);
				markersArray.length = 0;
				var marker = new google.maps.Marker({position:event.latLng,map:map});
				markersArray.push(marker);
				$("input[name=coordinates]").val(event.latLng);
			});
		}
		return false;
	});

    $(document).on("change",".form select[name=city]",function(){
	// меняем районы
            var cities = $(this);
            var neighborhoods = $(cities).closest(".form").find("select[name=neighborhood]");
            $.ajax({
                url: "/ajax.php",
                data: {
                    file: "neighborhoods",
                    city: $(cities).val(),
                },
                type: "get",
                dataType: "json",
                beforeSend: function(){
                    $(neighborhoods).find("option").slice(1).remove();
                },
                success: function(json){
                    if(json.result)
                    {
                        $.each(json.data, function(i, v) {
                            $(neighborhoods).append($("<option>").text(v.name).attr("value", v.id));
                        });
                    }
                }
            });
	});
	//property_type
	$(document).on("change",".form select[name=property_type],.form select[name=residential]",function(){
		var property_type = $(".form select[name=property_type]").val();
		var residential = $(".form select[name=residential]").val();
		var base = $(".form input[name=base]").val();
		base = base<3 ? 2:1;
		if ((base==1 && property_type==2) || (base==2 && property_type==4)) {
			$(".form select[name=property_type2]").html("");
		}
		else {
			$.get(
				"/ajax.php?file=property_type",
				{"residential": residential, "property_type": property_type, "base": base},
				function (data) {
					$(".form select[name=property_type2]").html(data);
				}
			);
		}
	});
	//listing_details
	$(document).on("change",".form select[name=property_type],.form select[name=property_type2],.form select[name=residential]",function(){
		var property_type = $(".form select[name=property_type]").val();
		var property_type2 = $(".form select[name=property_type2]").val();
		var residential = $(".form select[name=residential]").val();
		var id = $(".form .form_head span").text();
		var base = $(".form input[name=base]").val();
		//base = base<3 ? 2:1;
		//alert(id);
		if (property_type==0 || residential==0) {
			$("#listing_details").html("Select Sale/Rent and Real Estate Type");
		}
		else {
			$.get(
				"/ajax.php?file=listing_details",
				{"id":id,"residential": residential, "property_type": property_type, "property_type2": property_type2,"base":base},
				function (data) {
					$("#listing_details").html(data);
					$("#listing_details .datepicker input").datepicker({dateFormat:"yy-mm-dd"});
				}
			);
		}
	});
	$(this).on("form.open",".form",function(){
		//alert(2);
		$(".form select[name=property_type2]").trigger("change");
	});
	$(".form select[name=property_type2]").trigger("change");

	/*
	$(document).on("change",".form select[name=property_type]",function(){
		// меняем районы
		var property_type = $(this).val(),
			content = (property_type>0 && $("#type"+property_type).length>0) ? $("#type"+property_type).html() : "";
			//alert(content);
			//alert($("#type2").html());
		$(".form select[name=property_type2]").html(content);
	});
	*/

        //поиск по гуглу
	$(document).on("click","#coordinates_search",function () {
		marker_search();
		return false;
	});

	function marker_search() {
		var address = $("input[name=\"address\"]").val();
		geocoder.geocode( { "address": address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				for (i in markersArray) markersArray[i].setMap(null);
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});
				markersArray.push(marker);
                                console.log(results[0].geometry.location.toString());
                                $("input[name=coordinates]").val(results[0].geometry.location.toString());
			} else {
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
	}

});
</script>';

