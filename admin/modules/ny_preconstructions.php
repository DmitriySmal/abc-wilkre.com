<?php

$neighborhoods = mysql_select("SELECT id,name FROM shop_neighborhoods WHERE city!=6 ORDER BY name",'array');

$table = array(
	'id'		=>	'date:desc id:desc name price',
	'img'		=>	'img',
	'name'		=>	'',
	'neighborhood' => $neighborhoods,
	//'year'		=>	'',
	//'floors'		=>	'',
	//'units'		=>	'',
	'date'		=>	'date',
	'display'	=>	'boolean'
);

if ($get['u']=='edit') {
	if ($post['parameters']) {
		$parameters = array();
		$rows = explode("\r",$post['parameters']);
		foreach ($rows as $k=>$v) {
			$row = explode(":",$v,2);
			$key = trim($row[0]);
			$parameters[$key] = trim($row[1]);
		}
		$post['parameters'] = serialize($parameters);
	}
}

$where = '';
if (@$get['neighborhood']) $where = " AND shop_preconstructions.neighborhood=".intval($get['neighborhood']);
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(shop_preconstructions.name) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'

	)
";

$query = "
	SELECT
		shop_preconstructions.*
	FROM
		shop_preconstructions
	WHERE shop_preconstructions.base=2 $where
"; //echo $query;

$filter[] = array('search');
$filter[] = array('neighborhood',$neighborhoods,'-neighborhood-');

$delete = array(
);

$tabs = array(
	1=>'Common',
	2=>'Settings',
	3=>'Images',
	5=>'Map',
);

$form[1][] = '<input name="base" value="2" type="hidden"/>';
$form[1][] = array('input td8','name',true);
$form[1][] = array('checkbox','display',true);
$form[1][] = '<div class="clear"></div>';
$form[1][] = array('input td2','year',true);
$form[1][] = array('input td2','floors',true);
$form[1][] = array('input td2','units',true);
$form[1][] = array('select td3','neighborhood',array(true,$neighborhoods,'-change-'));
$form[1][] = array('input td2','date',true);
$form[1][] = array('input td12','booklet',true);
$form[1][] = array('input td12','video',true);

$form[1][] = array('tinymce td12','text',true);
$form[1][] = array('seo','seo url title keywords description',true);

$parameters = @$post['parameters'] ? unserialize($post['parameters']) : '';
$param = '';
if ($parameters) foreach ($parameters as $k=>$v) {
	$param.= $k.': '.$v."\r";
}
$form[2][] = array('textarea td12','parameters',trim($param),array('attr'=>'style="height:200px"'));

$form[3][] = array('file td6','img','Main image',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));
$form[3][] = array('file_multi','imgs','Additional images',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));

//$form[3][] = array('file_multi_db','shop_items','Additional images',array(''=>'resize 1000x1000','preview'=>'resize 150x150'));

$google = preg_replace('~[^-0-9.,]+~u','',@$post['coordinates']);
$google = $google ? $google : '39.5,-0.38';
//var_dump($google);
$form[5][] = array('input td10 readonly','address',@$post['address'],array('name'=>'address'));//, 'attr'=>'readonly'));
$form[5][] = '<a href="#" id="coordinates_search" class="button red" style="margin:15px 0 25px"><span>Search</span></a>';
$form[5][] = '<div style="display:none">';
$form[5][] = array('input td12','coordinates',@$post['coordinates'],array('name'=>'карта'));
$form[5][] = '</div>';
$form[5][] = '<div id="map" style="clear:both; width:790px; height:500px"></div>';

$content.= '
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
$(document).ready(function(){        

	//замена параметров при смене категории
	$(document).on("change",".form select[name=category]",function(){
		var category = $(this).val(),
			id = $(".form").prop("id").replace(/[^0-9]/g,"");
		$.get(
			"/admin.php",
			{"m":"shop_products","u":"shop_parameters","category":category,"id":id},
			function(data){$("#shop_parameters").html(data)}
		);
	});
        
        $(document).on("change",".form select[name=city]",function(){
		// меняем районы
                var cities = $(this);
                var neighborhoods = $(cities).closest(".form").find("select[name=neighborhood]");
                $.ajax({
                    url: "/ajax.php",
                    data: {
                        file: "neighborhoods",
                        city: $(cities).val(),
                    },
                    type: "get",
                    dataType: "json",
                    beforeSend: function(){                        
                        $(neighborhoods).find("option").slice(1).remove();
                    },
                    success: function(json){                
                        if(json.result)
                        {
                            $.each(json.data, function(i, v) {
                                $(neighborhoods).append($("<option>").text(v.name).attr("value", v.id));
                            });
                        }
                    }
                });
	});

        
        $(document).on("click",".bookmarks a",function(){
		var i = $(this).data("i");
                var coords = $("input[name=coordinates]").val().replace("(","").replace(")","").split(",");
                coords = (coords.length > 1) ? coords : new Array (25.7616798,-80.191);
		if (i==5) {                    
                        console.log(coords);
			geocoder = new google.maps.Geocoder();
			map = new google.maps.Map(document.getElementById("map"),{
				zoom: 10,
				center: new google.maps.LatLng(coords[0],coords[1]),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
			});
			markersArray = [];
			var marker = new google.maps.Marker({position:new google.maps.LatLng(coords[0],coords[1])});
			marker.setMap(map);
			markersArray.push(marker);
			google.maps.event.addListener(map, "click", function(event) {                                
				for (i in markersArray) markersArray[i].setMap(null);
				markersArray.length = 0;
				var marker = new google.maps.Marker({position:event.latLng,map:map});
				markersArray.push(marker);
				$("input[name=coordinates]").val(event.latLng);
			});
		}
		return false;
	});
        
        //поиск по гуглу
	$(document).on("click","#coordinates_search",function () {
		marker_search();
		return false;
	});
        
	function marker_search() {
		var address = $("input[name=\"address\"]").val();
		geocoder.geocode( { "address": address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				for (i in markersArray) markersArray[i].setMap(null);
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});
				markersArray.push(marker);
                                console.log(results[0].geometry.location.toString());
                                $("input[name=coordinates]").val(results[0].geometry.location.toString());
			} else {
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
	}

});
</script>';

