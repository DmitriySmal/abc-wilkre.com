<?php

//$config['multilingual'] = true;

$locales = array(
	'en'	=>	'Английский',
	'ar'	=>	'Арабский',
	'bg'	=>	'Болгарский',
	'ca'	=>	'Каталанский',
	'cn'	=>	'Китайский',
	'cs'	=>	'Чешский',
	'da'	=>	'Датский',
	'de'	=>	'Немецкий',
	'el'	=>	'Греческий',
	'es'	=>	'Испанский',
	'eu'	=>	'Баскский',
	'fa'	=>	'Фарси',
	'fi'	=>	'Финский',
	'fr'	=>	'Французский',
	'he'	=>	'Иврит',
	'hu'	=>	'Венгерский',
	'it'	=>	'Итальянский',
	'ja'	=>	'Японский',
	'kk'	=>	'Казахский',
	'lt'	=>	'Литовский',
	'lv'	=>	'Латышский',
	'nl'	=>	'Голландский',
	'no'	=>	'Норвежский',
	'pl'	=>	'Польский',
	'ptbr'	=>	'Португальский (Бразилия)',
	'ptpt'	=>	'Португальский',
	'ro'	=>	'Румынский',
	'ru'	=>	'Русский',
	'si'	=>	'Словенский',
	'sk'	=>	'Словацкий',
	'sl'	=>	'Словенский',
	'sr'	=>	'Сербский',
	'th'	=>	'Таиландский',
	'tr'	=>	'Турецкий',
	'tw'	=>	'Тайванский',
	'ua'	=>	'Украинский',
	'vi'	=>	'Вьетнамский',
);

//многоязычный
if ($config['multilingual']) {
	$table = array(
		'id'			=>	'rank:desc name id',
		'name'			=>	'',
		'rank'			=>	'',
		'url'			=>	'',
		'localization'	=>	$locales,
		'display'		=>	'display'
	);
	$form[0][] = array('input td4','name',true);
	$form[0][] = array('input td2','rank',true);
	$form[0][] = array('input td2','url',true);
	$form[0][] = array('select td2','localization',array(true,$locales));
	$form[0][] = array('checkbox td2','display',true);
}
//одноязычный
else {
	$pattern = 'one_form';
	$get['id'] = 1;
	if ($get['u']!='edit') {
		$post = mysql_select("
			SELECT *
			FROM languages
			WHERE id = 1
			LIMIT 1
		",'row');
	}
}

$a18n['localization'] = 'localization';

//исключения
if ($get['u']=='edit') {
	//$post['dictionary'] = serialize($post['dictionary']);
	if ($get['id'] > 0 AND (is_dir(ROOT_DIR . 'files/languages/' . $get['id'] . '/dictionary') || mkdir(ROOT_DIR . 'files/languages/' . $get['id'] . '/dictionary', 0755, true))) {
		foreach ($post['dictionary'] as $key => $val) {
			$str = '<?php' . PHP_EOL;
			$str .= '$lang[\'' . $key . '\'] = array(' . PHP_EOL;
			foreach ($val as $k => $v) {
				$str .= "	'" . $k . "'=>'" . str_replace("'", "\'", $v) . "'," . PHP_EOL;
			}
			$str .= ');';
			$str .= '?>';
			$fp = fopen(ROOT_DIR . 'files/languages/' . $get['id'] . '/dictionary/' . $key . '.php', 'w');
			fwrite($fp, $str);
			fclose($fp);
		}
	}
	unset($post['dictionary']);
	//если мультиязычный то нужно добавлять колонки в мультиязычные таблицы
	if ($config['multilingual']) {
		if ($get['id'] == 'new') {
			$max = mysql_select("SELECT id FROM languages ORDER BY id DESC LIMIT 1",'string');
			$get['id'] = mysql_fn('insert', $get['m'], $post);
			mysql_query("ALTER TABLE `shop_products` ADD `name".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `name".$max."`");
			mysql_query("ALTER TABLE `shop_products` ADD `text".$get['id']."` TEXT NOT NULL AFTER `text".$max."`");
		}
	}
}
else {
	//$dictionary = unserialize(@$post['dictionary']);
	$root = ROOT_DIR.'files/languages/'.$get['id'].'/dictionary';
	if (is_dir($root) && $handle = opendir($root)) {
		while (false !== ($file = readdir($handle))) {
			if (strlen($file)>2)
				include(ROOT_DIR.'files/languages/'.$get['id'].'/dictionary/'.$file);
		}
	}
}

//правила удаления для многоязычного
$delete['confirm'] = array('pages'=>'language');
$delete['delete'] = array(
	"ALTER TABLE `shop_products` DROP `name".$get['id']."`",
	"ALTER TABLE `shop_products` DROP `text".$get['id']."`",
);


//вкладки
$tabs = array(
	0 => 'Common',
	1 => 'Forms',
	2 => 'Profile',
	3 => 'Catalogue',
	//4 => 'Корзина',
	//5 => 'Яндекс-маркет',
	//6 => 'Рассылка'
);

$form[0][] = lang_form('input td12','common|site_name','название сайта');
$form[0][] = lang_form('textarea td12','common|txt_meta','metatag');
$form[0][] = lang_form('input td12','common|txt_head','текст в шапке');
//$form[0][] = lang_form('textarea td12','common|txt_index','текст на главной');
//$form[0][] = lang_form('textarea td12','common|txt_footer','текст в подвале');
$form[0][] = lang_form('input td12','common|str_no_page_name','название страницы 404');
$form[0][] = lang_form('textarea td12','common|txt_no_page_text','текст страницы 404');
$form[0][] = lang_form('input td12','common|wrd_more','подробнее');
$form[0][] = lang_form('input td12','common|msg_no_results','нет результатов');
$form[0][] = lang_form('input td12','common|wrd_no_photo','нет картинки');
$form[0][] = lang_form('input td8','common|breadcrumb_index','хлебные крошки: на главную');
$form[0][] = lang_form('input td4','common|breadcrumb_separator','хлебные крошки: разделитель');
$form[0][] = lang_form('input td4','common|make_selection','сделайте выбор');
$form[0][] = lang_form('textarea td12','common|google_translate_script','код переводчика google');
$form[0][] = lang_form('input td4','common|phone_top','phone top');
$form[0][] = lang_form('input td4','common|call_back','call back');
$form[0][] = lang_form('input td12','slider|new_arrivals','new arrivals');
$form[0][] = lang_form('input td12','slider|featured_properties','featured properties for sale');
$form[0][] = lang_form('input td12','slider|condos','condo');
$form[0][] = lang_form('input td12','slider|condos_preconstruction','preconstruction condo');

$form[0][] = lang_form('textarea td12','common|contact_us','Contact Us');
$form[0][] = lang_form('input td12','common|copyright','Copyright');
$form[0][] = lang_form('textarea td12','common|counters','Counters');


$form[0][] = '<h2>Agents</h2>';
$form[0][] = lang_form('input td12','agents|my_agents','agents');
$form[0][] = lang_form('input td12','agents|phone','phone');
$form[0][] = lang_form('input td12','agents|more','more');
$form[0][] = '<h2>Pluses</h2>';
$form[0][] = lang_form('input td3','plus|one','plus one');
$form[0][] = lang_form('input td9','plus|one_desc','plus one desc');
$form[0][] = lang_form('input td3','plus|two','plus two');
$form[0][] = lang_form('input td9','plus|two_desc','plus two desc');
$form[0][] = lang_form('input td3','plus|three','plus three');
$form[0][] = lang_form('input td9','plus|three_desc','plus three desc');


$form[1][] = '<h2>Feedback</h2>';
$form[1][] = lang_form('input td12','feedback|name','name ');
$form[1][] = lang_form('input td12','feedback|first_name','first name');
$form[1][] = lang_form('input td12','feedback|last_name','last name');
$form[1][] = lang_form('input td12','feedback|phone','phone');
$form[1][] = lang_form('input td12','feedback|email','еmail');
$form[1][] = lang_form('input td12','feedback|text','message');
$form[1][] = lang_form('input td12','feedback|send','send');
$form[1][] = lang_form('input td12','feedback|attach','attach file');
$form[1][] = lang_form('input td12','feedback|message_is_sent','message sent');
$form[1][] = '<h2>Massages in forms</h2>';
$form[1][] = lang_form('input td12','validate|no_required_fields','не заполнены обязательные поля');
$form[1][] = lang_form('input td12','validate|short_login','короткий логин');
$form[1][] = lang_form('input td12','validate|not_valid_login','некорректный логин');
$form[1][] = lang_form('input td12','validate|not_valid_email','некорректный email');
$form[1][] = lang_form('input td12','validate|not_valid_password','некорректный пароль');
$form[1][] = lang_form('input td12','validate|not_valid_captcha','некорректный защитный код');
$form[1][] = lang_form('input td12','validate|not_valid_captcha2','отключены скрипты');
$form[1][] = lang_form('input td12','validate|error_email','ошибка при отправке письма');
$form[1][] = lang_form('input td12','validate|no_email','в базе нету такого email');
$form[1][] = lang_form('input td12','validate|duplicate_login','дублирование логина');
$form[1][] = lang_form('input td12','validate|duplicate_email','дублирование email');
$form[1][] = lang_form('input td12','validate|not_match_passwords','пароли не совпадают');

$form[2][] = lang_form('input td12','profile|hello','здравствуйте');
$form[2][] = lang_form('input td12','profile|link','личный кабинет');
$form[2][] = lang_form('input td12','profile|user_edit','личные данные');
$form[2][] = lang_form('input td12','profile|exit','выйти');
$form[2][] = '<h2>Registration</h2>';
$form[2][] = lang_form('input td3','profile|email','еmail');
$form[2][] = lang_form('input td3','profile|password','пароль');
$form[2][] = lang_form('input td3','profile|password2','подтв. пароль');
$form[2][] = lang_form('input td3','profile|new_password','новый пароль');
$form[2][] = lang_form('input td3','profile|save','Save');
$form[2][] = lang_form('input td3','profile|registration','регистрация');
$form[2][] = lang_form('input td3','profile|enter','войти');
$form[2][] = lang_form('input td3','profile|remember_me','запомнить меня');
$form[2][] = lang_form('input td3','profile|auth','авторизация');
$form[2][] = lang_form('input td3','profile|remind','забыли пароль');
$form[2][] = lang_form('input td12','profile|successful_registration','успешная регистрация');
$form[2][] = lang_form('input td12','profile|successful_auth','успешная авторизация');
$form[2][] = lang_form('input td12','profile|error_auth','ошибка авторизации');
$form[2][] = lang_form('input td12','profile|msg_exit','Вы вышли!');
$form[2][] = lang_form('input td12','profile|go_to_profile','перейти в профиль');
$form[2][] = '<h2>Remind password</h2>';
$form[2][] = lang_form('input td12','profile|remind_button','отправить письмо по восстановлению пароля');
$form[2][] = lang_form('input td12','profile|successful_remind','отправлено письмо по восстановлению пароля');


$form[3][] = lang_form('input td3','shop|btn_print','Print');
$form[3][] = lang_form('input td3','shop|btn_schedule_showing','Schedule a Showing');
$form[3][] = lang_form('input td3','shop|btn_sent_email','Sent to e-mail');

//$form[3][] = lang_form('input td3','shop|catalog','каталог');
$form[3][] = lang_form('input td3','shop|category_default_name','category default name');
//$form[3][] = lang_form('input td3','shop|new','новинки');
//$form[3][] = lang_form('input td3','shop|brand','производитель');
//$form[3][] = lang_form('input td3','shop|article','артикул');
//$form[3][] = lang_form('input td3','shop|parameters','параметры');
$form[3][] = lang_form('input td3','shop|price','цена');
$form[3][] = lang_form('input td3','shop|currency','валюта');
//$form[3][] = lang_form('input td3','shop|product_random','случайный товар');
$form[3][] = lang_form('input td3','shop|filter_button','искать');
$form[3][] = lang_form('input td3','shop|year_built','year built');
$form[3][] = lang_form('input td3','shop|square_rage','square rage');
$form[3][] = lang_form('input td3','shop|ftsq','Ft/Sq');
$form[3][] = lang_form('input td3','shop|floors','floors');
$form[3][] = lang_form('input td3','shop|units','units');
$form[3][] = lang_form('input td3','shop|average_price','average price');
$form[3][] = lang_form('input td3','shop|sort_date_desc','sort date desc');
$form[3][] = lang_form('input td3','shop|sort_date_asc','sort date asc');
$form[3][] = lang_form('input td3','shop|sort_price_desc','sort price desc');
$form[3][] = lang_form('input td3','shop|sort_price_asc','sort price asc');
$form[3][] = lang_form('input td3','shop|sort_name_desc','sort name desc');
$form[3][] = lang_form('input td3','shop|sort_name_asc','sort name asc');
$form[3][] = lang_form('input td3','shop|sort_year_desc','sort year desc');
$form[3][] = lang_form('input td3','shop|sort_year_asc','sort year asc');

$form[3][] = lang_form('input td3','condos|send_request_1','send request first line');
$form[3][] = lang_form('input td3','condos|send_request_2','send request second line');
/*
$form[3][] = '<h2>Отзывы</h2>';
$form[3][] = lang_form('input td3','shop|reviews','Отзывы');
$form[3][] = lang_form('input td3','shop|review_add','Оставить отзыв');
$form[3][] = lang_form('input td3','shop|review_name','имя');
$form[3][] = lang_form('input td3','shop|review_email','еmail');
$form[3][] = lang_form('input td3','shop|review_text','сообщение');
$form[3][] = lang_form('input td3','shop|review_send','отправить');
$form[3][] = lang_form('input td12','shop|review_is_sent','отзыв добавлен');*/
/*
$form[4][] = lang_form('input td3','basket|buy','купить');
$form[4][] = lang_form('input td3','basket|basket','корзина');
$form[4][] = lang_form('input td12','basket|empty','пустая корзина');
$form[4][] = lang_form('input td12','basket|go_basket','перейти в корзину');
$form[4][] = lang_form('input td12','basket|go_next','продолжить покупки');
$form[4][] = lang_form('input td12','basket|product_added','товар добавлен');*/
/*$form[4][] = '<h2>Оплата</h2>';
$form[4][] = lang_form('input td12','order|payments','оплата');
$form[4][] = lang_form('input td12','order|pay','оплатить');
$form[4][] = lang_form('input td12','order|paid','оплачен');
$form[4][] = lang_form('input td12','order|not_paid','не плачен');
$form[4][] = lang_form('textarea td12','order|success','успешная оплата');
$form[4][] = lang_form('textarea td12','order|fail','отказ оплаты');*/

$form[4][] = '<h2>Таблица товаров</h2>';
$form[4][] = lang_form('input td3','basket|product_id','id товара');
$form[4][] = lang_form('input td3','basket|product_name','название товара');
$form[4][] = lang_form('input td3','basket|product_price','цена');
$form[4][] = lang_form('input td3','basket|product_count','количество');
$form[4][] = lang_form('input td3','basket|product_summ','сумма');
$form[4][] = lang_form('input td3','basket|product_cost','стоимость');
$form[4][] = lang_form('input td3','basket|product_delete','удалить');
$form[4][] = lang_form('input td3','basket|total','итого');
$form[4][] = '<h2>Параметры заказа</h2>';
$form[4][] = lang_form('input td3','basket|profile','личные данные');
$form[4][] = lang_form('input td3','basket|delivery','доставка');
$form[4][] = lang_form('input td3','basket|delivery_cost','стоимость доставки');
$form[4][] = lang_form('input td3','basket|comment','коммен к заказу');
$form[4][] = lang_form('input td3','basket|order','оформить заказ');
$form[4][] = '<h2>Статистика заказов</h2>';
$form[4][] = lang_form('input td3','basket|orders','статистика заказов');
$form[4][] = lang_form('input td3','basket|order_name','заказ');
$form[4][] = lang_form('input td3','basket|order_from','от');
$form[4][] = lang_form('input td3','basket|order_status','статус');
$form[4][] = lang_form('input td3','basket|order_date','дата');
$form[4][] = lang_form('input td3','basket|view_order','просмотр заказа');

$form[5][] = 'Полное описание можно найти на странице <a target="_balnk" href="http://help.yandex.ru/partnermarket/shop.xml">http://help.yandex.ru/partnermarket/shop.xml</a><br /><br />';
$form[5][] = lang_form('input td12','market|name','Короткое название магазина');
$form[5][] = lang_form('input td12','market|company','Полное наименование компании');
$form[5][] = lang_form('input td12','market|currency','Валюта магазина');

$form[6][] = '<h2>Основной шаблон автоматического письма</h2>';
$form[6][] = lang_form('textarea td12','common|letter_top','Текст в шапке письма');
$form[6][] = lang_form('textarea td12','common|letter_footer','Текст в подвале письма');
$form[6][] = '<h2>Основной шаблон письма рассылки</h2>';
$form[6][] = lang_form('textarea td12','subscribe|top','Текст в шапке рассылки');
$form[6][] = lang_form('textarea td12','subscribe|bottom','Текст в подвале рассылки');
$form[6][] = lang_form('input td8','subscribe|letter_failure_str','Если вы хотите отписаться от рассылки нажмите на');
$form[6][] = lang_form('input td4','subscribe|letter_failure_link','ссылку');
$form[6][] = '<h2>Подписка</h2>';
$form[6][] = lang_form('input td12','subscribe|on_button','Подписаться');
$form[6][] = lang_form('input td12','subscribe|on_success','Вы успешно подписаны');
$form[6][] = lang_form('input td12','subscribe|failure_text','Подтвердите, что хотите отписаться');
$form[6][] = lang_form('input td12','subscribe|failure_button','Отписаться');
$form[6][] = lang_form('input td12','subscribe|failure_success','Вы отписаны');

function lang_form($type,$key,$name) {
	global $lang;
	$name = '';
	$key = explode('|',$key);
	return array ($type,'dictionary['.$key[0].']['.$key[1].']',isset($lang[$key[0]][$key[1]]) ? $lang[$key[0]][$key[1]] : '',array('name'=>$name.' <b>'.$key[0].'|'.$key[1].'</b>','title'=>$key[0].'|'.$key[1]));
}

