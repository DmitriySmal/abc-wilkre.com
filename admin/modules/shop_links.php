<?php

$a18n['neighborhood'] = 'neighborhood';
$a18n['residential'] = 'sale|rent';
$a18n['property_type'] = 'real estate type';
$a18n['property_type2'] = 'property type';

$states = mysql_select("SELECT * FROM shop_states",'array');
$neighborhoods = mysql_select("SELECT * FROM shop_neighborhoods",'array');
$property_types2 = mysql_select("SELECT * FROM shop_property_types2",'array');

$table = array(
	'id'		=>	'rank id:desc name',
	'name'		=>	'',
	'state' => $states,
	'neighborhood'=>$neighborhoods,
	'residential'       => $config['object_residential_url'],
	'property_type'		=>	'',
	'property_type2'		=>	$property_types2,
	'rank'		=>	'',
);

$where = (@$get['state'] > 0) ? " AND state = ".$get['state'] : '';

$filter[] = array('state',$states,'-state-');

$delete['confirm'] = array('shop_products'=>'city');

$form[] = array('input td3','name',true);
$form[] = array('select td3','state',array(true,$states));
$form[] = array('input td3','neighborhood',true);
$form[] = array('select td3','residential',array(true,$config['object_residential_url']));
$form[] = array('input td3','property_type',true);
$form[] = array('input td3','property_type2',true);
$form[] = array('input td3','rank',true);