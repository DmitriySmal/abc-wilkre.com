<?php

//var_dump($config['object_statuses']);
$config['hide_add_button'] = true;

//$save_as = true;
$neighborhoods = mysql_select("SELECT * FROM shop_neighborhoods WHERE city = 6",'array');
$cities = mysql_select("SELECT * FROM shop_cities",'array');
$categories = $config['categories'];
$statuses = $config['object_statuses'];
$types = $config['for_sale'];
$agents = mysql_select("SELECT id,name FROM users WHERE agent=1 ORDER BY name",'array');

$property_type = array();
foreach($config['object_groups'] as $k=>$v) $property_type[$k] = $v['name'];


if ($get['u']=='edit') {
    if($post['neighborhood'] && empty($post['zip_code'])){
        $zip = mysql_select("SELECT * FROM shop_neighborhoods_zips WHERE nb_id = {$post['neighborhood']} LIMIT 1",'row');
        if ($zip)
            $post['zip_code'] = $zip['zip'];
    }
    /*
    if($post['zip_code'] && empty($post['neighborhood'])){
        $zip = mysql_select("SELECT * FROM shop_neighborhoods_zips WHERE zip = {$post['zip_code']} LIMIT 1",'row');
        if ($zip)
            $post['neighborhood'] = $zip['nb_id'];
    }*/

    if($post['coordinates'] && !empty($post['coordinates'])){
        $coordinates = trim($post['coordinates'],' ()');
        $location = explode(',', $coordinates);
        //var_dump($location);
        $post['lat'] = trim($location[0]);
        $post['lng'] = trim($location[1]);
    }
}

$a18n['type'] = 'property type';

$config['hide_add_button'] = true;

$table = array(
	'_edit'		=>	true,
	'id'		=>	'special:desc date:desc id:desc name price errors unit status',
	'mln'		=>	'<a target="_blank" href="/cron.php?file=sf_object&id={id}">{mln}</a>',
	'type'      => '::property_type',//'{property_type}|{property_type2}',
	'name'		=>	'::row_object',
	//'unit'      => '',
	'city' => '::cities',//$cities,
	//'neighborhood' => $neighborhoods,
	'zip_code' => '',
	//'category'	=> $categories,//'<a href="/admin.php?m=shop_categories&id={category}">{sc_name}</a>',
	'date'		=>	'date',
	'price'		=>	'right',
	'errors'    =>  '',
	'status'	=>	'',
	'landing'	=>	'boolean',
	'special'	=>	'boolean',
	'display'	=>	'boolean',
	//'hide'	    =>	'boolean'
);

function property_type($q) {
	global $property_type,$categories;
	$content = $property_type[$q['property_type']];
	if ($q['property_type2']) {
		$content .= ' | ';
		$content .= str_replace('"','',$q['property_type2']);
	}
	$content .= '<br>';
	$content.= $categories[$q['category']];
	return '<td>'.$content.'</td>';
}
function cities($q) {
	global $cities,$neighborhoods;
	$content = $cities[$q['city']];
	$content .= '<br>';
	$content.= $neighborhoods[$q['neighborhood']];
	return '<td>'.$content.'</td>';
}


$where = '';
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(shop_products.mln) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(shop_products.name) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
		OR shop_products.id = '".mysql_real_escape_string($get['search'])."'
	)
";
$where .= (@$get['neighborhood'] > 0) ? " AND neighborhood = ".$get['neighborhood'] : '';
$where .= (@$get['city'] > 0) ? " AND city = ".$get['city'] : '';
$where .= (@$get['display'] > 0) ? " AND display = 1" : '';
$where .= (@$get['category'] > 0) ? " AND category = ".$get['category'] : '';
$where .= (@$get['errors']) ? " AND errors = ".$get['errors'] : '';
$where.= (@$get['property_type'] > 0) ? " AND property_type = ".$get['property_type'] : '';
$where .= (@$_GET['status']) ? " AND status = '".mysql_res($_GET['status'])."'" : '';
if (@$_GET['property_type2']) {
	if ($property_type2 = mysql_select("SELECT name FROM shop_property_types2 WHERE id=".intval($_GET['property_type2']),'string')) {
		$where .= " AND property_type2 LIKE '%" . mysql_res(mb_strtolower($property_type2)) . "%'";
	}
}
//echo $query;

$filter[] = array('search');
if ($user['agent']==0) {
	//$filter[] = array('city',$cities,'-city-');
	$filter[] = array('neighborhood',$neighborhoods,'-neighborhood-');
	$filter[] = array('category',$categories,'-category-');
	$filter[] = array('status',$statuses,'-status-');
	$filter[] = array('property_type', $property_type, '-Real Estate Types-');
	if (@$_GET['property_type']) {
		require_once(ROOT_DIR.'functions/mls_func.php');
		$post2 = array(
			'base'=>2,
			'property_type'=>$_GET['property_type']
		);
		$property_types2 = get_property_types2 ($post2);
		//print_R($property_types2);
		if ($property_types2) {
			$filter[] = array('property_type2', $property_types2, '-Property Type-');
		}
	}
}
else {
	if ($get['search']=='') $where.= ' AND 1=2';
}

$query = "
	SELECT
		shop_products.*,sn.url neighborhood_url
	FROM
		shop_products
		LEFT JOIN shop_neighborhoods sn ON sn.id = shop_products.neighborhood
	WHERE shop_products.id>0 AND shop_products.base = 2 $where
";


$delete = array(
	'delete'=>"DELETE FROM shop_reviews WHERE product = '".$get['id']."'"  //удаление отзывов
);

$tabs = array(
	1=>'Main',
	2=>'Settings',
	3=>'Pictures',
	5=>'Map',
);

$form[1][] = '<input name="base" value="2" type="hidden"/>';
$form[1][] = '<input name="city" value="6" type="hidden"/>';

$form[1][] = array('input td4','name',true);
if ($user['agent']==0) {
	$form[1][] = array('select td3','agent',array(true,$agents,'-select-'));
}
$form[1][] = array('select td2','status',array(true,$statuses));
$form[1][] = array('select td2','category',array(true,$categories));
$form[1][] = array('checkbox line','landing',true);
$form[1][] = array('checkbox line','special',true);
//$form[1][] = array('checkbox','market',true);
$form[1][] = array('checkbox line','display',true);
//$form[1][] = array('checkbox line','hide',true);
//$form[1][] = array('select td3','brand',array(true,"SELECT id,name FROM shop_brands ORDER BY name"));
//$form[1][] = array('select td2','type',array(true,$types),array('name'=>'for sale'));

$form[1][] = '<div class="clear"></div>';
$form[1][] = array('select td3','property_type',array(true,$property_type));
//$form[1][] = array('select td3','city',array(true,$cities,'-select-'));
$form[1][] = array('select td3','neighborhood',array(true,$neighborhoods,'-select-'));
$form[1][] = array('input td5','zip_code',true);
/*if(isset($post['zip_code']) && !empty($post['zip_code'])){
    $form[1][] = '<input type="hidden" name="zip_code" value="'.$post['zip_code'].'">';
}*/

//$form[1][] = array('multicheckbox td3','categories',array(true,'SELECT id,name,level FROM shop_categories ORDER BY left_key'));
$form[1][] = array('input td1 right','price',true);
//$form[1][] = array('input td1 right','price2',true);
//$form[1][] = array('input td1 right','rating',true,@$get['id']>0?array('name'=>'<a target="_blank" href="?m=shop_reviews&product='.@$get['id'].'">оценки</a>'):NULL);
//$form[1][] = array('input td1','article',true);
$form[1][] = array('input td2','date',true);

$form[1][] = array('input td9','property_type2',true);
$form[1][] = array('select td3','residential',array(true,$config['object_residential'],''));

$form[1][] = array('input td3','office_name',true);
$form[1][] = array('input td3','office_agent',true);
$form[1][] = array('input td3','office_phone',true);
$form[1][] = array('input td3','complex_name',true);
$form[1][] = array('input td3','development_name',true);
$form[1][] = array('input td1','beds',true);
$form[1][] = array('input td1','baths',true);
$form[1][] = array('input td1','square',true);


//$form[1][] = array('checkbox','is_condo',true);
$form[1][] = array('tinymce td12','text',true);
$form[1][] = array('seo','seo url title keywords description',true);

$form[2][] = '';
if ($get['u']=='form' OR ($get['id']>0 AND $get['u']=='')) {
	$parameters = $post['parameters'] ? unserialize($post['parameters']) : false;
	if ($parameters) foreach ($parameters as $k=>$v) {
		$form[2][] = $k.' - '.$v.'<br>';
	}
}

$form[3][] = array('file td6','img','Main image',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));
//$form[3][] = array('file td6','booklet','Booklet',true);
//$form[3][] = array('input td12','video','Video Link',true);
$form[3][] = array('input td12','3d',true);
$form[3][] = array('file_multi','imgs','Additional images',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));

//$form[3][] = array('file_multi_db','shop_items','Additional images',array(''=>'resize 1000x1000','preview'=>'resize 150x150'));

$google = preg_replace('~[^-0-9.,]+~u','',@$post['coordinates']);
$google = $google ? $google : '39.5,-0.38';
//var_dump($google);
$form[5][] = array('input td10','address',@$post['address'],array('name'=>'address'));
$form[5][] = '<a href="#" id="coordinates_search" class="button red" style="margin:15px 0 25px"><span>Search</span></a>';
$form[5][] = '<div style="display:none">';
$form[5][] = array('input td12','coordinates',@$post['coordinates'],array('name'=>'map'));
$form[5][] = '</div>';
$form[5][] = '<div id="map" style="clear:both; width:790px; height:500px"></div>';

$content.= '

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
$(document).ready(function(){

	//замена параметров при смене категории
	$(document).on("change",".form select[name=category]",function(){
		var category = $(this).val(),
			id = $(".form").prop("id").replace(/[^0-9]/g,"");
		$.get(
			"/admin.php",
			{"m":"shop_products","u":"shop_parameters","category":category,"id":id},
			function(data){$("#shop_parameters").html(data)}
		);
	});

	$(document).on("click",".bookmarks a",function(){
		var i = $(this).data("i");
                var coords = $("input[name=coordinates]").val().replace("(","").replace(")","").split(",");
                coords = (coords.length > 1) ? coords : new Array (25.7616798,-80.191);
		if (i==5) {
                        console.log(coords);
			geocoder = new google.maps.Geocoder();
			map = new google.maps.Map(document.getElementById("map"),{
				zoom: 10,
				center: new google.maps.LatLng(coords[0],coords[1]),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
			});
			markersArray = [];
			var marker = new google.maps.Marker({position:new google.maps.LatLng(coords[0],coords[1])});
			marker.setMap(map);
			markersArray.push(marker);
			google.maps.event.addListener(map, "click", function(event) {
				for (i in markersArray) markersArray[i].setMap(null);
				markersArray.length = 0;
				var marker = new google.maps.Marker({position:event.latLng,map:map});
				markersArray.push(marker);
				$("input[name=coordinates]").val(event.latLng);
			});
		}
		return false;
	});

        $(document).on("change",".form select[name=city]",function(){
		// меняем районы
                var cities = $(this);
                var neighborhoods = $(cities).closest(".form").find("select[name=neighborhood]");
                $.ajax({
                    url: "/ajax.php",
                    data: {
                        file: "neighborhoods",
                        city: $(cities).val(),
                    },
                    type: "get",
                    dataType: "json",
                    beforeSend: function(){
                        $(neighborhoods).find("option").slice(1).remove();
                    },
                    success: function(json){
                        if(json.result)
                        {
                            $.each(json.data, function(i, v) {
                                $(neighborhoods).append($("<option>").text(v.name).attr("value", v.id));
                            });
                        }
                    }
                });
	});

        //поиск по гуглу
	$(document).on("click","#coordinates_search",function () {
		marker_search();
		return false;
	});

	function marker_search() {
		var address = $("input[name=\"address\"]").val();
		geocoder.geocode( { "address": address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				for (i in markersArray) markersArray[i].setMap(null);
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});
				markersArray.push(marker);
                                console.log(results[0].geometry.location.toString());
                                $("input[name=coordinates]").val(results[0].geometry.location.toString());
			} else {
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
	}

});
</script>';

