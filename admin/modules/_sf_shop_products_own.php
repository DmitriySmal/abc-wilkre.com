<?php

include(ROOT_DIR.'_config_ny.php');


//$save_as = true;
$neighborhoods = mysql_select("SELECT * FROM shop_neighborhoods WHERE 1 OR city = 6",'array');
$cities = mysql_select("SELECT * FROM shop_cities WHERE id=6",'array');
$categories = $config['categories'];
$statuses = $config['object_statuses'];
$types = $config['for_sale'];



if ($get['u']=='edit') {
    if($post['neighborhood'] && empty($post['zip_code'])){
        $zip = mysql_select("SELECT * FROM shop_neighborhoods_zips WHERE nb_id = {$post['neighborhood']} LIMIT 1",'row');
        if ($zip)
            $post['zip_code'] = $zip['zip'];
    }
	$post['address'] = $post['parameters'][15].' '.$post['parameters'][16];
	$post['name'] = $post['address'].' '.$post['parameters'][18];
	$post['beds'] = $post['parameters'][21];
	$post['baths'] = $post['parameters'][22];
	$post['square'] = $post['parameters'][19];
	$post['price'] = $post['parameters'][36];
	$post['unit'] = $post['parameters'][18];
	$post['parameters'] = serialize($post['parameters']);
    /*
    if($post['zip_code'] && empty($post['neighborhood'])){
        $zip = mysql_select("SELECT * FROM shop_neighborhoods_zips WHERE zip = {$post['zip_code']} LIMIT 1",'row');
        if ($zip)
            $post['neighborhood'] = $zip['nb_id'];
    }*/

    if($post['coordinates'] && !empty($post['coordinates'])){
        $coordinates = trim($post['coordinates'],' ()');
        $location = explode(',', $coordinates);
        //var_dump($location);
        $post['lat'] = trim($location[0]);
        $post['lng'] = trim($location[1]);
    }
}


$table = array(
	'_edit'		=>	true,
	'id'		=>	'date:desc id:desc name price errors',
	'uid'		=>	'',
	'property_type' => $config['ny_property_types'],
	'name'		=>	'',
	'unit'      => '',
	'neighborhood' => $neighborhoods,
	'zip_code' => '',
	'coords' => '{lat}<br>{lng}',
	'date'		=>	'date',
	'price'		=>	'right',
	//'errors'    =>  '',
	//'status'	=>	'',
	'special'	=>	'boolean',
	'display'	=>	'boolean'
);

$where = (isset($get['brand']) && $get['brand']>0) ? " AND shop_products.brand = '".intval($get['brand'])."' " : "";
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(shop_products.mln) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
		OR shop_products.id = '".mysql_real_escape_string($get['search'])."'
	)
";
$where .= (@$get['neighborhood'] > 0) ? " AND neighborhood = ".$get['neighborhood'] : '';
$where .= (@$get['display'] > 0) ? " AND display = 1" : '';
$where .= (@$get['category'] > 0) ? " AND category = ".$get['category'] : '';
$where .= (@$get['errors']) ? " AND errors = ".$get['errors'] : '';
//if (@$_GET['status']=='A,PS,T,R') $where .= " AND status IN ('A','PS','T','R') AND mln LIKE ('A%')" ;
//else
$where .= (@$_GET['status']) ? " AND status = '".mysql_res($_GET['status'])."'" : '';
$query = "
	SELECT
		shop_products.*
	FROM
		shop_products
	WHERE shop_products.id>0 AND shop_products.base = 1 $where
";
//echo $query;

$filter[] = array('search');
$filter[] = array('neighborhood',$neighborhoods,'-neighborhood-');
//$filter[] = array('category',$categories,'-category-');
//$statuses['A,PS,T,R'] = 'A,PS,T,R';
//$filter[] = array('status',$statuses,'-status-');
//unset($statuses['A,PS,T,R']);

$delete = array(
	'delete'=>"DELETE FROM shop_reviews WHERE product = '".$get['id']."'"  //удаление отзывов
);

$tabs = array(
	1=>'Settings',
	2=>'Listing Information',
	3=>'Property Information',
	4=>'Financial Information',
	5=>'Owner Information',
	6=>'Commercial Type',
	7=>'Pictures',
	8=>'Map',
);

if ($get['u']=='form') {
	$parameters = unserialize($post['parameters']);
}

$form[1][] = '<input name="base" value="1" type="hidden"/>';
//$form[1][] = '<input name="office_name" value="WILK REAL ESTATE I LLC" type="hidden"/>';

$form[1][] = array('select td4','property_type',array(true,$config['ny_property_types']));
$form[1][] = array('select td4','residential',array(true,$config['object_residential'],''));


$form[3][] = array('select td4','city',array(true,$cities,'-change-'));
$form[3][] = array('select td4','neighborhood',array(true,$neighborhoods,'-change-'));
$form[3][] = array('input td4', 'zip_code', true);



foreach ($config['shop_params_groups'] as $key=>$val) {
	foreach ($config['shop_params'] as $k=>$v) if ($v['group']==$key) {
		if (@$v['type']=='yn') {
			$form[$key][] = array('select td4', 'parameters[' . $k . ']', array(@$parameters[$k],array(1=>'Yes',2=>'No'),'--'),array('name'=>$v['name']));
		}
		elseif (@$v['type']=='date') {
			$form[$key][] = array('input td4 datepicker', 'parameters[' . $k . ']', @$parameters[$k],array('name'=>$v['name']));
		}
		elseif (@$v['type']=='multicheckbox') {
			$array = array();
			foreach ($v['data'] as $k1=>$v1) $array[$v1] = $v1;
			$form[$key][] = array('multicheckbox td4', 'parameters[' . $k . ']',  array(@$parameters[$k],$array),array('name'=>$v['name']));
		}
		elseif (isset($v['data'])) {
			$array = array();
			foreach ($v['data'] as $k1=>$v1) $array[$v1] = $v1;
			$form[$key][] = array('select td4', 'parameters[' . $k . ']', array(@$parameters[$k],$array,'--'),array('name'=>$v['name']));
		}
		else $form[$key][] = array('input td4', 'parameters[' . $k . ']', @$parameters[$k],array('name'=>$v['name']));
	}
}

$form[1][] = array('input td4', 'date', true);
$form[1][] = array('checkbox','special',true);
$form[1][] = array('checkbox','display',true);

$form[3][] = array('tinymce td12','text',true);
$form[3][] = array('seo','seo url title keywords description',true);


$form[7][] = array('file td6','img','Main image',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));
$form[7][] = array('input td12','booklet',true);
$form[7][] = array('input td12','video',true);
$form[7][] = array('file_multi','imgs','Additional images',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));

//$form[3][] = array('file_multi_db','shop_items','Additional images',array(''=>'resize 1000x1000','preview'=>'resize 150x150'));

$google = preg_replace('~[^-0-9.,]+~u','',@$post['coordinates']);
$google = $google ? $google : '39.5,-0.38';
//var_dump($google);
$form[8][] = array('input td10','address',@$post['address'],array('name'=>'address'));
$form[8][] = '<a href="#" id="coordinates_search" class="button red" style="margin:15px 0 25px"><span>Search</span></a>';
$form[8][] = '<div style="display:none">';
$form[8][] = array('input td12','coordinates',@$post['coordinates'],array('name'=>'карта'));
$form[8][] = '</div>';
$form[8][] = '<div id="map" style="clear:both; width:790px; height:500px"></div>';

$content.= '

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
$(document).ready(function(){


	$(document).on("click",".bookmarks a",function(){
		var i = $(this).data("i");
                var coords = $("input[name=coordinates]").val().replace("(","").replace(")","").split(",");
                coords = (coords.length > 1) ? coords : new Array (25.7616798,-80.191);
		if (i==8) {
                        console.log(coords);
			geocoder = new google.maps.Geocoder();
			map = new google.maps.Map(document.getElementById("map"),{
				zoom: 10,
				center: new google.maps.LatLng(coords[0],coords[1]),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
			});
			markersArray = [];
			var marker = new google.maps.Marker({position:new google.maps.LatLng(coords[0],coords[1])});
			marker.setMap(map);
			markersArray.push(marker);
			google.maps.event.addListener(map, "click", function(event) {
				for (i in markersArray) markersArray[i].setMap(null);
				markersArray.length = 0;
				var marker = new google.maps.Marker({position:event.latLng,map:map});
				markersArray.push(marker);
				$("input[name=coordinates]").val(event.latLng);
			});
		}
		return false;
	});

        $(document).on("change",".form select[name=city]",function(){
		// меняем районы
                var cities = $(this);
                var neighborhoods = $(cities).closest(".form").find("select[name=neighborhood]");
                $.ajax({
                    url: "/ajax.php",
                    data: {
                        file: "neighborhoods",
                        city: $(cities).val(),
                    },
                    type: "get",
                    dataType: "json",
                    beforeSend: function(){
                        $(neighborhoods).find("option").slice(1).remove();
                    },
                    success: function(json){
                        if(json.result)
                        {
                            $.each(json.data, function(i, v) {
                                $(neighborhoods).append($("<option>").text(v.name).attr("value", v.id));
                            });
                        }
                    }
                });
	});

        //поиск по гуглу
	$(document).on("click","#coordinates_search",function () {
		marker_search();
		return false;
	});

	function marker_search() {
		var address = $("input[name=\"address\"]").val();
		geocoder.geocode( { "address": address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				for (i in markersArray) markersArray[i].setMap(null);
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});
				markersArray.push(marker);
                                console.log(results[0].geometry.location.toString());
                                $("input[name=coordinates]").val(results[0].geometry.location.toString());
			} else {
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
	}

});
</script>';

