<?php

if (!isset($base)) $base = 6;

$types = array(
	0=> 'text',
	1=> 'square',
	2=> 'price',
	3=> 'thousand',
	4=> 'Y/N',
	5=> 'url'
);

$a18n['type']			= 'type';
$a18n['key']			= 'key in MLS';

//$config['hide_add_button'] = true;

$where = '';
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(shop_parameters.name) like '%".mysql_res(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(shop_parameters.key) like '%".mysql_res(mb_strtolower($get['search'],'UTF-8'))."%'
	)
";

$query = "SELECT * FROM shop_parameters WHERE base='".$base."' ".$where;
//echo $query;

$table = array(
	'_delete'=>false,
	'id'	=>	'rank:desc name id key',
	'key'   => '{key}',
	'name'	=>	'',
	'template'=>'',
	'type:'  => $types,
	'rank'	=>	'',
	'display' => 'display'
);

$filter[] = array('search');

$form[] = '<input type="hidden" name="base" value="'.$base.'">';
$form[] = array('input td3','key',true);
$form[] = array('input td3','name',true);
$form[] = array('select td2','type',array(true,$types));
$form[] = array('input td2','rank',true);
$form[] = array('checkbox','display',true);
$form[] = array('input td12','template',true);
