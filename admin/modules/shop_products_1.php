<?php

//var_dump($config['object_statuses']);
$config['hide_add_button'] = true;

//$save_as = true;
$neighborhoods = mysql_select("SELECT * FROM shop_neighborhoods WHERE 1 ". ((isset($get['city']) && $get['city']>0) ? " AND city = '".intval($get['city'])."' " : "") ." ",'array');
$cities = mysql_select("SELECT * FROM shop_cities",'array');
$types = $config['for_sale'];

$property_type = array();
foreach($config['object_groups'] as $k=>$v) $property_type[$k] = $v['name'];

if ($get['u']=='edit') {
    if($post['neighborhood'] && empty($post['zip_code'])){
        $zip = mysql_select("SELECT * FROM shop_neighborhoods_zips WHERE nb_id = {$post['neighborhood']} LIMIT 1",'row');
        if ($zip)
            $post['zip_code'] = $zip['zip'];
    }
    /*
    if($post['zip_code'] && empty($post['neighborhood'])){
        $zip = mysql_select("SELECT * FROM shop_neighborhoods_zips WHERE zip = {$post['zip_code']} LIMIT 1",'row');
        if ($zip)
            $post['neighborhood'] = $zip['nb_id'];
    }*/

    if($post['coordinates'] && !empty($post['coordinates'])){
        $coordinates = trim($post['coordinates'],' ()');
        $location = explode(',', $coordinates);
        //var_dump($location);
        $post['lat'] = trim($location[0]);
        $post['lng'] = trim($location[1]);
    }
}

$config['hide_add_button'] = true;

$table = array(
	'_edit'		=>	true,
	'id'		=>	'date:desc id:desc name price errors',
	//'mln'		=>	'text',
	//'type'      => '{property_type}|{property_type2}',
	'name'		=>	'',
	'city' => $cities,
	'neighborhood' => $neighborhoods,
	'zip_code' => '',
	//'category'	=> $categories,//'<a href="/admin.php?m=shop_categories&id={category}">{sc_name}</a>',
	'date'		=>	'date',
	'price'		=>	'right',
	//'errors'    =>  '',
	//'status'	=>	'',
	'special'	=>	'boolean',
	'display'	=>	'boolean'
);

$where = (isset($get['brand']) && $get['brand']>0) ? " AND shop_products.brand = '".intval($get['brand'])."' " : "";
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(shop_products.mln) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
		OR shop_products.id = '".mysql_real_escape_string($get['search'])."'
	)
";
$where .= (@$get['neighborhood'] > 0) ? " AND neighborhood = ".$get['neighborhood'] : '';
$where .= (@$get['city'] > 0) ? " AND city = ".$get['city'] : '';
$where .= (@$get['display'] > 0) ? " AND display = 1" : '';
$where .= (@$get['category'] > 0) ? " AND category = ".$get['category'] : '';
$where .= (@$get['errors']) ? " AND errors = ".$get['errors'] : '';
//if (@$_GET['status']=='A,PS,T,R') $where .= " AND status IN ('A','PS','T','R') AND mln LIKE ('A%')" ;
//else
$where .= (@$_GET['status']) ? " AND status = '".mysql_res($_GET['status'])."'" : '';
$query = "
	SELECT
		shop_products.*
	FROM
		shop_products
	WHERE shop_products.base = 1 $where
";
//echo $query;

$filter[] = array('search');
$filter[] = array('city',$cities,'-city-');
$filter[] = array('neighborhood',$neighborhoods,'-neighborhood-');
$filter[] = array('category',$categories,'-category-');
//$statuses['A,PS,T,R'] = 'A,PS,T,R';
$filter[] = array('status',$statuses,'-status-');
//unset($statuses['A,PS,T,R']);

$delete = array(
	'delete'=>"DELETE FROM shop_reviews WHERE product = '".$get['id']."'"  //удаление отзывов
);

$tabs = array(
	1=>'Main',
	//2=>'Settings',
	3=>'Pictures',
	5=>'Map',
);

$form[1][] = '<input name="base" value="1" type="hidden"/>';

$form[1][] = array('input td9','name',true);
$form[1][] = array('checkbox','special',true);
//$form[1][] = array('checkbox','market',true);
$form[1][] = array('checkbox','display',true);
$form[1][] = array('input td9','address',true);
$form[1][] = array('input td3','unit',true);
//$form[1][] = array('select td3','brand',array(true,"SELECT id,name FROM shop_brands ORDER BY name"));
//$form[1][] = array('select td2','type',array(true,$types),array('name'=>'for sale'));

$form[1][] = '<div class="clear"></div>';
$form[1][] = array('select td3','city',array(true,$cities,'-change-'));
$form[1][] = array('select td3','neighborhood',array(true,$neighborhoods,'-change-'));
if(isset($post['zip_code']) && !empty($post['zip_code'])){
    $form[1][] = '<input type="hidden" name="zip_code" value="'.$post['zip_code'].'">';
}
$form[1][] = array('input td1 right','price',true);
$form[1][] = array('input td2','date',true);

$form[1][] = array('select td3','residential',array(true,$config['object_residential'],''));

$form[1][] = array('input td3','office_name',true);
$form[1][] = array('input td3','complex_name',true);
$form[1][] = array('input td3','development_name',true);
$form[1][] = array('input td1','beds',true);
$form[1][] = array('input td1','baths',true);
$form[1][] = array('input td1','square',true);


//$form[1][] = array('checkbox','is_condo',true);
$form[1][] = array('tinymce td12','text',true);
$form[1][] = array('seo','seo url title keywords description',true);

$form[2][] = '';
if ($get['u']=='form' OR ($get['id']>0 AND $get['u']=='')) {
	$parameters = $post['parameters'] ? unserialize($post['parameters']) : false;
	if ($parameters) foreach ($parameters as $k=>$v) {
		$form[2][] = $k.' - '.$v.'<br>';
	}
}

$form[3][] = array('file td6','img','Main image',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));
$form[3][] = array('file td6','booklet','Booklet',true);
$form[3][] = array('input td12','video',true);
$form[3][] = array('file_multi','imgs','Additional images',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));

//$form[3][] = array('file_multi_db','shop_items','Additional images',array(''=>'resize 1000x1000','preview'=>'resize 150x150'));

$google = preg_replace('~[^-0-9.,]+~u','',@$post['coordinates']);
$google = $google ? $google : '39.5,-0.38';
//var_dump($google);
$form[5][] = array('input td10','address',@$post['address'],array('name'=>'address'));
$form[5][] = '<a href="#" id="coordinates_search" class="button red" style="margin:15px 0 25px"><span>Search</span></a>';
$form[5][] = '<div style="display:none">';
$form[5][] = array('input td12','coordinates',@$post['coordinates'],array('name'=>'карта'));
$form[5][] = '</div>';
$form[5][] = '<div id="map" style="clear:both; width:790px; height:500px"></div>';

$content.= '

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
$(document).ready(function(){

	//замена параметров при смене категории
	$(document).on("change",".form select[name=category]",function(){
		var category = $(this).val(),
			id = $(".form").prop("id").replace(/[^0-9]/g,"");
		$.get(
			"/admin.php",
			{"m":"shop_products","u":"shop_parameters","category":category,"id":id},
			function(data){$("#shop_parameters").html(data)}
		);
	});

	$(document).on("click",".bookmarks a",function(){
		var i = $(this).data("i");
                var coords = $("input[name=coordinates]").val().replace("(","").replace(")","").split(",");
                coords = (coords.length > 1) ? coords : new Array (25.7616798,-80.191);
		if (i==5) {
                        console.log(coords);
			geocoder = new google.maps.Geocoder();
			map = new google.maps.Map(document.getElementById("map"),{
				zoom: 10,
				center: new google.maps.LatLng(coords[0],coords[1]),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
			});
			markersArray = [];
			var marker = new google.maps.Marker({position:new google.maps.LatLng(coords[0],coords[1])});
			marker.setMap(map);
			markersArray.push(marker);
			google.maps.event.addListener(map, "click", function(event) {
				for (i in markersArray) markersArray[i].setMap(null);
				markersArray.length = 0;
				var marker = new google.maps.Marker({position:event.latLng,map:map});
				markersArray.push(marker);
				$("input[name=coordinates]").val(event.latLng);
			});
		}
		return false;
	});

        $(document).on("change",".form select[name=city]",function(){
		// меняем районы
                var cities = $(this);
                var neighborhoods = $(cities).closest(".form").find("select[name=neighborhood]");
                $.ajax({
                    url: "/ajax.php",
                    data: {
                        file: "neighborhoods",
                        city: $(cities).val(),
                    },
                    type: "get",
                    dataType: "json",
                    beforeSend: function(){
                        $(neighborhoods).find("option").slice(1).remove();
                    },
                    success: function(json){
                        if(json.result)
                        {
                            $.each(json.data, function(i, v) {
                                $(neighborhoods).append($("<option>").text(v.name).attr("value", v.id));
                            });
                        }
                    }
                });
	});

        //поиск по гуглу
	$(document).on("click","#coordinates_search",function () {
		marker_search();
		return false;
	});

	function marker_search() {
		var address = $("input[name=\"address\"]").val();
		geocoder.geocode( { "address": address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				for (i in markersArray) markersArray[i].setMap(null);
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});
				markersArray.push(marker);
                                console.log(results[0].geometry.location.toString());
                                $("input[name=coordinates]").val(results[0].geometry.location.toString());
			} else {
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
	}

});
</script>';

