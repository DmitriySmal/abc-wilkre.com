<?php

$status = array(
	1 => 'Hot task',
	2 => 'Urgent task',
	3 => 'Normal task'
);

$type = array(
	1 => 'Bugfix',
	2 => 'Modification',
);

//$user['agent']=1;
$a18n['name'] = 'subject';
$a18n['type'] = 'type';

if ($get['u']=='edit') {
	if ($user['agent']==1) {
		$post['agent'] = $user['id'];
		$post['date'] = date('Y-m-d H:i:s');
	}
}

$agents = mysql_select("SELECT id,name FROM users WHERE agent=1 ORDER BY name",'array');
if ($user['agent']==1) {
	$table = array(
		'_edit'=>false,
		'id' => 'date:desc name url user title id',
		'name' => '{name}',
		'date' => '{date}',
		'status'=>$status,
		'type'=>$type,
	);
}
else {
	$table = array(
		'id' => 'date:desc name url user title id',
		'name' => '',
		'status'=>$status,
		'type'=>$type,
		'agent' => $agents,
		'date' => 'date',
		'display' => 'boolean'
	);
	$filter[] = array('agent', $agents, '-agent-');
	$filter[] = array('status', $status, '-status-');
	$filter[] = array('type', $type, '-type-');
}

$where = '';
if ($user['agent']==1) $where.= " AND agent=".$user['id'];
if (@$_GET['agent']) $where.= " AND agent=".intval($_GET['agent']);
if (@$_GET['status']) $where.= " AND status=".intval($_GET['status']);
if (@$_GET['type']) $where.= " AND type=".intval($_GET['type']);
$query = "
	SELECT *
	FROM support
	WHERE 1 $where
";

if ($get['u']=='form' AND $get['id']=='new') {
	$post['status'] = 3;
}

if ($user['agent']==0 OR $get['id']=='new') {
	$form[] = array('input td12', 'name', true);
	$form[] = array('select td3', 'status', array(true, $status));
	$form[] = array('select td3', 'type', array(true, $type));
	if ($user['agent']==0) {
		$form[] = array('select td3', 'agent', array(true, $agents, '-change-'));
		$form[] = array('input td2', 'date', true);
		$form[] = array('checkbox', 'display', true);
	}
	$form[] = array('textarea td12', 'text', true);
	if ($user['agent']==0) {
		$form[] = array('textarea td12', 'comment', true);
	}
	$form[] = array('file_multi', 'imgs', 'Additional images', array('' => 'resize 1000x1000'));
}