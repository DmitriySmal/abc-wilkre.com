<?php

$filter[] = array('search');

$table = array(
	'id'		=> 'id:desc name url',
	'name'		=> '',
	'url'		=> '',
);

$where = '';
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(seo_anckors.name) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(seo_anckors.url) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
	)
";

$query = "
	SELECT seo_anckors.*
	FROM seo_anckors
	WHERE 1 $where
";

$form[] = array('input td6','name',true);
$form[] = array('input td6','url',true);
