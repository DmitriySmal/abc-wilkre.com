<?php

define('ROOT_DIR', dirname(__FILE__).'/');
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек
require_once(ROOT_DIR.'functions/mysql_func.php');
require_once(ROOT_DIR.'functions/rets3_func.php');
require_once(ROOT_DIR.'functions/html_func.php');

$types = array(
	'A' => 'Residential',
	'B' => 'Residential Income',
	'C' => 'Land/Docks',
	'D' => 'Business',
	'E' => 'Comm/Industry',
	'F' => 'Rental',
);

//echo array_rand($types, 1);

$status = array(
	'Active'=>'Active',
	//'Temp Off Market'=>'Temp Off Market',
	'Pending'=>'Pending',
	//'Expired'=>'Expired',
	//'Deleted'=>'Deleted',
	//'Closed'=>'Closed',
	//'Cancelled'=>'Cancelled',
	'Backup'=>'Backup',
	'Contingent'=>'Contingent'
);
$statuses = array(
	'A'=>array(
		'12LL26N0CFUH'=>'Active',
		//'12LL26N0CVC3'=>'Deleted',
		//'12LL26N0CSMH'=>'Cancelled',
		//'12LL26N0CQ28'=>'Temp Off Market',
		//'12LL26N0CND9'=>'Expired',
		'12LL26N0CKTY'=>'Pending',
		//'12LL26N0CIFT'=>'Closed',
		'PWC_181KDWIXFVLN'=>'Backup',
		'PWC_15429SGZYQIT'=>'Contingent',
	),
	'B'=>array(
		'12MKUJQH3QE8'=>'Active',
		//'12MKUJQH4ANF'=>'Temp Off Market',
		'12MKUJQH471V'=>'Pending',
		//'12MKUJQH43SY'=>'Expired',
		//'12MKUJQH408N'=>'Deleted',
		//'12MKUJQH3WOS'=>'Closed',
		//'12MKUJQH3TFH'=>'Cancelled',
		'PWC_181KDWIXMZMX'=>'Backup',
		'PWC_15429SI5IHF3'=>'Contingent',
	),
	'C'=>array(
		'12MKULNSLMH4'=>'Active',
		//'12MKULNSM30R'=>'Temp Off Market',
		'12MKULNSM049'=>'Pending',
		//'12MKULNSLXIY'=>'Expired',
		//'12MKULNSLUQX'=>'Deleted',
		//'12MKULNSLRYR'=>'Closed',
		//'12MKULNSLP5D'=>'Cancelled',
		'PWC_181KDWIXON0H'=>'Backup',
		'PWC_15429SI5IO7B'=>'Contingent',
	),
	'D'=>array(
		'12MKV68TFZZA'=>'Active',
		//'12MKV68TGHJ6'=>'Temp Off Market',
		'12MKV68TGEQG'=>'Pending',
		//'12MKV68TGBUO'=>'Expired',
		//'12MKV68TG8WZ'=>'Deleted',
		//'12MKV68TG5YP'=>'Closed',
		//'12MKV68TG303'=>'Cancelled',
		'PWC_181KDWIXR2M3'=>'Backup',
		'PWC_15429SI5IV6P'=>'Contingent',
	),
	'E'=>array(
		'12ML73ZZOBCU'=>'Active',
		//'12ML73ZZOU7X'=>'Temp Off Market',
		'12ML73ZZOR40'=>'Pending',
		//'12ML73ZZONW1'=>'Expired',
		//'12ML73ZZOL2Y'=>'Deleted',
		//'12ML73ZZOHYK'=>'Closed',
		//'12ML73ZZOF1S'=>'Cancelled',
		'PWC_181KDWIXTBXH'=>'Backup',
		'PWC_15429SI5J1XZ'=>'Contingent',
	),
	'F'=>array(
		'12MKV6FH8HUD'=>'Active',
		//'12MKV6FH8YTE'=>'Temp Off Market',
		'12MKV6FH8VXQ'=>'Pending',
		//'12MKV6FH8T6J'=>'Expired',
		//'12MKV6FH8QFB'=>'Deleted',
		//'12MKV6FH8NEG'=>'Closed',
		//'12MKV6FH8KN4'=>'Cancelled',
		'PWC_181KDWIXV3M0'=>'Backup',
		'PWC_15429SI5J9U2'=>'Contingent',
	),
);
//echo array_rand($statuses, 1);



$search = isset($_GET['search']) ? $_GET['search'] : '(LIST_15=12ML73ZZOBCU)';//'(LIST_39=12ML73ZYMDNU)';
if (@$_GET['status']) $search = '(LIST_15='.$_GET['status'].')';
echo @$_GET['type'].' '.$search;
?>
<form action="" method="get">
	<select name="type"><?=select(@$_GET['type'],$types)?></select>
	<br>
	<input name="search" value="<?=$search?>">
	<? /*OR <select name="status"><?=select(@$_GET['status'],$status,' ')?></select>*/?>
	<input type="submit" value="submit">
</form>
<br>
<?php
if (@$_GET['type'] AND $result = flex_list($search, @$_GET['type'])) {
	echo count($result);
	echo '<br>';
	foreach ($result as $q) {
		echo $q['LIST_3'];
		echo ' ';
		//echo $q['LIST_15'];
		echo ' ';
		echo $q['LIST_39'];
		echo ' ';
		echo $q['LIST_132'];
		echo '<br>';
	}
}
else echo 'no results';

