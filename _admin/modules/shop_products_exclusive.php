<?php
//$save_as = true;
$neighborhoods = mysql_select("SELECT * FROM shop_neighborhoods WHERE 1 ". ((isset($get['city']) && $get['city']>0) ? " AND city = '".intval($get['city'])."' " : "") ." ",'array');
$cities = mysql_select("SELECT * FROM shop_cities",'array');
$statuses = $config['object_statuses'];
$types = $config['for_sale'];
/*
//ответ аджакса со списком параметров товара при изменении категории
if ($get['u']=='shop_parameters') {
	$parameters = mysql_select("SELECT parameters FROM shop_categories WHERE id=".intval(isset($get['category']) ? $get['category'] : 0),'string');
	$parameters = $parameters ? unserialize($parameters) : array();
	$shop_parameters = mysql_select("SELECT id,name,type,`values`,units FROM shop_parameters ORDER BY rank DESC",'rows_id');        
        $product_parameters = mysql_select("SELECT * FROM shop_products_parameters WHERE product_id = '".$post['id']."'",'rows_pid');
	foreach ($parameters as $k=>$v) if (isset($v['display']) && $v['display']==1){
		$name = $shop_parameters[$k]['name'].($shop_parameters[$k]['units'] ? ' ('.$shop_parameters[$k]['units'].')' : '');
		if (array_key_exists($k,$shop_parameters)) {
			if (!isset($product_parameters[$k])) $product_parameters[$k] = '';
			if (in_array($shop_parameters[$k]['type'],array(1,3)))
				echo form('select td3','parameters['.$k.']',array($product_parameters[$k],unserialize($shop_parameters[$k]['values']),''),array('name'=>$name));
			else echo form('input td3','parameters['.$k.']',$product_parameters[$k],array('name'=>$name));
		}
	}
	die();
}
*/
//поиск сопуствующи товаров
if ($get['u']=='similar_search') {
	$search = stripslashes_smart(@$_GET['value']);
	if ($i=intval($search)) $where = " id=".$i." ";
	else $where = " LOWER(name) LIKE '%".mysql_real_escape_string(mb_strtolower($search,'UTF-8'))."%' OR LOWER(article) LIKE '%".mysql_real_escape_string(mb_strtolower($search,'UTF-8'))."%' ";
	$query = "SELECT * FROM shop_products WHERE ".$where." LIMIT 10";
	if ($products = mysql_select($query,'rows')) {
		foreach ($products as $k=>$v) {
			echo '<li data-id="'.$v['id'].'" title="Перетащите в правую колонку для сохранения">';
			echo $v['img'] ? '<img src="/files/shop_products/'.$v['id'].'/img/a-'.$v['img'].'" />' : '<div></div>';
			echo '<b>'.$v['article'].'</b><br />';
			echo $v['name'].'<br />';
			echo $v['price'].' руб.';
			echo '</li>';
		}
	}
	die();
}

if($get['u']=='form'){
    if (isset($post['owner']) && $post['owner']){
        // если есть данные о владельце, заполняем
        $post['owner'] = unserialize($post['owner']);
    }
}

if ($get['u']=='edit') {    
    $get['m'] = 'shop_products';
    if ($get['id'] == 'new'){
        if($new_id = select_empty_id('shop_products')){
            $get['id'] = $new_id;
        }
        else 
        {
            die();
        }
    }       
    $post['is_exclusive'] = 1; 
    
    if (isset($post['owner']) && $post['owner']){
        // если есть данные о владельце, заполняем
        $post['owner'] = serialize($post['owner']);
        //unset($post['owner']);
    }
    
    if($post['neighborhood']){
        $zip = mysql_select("SELECT * FROM shop_neighborhoods_zips WHERE nb_id = {$post['neighborhood']} LIMIT 1",'row');
        if ($zip)
            $post['zip_code'] = $zip['zip'];
    }
    $params = $post['parameters'];
    unset($post['parameters']);
    $p = array();
    if (!intval($get['id'])) $get['id'] = mysql_fn('insert','shop_products',$post);
    $product_id = intval($get['id']);
    if($params){
        foreach ($params as $k=>$value)
        {   //TODO: предварительно Save, если id нет
            $delete_parameters[] = $k;
            $p[] = array(
                'product_id' => $product_id,
                'parameter_id' => $k,
                'value' => $value,
            );
        }    
        if (isset($delete_parameters) && $delete_parameters) mysql_fn('delete', 'shop_products_parameters', '', " AND product_id = ".$product_id." AND parameter_id IN (".implode(', ',$delete_parameters).")");
        if (count($p)) mysql_fn('insert values', 'shop_products_parameters', $p, true); 
    }
}

$brand = mysql_select("SELECT id,name FROM shop_brands ORDER BY name",'array');

$a18n['sb_name'] = 'производители';
$a18n['sc_name'] = 'категории';

$table = array(
	//'_sorting'	=>	'n',
	'_edit'		=>	true,
	'id'		=>	'date:desc id:desc name price',
        'mln'		=>	'text',
	//'img'		=>	'img',
	'name'		=>	'',
	//'article'	=>	'',
	//'brand'		=>	$brand,
        //'type'    => $types,
        //'status'    => $statuses,
        'city' => $cities,
        'neighborhood' => $neighborhoods,
        'zip_code'      => '',
	'category'	=>	'<a href="/admin.php?m=shop_categories&id={category}">{sc_name}</a>',
	'date'		=>	'date',
	'price'		=>	'right',
	//'price2'	=>	'right',
	'special'	=>	'boolean',
	//'market'	=>	'boolean',
	'display'	=>	'boolean'
);

$join = (isset($get['category']) && $get['category']>0) ? " RIGHT JOIN shop_categories sc2 ON sc2.id = '".intval($get['category'])."' AND sc.left_key>=sc2.left_key AND sc.right_key<=sc2.right_key" : "";
$where = (isset($get['brand']) && $get['brand']>0) ? " AND shop_products.brand = '".intval($get['brand'])."' " : "";
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(shop_products.name) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(shop_products.article) like '%".mysql_real_escape_string(mb_strtolower($get['search'],'UTF-8'))."%'
	)
";

$query = "
	SELECT
		shop_products.*,
		sc.name sc_name
	FROM
		shop_products
	LEFT JOIN shop_categories sc ON shop_products.category = sc.id
	$join
	WHERE shop_products.id>0 AND shop_products.is_exclusive = 1 $where
";

$filter[] = array('search');
//$filter[] = array('brand',$brand,'производители');
$filter[] = array('city',$cities,'-citiy-');
$filter[] = array('neighborhood',$neighborhoods,'-neighborhood-');
$filter[] = array('category','shop_categories','-category-',true);

$delete = array(
	'delete'=>"DELETE FROM shop_reviews WHERE product = '".$get['id']."'"  //удаление отзывов
);

$tabs = array(
	1=>'Main',
	2=>'Settings',
	3=>'Pictures',
        5=>'Map',
	//4=>'Сопутсвующие товары'
);
//если мультиязычный то нужно добавляем вкладки языков кроме главного языка
if ($config['multilingual']) {
	$config['languages'] = mysql_select("SELECT id,name FROM languages ORDEr BY display DESC, rank DESC",'rows');
	if ($get['u']=='edit') {
		//перезапись названия в основной язык
		$k = $config['languages'][0]['id'];
		$post['name'.$k] = $post['name'];
		$post['text'.$k] = $post['text'];
	}
	//вкладку с главным языком не показываем
	foreach ($config['languages'] as $k => $v) if ($k>0) {
		//вкладки
		$tabs['1' . $v['id']] = $v['name'];
		//поля
		$form['1' . $v['id']][] = array('input td12', 'name' . $v['id'], @$post['name' . $v['id']], array('name' => $a18n['name']));
		$form['1' . $v['id']][] = array('tinymce td12', 'text' . $v['id'], @$post['text' . $v['id']], array('name' => $a18n['text']));
	}
}

$salutation = array(
    1=>'Mr.',
    2=>'Mrs.',
    3=>'Mr. & Mrs.',
    4=>'Ms.',
    5=>'Dr.',    
);

$form[1][] = '<h2>Owner information</h2>';
$form[1][] = array('select td2','owner[salutation]',array(@$post['owner']['salutation'],$salutation,'-select-'));
$form[1][] = array('input td3','owner[first_name]',@$post['owner']['first_name']);
$form[1][] = array('input td4','owner[last_name]',@$post['owner']['last_name']);
$form[1][] = array('select td3','category',array(true,'shop_categories'));
$form[1][] = array('input td9','owner[address]',@$post['owner']['address']);
$form[1][] = array('select td3','status',array(true,$statuses));
$form[1][] = array('select td3','owner[city]',array(@$post['owner']['city'],$cities,'-change-'), array('attr'=>'id="owner_city"'));
$form[1][] = array('select td3','owner[neighborhood]',array(@$post['owner']['neighborhood'],$neighborhoods,'-change-'),array('attr'=>'id="owner_neighborhood"'));
$form[1][] = array('input td3','owner[zip]',@$post['owner']['zip']);
$form[1][] = array('select td3','type',array(true,$types),array('name'=>'for sale'));
$form[1][] = array('input td3','owner[phone]',@$post['owner']['phone']);
$form[1][] = array('input td3','owner[cell]',@$post['owner']['cell']);
$form[1][] = array('input td3','owner[work]',@$post['owner']['work']);
$form[1][] = array('input td3','owner[fax]',@$post['owner']['fax']);
$form[1][] = array('input td12','owner[email]',@$post['owner']['email']);

$form[1][] = '<div class="field td6">';
$form[1][] = '<h2>Property Address</h2>';
$form[1][] = array('select td6','city',array(true,$cities,'-change-'));
$form[1][] = array('select td6','neighborhood',array(true,$neighborhoods,'-change-'));
$form[1][] = array('input td6','zip_code',true);
$form[1][] = array('input td6','address',true);
$form[1][] = array('input td6','unit',true);
$form[1][] = '</div>';

$form[1][] = '<div class="field td6">';
$form[1][] = '<h2>Property Details</h2>';
$form[1][] = array('input td4','name',true);
$form[1][] = array('checkbox','display',true);
$form[1][] = array('input td4','date',true);
$form[1][] = array('checkbox','special',true);
$form[1][] = '<div class="clear"></div>';
$form[1][] = '<h2>Financial Information</h2>';
$form[1][] = array('input td6 right','price',true);
$form[1][] = '</div>';

$form[1][] = array('tinymce td12','text',true);
$form[1][] = array('seo','seo url title keywords description',true);

$form[2][] = '';
if ($get['u']=='form' OR ($get['id']>0 AND $get['u']=='')) {
	$parameters = mysql_select("SELECT parameters FROM shop_categories WHERE id=".intval(isset($post['category']) ? $post['category'] : 0),'string');
	$parameters = $parameters ? unserialize($parameters) : array();
	$shop_parameters = mysql_select("SELECT id,name,type,`values`,units FROM shop_parameters ORDER BY rank DESC",'rows_id');
        $product_parameters = mysql_select("SELECT * FROM shop_products_parameters WHERE product_id = '".$post['id']."'",'rows_pid');        
        //var_dump($post);
	$form[2][] = 'Параметры добавляются и редактируются в разделе <a href="?m=shop_parameters"><u>параметры</u></a>.';
	$form[2][] = '<br />Настройка сортировки и отображения параметров на сайте редактируется в разделе <a href="?m=shop_categories"><u>категории</u></a>.
	<br />Здесь редактируются только значения параметров товара.<br /><br />';
	$form[2][] = '<div id="shop_parameters">';
	foreach ($shop_parameters as $k=>$v) if (isset($parameters[$v['id']]['display']) && $parameters[$v['id']]['display']==1){
		$name = $v['name'].($v['units'] ? ' ('.$v['units'].')' : '');
		if (array_key_exists($v['id'],$parameters)) {
			if (!isset($product_parameters[$v['id']])) $post['parameters'][$v['id']] = '';
                        //var_dump($k.' - '.);
			if (in_array($v['type'],array(1,3)))
                            $form[2][] = array('select td3','parameters['.$v['id'].']',array((isset($product_parameters[$v['id']]['value']) ? $product_parameters[$v['id']]['value'] : '' ),unserialize($v['values']),''),array('name'=>$name));
			else 
                            $form[2][] = array('input td3','parameters['.$v['id'].']',(isset($product_parameters[$v['id']]['value']) ? $product_parameters[$v['id']]['value'] : '' ),array('name'=>$name));
		}
	}
	$form[2][] = '</div>';
}

$form[3][] = array('file td6','img','Main image',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));
$form[3][] = array('file_multi','imgs','Additional images',array(''=>'resize 1000x1000','m-'=>'cut 536x320','p-'=>'resize 293x117'));

//$form[3][] = array('file_multi_db','shop_items','Additional images',array(''=>'resize 1000x1000','preview'=>'resize 150x150'));

$google = preg_replace('~[^-0-9.,]+~u','',@$post['coordinates']);
$google = $google ? $google : '39.5,-0.38';
//var_dump($google);
$form[5][] = array('input td10','address',@$post['address'],array('name'=>'address'));
$form[5][] = '<a href="#" id="coordinates_search" class="button red" style="margin:15px 0 25px"><span>Search</span></a>';
$form[5][] = '<div style="display:none">';
$form[5][] = array('input td12','coordinates',@$post['coordinates'],array('name'=>'карта'));
$form[5][] = '</div>';
$form[5][] = '<div id="map" style="clear:both; width:790px; height:500px"></div>';

$form[4][] = array('input td6','','',array('name'=>'Поиск товаров по названию, артикулу, ID','attr'=>'id="similar_search"'));
$form[4][] = array('input td6','similar',true,array('name'=>'ID сопутсвующих товаров через запятую'));
$form[4][] = '<ul id="similar_results" class="product_list"></ul>';
$form[4][] = '<ul id="similar" class="product_list">';
if (@$post['similar']) {
	$query2 = "SELECT * FROM shop_products WHERE id IN (".$post['similar'].") LIMIT 10";
	if ($products = mysql_select($query2,'rows_id')) {
		$similar = explode(',',$post['similar']);
		foreach ($similar as $k=>$v) if (isset($products[$v])) {
			$form[4][] = '<li data-id="'.$products[$v]['id'].'" title="Перетащите в правую колонку для сохранения">';
			$form[4][] = $products[$v]['img'] ? '<img src="/files/shop_products/'.$products[$v]['id'].'/img/a-'.$products[$v]['img'].'" />' : '<div></div>';
			$form[4][] = '<b>'.$products[$v]['article'].'</b><br />';
			$form[4][] = $products[$v]['name'].'<br />';
			$form[4][] = $products[$v]['price'].' руб.';
			$form[4][] = '</li>';
		}
	}
}
$form[4][] = '</ul>';

$content.= '
<style>
.product_list {float:left; min-height:300px; width:431px; background:#d6d6d6;}
#similar_results {margin:0 13px 0 0;}
.product_list li {clear:both; padding:5px; height:50px; cursor:move}
.product_list li img,
.product_list li div {width:50px; height:50px; float:left; margin:0 5px 0 0}
.product_list li:hover {background:#FFFEDF}
</style>

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
$(document).ready(function(){        

        $(document).on("keyup", "input[name=address]", function(){
            var value = $(this).val();
            $("input[name=address]").not($(this)).val(value);
        });
        
	//замена параметров при смене категории
	$(document).on("change",".form select[name=category]",function(){
		var category = $(this).val(),
			id = $(".form").prop("id").replace(/[^0-9]/g,"");
		$.get(
			"/admin.php",
			{"m":"shop_products_exclusive","u":"shop_parameters","category":category,"id":id},
			function(data){$("#shop_parameters").html(data)}
		);
	});
        
        $(document).on("change",".form select[name=city]",function(){
		// меняем районы
                var cities = $(this);
                var neighborhoods = $(cities).closest(".form").find("select[name=neighborhood]");
                $.ajax({
                    url: "/ajax.php",
                    data: {
                        file: "neighborhoods",
                        city: $(cities).val(),
                    },
                    type: "get",
                    dataType: "json",
                    beforeSend: function(){                        
                        $(neighborhoods).find("option").slice(1).remove();
                    },
                    success: function(json){                
                        if(json.result)
                        {
                            $.each(json.data, function(i, v) {
                                $(neighborhoods).append($("<option>").text(v.name).attr("value", v.id));
                            });
                        }
                    }
                });
	});
        
        $(document).on("change",".form select#owner_city",function(){
		// меняем районы
                var cities = $(this);
                var neighborhoods = $(cities).closest(".form").find("select#owner_neighborhood");
                $.ajax({
                    url: "/ajax.php",
                    data: {
                        file: "neighborhoods",
                        city: $(cities).val(),
                    },
                    type: "get",
                    dataType: "json",
                    beforeSend: function(){                        
                        $(neighborhoods).find("option").slice(1).remove();
                    },
                    success: function(json){                
                        if(json.result)
                        {
                            $.each(json.data, function(i, v) {
                                $(neighborhoods).append($("<option>").text(v.name).attr("value", v.id));
                            });
                        }
                    }
                });
	});

	//поиск сопуствующих товаров
	$(document).on("keyup","#similar_search",function(e) {
		var value	= $(this).val();
		$.get(
			"/admin.php?m=shop_products&u=similar_search",
			{"value":value},
			function(data){
				$("#similar_results").html(data);
			}
		).fail(function() {
			alert("Нет соединения!");
		});
	});

	similar_results();
	$(document).on("form.open",".form",function(){
		similar_results();
	});

	//сортировка товаров
	function similar_results () {
		$("#similar_results, #similar" ).sortable({
			connectWith: ".product_list",
			stop: function() {
				var similar = [];
				$("#similar li").each(function(){
					similar.push($(this).data("id"));
				});
				$("input[name=similar]").val(similar);
			}
		}).disableSelection();
	}
        
        $(document).on("click",".bookmarks a",function(){
		var i = $(this).data("i");
                var coords = $("input[name=coordinates]").val().replace("(","").replace(")","").split(",");
                coords = (coords.length > 1) ? coords : new Array (25.7616798,-80.191);
		if (i==5) {                    
                        console.log(coords);
			geocoder = new google.maps.Geocoder();
			map = new google.maps.Map(document.getElementById("map"),{
				zoom: 10,
				center: new google.maps.LatLng(coords[0],coords[1]),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
			});
			markersArray = [];
			var marker = new google.maps.Marker({position:new google.maps.LatLng(coords[0],coords[1])});
			marker.setMap(map);
			markersArray.push(marker);
			google.maps.event.addListener(map, "click", function(event) {                                
				for (i in markersArray) markersArray[i].setMap(null);
				markersArray.length = 0;
				var marker = new google.maps.Marker({position:event.latLng,map:map});
				markersArray.push(marker);
				$("input[name=coordinates]").val(event.latLng);
			});
		}
		return false;
	});
        
        //поиск по гуглу
	$(document).on("click","#coordinates_search",function () {
		marker_search();
		return false;
	});
        
	function marker_search() {
		var address = $("input[name=\"address\"]").val();
		geocoder.geocode( { "address": address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				for (i in markersArray) markersArray[i].setMap(null);
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});
				markersArray.push(marker);
                                console.log(results[0].geometry.location.toString());
                                $("input[name=coordinates]").val(results[0].geometry.location.toString());
			} else {
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
	}

});
</script>';

